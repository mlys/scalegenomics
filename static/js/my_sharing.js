"use strict";

function Sharing(lab_id) {
    this.lab_id = lab_id;
    this.is_loading_info = false;
    this.is_loading_dialog = false;
    this.is_loading_sharing_dialog = false;

    this.add_limit = 0;
    this.sharing_limit = 3;
}

Sharing.prototype = {
    setPreloader: function() {
        $('.sharing-body').html('<img src="/static/img/h_preloader_160_24.png">');
    },
    showErrorMessage: function() {
        $('.sharing-body').html('<div class="message">Please reload page</div>');
    },
    loadInfo: function(url) {
        if (this.is_loading_info)
            return;
        this.is_loading_info = true;
        this.setPreloader();
        var self = this;
        $.ajax({
            url: url,
            success: function(res) {
                self.is_loading_info = false;
                $('.sharing-body').html(res);
            },
            error: function() {
                self.is_loading_info = false;
                self.showErrorMessage();
            }
        });
    },
    selectByNum: function(n) {
        $('.sharing-sections ul li').removeClass('selected');
        $('.sharing-sections ul li:nth-child('+n+')').addClass('selected');
    },
    showDisks: function() {
        this.selectByNum(1)
        this.loadInfo('/my/'+this.lab_id+'/sharing/disks');
    },
    showWithMe: function() {
        this.selectByNum(2);
        this.loadInfo('/my/'+this.lab_id+'/sharing/withme');
    },
    delete: function(storage_id, sh_id) {
        $.post('/my/'+this.lab_id+'/sharing/'+storage_id+'/delete', {'sh_id': sh_id}, function(res) {
            if (res.result == 'ok' && res.deleting) {
                $('#sharing-'+sh_id+' span').html('Deleting');
            } else {
                $('#sharing-'+sh_id).remove();
                if ($('#my-shared-storage-'+storage_id+' ul li').length == 0) {
                    $('#my-shared-storage-'+storage_id).remove();
                }
            }
        });
    },
    deleteShared: function(storage_id, sh_id) {
        $.post('/my/'+this.lab_id+'/sharing/'+storage_id+'/delete', {'sh_id': sh_id}, function(res) {
            if (res.result == 'ok' && res.deleting) {
                $('#sharing-'+storage_id+' td:nth-child(6) a').remove();
                $('#sharing-'+storage_id+' td:nth-child(7)').html('Deleting');
            } else {
                $('#sharing-'+storage_id).remove();
            }
        });
    },
    deleteFromStorage: function(storage_id, sh_id) {
        $.post('/my/'+this.lab_id+'/sharing/'+storage_id+'/delete', {'sh_id': sh_id}, function(res) {
            if (res.result == 'ok' && res.deleting) {
                $('#sh-'+sh_id+' td:nth-child(2)').html('Deleting');
                $('#sh-'+sh_id+' td:nth-child(3)').html('');
            } else {
                $('#sh-'+sh_id).remove();
                if ($('.my-sharings tbody tr').length == 0) {
                    $('.my-sharings').remove();
                    $('.red-button').removeClass('hidden');
                }
            }
        });
    },
    initMountDialog: function() {
        var dhtml = '<div class="mount-sharing-dialog"></div>';
        $('body').append(dhtml);
        this.jq_mount_dialog = $('.mount-sharing-dialog');
        this.jq_mount_dialog.dialog({
            title:     'Mount to server',
            autoOpen:  false,
            resizable: false,
            width:     700,
            height:    500,
            maxHeight: 500
        });
    },
    setMountDialogPreloader: function() {
        this.jq_mount_dialog.html('<img src="/static/img/h_preloader_160_24.png">');
    },
    openMountDialog: function(storage_id, sh_id) {
        this.jq_mount_dialog.dialog('open');
        this.storage_id = storage_id;
        this.sh_id = sh_id;
        this.loadMyServers();
    },
    loadMyServers: function() {
        if (this.is_loading_dialog)
            return;
        this.is_loading_dialog = true;
        this.setMountDialogPreloader();
        var self = this;
        $.ajax({
            url: '/my/'+this.lab_id+'/sharing/servers',
            data: {'sh_id': this.sh_id},
            success: function(res) {
                self.jq_mount_dialog.html(res);
                self.is_loading_dialog = false;
            },
            error: function() {
                self.is_loading_dialog = false;
            }
        });

    },
    initSharingDialog: function() {
        var dhtml = '<div class="storage-sharing-dialog"></div>';
        $('body').append(dhtml);
        this.jq_sharing_dialog = $('.storage-sharing-dialog');
        this.jq_sharing_dialog.dialog({
            title:     'Share with',
            autoOpen:  false,
            resizable: false,
            width:     700,
            maxWidth:  700,
            height:    500,
            maxHeight: 500,
        });
        this.is_loading_sharing_dialog = false;

        var sharing_obj = this;
        var ul = document.createElement('ul');
        ul.setAttribute('class', 'inline-sections');
        var a = document.createElement('a');
        a.setAttribute('href', '#');
        a.onclick =  function() {
            sharing_obj.loadSharingUnits('members');
            return false;
        };
        a.innerHTML = 'Members';
        var li = document.createElement('li');
        li.appendChild(a);
        ul.appendChild(li);

        var a = document.createElement('a');
        a.setAttribute('href', '#');
        a.onclick =  function() {
            sharing_obj.loadSharingUnits('labs');
            return false;
        };
        a.innerHTML = 'Labs';
        var li = document.createElement('li');
        li.appendChild(a);
        ul.appendChild(li);

        $('.storage-sharing-dialog').html(ul);
    },
    getUnit: function(unit, checked) {
        var sharing_obj = this;

        var checkbox = document.createElement('input');
        checkbox.setAttribute('type', 'checkbox');
        checkbox.setAttribute('name', unit.unit_id);
        if (checked) {
            checkbox.setAttribute('checked', 'checked');
        }
        checkbox.onclick = function() {
            sharing_obj.recalcLimit();
        }
        var img = document.createElement('img');
        img.setAttribute('src', unit.owner_gravatar+'?size=30');
        img.setAttribute('alt', unit.owner_name);
        img.setAttribute('title', unit.owner_name);

        var owner = document.createElement('a');
        owner.setAttribute('href', unit.owner_url);
        owner.appendChild(img);

        var name = document.createElement('a');
        name.setAttribute('href', unit.unit_url);
        name.innerHTML = unit.unit_name;

        var mounts = document.createElement('span');
        if (unit.count != null) {
            if (unit.count == 0) {
                mounts.innerHTML = '(not mounted)';
            } else if (unit.count > 0) {
                mounts.innerHTML = '('+unit.count+' mounts)';
            }
        }

        var li = document.createElement('li');
        li.appendChild(checkbox);
        li.appendChild(owner);
        li.appendChild(name);
        li.appendChild(mounts);

        return li;
    },
    updateUnits: function(type, shared, not_shared) {
        var sharing_obj = this;
        var lis = [];
        for (var i in shared) {
            var li = this.getUnit(shared[i]);
            lis.push(li);
        }
        for (var i in not_shared) {
            var li = this.getUnit(not_shared[i], true);
            lis.push(li);
        }
        var ul = document.createElement('ul');
        for (var i in lis) {
            ul.appendChild(lis[i]);
        }
        var checked = document.createElement('p');

        var button = document.createElement('button');
        button.setAttribute('class', 'to-button');
        button.innerHTML = 'Share';
        button.onclick = function() {
            sharing_obj.shareWith();
        }

        var div = document.createElement('div');
        div.setAttribute('class', 'sharing-units');
        div.appendChild(checked);
        div.appendChild(ul);
        div.appendChild(button);

        this.jq_sharing_dialog.append(div);
        this.recalcLimit();
    },
    openSharingDialog: function(storage_id) {
        this.storage_id = storage_id;
        this.jq_sharing_dialog.dialog('open');
        this.loadSharingUnits('members');
    },
    loadSharingUnits: function(section) {
        if (this.is_loading_sharing_dialog) {
            return;
        }
        this.dialog_section = section;
        $('.inline-sections li a').removeClass('selected');
        if (section == "members") {
            $('.inline-sections li:nth-child(1) a').addClass('selected');
        } else {
            $('.inline-sections li:nth-child(2) a').addClass('selected');
        }
        $('.storage-sharing-dialog .sharing-units').remove();
        $('.storage-sharing-dialog .neutral-message').remove();
        this.jq_sharing_dialog.append('<img id="dialog-preloader" style="position: relative; top: 170px; left: 315px;" src="/static/img/c_preloader_64.gif">');
        this.is_loading_sharing_dialog = true;

        var sharing_obj = this;
        $.ajax({
            url: "/my/"+this.lab_id+"/storage/"+this.storage_id+'/sharing',
            data: {'section': section},
            success: function(res) {
                $('#dialog-preloader').remove();
                sharing_obj.is_loading_sharing_dialog = false;
                if (res.result == 'ok') {
                    sharing_obj.add_limit = res.add_limit;
                    sharing_obj.updateUnits(section, res.not_shared, res.shared);
                } else if (res.result == 'error') {
                    if (res.message == 'storage') {
                        sharing_obj.addDialogError('Ops! Wrong storage.');
                    }
                }
            },
            error: function() {
                $('#dialog-preloader').remove();
                sharing_obj.is_loading_sharing_dialog = false;
            }
        });
    },
    addDialogError: function(txt) {
        this.jq_sharing_dialog.append('<div class="neutral-message">'+txt+'</div>');
    },
    loadSharings: function(storage_id) {
        $.get('/my/'+this.lab_id+'/storage/'+storage_id+'/slist', {}, function(res) {
            if (res) {
                $('.storage-sharings').html(res);
                $('.red-button').addClass('hidden');
                $('.destroy-block').hide();
            } else {
                $('.red-button').removeClass('hidden');
            }
        });
    },
    recalcLimit: function() {
        var checked = $('.sharing-units ul li input:checked').length;
        checked += this.add_limit;
        if (checked == this.sharing_limit) {
            $('.sharing-units p').html('Total limit: <span>' + checked + ' of ' + this.sharing_limit + '</span>');
            $('.sharing-units input').not(':checked').each(function(i, el) {
                el.setAttribute('disabled', 'disabled');
            });
        } else {
            $('.sharing-units p').html('Total limit: ' + checked + ' of ' + this.sharing_limit);
            $('.sharing-units input').not(':checked').each(function(i, el) {
                el.removeAttribute('disabled');
            });
        }
    },
    shareWith: function() {
        this.jq_sharing_dialog.dialog('close');

        var where_is = window.location.pathname.indexOf('sharing');
        var units = [];
        $('.sharing-units ul li input').filter(':checked').each(function(i, el) {
            units.push(el.name);
        });
        var sharing_obj = this;
        var data = {'unit_type': this.dialog_section,
                    'units': units.join(',')};
        $.post('/my/'+this.lab_id+'/storage/'+this.storage_id+'/sharing', data, function(res) {
            if (where_is == -1) {
                sharing_obj.loadSharings(sharing_obj.storage_id);
            } else {
                sharing_obj.showDisks();
            }
        });
    },
    mount: function(server_id) {
        this.jq_mount_dialog.dialog('close');
        var params = {'server_id':  server_id,
                      'storage_id': this.storage_id,
                      'sh_id':      this.sh_id};
        var self = this;
        $.post('/my/'+this.lab_id+'/sharing/mount', params, function(res) {
            self.showWithMe();
        });
    },
    umount: function(storage_id, sh_id, rel_id) {
        $('#rel-'+rel_id).remove();
        var len = $('#sharing-'+storage_id+' table tr').length;
        if (len == 0) {
            $('#sharing-'+storage_id+' table').remove();
        }
        $.post('/my/'+this.lab_id+'/sharing/umount', {'rel_id': rel_id});
    },
    showMounts: function(storage_id) {
        $.ajax({
            url: '/my/'+this.lab_id+'/storage/'+storage_id+'/nfs',
            success: function(res) {
                $('.storage-nfs-mounts').html(res);
            }
        });
    }
}
