"use strict";

function activateUser(user_id) {
  $.post('/root/users/'+user_id+'/activate', {}, function(res) {
    if(res.result == 'ok') {
      $('#is-active-'+user_id).html('<a href="#" onclick="blockUser('+user_id+'); return false;">True</a>');
    }
  });
}

function blockUser(user_id) {
  $.post('/root/users/'+user_id+'/block', {}, function(res) {
    if(res.result == 'ok') {
      $('#is-active-'+user_id).html('<a href="#" onclick="activateUser('+user_id+'); return false;">False</a>');
    }
  });
}

function changeLabState(lab_id) {
  $.post('/root/labs/'+lab_id+'/chstate', {}, function(res) {
    if (res.result == 'ok') {
      if (res.state == 'active') {
        $('#lab-'+lab_id).removeClass('hl-red-row');
        $('#lab-'+lab_id+' td:last-child a').html(res.state);
      } else {
        $('#lab-'+lab_id).addClass('hl-red-row');
        $('#lab-'+lab_id+' td:last-child a').html(res.state);
      }
    }
  });
}

function changeLabStateOth(lab_id) {
  $.post('/root/labs/'+lab_id+'/chstate', {}, function(res) {
    if (res.result == 'ok') {
      $('.message').remove();
      $('.unit-dl dd:last-child a').html(res.state);
    }
  });
}
