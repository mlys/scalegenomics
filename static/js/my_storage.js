"use strict";

var re_storage_alias = /^[a-zA-Z0-9]{1,30}$/;
var re_storage_name = /^[a-zA-Z0-9][a-zA-Z0-9\s\-\']{0,30}$/;


function getRand() {
    return Math.floor((Math.random()+1)*1000000000);
}

function Storage(lab_id, storage_id, in_project) {
    this.lab_id = lab_id;
    this.storage_id = storage_id;
    this.in_project = in_project;

    this.status_interval_id = null;
}

Storage.prototype = {
    setProject : function(project_id) {
        this.project_id = project_id;
    },
    setStorage : function(storage_id) {
        this.storage_id = storage_id;
    },
    showDestroyBlock : function() {
        $('.destroy-block').toggle();
    },
    hideDestroyBlock : function() {
        $('.destroy-block').hide();
    },
    setStatus: function(status) {
        var jq_status = $('.storage-status');
        jq_status.val(status);

        jq_status.html('<img class=\"inline-preloader\" src="/static/img/c_preloader_blue_13.gif" title="'+status+'">');
        if (status == 'Creating' || status == 'Removing') {
            jq_status.html('<img ckass=\"inline-preloader\" src="/static/img/c_preloader_blue_13.gif" title="'+status+'"> '+status+'...');
        } else {
            jq_status.html(status);
        }
        if(this.in_project) {
            if (status == 'Ok') {
                $('.storage-action-list').removeClass('hidden');
            } else {
                $('.storage-action-list').addClass('hidden');
            }
        }
    },
    updateStatus: function() {
        // if we can't find element then stop monitoring
        if ($('.storage-status').length == 0) {
            clearInterval(this.status_interval_id);
            this.status_interval_id = null;
            return;
        }
        var self = this;
        $.get('/my/'+this.lab_id+'/storage/'+this.storage_id+'/status?'+getRand(), {}, function(res) {
            if (res.result != 'ok')
              return;
            if ($('.storage-status').val() == res.status)
              return;
            self.setStatus(res.status);
        });
    },
    startStatusMonitor : function() {
        if (this.status_interval_id) {
            clearInterval(this.status_interval_id);
            this.status_interval_id = null;
        }
        this.status_interval_id = setInterval('storage.updateStatus();', 25000);
    },
    destroy : function() {
        var self = this;
        $.post('/my/'+this.lab_id+'/storage/'+this.storage_id+'/destroy', {}, function() {
            window.location = '/my/'+self.lab_id+'/storage';
        });
    },
    destroyFromProject : function() {
        var self = this;
        $.post('/my/'+this.lab_id+'/storage/'+this.storage_id+'/destroy', {}, function() {
            ppane.showStorages();
        });
    },
    setDisk : function(disk_id) {
        this.disk_id = disk_id;
    },
    showEditForm : function() {
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/disks/'+this.storage_id+'/ed', {}, function(res) {
            $('#show-project-content').html(res);
        });
    },
    unmount: function(server_id, storage_id) {
      $('#nfs-storage-'+storage_id).remove();
      if ($('.nfs-mounted-to tbody tr td').length == 0) {
          $('.nfs-mounted-to').remove();
      }
      $.post('/my/'+this.lab_id+'/servers/'+server_id+'/umount', {'storage_id': storage_id});
    }
}

function updateLabStoragesStatus(lab_id) {
    $.post('/my/'+lab_id+'/storage?'+getRand(), {}, function(res) {
        for(var i in res) {
            var jq_status = $('#storages-tbl tbody tr#'+res[i].id+' td:nth-child(4)');
            jq_status.html(res[i].status);
        }
    });
}

function validateNewStorageForm(lab_id) {
    delFormError();
    var name = $('#name').val().trim();
    var alias = $('#alias').val().trim();
    var description = $('#description').val().trim();
    var size = $('#size').val();
    size = parseInt(size.trim());
    var type = $('#type').val();
    var project = $('#project').val();

    if (name.length < 1) {
        addFormError('#name', 'Please enter disk name');
        return;
    }
    var res = name.match(re_storage_name);
    if (res == null) {
        addFormError('#name', 'Please use latin letter and numbers');
        return;
    }
    if (alias) {
        res = alias.match(re_storage_alias);
        if (res == null) {
            addFormError('#alias', 'Please use letters, numbers or spaces');
            return;
        }
    }
    var params = {'name':        name,
                  'alias':       alias,
                  'description': description,
                  'size':        size,
                  'type':        type,
                  'project':     project};
    var cserver_id = $('.cservers-side-block div.selected').attr('id');
    if (cserver_id != undefined) {
        cserver_id = cserver_id.slice(8);
        params['cserver'] = cserver_id;
    }
    $('.storage-form button').attr('disabled', 'disabled');
    $.post('/my/'+lab_id+'/storage/new', params, function(res) {
        if(res.result == 'ok') {
            window.location = res.redirect_to;
        } else if (res.result == 'error' && res.message == 'wrong alias') {
            addFormError('#alias', 'Mount point name is busy');
            $('.storage-form button').removeAttr('disabled');
        } else {
            window.location = '/my/'+lab_id+'/storage';
        }
    });
}

function validateEditStorageForm(lab_id, storage_id, storage_alias) {
    var name = $('#storage-name').val().trim();
    var alias = $('#storage-alias').val().trim();
    var description = $('#storage-description').val().trim();
    var type = $('#storage-type').val();
    var project = $('#storage-project').val();

    $('#js-form-error').remove();

    if(name.length < 1) {
        $('#storage-name').after('<span id="js-form-error" class="form-error">Please enter disk name</span>');
        return;
    }
    var res = name.match(re_storage_name);
    if(res == null) {
        $('#storage-name').after('<span id="js-form-error" class="form-error">Please use latin letter</span>');
        return;
    }
    if (alias && alias != storage_alias) {
        res = alias.match(re_storage_alias);
        if(res == null) {
            $('#storage-alias').after('<span id="js-form-error" class="form-error">Please use only letters and numbers</span>');
            return;
        }
    }
    var params = {'name':        name,
                  'alias':       alias,
                  'description': description,
                  'type':        type,
                  'project':     project};
    $.post('/my/'+lab_id+'/storage/'+storage_id+'/edit', params, function(res) {
        if(res.result == 'ok') {
            window.location = '/my/'+lab_id+'/storage/'+storage_id;
        } else if (res.result == 'error' && res.message == 'wrong alias') {
            $('#storage-alias').after('<span id="js-form-error" class="form-error">This name is busy</span>');
        } else {
            window.location = '/my/'+lab_id+'/storage/'+storage_id;
        }
    });
}

function initStoragesTable(lab_id, project_id) {
  if (lab_id && project_id) {
    var requestUrl = '/my/'+lab_id+'/storage/list?project_id='+project_id;
    var colNames = ['Name', 'Mount point name', 'Status', 'Size', 'Type', 'Created by'];
    var colModel = [{name:'name',       index:'name',       width:130},
                    {name:'mname',      index:'mname',      width:130},
                    {name:'status',     index:'status',     width:100},
                    {name:'size',       index:'size',       width:150},
                    {name:'type',       index:'type',       width:150},
                    {name:'created_by', index:'created_by', width:150}];
  } else {
    var requestUrl = 'storage/list';
    var colNames = ['Name', 'Mount point name', 'Project', 'Status', 'Size', 'Type', 'Created by'];
    var colModel = [{name:'name',       index:'name',       width:130},
                    {name:'mname',      index:'mname',      width:130},
                    {name:'project',    index:'project',    width:100},
                    {name:'status',     index:'status',     width:100},
                    {name:'size',       index:'size',       width:150},
                    {name:'type',       index:'type',       width:150},
                    {name:'created_by', index:'created_by', width:150}];
  }
  $('#storages-tbl').jqGrid({
    url         : requestUrl,
    datatype    : 'json',
    colNames    : colNames,
    colModel    : colModel,
    rowNum      : -1,
    rowTotal    : 2000,
    pager       : '#storages-pager',
    sortname    : 'name',
    viewrecords : true,
    sortorder   : 'desc',
    caption     : 'Disks',
    scroll      : 1,
    rownumWidth : 40,
    gridview    : true,
    height      : 300,
    loadonce    : false
  });
  $('#storages-tbl').jqGrid('navGrid','#storages-pager', {edit:false,add:false,del:false,search:false});
  // fix
  $('#gview_storages-tbl > div:first-child.ui-helper-clearfix').removeClass('ui-helper-clearfix');
  $('#gview_storages-tbl > div:first-child').css('padding', '3px 0 20px 3px');
  $('#gbox_storages-tbl').css('margin', '20px 0 0 0');
}

function initStorageCServer() {
    var cs = cservs_info[0][0];
    var limits = cservs_info[0][1];
    var max = 0;
    for (var i in cservs_info) {
        if (cservs_info[i][1]['max'] > max) {
            cs = cservs_info[i][0];
            max = cservs_info[i][1]['max'];
            limits = cservs_info[i][1];
        }
    }
    $('#cserver-'+cs).addClass('selected');
    $('#storage-size-slider').slider({
        min:   limits['min'],
        max:   limits['max'],
        value: limits['default'],
        slide: function(event, ui) {
            $('#size').val(ui.value+' GB');
        }
    });
    $('#size').val($("#storage-size-slider").slider('value')+' GB');
}

function selectCServerForStorage(cserv_id) {
    for (var i in cservs_info) {
        if (cservs_info[i][0] == cserv_id) {
            var limits = cservs_info[i][1];
            if (limits['max'] <= 0) {
                return;
            }
            var value = $('#storage-size-slider').slider('option', 'value');
            if (value > limits['max']) {
                value = limits['max'];
                $('#size').val(value+' GB');
            }
            $('#storage-size-slider').slider('option', {
                'min':   limits['min'],
                'max':   limits['max'],
                'value': value
            });
            break;
        }
    }
    $('.cservers-side-block > div').removeClass('selected');
    $('#cserver-'+cserv_id).addClass('selected');
}
