"use strict";

function buildOpt(value, text) {
    return "<option value='" + value + "'>" + text + "</option>";
}

function bindUpdateVolumes(){
  $('#dataset_id').change(function(){
  	$.post('/root/dataset/'+this.value+'/volumes/load', {}, function(res) {
    if (res.result == 'ok') {
    	var options = buildOpt(-1, 'All');
    	for (var i in res.volumes){	
  			options += buildOpt(res.volumes[i][0], res.volumes[i][1]);	
    	}	
    	$('#dataset_volume_id').html(options);
    	}
    else {
    	$('#dataset_volume_id').html(buildOpt(-2, 'None yet'));
    }
    });
  });
}