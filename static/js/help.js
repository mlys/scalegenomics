'use strict';

function Help() {
    this.sections = {
        'main':    [1, '/my/help/main'],
        'vpn':     [2, '/my/help/openvpn'],
        'disks':   [3, '/my/help/disks']
        /* 'sharing': [4, '/my/help/sharing'] */
    }
    this.content = $('.help-content');
}

Help.prototype = {
    showPreloader: function() {
        this.content.html('<img src="/static/img/h_preloader_160_24.png">');
    },
    showErrorMessage: function() {
        this.content.html('<div class="message">Please reload page</div>');
    },
    loadPage: function(url, hash) {
        this.showPreloader();
        var self = this;
        $.ajax({
            url: url,
            success: function(res) {
                self.content.html(res);
                if (hash != null) {
                    if (hash.length > 1) {
                        var selector = '[name="'+hash.join(",")+'"]';
                        var position = $(selector).position();
                        if (position) {
                            window.scrollTo(0, position.top);
                        }
                    }
                }
            },
            error: function() {
                self.showErrorMessage();
            }
        });

    },
    showSection: function(section, hash) {
        if (hash == undefined) {
            this.setHash(section);
        } else {
            this.setHash(hash.join(","));
        }
        $('.help-sections li span').hide();
        $('.help-sections li:nth-child('+this.sections[section][0]+') span').show();

        $('.help-sections li').removeClass('selected');
        $('.help-sections li:nth-child('+this.sections[section][0]+')').addClass('selected');

        this.loadPage(this.sections[section][1], hash);
    },
    setHash: function(value) {
        window.location.hash = '#'+value
    },
    readHash: function() {
        var hash = window.location.hash.substr(1);
        hash = hash.split(',');
        for (var section in this.sections) {
            if (section == hash[0]) {
                this.showSection(section, hash);
                return;
            }
        }
        this.showSection('main');
    }
}
