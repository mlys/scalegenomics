function joinToLab(lab_id) {
  var el = $('.position-'+lab_id);
  el.html('waiting to be approved');
  $.post('/my/a/join_to_lab', {'lab_id': lab_id}, function(json) {});
}

function showAllProjectsByType(type, i) {
    $('.lab-types li a').removeClass('selected')
    $('.lab-types li:nth-child('+i+') a').addClass('selected');
    $('.all-labs').html('<img src="/static/img/h_preloader_160_24.png" alt="Loading" />');
    $.get('/my/labs/all/type', {'type' : type}, function(res) {
        $('.all-labs').html(res);
    });
}

