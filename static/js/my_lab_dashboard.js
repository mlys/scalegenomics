function fixHeight() {
    var max_len = $('#lab-dashboard-projects-widget').css('height');
    var ser_len = $('#lab-dashboard-servers-widget').css('height');
    var sto_len = $('#lab-dashboard-storages-widget').css('height');
    max_len = parseInt(max_len.substr(0, max_len.length-2));
    ser_len = parseInt(ser_len.substr(0, ser_len.length-2));
    sto_len = parseInt(sto_len.substr(0, sto_len.length-2));
    if (max_len < ser_len) { max_len = ser_len; }
    if (max_len < sto_len) { max_len = sto_len; }

    $('#lab-dashboard-projects-widget').css('height', max_len);
    $('#lab-dashboard-servers-widget').css('height', max_len);
    $('#lab-dashboard-storages-widget').css('height', max_len);
}
