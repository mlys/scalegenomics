function leaveLab(lab_id) {
    var ask = confirm('Leave this lab ?');
    if (ask) {
        $.post('/my/a/'+lab_id+'/leave_lab', {}, function(res) {
            $('#lab-'+lab_id).fadeOut();
        });
    }
}