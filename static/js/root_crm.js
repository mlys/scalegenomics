function showCRMCommunicationsTable(user_id) {
  if (user_id){
    var requestUrl = '/root/crm/user/get/'+user_id;
    var colNames = ['Date', 'Client', 'Subject', 'Type'];
    var colModel = [ {name:'date',       index:'date',       width:100},
                     {name:'client',     index:'client',     width:100},
                     {name:'subject',    index:'subject',    width:300},
                     {name:'type',       index:'type',       width:70},];
  } else {
    var requestUrl = '/root/crm/get';
    var colNames = ['Date', 'Client', 'Subject', 'Type'];
    var colModel = [ {name:'date',       index:'date',       width:100},
                     {name:'client',     index:'client',     width:100},
                     {name:'subject',    index:'subject',    width:300},
                     {name:'type',       index:'type',       width:70},];
  }
  $('#crm-logs-tbl').jqGrid({
    url         : requestUrl,
    datatype    : 'json',
    colNames    : colNames,
    colModel    : colModel,
    rowNum      : -1,
    rowTotal    : 2000,
    pager       : '#crm-logs-pager',
    sortname    : 'date',
    viewrecords : true,
    sortorder   : 'desc',
    caption     : 'CRM Communications',
    scroll      : 1,
    rownumWidth : 40,
    gridview    : true,
    height      : 300,
    loadonce    : true
  });
  $('#crm-logs-tbl').jqGrid('navGrid','#crm-logs-pager', {edit:false,add:false,del:false,search:false});
  // fix
  $('#gview_crm-logs-tbl > div:first-child.ui-helper-clearfix').removeClass('ui-helper-clearfix');
  $('#gview_crm-logs-tbl > div:first-child').css('padding', '3px 0 20px 3px');
  $('#gbox_crm-logs-tbl').css('margin', '20px 0 0 0');
}