"use strict";

function initDropMenu() {
    $('.dropmenu').parent().hover(
        function() {
            $('ul', this).addClass('dropmenu-down');
            $(this).addClass('hover-item');
        },
        function() {
            $('ul', this).removeClass('dropmenu-down');
            $(this).removeClass('hover-item');
        }
    );
}

function requestLabValidation(lab_id) {
    $('.verify-block .not-verified').remove();
    $('.verify-block button').remove();
    $('.verification').show();
    $.post('/my/'+lab_id+'/activation');
}

function updateLabLimits(lab_id) {
    $.get('/my/'+lab_id+'/limits', {}, function(res) {
        $('.cpu-bar span').css('width', res.cpu_proc);
        $('.ram-bar span').css('width', res.ram_proc);
        $('.sto-bar span').css('width', res.storage_proc);
        $('.lab-limit .info dl dd:nth-child(2)').html(res.cpu_text);
        $('.lab-limit .info dl dd:nth-child(4)').html(res.ram_text);
        $('.lab-limit .info dl dd:nth-child(6)').html(res.storage_text);
    });
}

function addFormError(selector, text) {
    $(selector).after('<span id="js-form-error" class="form-error">'+text+'</span>');
}

function delFormError() {
    $('#js-form-error').remove();
}
