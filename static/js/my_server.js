"use strict";

function getRand() {
    return Math.floor((Math.random()+1)*1000000000);
}

function Server(lab_id, project_id, server_id) {
  this.lab_id     = lab_id;
  this.project_id = project_id;
  this.server_id  = server_id;
  this.server_buttons = {
    'start'   : 1,
    'stop'    : 2,
    'restart' : 3,
    'mount'   : 4,
    'edit'    : 5 };
  this.status_interval_id = null;
}

Server.prototype = {
  initMountDialog : function() {
    $('body').append('<div class="server-mount-dialog"></div>');
    var serv = this;
    this.jq_mount_dialog = $('.server-mount-dialog');
    this.jq_mount_dialog.dialog({
      title:     'Disks',
      autoOpen:  false,
      resizable: false,
      width:     700,
      maxWidth:  700,
      height:    500,
      maxHeight: 500,
      open:      function(event, ui) {},
      close:     function(event, ui) {
        serv.enableMountButton();
        serv.updateStatus();
        $(this).html('');
      }
    });
  },
  setProject : function(project_id) {
    this.project_id = project_id;
  },
  setServer : function(server_id) {
    this.server_id = server_id;
  },
  disableAllButtons : function() {
    for (var i in this.server_buttons) {
      if (i == 'mount') continue;
      if (i == 'edit') continue;
      if (i == 'log') continue;
      $('.small-buttons-pane li:nth-child('+this.server_buttons[i]+')').unbind('click').addClass('disabled');
    }
  },
  enableAllButtons : function() {
    for (var i in this.server_buttons) {
      $('.small-buttons-pane li:nth-child('+this.server_buttons[i]+')').removeClass('disabled');
    }
  },
  setStatus : function(status) {
    var jq_status = $('.server-status');
    var pre_status = jq_status.val();

    if ('Starting' == pre_status && 'Stopped' == status) return;
    if ('Stopping' == pre_status && 'Running' == status) return;

    this.disableAllButtons();
    jq_status.val(status);

    if (status == 'Creating' || status == 'Configuring' || status == 'Starting' || status == 'Stopping' || status == 'Restarting') {
      status = '<img class="inline-preloader" src="/static/img/c_preloader_blue_13.gif" alt="Loading">' + status;
    }
    $('.server-status').html(status);

    if (status == 'Stopped') {
      this.enableStartButton();
    } else if (status == 'Running') {
      this.enableStopButton();
      this.enableRestartButton();
      this.loadMountList();
    }
  },
  setIp: function(ip) {
    if (ip != undefined) {
      $('.server-ip').html(ip);
      $('.server-instruction').fadeIn();
      $('.server-instruction').removeClass('hidden');
    }
  },
  updateStatus : function() {
    if ($('.server-status').length == 0) {
      clearInterval(this.status_interval_id);
      this.status_interval_id = null;
      return;
    }
    var self = this;
    var url = '/my/'+this.lab_id+'/servers/'+this.server_id+'/status?'+getRand();
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(res) {
            var status = $('.server-status');
            if (res.result != 'ok')
                return;
            if (status.val() != res.status) {
                self.setStatus(res.status);
                self.setIp(res.ip);
                self.loadMountList();
            } else {
                for( var i in res.mounts) {
                    var tr = $('#server-mounts-storage-'+res.mounts[i].storage_id);
                    var st = tr.find('td:nth-child(5)');
                    if (res.mounts[i].status != st.html()) {
                        st.html(res.mounts[i].status);
                    }
                }
            }
        }
    });
  },
  bindRemoveMessageListener : function() {
    $('body').click(function() {
      setTimeout('$("body").unbind("click"); $("#xmessage").fadeOut();', 3500);
    });
  },
  startStatusMonitor : function() {
    if (this.status_interval_id) {
      clearInterval(this.status_interval_id);
      this.status_interval_id = null;
    }
    this.status_interval_id = setInterval('server.updateStatus();', 25000);
  },
  loadMountList : function() {
    var self = this;
    $.post('/my/'+this.lab_id+'/servers/'+this.server_id+'/mlist', {}, function(res) {
        $('.server-mounted-storages').html(res);
    });
  },
  enableStartButton : function() {
    var self = this;
    $('.small-buttons-pane li:nth-child('+this.server_buttons['start']+')').removeClass('disabled');
    $('.small-buttons-pane li:nth-child('+this.server_buttons['start']+')').click(function() {
      $(this).addClass('disabled').unbind('click');
      $.post('/my/'+self.lab_id+'/servers/'+self.server_id+'/start', {}, function(res) {
        self.disableAllButtons();
        $('.server-status').val('Starting');
        $('.server-status').html('<img class=\"inline-preloader\" src="/static/img/c_preloader_blue_13.gif"> Starting');
      });
    });
  },
  enableStopButton : function() {
    var self = this;
    $('.small-buttons-pane li:nth-child('+this.server_buttons['stop']+')').removeClass('disabled');
    $('.small-buttons-pane li:nth-child('+this.server_buttons['stop']+')').click(function() {
      $(this).addClass('disabled').unbind('click');
      self.disableAllButtons();
      $('.server-status').val('Stopping');
      $('.server-status').html('<img class=\"inline-preloader\" src="/static/img/c_preloader_blue_13.gif"> Stopping');
      $.post('/my/'+self.lab_id+'/servers/'+self.server_id+'/stop', {});
    });
  },
  enableRestartButton : function() {
    var self = this;
    $('.small-buttons-pane li:nth-child('+this.server_buttons['restart']+')').removeClass('disabled');
    $('.small-buttons-pane li:nth-child('+this.server_buttons['restart']+')').click(function() {
      $(this).addClass('disabled').unbind('click');
      self.disableAllButtons();
      $('.server-status').val('Restaring');
      $('.server-status').html('<img class=\"inline-preloader\" src="/static/img/c_preloader_blue_13.gif"> Restarting');
      $.post('/my/'+self.lab_id+'/servers/'+self.server_id+'/restart', {}, function(res) {});
    });
  },
  enableMountButton : function() {
    var self = this;
    $('.small-buttons-pane li:nth-child('+this.server_buttons['mount']+')').removeClass('disabled');
    $('.small-buttons-pane li:nth-child('+this.server_buttons['mount']+')').click(function() {
      $(this).addClass('disabled').unbind('click');
      self.jq_mount_dialog.dialog('open');
      self.loadDisksList();
    });
  },
  enableEditButton : function() {
    var self = this;
    $('.small-buttons-pane li:nth-child('+this.server_buttons['edit']+')').removeClass('disabled');
    $('.small-buttons-pane li:nth-child('+this.server_buttons['edit']+')').click(function() {
      window.location = '/my/'+self.lab_id+'/servers/'+self.server_id+'/edit';
    });
  },
  showDestroyBlock : function() {
    $('.destroy-block').toggle();
  },
  hideDestroyBlock : function() {
    $('.destroy-block').fadeOut();
  },
  destroy : function() {
    var self = this;
    $.post('/my/'+this.lab_id+'/servers/'+this.server_id+'/destroy', {}, function(res) {
      window.location = '/my/'+self.lab_id+'/servers';
    });
  },
  loadDisksList : function() {
    this.jq_mount_dialog.append('<img id="dialog-preloader" style="position: relative; top: 170px; left: 315px;" src="/static/img/c_preloader_64.gif">');
    $.get('/my/'+this.lab_id+'/servers/'+this.server_id+'/slist', {}, function(res) {
      $('#dialog-preloader').after(res);
      $('#dialog-preloader').remove();
    });
  },
  mount : function() {
    var my_local_id_list = [];
    var my_nfs_id_list = [];
    var sh_id_list = [];
    $('.server-mount-dialog input:checked').filter('[data=my-local]').each(function(i, el) {
      my_local_id_list.push(el.name);
    });
    $('.server-mount-dialog input:checked').filter('[data=my-nfs]').each(function(i, el) {
      my_nfs_id_list.push(el.name);
    });
    $('.server-mount-dialog input:checked').filter('[data=shared]').each(function(i, el) {
      sh_id_list.push(el.name);
    });
    var data = {'my_local': my_local_id_list.join(','),
                'my_nfs':   my_nfs_id_list.join(','),
                'shared':   sh_id_list.join(',')}
    var self = this;
    $.post('/my/'+this.lab_id+'/servers/'+this.server_id+'/mount', data, function(res) {
      self.jq_mount_dialog.dialog('close');
      self.jq_mount_dialog.html('');
      self.loadMountList();
    });
  },
  showEdit : function() {
    $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/servers/'+this.server_id+'/edit', {}, function(res) {
      $('#show-project-content').html(res);
    });
  },
  showLog : function() {
    $.get('/my/'+this.lab_id+'/servers/'+this.server_id+'/log', {'project_id' : this.project_id }, function(res) {
      $('#show-project-content').html(res);
    });
  },
  create : function(project_id) {
    var name = $('#name').val().trim();
    var description = $('#description').val().trim();
    var cpu = $('#cpu').val().trim();
    cpu = parseInt(cpu);
    var ram = $('#ram').val().trim();
    ram = parseInt(ram);
    var image = $('#image').val();

    $('#js-form-error').remove();

    if(name.length < 1) {
      addServerFormError('#name', 'Please enter server name.');
      return;
    }
    var res = name.match(/^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{0,30}$/);
    if(res == null) {
      addServerFormError('#name', 'Please use latin letter');
      return;
    }
    var params = {'name'        : name,
                  'description' : description,
                  'cpu'         : cpu,
                  'ram'         : ram,
                  'image'       : image,
                  'project'     : project_id};
    var self = this;
    $.post('/my/'+this.lab_id+'/servers/new', params, function(res) {
      if(res.result == 'ok') {
        self.setServer(res.server_id);
        ppane.showServer(res.server_id);
      }
    });
  },
  umount : function(storage_id, cls) {
    $('#server-mounts-storage-'+storage_id).remove();
    if ($('.'+cls+' tbody tr td').length == 0) {
      $('.'+cls).remove();
    }
    $.post('/my/'+this.lab_id+'/servers/'+this.server_id+'/umount', {'storage_id': storage_id}, function(res) {
    });
  }
}


function updateLabServersStatus(lab_id) {
  $.get('/my/'+lab_id+'/servers/status?'+getRand(), {}, function(res) {
    for(var i in res) {
      $("#servers-tbl tbody tr#"+res[i].id+" td:nth-child(3)").html(res[i].status);
    }
  });
}

function validateEditServerForm(lab_id, server_id) {
  var name = $('#server-name').val().trim();
  var description = $('#server-description').val().trim();

  $('#js-form-error').remove();

  if(name.length < 1) {
    $('#server-name').after('<span id="js-form-error" class="form-error">Please enter server name</span>');
    return;
  }
  var res = name.match(/^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{0,30}$/);
  if(res == null) {
    $('#server-name').after('<span id="js-form-error" class="form-error">Please use latin letter</span>');
    return;
  }

  var cpu = $('#server-cpu').val();
  if (cpu) {
    cpu = parseInt(cpu.trim());
  } else {
    cpu = 0;
  }
  var memory = $('#server-memory').val();
  if (memory) {
    memory = parseInt(memory.trim());
  } else {
    memory = 0;
  }
  var cpulimit = $('#server-cpulimit').val();

  $('#edit-server-form button').parent().append('<img src="/static/img/c_preloader_24.gif" style="position: relative; top: 7px; left: 5px;" >');

  var request = {'name'        : name,
                 'description' : description,
                 'cpu'      : cpu,
                 'memory'   : memory,
                 'cpulimit' : cpulimit};

  $.post('/my/'+lab_id+'/servers/'+server_id+'/edit', request, function(res) {
    window.location = '/my/'+lab_id+'/servers/'+server_id;
  });
}

function addServerFormError(id, text) {
  $(id).after('<span id="js-form-error" class="form-error">'+text+'</span>');
}

function validateNewServerForm(lab_id) {
  var name = $('#name').val().trim();
  var description = $('#description').val().trim();
  var cpu = $('#cpu').val().trim();
  cpu = parseInt(cpu);
  var ram = $('#ram').val().trim();
  ram = parseInt(ram);
  var image = $('#image').val();
  var project = $('#project').val();

  $('#js-form-error').remove();

  if(name.length < 1) {
    addServerFormError('#name', 'Please enter server name.');
    return;
  }
  var res = name.match(/^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{0,30}$/);
  if(res == null) {
    addServerFormError('#name', 'Please use latin letter');
    return;
  }

  $('.server-form button').attr('disabled', 'disabled');
  $('.server-form > div:last-child').append('<img src="/static/img/c_preloader_24.gif" style="position: relative; top: 7px; left: 5px;" />');
  var params = {'name'        : name,
                'description' : description,
                'cpu'         : cpu,
                'ram'         : ram,
                'image'       : image,
                'project'     : project};
  var cserver_id = $('.cservers-side-block div.selected').attr('id');
  if (cserver_id != undefined) {
    cserver_id = cserver_id.slice(8);
    params['cserver'] = cserver_id;
  }
  $.post('/my/'+lab_id+'/servers/new', params, function(res) {
    if (res.result == "ok") {
      window.location = '/my/'+lab_id+'/servers/'+res.server_id;
    } else {
      window.location = '/my/'+lab_id+'/servers';
    }
  });
}

function initServersTable(lab_id, project_id) {
  if (lab_id && project_id) {
    var requestUrl = '/my/'+lab_id+'/servers/list?project_id='+project_id;
    var colNames = ['Name', 'Status', 'Ip', 'Cores', 'RAM', 'Created by'];
    var colModel = [ {name:'name',       index:'name',       width:130},
                     {name:'status',     index:'status',     width:100},
                     {name:'ip',         index:'ip',         width:100},
                     {name:'cpu',        index:'cpu',        width:80,  align: 'right'},
                     {name:'ram',        index:'ram',        width:80,  align: 'right'},
                     {name:'created_by', index:'created_by', width:150, align: 'right'} ];
  } else {
    var requestUrl = 'servers/list';
    var colNames = ['Name', 'Project', 'Status', 'Ip', 'Cores', 'RAM', 'Created by'];
    var colModel = [ {name:'name',       index:'name',       width:130},
                     {name:'project',    index:'project',    width:100},
                     {name:'status',     index:'status',     width:100},
                     {name:'ip',         index:'ip',         width:100},
                     {name:'cpu',        index:'cpu',        width:80,  align: 'right'},
                     {name:'ram',        index:'ram',        width:80,  align: 'right'},
                     {name:'created_by', index:'created_by', width:150, align: 'right'} ];
  }
  $('#servers-tbl').jqGrid({
    url         : requestUrl,
    datatype    : 'json',
    colNames    : colNames,
    colModel    : colModel,
    rowNum      : -1,
    rowTotal    : 2000,
    pager       : '#servers-pager',
    sortname    : 'name',
    viewrecords : true,
    sortorder   : 'desc',
    caption     : 'Servers',
    scroll      : 1,
    rownumWidth : 40,
    gridview    : true,
    height      : 300,
    loadonce    : false
  });
  $('#servers-tbl').jqGrid('navGrid','#servers-pager', {edit:false,add:false,del:false,search:false});
  // fix
  $('#gview_servers-tbl > div:first-child.ui-helper-clearfix').removeClass('ui-helper-clearfix');
  $('#gview_servers-tbl > div:first-child').css('padding', '3px 0 20px 3px');
  $('#gbox_servers-tbl').css('margin', '20px 0 0 0');
}

function setLimits(cpu_lim, mem_lim) {
    $('#server-cpu-slider').slider({
        min: cpu_lim[0],
        max: cpu_lim[1],
        step: 1,
        slide: function(event, ui) {
            $('#cpu').val(ui.value);
        }
    });
    $('#cpu').val($("#server-cpu-slider").slider('value'));
    $('#server-ram-slider').slider({
        min: mem_lim['min'],
        max: mem_lim['max'],
        step: mem_lim['step'],
        slide: function(event, ui) {
            $('#ram').val(ui.value+' MB');
        }
    });
    $('#ram').val($('#server-ram-slider').slider('value'));
}
function initServerCServer() {
    for (var i in cservs_info) {
        if (cservs_info[i][3] == 0)
            continue;
        var cs = cservs_info[i][0];
        var cpu_limits = cservs_info[i][1];
        var mem_limits = cservs_info[i][2];
        $('#cserver-'+cs).addClass('selected');
        setLimits(cpu_limits, mem_limits);
        break;
    }
}
function selectCServerForServer(cserv_id) {
    for (var i in cservs_info) {
        if (cservs_info[i][0] == cserv_id) {
            if (cservs_info[i][3] == 0)
                return;
            break;
        }
    }
    $('.cservers-side-block > div').removeClass('selected');
    $('#cserver-'+cserv_id).addClass('selected');
}
