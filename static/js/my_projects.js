"use strict";

function setSlider(slider, value, values) {
  $(slider).slider({
      min   : values.min,
      max   : values.max,
      value : values.default,
      slide: function(event, ui) {
          $(value).val(ui.value+' GB');
      }
  });
  $(value).val($(slider).slider('value')+' GB');
}

function showProjectContent(selector, body) {
    $(selector).html(body).slideDown();
}

function ProjectsPane(lab_id) {
  this.lab_id = lab_id;
  this.project_pane = {
    'servers':  1,
    'storages': 2,
    'members':  3,
    'updates':  4
  }
  this.servers_interval_id = null;
  this.storages_interval_id = null;
}

ProjectsPane.prototype = {
    /* common */
    showPreloader : function() {
        $('.pane-content').html('<img src="/static/img/h_preloader_160_24.png" alt="Loading" title="Loading" />');
    },
    showProjectPreloader : function() {
        $('.project-content').html('<img src="/static/img/h_preloader_160_24.png" alt="Loading" title="Loading" />');
    },
    /* projects */
    showMyProjects : function() {
        this.unselectProjects();
        this.showPreloader();
        var self = this;
        $.get('/my/'+this.lab_id+'/projects/my', {}, function(res) {
            self.hideProjectPane();
            $('.pane-content').html(res);
        });
    },
    showAllProjects : function() {
        this.unselectProjects();
        this.showPreloader();
        var self = this;
        $.get('/my/'+this.lab_id+'/projects/all', {}, function(res) {
            self.hideProjectPane();
            $('.pane-content').html(res);
        });
    },
    showNewForm : function() {
        this.unselectProjects();
        this.showPreloader();
        var self = this;
        $.get('/my/'+this.lab_id+'/projects/new', {}, function(res) {
            self.hideProjectPane();
            $('.pane-content').html(res);
        });
    },
    showProject: function(project_id, handler) {
        var project_changed = true;
        if (this.project_id != project_id) {
            project_changed = false;
        }
        this.project_id = project_id;
        this.showPreloader();
        server.setProject(project_id);
        storage.setProject(project_id);

        $('.my-projects-pane > ul > li a').removeClass('selected');
        $('#project-'+project_id+' > a').addClass('selected');

        var project_name = $('#project-'+project_id+' a').html();
        $('.project-pane h4 a').html(project_name);

        if (!project_changed) {
            $('.my-projects-pane .menu').slideUp();
            $('#project-'+project_id+' .menu').slideDown();
        }
        var self = this;
        $.get('/my/'+this.lab_id+'/projects/'+project_id, {}, function(res) {
            $('.pane-content').html(res);
            if (!handler) {
              self.showServers();
              return;
            }
            if ('servers' == handler) {
                self.showServers();
            } else if ('storages' == handler) {
                self.showStorages();
            } else if ('members' == handler) {
                self.showMembers();
            } else if ('updates' == handler) {
                self.showUpdates();
            }
        });
    },
    unselectProjects : function() {
        $('.my-projects-pane ul li').removeClass('selected');
    },
    setProjectPane : function(pane) {
        $('.my-projects-pane .menu li').removeClass('selected');
        $('.my-projects-pane #project-'+this.project_id+' .menu li:nth-child('+this.project_pane[pane]+')').addClass('selected');
    },
    hideProjectPane : function() {
        $('.project-pane').slideUp();
    },
    validateNewProject: function() {
        delFormError();
        var name = $('#project-name').val().trim();
        var description = $('#project-description').val().trim();

        if(name.length < 2) {
            addFormError('#project-name', 'Min project name length 2.')
            return {'result': 'error'}
        }
        var res = name.match(/^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{1,20}$/);
        if(res == null) {
            addFormError('#project-name', 'Please use latin letter.');
            return {'result': 'error'}
        }
        if(description == '') {
            description = '';
        }
        $('#new-project-form button').attr('disabled', 'disabled');
        $('#new-project-form button').parent().append('<img src="/static/img/c_preloader_24.gif" style="position: relative; top: 7px; left: 5px;" >');
        return {'result':      'ok',
                'name':        name,
                'description': description}
    },
    newProject: function() {
        var data = this.validateNewProject();
        if (data.result == 'error') {
            return;
        }
        var self = this;
        $.post('/my/'+this.lab_id+'/projects/new', data, function(res) {
            if(res.result == 'ok') {
                self.addProjectToPane(res.project_id, data.name);
                self.showProject(res.project_id, 'servers');
            }
        });
    },
    resetProjects: function() {
        $('.projects-aside .my-projects-pane > ul li').remove();
    },
    addProjectToPane : function(project_id, project_name) {
        var menu = '<li><a href="#" onclick="ppane.showServers(); return false;">Servers</a></li>';
        menu += '<li><a href="#" onclick="ppane.showStorages(); return false;">Disks</a></li>';
        menu += '<li><a href="#" onclick="ppane.showMembers(); return false;">Members</a></li>';
        menu += '<li><a href="#" onclick="ppane.showUpdates(); return false;">Updates</a></li>';
        menu = '<ul class="menu">'+menu+'</ul>';
        var name = '<a href="#" onclick="ppane.showProject('+project_id+'); return false;">'+project_name+'</a>';
        var html = '<li id="project-'+project_id+'">'+name+menu+'</li>';
        $('.projects-aside .my-projects-pane > ul').append(html);
    },
    initProjectDialog : function() {
        $('#show-project-dialog').dialog({
            title     : 'Project',
            autoOpen  : false,
            resizable : false,
            width     : 450,
            maxWidth  : 450,
            height    : 400,
            maxHeight : 400
        });
    },
    showDeleteProject : function(project_id) {
        this.showProjectPreloader();
        $.get('/my/'+this.lab_id+'/projects/'+project_id+'/delete', function(res) {
            $('.project-content').html(res);
        });
    },
    deleteProject : function(project_id) {
        var self = this;
        $.post('/my/'+this.lab_id+'/projects/'+project_id+'/delete', {}, function(res) {
            if (res.result == 'ok') {
                $('#project-'+project_id).remove();
                self.showMyProjects();
            }
        });
    },
    showEditProject: function(project_id) {
        this.showProjectPreloader();
        $.get('/my/'+this.lab_id+'/projects/'+project_id+'/edit', {}, function(html) {
            $('.project-content').html(html);
        });
    },
    editProject: function(project_id) {
        var data = this.validateNewProject();
        if (data.result == 'error') {
            return;
        }
        data.project = project_id;
        var self = this;
        $.post('/my/'+this.lab_id+'/projects/'+project_id+'/edit', data, function(res) {
            if (res.result == 'ok') {
                self.showProject(project_id, 'servers');
            } else {
                self.showMyProjects();
            }
        });
    },
    parseHash : function() {
        var hash = window.location.hash.substr(1);
        if (hash == '' || hash == 'my') {
            this.showMyProjects();
            return;
        }
        if (hash == 'all') {
            this.showAllProjects();
            return;
        }
        if (hash == 'new') {
            this.showNewForm();
            return;
        }
        hash = hash.split(';');
        var params = {};
        for (var h in hash) {
            var s = hash[h].split('=');
            params[s[0]] = s[1]
        }
        if (params.project) {
            if (params.servers) {
                this.showProject(params.project, 'servers');
                return;
            } else if (params.disks) {
                this.showProject(params.project, 'storages');
                return;
            } else if (params.members) {
                this.showProject(params.project, 'members');
                return;
            } else if (params.updates) {
                this.showProject(params.project, 'updates')
                return;
            } else {
                this.showProject(params.project, 'servers');
                return;
            }
        }
        this.showMyProjects();
    },
    updateMyProjects: function(projects) {
        this.resetProjects();
        for (var i in projects) {
            this.addProjectToPane(projects[i].id, projects[i].name);
        }
    },
    /* servers */
    showServers : function() {
      this.setServersInterval();
      this.setProjectPane('servers');
      this.showProjectPreloader();
      $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/servers', {}, function(res) {
        $('.project-content').html(res);
      });
    },
    setServersInterval : function() {
      if (this.servers_interval_id != null) {
        clearInterval(this.servers_interval_id);
      }
      this.servers_interval_id = setInterval('ppane.updateServersStatus();', 15000);
    },
    updateServersStatus : function() {
      if ($('#servers-tbl').length == 0) {
        clearInterval(this.servers_interval_id);
        this.servers_interval_id = null;
        return;
      }
      $.get('/my/'+this.lab_id+'/servers/status?'+getRand(), {}, function(res) {
        for(var i in res) {
          $("#servers-tbl tbody tr#"+res[i].id+" td:nth-child(2)").html(res[i].status);
        }
      });
    },
    showServer : function(server_id) {
      this.showProjectPreloader();
      $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/servers/'+server_id, {}, function(res) {
        $('.project-content').html(res);
      });
    },
    showServerNewForm : function() {
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/servers/new', {}, function(res) {
            $('.project-content').html(res);
        });
    },
    createNewServer : function() {
        var name = $('#name').val().trim();
        var description = $('#description').val().trim();
        var cpu = $('#cpu').val().trim();
        cpu = parseInt(cpu);
        var ram = $('#ram').val().trim();
        ram = parseInt(ram);
        var image = $('#image').val();

        $('#js-form-error').remove();

        if(name.length < 1) {
            addFormError('#name', 'Please enter server name.');
            return;
        }
        var res = name.match(/^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{0,30}$/);
        if(res == null) {
            addFormError('#name', 'Please use latin letter');
            return;
        }
        $('.server-form button').attr('disabled', 'disabled');
        var params = {'name'        : name,
                      'description' : description,
                      'cpu'         : cpu,
                      'ram'         : ram,
                      'image'       : image,
                      'project'     : this.project_id};
        var cserver_id = $('.cservers-side-block div.selected').attr('id');
        if (cserver_id != undefined) {
          cserver_id = cserver_id.slice(8);
          params['cserver'] = cserver_id;
        }
        var self = this;
        $.post('/my/'+this.lab_id+'/servers/new', params, function(res) {
            if(res.result == 'ok') {
                self.showServer(res.server_id);
            } else {
                self.showServers();
            }
            updateLabLimits(self.lab_id);
        });
    },
    showServerEditForm : function(server_id) {
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/servers/'+server_id+'/edit', {}, function(res) {
            $('.project-content').html(res);
        });
    },
    updateServer : function(server_id) {
        var name = $('#server-name').val().trim();
        var description = $('#server-description').val().trim();

        var cpu = $('#server-cpu').val();
        if (cpu) {
            cpu = parseInt(cpu.trim());
        } else {
            cpu = 0;
        }
        var ram = $('#server-ram').val();
        if (ram) {
            ram = parseInt(ram.trim());
        } else {
            ram = 0;
        }
        $('#js-form-error').remove();

        if(name.length < 1) {
            addFormError('#server-name', 'Please enter server name');
            return;
        }
        var res = name.match(/^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{0,30}$/);
        if(res == null) {
            addFormError('#server-name', 'Please use latin letter');
            return;
        }
        var request =  {'name'          : name,
                        'description'   : description,
                        'cpu'           : cpu,
                        'memory'        : ram,
                        'without-flash' : true}
        var self = this;
        $.post('/my/'+this.lab_id+'/servers/'+server_id+'/edit', request, function(json) {
            self.showServer(server_id);
        });
    },
    showServerLog : function(server_id) {
        $.get('/my/'+this.lab_id+'/servers/'+server_id+'/log', {'project_id' : this.project_id }, function(res) {
            $('.project-content').html(res);
        });
    },
    showServerDestroyBlock : function() {
        $('.destroy-block').toggle();
    },
    hideServerDestroyBlock : function() {
        $('.destroy-block').hide();
    },
    destroyServer : function(server_id) {
        var self = this;
        $.post('/my/'+this.lab_id+'/servers/'+server_id+'/destroy', {}, function(res) {
            self.showServers();
            updateLabLimits(self.lab_id);
        });
    },
    /* storages */
    showStorages : function() {
      this.setStoragesInterval();
      this.setProjectPane('storages');
      this.showProjectPreloader();
      $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/disks', {}, function(res) {
        $('.project-content').html(res);
      });
    },
    setStoragesInterval : function() {
      if (this.storages_interval_id != null) {
        clearInterval(this.storages_interval_id);
      }
      this.storages_interval_id = setInterval('ppane.updateStoragesStatus();', 15000);
    },
    updateStoragesStatus : function() {
      if ($('#storages-tbl').length == 0) {
        clearInterval(this.storages_interval_id);
        this.storages_interval_id = null;
        return;
      }
      $.post('/my/'+this.lab_id+'/storage?'+getRand(), {}, function(res) {
        for(var i in res) {
          var jq_status = $('#storages-tbl tbody tr#'+res[i].id+' td:nth-child(3)');
          jq_status.html(res[i].status);
        }
      });
    },
    showStorageNewForm : function() {
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/disks/new', {}, function(res) {
            $('.project-content').html(res);
        });
    },
    createNewStorage : function() {
        delFormError();
        var name = $('#name').val().trim();
        var alias = $('#alias').val().trim();
        var description = $('#description').val().trim();
        var size = $('#size').val();
        size = parseInt(size.trim());
        var type = $('#type').val();

        if(name.length < 1) {
            addFormError('#name', 'Please enter disk name.');
            return;
        }
        var res = name.match(/^[a-zA-Z0-9][a-zA-Z0-9\s\-\']{0,30}$/);
        if(res == null) {
            addFormError('#name', 'Please use latin letters.');
            return;
        }
        if (alias) {
            var res = alias.match(/^[a-zA-Z0-9]{1,30}$/);
            if(res == null) {
                addFormError('#alias', 'Please use only letters and numbers');
                return;
            }
        }
        var params = {'name':        name,
                      'alias':       alias,
                      'description': description,
                      'size':        size,
                      'type':        type,
                      'project':     this.project_id};
        var cserver_id = $('.cservers-side-block div.selected').attr('id');
        if (cserver_id != undefined) {
            cserver_id = cserver_id.slice(8);
            params['cserver'] = cserver_id;
        }
        $('.storage-form button').attr('disabled', 'disabled');
        var self = this;
        $.post('/my/'+this.lab_id+'/storage/new', params, function(res) {
            if(res.result == 'ok') {
                self.showStorage(res.storage_id);
            } else if (res.result == 'error' && res.message == 'wrong alias') {
                addFormError('#alias', 'Mount point name is busy');
            } else {
                self.showStorages();
            }
            updateLabLimits(self.lab_id);
        });
    },
    showStorage : function(storage_id) {
        this.showProjectPreloader();
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/disks/'+storage_id, {}, function(res) {
            $('.project-content').html(res);
        });
    },
    showEditStorage : function(storage_id) {
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/disks/'+storage_id+'/ed', {}, function(res) {
            $('.project-content').html(res);
        });
    },
    updateStorage : function(storage_id, pre_alias) {
        var name = $('#storage-name').val().trim();
        var alias = $('#storage-alias').val().trim();
        var description = $('#storage-description').val().trim();
        var type = $('#storage-type').val();

        $('#js-form-error').remove();

        if(name.length < 1) {
            addFormError('#storage-name', 'Please enter disk name');
            return;
        }
        var res = name.match(/^[a-zA-Z0-9][a-zA-Z0-9\s\-\']{0,30}$/);
        if(res == null) {
            addFormError('#storage-name', 'Please use latin letter');
            return;
        }
        if (alias && alias != pre_alias) {
            var res = alias.match(/^[a-zA-Z0-9]{1,30}$/);
            if(res == null) {
                addFormError('#storage-alias', 'Please use only letters and numbers');
                return;
            }
        }
        var params = {'name'          : name,
                      'alias'         : alias,
                      'description'   : description,
                      'project'       : this.project_id,
                      'type'          : type,
                      'without-flash' : true}
        var self = this;
        $.post('/my/'+this.lab_id+'/storage/'+storage_id+'/edit', params, function(res) {
            if(res.result == 'ok') {
                self.showStorage(storage_id);
            } else if (res.result == 'error' && res.message == 'wrong alias') {
                addFormError('#storage-alias', 'This name is busy');
            } else {
                self.showStorages();
            }
        });
    },
    destroyStorage : function(storage_id) {
        var self = this;
        $.post('/my/'+this.lab_id+'/storage/'+storage_id+'/destroy', {}, function() {
            self.showStorages();
            updateLabLimits(self.lab_id);
        });
    },
    showStorageDestroyBlock : function() {
        $('.destroy-block').toggle();
    },
    hideStorageDestroyBlock : function() {
        $('.destroy-block').hide();
    },
    /* members */
    showMembers : function() {
        this.setProjectPane('members');
        this.showProjectPreloader();
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/members', {}, function(res) {
            $('.project-content').html(res);
        });
    },
    showAddMembersDialog : function() {
        $('#show-project-dialog').dialog('open');
        var preloader = '<img src="/static/img/c_preloader_64.gif" alt="Loading...">';
        $('#show-project-dialog').html(preloader);
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/labmembers', {}, function(res) {
            $('#show-project-dialog').html(res);
        });
    },
    addUserToProject : function(user_id) {
        var preloader = '<img src="/static/img/c_preloader_64.gif" alt="Loading...">';
        $('#show-project-dialog').html(preloader);
        var params = {'action'  : 'add',
                      'user_id' : user_id};
        var self = this;
        $.post('/my/'+this.lab_id+'/projects/'+this.project_id+'/labmembers', params, function(res) {
            $('#show-project-dialog').dialog('close');
            self.showMembers();
        });
    },
    actionUserToProject : function(action, user_id) {
        if (action == 'add-in-dashboard' || action == 'add-in-members') {
            var params = {'action'  : 'add',
                          'user_id' : user_id};
        } else {
            var params = {'action'  : action,
                          'user_id' : user_id};
        }
        var self = this;
        $.post('/my/'+this.lab_id+'/projects/'+this.project_id+'/members', params, function(res) {
            if (action == 'add-in-members') {
                var html = '<a href="#" onclick="ppane.actionUserToProject(\'delete\', '+user_id+'); return false;">Delete</a>'
                $('#show-project-members-user-'+user_id).removeClass('hl-row');
                $('#show-project-members-user-'+user_id+' td:nth-child(3)').html('member');
                $('#show-project-members-user-'+user_id+' td:last-child').html(html);
            } else if (action == 'reject') {
                $('#show-project-members-user-'+user_id).remove();
            } else if (action == 'delete') {
                $('#show-project-members-user-'+user_id).remove();
            }
        });
    },
    joinToProject : function(project_id) {
        var self = this;
        $.post('/my/'+this.lab_id+'/projects/'+project_id+'/join', {}, function(res) {
            $('.project-body .join-pane').remove();
            $('.project-body .neutral-message').html('Waiting to be approved in this project');
        });
    },
    unsubscribeProject : function(project_id) {
        var self = this;
        $.post('/my/'+this.lab_id+'/projects/'+project_id+'/us', {}, function(res) {
            $('#project-'+project_id).remove();
            self.showMyProjects();
        });
    },
    /* updates */
    showUpdates : function() {
        this.setProjectPane('updates');
        this.showProjectPreloader();
        $.get('/my/'+this.lab_id+'/projects/'+this.project_id+'/allevn', {}, function(res) {
            $('.project-content').html(res);
        });
    }
}
