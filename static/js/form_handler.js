preloader_path = '/static/img/c_preloader_18.gif';

function FormHandler(id, opt) {
    this.id = id;
    this.jq_id = $('#'+id);
    this.is_error = false;
    this.is_valid = false;
    this.min_length = opt['min_length'];
    this.max_length = opt['max_length'];
    this.pattern = opt['pattern'];
    this.min_error = opt['min_error'];
    this.max_error = opt['max_error'];
    this.pat_error = opt['pat_error'];
}

FormHandler.prototype.setError = function(textError) {
    if(this.is_error) {
        $('#fh-'+this.id+'-error').html(textError);
    } else {
        this.jq_id.css({'margin' : '1px 0 1px 0',
                        'border' : '2px solid red',
                        'border-radius' : '5px'});
        this.jq_id.parent().append('<span id="fh-'+this.id+'-error" class="form-error">'+textError+'</span>');
        this.is_error = true;
    }
}

FormHandler.prototype.setValid = function() {
    this.delError();
    this.jq_id.css({'margin' : '1px 0 1px 0',
                    'border' : '2px solid #00ff00',
                    'border-radius' : '5px'});
}

FormHandler.prototype.delError = function() {
    this.jq_id.css('border', '');
    $('#fh-'+this.id+'-error').remove();
    this.is_error = false;
    this.is_valid = true;
}

FormHandler.prototype.valid = function() {
    this.is_valid = false;
    value = this.jq_id.val();
    len = value.length;
    if(len < this.min_length) {
        this.setError(this.min_error);
        return false;
    }
    if(len > this.max_length) {
        this.setError(this.max_error);
        return false;
    }
    res = value.match(this.pattern);
    if(res == null) {
        this.setError(this.pat_error);
        return false;
    } else {
        this.setValid();
        this.is_valid = true;
        return true;
    }
}

FormHandler.prototype.url_valid = function(url, textError) {
    this.is_valid = false;
    value = this.jq_id.val();
    len = value.length;
    if(len < this.min_length) {
        this.setError(this.min_error);
        return;
    }
    if(len > this.max_length) {
        this.setError(this.max_error);
        return;
    }
    res = value.match(this.pattern);
    if(res == null) {
        this.setError(this.pat_error);
        return;
    } else {
        $('#'+this.id+'_preloader').remove();
        this.jq_id.parent().append('<img id="fh-'+this.id+'-preloader" src="'+preloader_path+'" style="position: relative; top: 3px; left: 5px;">');
        that = this;
        $.post(url, { data : value }, function(res) {
            if(res == 'true') {
                that.setValid();
                that.is_valid = true;
            } else {
                that.setError(textError);
            }
            $('#fh-'+that.id+'-preloader').remove();
        });
    }
}

FormHandler.prototype.bindUrlValidate = function(url, message) {
    var self = this;
    this.jq_id.bind('blur', function() {
        self.url_valid(url, message);
    });
}

FormHandler.prototype.bindValidate = function() {
    var self = this;
    this.jq_id.bind('blur', function() {
        self.valid();
    });
}