function LabMembers(lab_id) {
    this.lab_id = lab_id;
}
LabMembers.prototype = {
    showForm : function() {
        $('.add-new-lab-member-form').toggle();
    },
    hideForm : function() {
        $('.add-new-lab-member-form').fadeOut();
    },
    addMember : function() {
        var self = this;
        $('#email-form-error').remove();
        var email = $('#email').val().trim();
        $.post('/my/a/'+this.lab_id+'/add_member', {'email': email}, function(res) {
            if (res.result == 'error') {
                $('#email').after('<span id="email-form-error" class="form-error">'+res.message+'</span>');
            } else if (res.result == 'ok') {
	            var el = '<tr id="user-'+res.user.id+'"><td><img src="'+res.user.gravatar+'?size=30"></td><td><a href="/my/users/'+res.user.id+'">'+res.user.full_name+'</a></td><td>'+res.user.email+'</td><td class="relation">member</td><td class="control"><a href="#" onclick="lab_members.deleteUser('+res.user.id+', false); return false;">Delete</a></td></tr>';
	            $('table tbody').append(el);
	            $('#email').val('');
	            $('#email').after('<span id="email-form-error" class="form-error">Added</span>');
	            setInterval('$("#email-form-error").remove()', 1100);
	        }
        });
    },
    confirmUser : function(user_id) {
        $.post('/my/a/'+this.lab_id+'/confirm_user', {'user_id': user_id}, function(res) {
            if(res.result == 'ok') {
                $('#user-'+user_id+' td:last-child').html('<a href="#" onclick="lab_members.deleteUser('+user_id+', false); return false;">Delete</a>');
                $('#user-'+user_id+' .relation').html('member');
                $('#user-'+user_id).removeClass('hl-row');
            }
        })
    },
    deleteUser : function(user_id, is_reject) {
        if(confirm('Delete user from lab ?')) {
	        var params = {'user_id' : user_id, 'is_reject' : is_reject};
            $.post('/my/a/'+this.lab_id+'/delete_user', params, function(res) {
                if(res.result == 'ok') {
                    $('#user-'+user_id).remove();
                }
            });
        }
    }
}