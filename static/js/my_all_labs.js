function showAllProjectsByType(type, i) {
    $('.lab-types li a').removeClass('selected')
    $('.lab-types li:nth-child('+i+') a').addClass('selected');
    $('.all-labs').html('<img src="/static/img/h_preloader_160_24.png" alt="Loading" />');
    $.get('/my/labs/all/type', {'type' : type}, function(res) {
        $('.all-labs').html(res);
    });
}
