function LastEvents(url) {
    this.start = 0;
    this.count = 25;
    this.url = url;
    this.is_loading = false;
    this.end = false;
}

LastEvents.prototype = {
    add_preloader : function() {
        $('table tbody').append('<img class="preloader" src="/static/img/c_preloader_32.gif" alt="Loading" />');
    },
    remove_preloader : function() {
        $('.preloader').remove();
    },
    update : function() {
        if (this.is_loading) { return; }
        this.is_loading = true;
        if (this.end) { return; }

        $('table tbody tr').removeClass('hl-event-row');
        this.add_preloader();
        var self = this;
        $.get(self.url, {'start' : self.start}, function(data){
            if (data.length == 0) {
                self.end = true;
                self.remove_preloader();
                return;
            }

            var html = '';
            for (var i in data) {
                if (self.start != 0) {
                    html += '<tr class="hl-event-row">';
                } else {
                    html += '<tr>';
                }
                html += '<td>'+data[i]['data']+'</td><td>'+data[i]['text']+'</td></tr>';
            }
            $('table tbody').append(html);
            self.start += self.count;
            self.remove_preloader();
            self.is_loading = false;
        });
    },

    bind_scroll : function() {
        var self = this;
        var elem = $(window);
        elem.scroll(function() {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                self.update();
            }
        });
    }  
}