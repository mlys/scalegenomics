function setDescAutoResize(selector, dheight) {
    $(selector).autoResize({
        animateDuration: 300,
        extraSpace: 20
    });
    if (dheight != undefined) {
        $(selector).css('height', dheight);
    }
}

