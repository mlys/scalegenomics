var __month = 1;
var __months = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

function setPreloader() {
    var preloader = '<img src="/static/img/c_preloader_64.gif" alt="Loading" title="Loading">';
    $('.monthly-content').html(preloader);
}

function setCurrentMonth(month) {
    $('.left-months-bar ul li').removeClass('current');
    $('.left-months-bar ul li:nth-child('+month+')').addClass('current');
    $('.current-month').html(__months[month-1]);
}

function loadMonthInvoices(month) {
  setPreloader();
  setCurrentMonth(month);
  __month = month;

  var params = {'month' : __month }
  $.get('/root/invoices/month', params, function(res) {
    $('.monthly-content').html(res);
  });
}

function loadInvoice(lab_id) {
    setPreloader();
    var params = { 'lab_id' : lab_id,
                   'month'  : __month };
    $.get('/root/invoices/show', params, function(res) {
        $('.monthly-content').html(res);
    })
}

function loadEditInvoice(lab_id) {
  setPreloader();
  var params = {'lab_id': lab_id,
                'month' : __month};
  $.get('/root/invoices/edit', params, function(res) {
    $('.monthly-content').html(res);
  })
}

function updateInvoice(lab_id) {
  var params = {'lab_id'      : lab_id,
                'month'       : __month,
                'server_cpu'  : $('#server-cpu-price').val(),
                'server_ram'  : $('#server-ram-price').val(),
                'server_size' : $('#server-size-price').val(),
                'disk_size'   : $('#disk-size-price').val(),
                'paid'        : $('#paid').val() };
  setPreloader();
  $.post('/root/invoices/edit', params, function(res) {
    loadInvoice(lab_id);
  });
}

