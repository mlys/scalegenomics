function cleanSignupForm() {
    $('#username').val('');
    $('#password').val('');
    $('#email').val('');
    $('#first_name').val('');
    $('#last_name').val('');
    $('#password').val('');
    $('#password_confirm').val('');
    $('#organization_name').val('');
    $('#department').val('');
    $('#title').val('');
    $('#phone').val('');
    $('#how_how').val('');
    $('#tell_us').val('');
}

function initSignupForm() {
    cleanSignupForm();
    fh_un_opt = {
        'min_length': 4,
        'max_length': 20,
        'pattern': /^[a-zA-Z0-9]{4,20}$/,
        'min_error': 'Please enter at least 4 characters',
        'max_error': 'Please enter maximum 20 characters',
        'pat_error': 'Only letters and numbers are allowed'
    }
    fh_un = new FormHandler('username', fh_un_opt);
    fh_un.bindUrlValidate('/sso/a/userexists', 'User already exists');

    fh_em_opt = {
        'min_length': 7,
        'max_length': 30,
        'pattern'  : /^([a-zA-Z0-9][a-zA-Z0-9_\-\.]*\@([a-zA-Z0-9]{2,}\.){1,}[a-zA-Z]{2,4})$/,
        'min_error': 'Please enter at least 7 characters',
        'max_error': 'Please enter maximum 30 characters',
        'pat_error': 'Enter valid email'
    }
    fh_em = new FormHandler('email', fh_em_opt);
    fh_em.bindUrlValidate('/sso/a/emailexists', 'Email already exists');

    fh_fl_opt = {
        'min_length': 2,
        'max_length': 20,
        'pattern': /^([a-zA-Z][a-zA-Z\-]{1,20})$/,
        'min_error': 'Please enter at least 2 characters',
        'max_error': 'Please enter maximum 20 characters',
        'pat_error': 'Only letters, and - are allowed'
    }
    fh_fn = new FormHandler('first_name', fh_fl_opt);
    fh_ln = new FormHandler('last_name', fh_fl_opt);
    fh_fn.bindValidate();
    fh_ln.bindValidate();

    fh_pw_opt = {
        'min_length': 6,
        'max_length': 20,
        'pattern': /^.{6,20}$/,
        'min_error': 'Please enter at least 6 characters',
        'max_error': 'Please enter maximum 20 characters',
        'pat_error': ''
    }
    fh_pw = new FormHandler('password', fh_pw_opt);
    fh_cp = new FormHandler('confirm_password', fh_pw_opt);
    $('#password').bind('blur', function() {
        fh_pw.valid();
        if(fh_pw.is_valid && fh_cp.is_valid) {
            p_value = $('#password').val();
            cp_value = $('#confirm_password').val();
            if(p_value == cp_value) {
                fh_pw.setValid();
                fh_cp.setValid();
            } else {
                fh_pw.setError('');
                fh_cp.setError("Passwords did't match");
            }
        }
    });
    $('#confirm_password').bind('blur', function() {
        fh_cp.valid();
        if(fh_pw.is_valid && fh_cp.is_valid) {
            pw_value = $('#password').val();
            cp_value = $('#confirm_password').val();
            if(pw_value == cp_value) {
                fh_pw.setValid();
                fh_cp.setValid();
            } else {
                fh_pw.setError('');
                fh_cp.setError("Passwords did't match");
            }
        }
    });
    fh_org_opt = {
        'min_length': 0,
        'max_length': 50,
        'pattern': /^[a-zA-Z0-9\s\']{0,50}$/,
        'min_error': '',
        'max_error': 'Please enter maximum 50 characters',
        'pat_error': 'Only letters and numbers are allowed'
    }
    fh_org = new FormHandler('organization_name', fh_org_opt);
    fh_org.bindValidate();

    fh_dep_opt = {
        'min_length': 0,
        'max_length': 50,
        'pattern': /^[a-zA-Z0-9\s\']{0,50}$/,
        'min_error': '',
        'max_error': 'Please enter maximum 50 characters',
        'pat_error': 'Only letters and numbers are allowed'
    }
    fh_dep = new FormHandler('department', fh_dep_opt);
    fh_dep.bindValidate();

    fh_tit_opt = {
        'min_length': 0,
        'max_length': 50,
        'pattern':  /^[a-zA-Z0-9\s\']{0,50}$/,
        'min_error': '',
        'max_error': 'Please enter maximum 50 characters',
        'pat_error': 'Only letters and numbers are allowed'
    }
    fh_tit = new FormHandler('title', fh_tit_opt);
    fh_tit.bindValidate();

    fh_pho_opt = {
        'min_length': 0,
        'max_length': 50,
        'pattern': /^[0-9\s]{0,50}$/,
        'min_error': '',
        'max_error': 'Please enter maximum 50 characters',
        'pat_error': 'Only numbers are allowed'
    }
    fh_pho = new FormHandler('phone', fh_pho_opt);
    fh_pho.bindValidate();
}

function validSignupForm() {
    if(!fh_un.is_valid) {
        fh_un.url_valid('/sso/a/userexists', 'User already exists');
        return;
    }
    if(!fh_em.is_valid) {
        fh_em.url_valid('/sso/a/emailexists', 'Email already exists');
        return;
    }
    if(!fh_fn.is_valid) {
        fh_fn.valid();
        return;
    }
    if(!fh_ln.is_valid) {
        fh_ln.valid();
        return;
    }
    if(!fh_pw.is_valid) {
        fh_pw.valid();
        return;
    }
    if(!fh_cp.is_valid) {
        fh_pw.valid();
        return;
    }
    pw_value = $('#password').val();
    cp_value = $('#confirm_password').val();
    if(pw_value != cp_value) {
        fh_cp.setError("Passwords did't match");
        return;
    }

    fh_org.valid();
    if (!fh_org.is_valid) {
        fh_org.valid();
        return;
    }
    fh_dep.valid();
    if (!fh_dep.is_valid) {
        fh_dep.valid();
        return;
    }
    fh_tit.valid();
    if (!fh_tit.is_valid) {
        fh_tit.valid();
        return;
    }
    fh_pho.valid();
    if (!fh_pho.is_valid) {
        fh_pho.valid();
        return;
    }

    var b = $('button');
    b.parent().append('<img src="/static/img/c_preloader_24.gif" style="position: relative; top: 7px; left: 5px;">');

    sdata = {
        'username': $('#username').val(),
        'email': $('#email').val(),
        'first_name': $('#first_name').val(),
        'last_name': $('#last_name').val(),
        'password': pw_value,
        'confirm_password': cp_value,
        'organization': $('#organization_name').val(),
        'department': $('#department').val(),
        'title': $('#title').val(),
        'phone': $('#phone').val(),
        'how_how': $('#how_how').val(),
        'tell_us': $('#tell_us').val()
    }
    $.post('/sso/a/newuser', sdata, function(res) {
        window.location = '/';
    });
}

