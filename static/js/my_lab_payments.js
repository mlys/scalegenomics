"use strict";

function LabPayments(lab_id, year, month) {
    this.lab_id = lab_id;
    this.year = year;
    this.month = month;

    this.loadData();
}

LabPayments.prototype.setYear = function(year) {
    this.year = year;
    this.loadData();
}

LabPayments.prototype.setMonth = function(month) {
    this.month = month;
    this.loadData();
}

LabPayments.prototype.loadData = function() {
    $('div.months ul li').removeClass('current')
    $('div.months ul li:nth-child('+this.month+')').addClass('current');

    $('div.years ul li').removeClass('current')
    $('div.years ul li[value="'+this.year+'"]').addClass('current');

    $.get('/my/'+this.lab_id+'/payments/plans/'+this.year+'/'+this.month, {}, function(res) {
        $('.lab-payments .plans').html(res);
    });
}
