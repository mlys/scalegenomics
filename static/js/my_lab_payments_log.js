"use strict";

function LabPayments(lab_id, year, month) {
    this.lab_id = lab_id;
    this.now_year  = year;
    this.now_month = month;
    this.current_year  = year;
    this.current_month = month;

    this.months = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
    this.sort_by = 'all';

    this.setNow();
    this.parseHash();
}

LabPayments.prototype.setNow = function() {
    $('.payments-months-bar ul li:nth-child('+this.now_month+')').addClass('now');
}

LabPayments.prototype.parseHash = function() {
    var hash = window.location.hash.substr(1);
    for (var i in this.months) {
        if (hash == this.months[i]) {
            var pos = parseInt(i);
            this.setMonth(pos+1);
            return;
        }
    }
    this.setMonth(this.current_month);
}

LabPayments.prototype.setMonth = function(month) {
    this.current_month = month;

    $('.payments-months-bar ul li').removeClass('current');
    $('.payments-months-bar ul li:nth-child('+this.current_month+')').addClass('current');

    window.location.hash = this.months[this.current_month-1];

    this.showPayments();
}

LabPayments.prototype.sortBy = function(sort_by) {
    this.sort_by = sort_by;

    $('.current-month').html(this.months[this.current_month-1]);
    $('.payments-sort-pane li').removeClass('current');

    if (this.sort_by == 'all') {
        $('.payments-sort-pane li:nth-child(2)').addClass('current');
    } else if (this.sort_by == 'server') {
        $('.payments-sort-pane li:nth-child(3)').addClass('current');
    } else if (this.sort_by == 'disk') {
        $('.payments-sort-pane li:nth-child(4)').addClass('current');
    }
    this.showPayments();
}

LabPayments.prototype.showPayments = function() {
    this.setPreloader();
    $.get('/my/'+this.lab_id+'/payments/'+this.sort_by+'/month', {'month' : this.current_month}, function(res) {
        $('.lab-payments-info').html(res);
    });
}

LabPayments.prototype.setPreloader = function() {
    var preloader = '<img alt="Loading" title="Loading" src="/static/img/c_preloader_64.gif">';
    $('.lab-payments-info').html(preloader);
}

function showProjectPayments(lab_id, project_id) {
    setPreloader();
    var params = {'project-id' : project_id,
                  'month'      : __month};
    $.get('/my/'+lab_id+'/payments/project', params, function(res) {
        $('.lab-payments-info').html(res);
    });
}
