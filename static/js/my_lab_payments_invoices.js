function LabInvoices(lab_id) {
    this.lab_id = lab_id;
}
LabInvoices.prototype = {
    parseHash : function() {
        var hash = window.location.hash.substr(1);
        if (hash == '' || parseInt(hash) == NaN) {
            this.showAll();
        } else {
            this.showInvoice(hash);
        }
    },
    showAll : function() {
        $('.left-bar li').removeClass('selected');
        $('.left-bar li:first-child').addClass('selected');

        window.location.hash = 'all';
        $.get('/my/'+this.lab_id+'/payments/invoices/all', function(res) {
            $('.left-bar-content').html(res);
        });
    },
    showInvoice : function(inv_id) {
        $('.left-bar li').removeClass('selected');
        $('#invoice-'+inv_id).addClass('selected');

        window.location.hash = inv_id;
        $.get('/my/'+this.lab_id+'/payments/invoices/'+inv_id, function(res) {
            $('.left-bar-content').html(res);
        });
    }
}