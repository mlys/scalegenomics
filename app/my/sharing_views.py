import web
from sqlalchemy import not_

from app.core.decorators import as_json, as_html
from app.sso.models      import User, UserToLab
from app.my.models       import Lab, KVMServer, KVMStorage, Sharing, \
        KVMMountRelation
from app.my.decorators   import has_relations
from app.my.utils        import unmount_my_shared


def get_users_d(ids):
    ids = set(ids)
    if ids:
        users = (web.ctx.orm.query(User)
                .filter(User.id.in_(ids))
                .all())
    else:
        users = []
    result = {}
    for u in users:
        result[u.id] = u
    return result


def get_labs_d(ids):
    ids = set(ids)
    if ids:
        labs = (web.ctx.orm.query(Lab)
                .filter(Lab.id.in_(ids))
                .all())
    else:
        labs = []
    result = {}
    for l in labs:
        result[l.id] = l
    return result


def get_rels_d(storages_id):
    storages_id = set(storages_id)
    result = {}
    for storage_id in storages_id:
        rels = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMMountRelation.storage_id == storage_id)
                .filter(KVMServer.user_id == web.ctx.user.id)
                .filter(KVMMountRelation.status !=
                        KVMMountRelation.STATUS["UMOUNTING"])
                .filter(KVMMountRelation.status !=
                        KVMMountRelation.STATUS["UMOUNTED"])
                .all())
        result[storage_id] = rels
    return result


def sh_mount_counter(shs):
    result = {}
    for storage, sh in shs:
        rels = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMMountRelation.storage_id == storage.id)
                .filter(KVMMountRelation.status.in_([
                    KVMMountRelation.STATUS["MOUNTING"],
                    KVMMountRelation.STATUS["MOUNTED"]]))
                .all())
        mounts = []
        for server, rel in rels:
            if server.user_id != sh.user_id:  # mounted by another users
                if sh.mounted_to(server):
                    mounts.append((server, rel))
        result[storage.id] = len(mounts)
    return result


def unit_mount_counter(storage, shs):
    result = {}
    rels = (web.ctx.orm.query(KVMServer, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == storage.id)
            .filter(KVMMountRelation.status.in_([
                KVMMountRelation.STATUS["MOUNTING"],
                KVMMountRelation.STATUS["MOUNTED"]]))
            .all())
    for sh in shs:
        mounts = 0
        for server, rel in rels:
            if sh.mounted_to(server):
                mounts += 1
        result[sh.id] = mounts
    return result


def get_users_and_labs(shs):
    "Sort by Sharing type."
    users_id = []
    labs_id = []
    for stor, sh in shs:
        if sh.sharing_type == Sharing.TYPES["user"]:
            users_id.append(sh.sharing_with_id)
        elif sh.sharing_type == Sharing.TYPES["lab"]:
            labs_id.append(sh.sharing_with_id)
    return get_users_d(users_id), get_labs_d(labs_id)


def get_my_shs(user_id, lab_id):
    # result = [storage, {"users": [], "labs": []}]
    shs = (web.ctx.orm.query(KVMStorage, Sharing)
            .filter(KVMStorage.id == Sharing.storage_id)
            .filter(KVMStorage.user_id == user_id)
            .filter(KVMStorage.lab_id == lab_id)
            .all())
    users, labs = get_users_and_labs(shs)
    count = sh_mount_counter(shs)

    result = {}
    for stor, sh, in shs:
        if stor.id not in result:
            result[stor.id] = [stor, {"users":[], "labs":[]}]

        if sh.sharing_type == Sharing.TYPES["user"]:
            data = (users[sh.sharing_with_id], count[stor.id], sh)
            result[stor.id][1]["users"].append(data)

        elif sh.sharing_type == Sharing.TYPES["lab"]:
            data = (labs[sh.sharing_with_id], count[stor.id], sh)
            result[stor.id][1]["labs"].append(data)
    return result.values()


def get_shs_with_me(user_id, lab_id):
    # with me, like a user
    u_shs = (web.ctx.orm.query(KVMStorage, Sharing)
                .filter(Sharing.storage_id == KVMStorage.id)
                .filter(Sharing.sharing_type == Sharing.TYPES["user"])
                .filter(Sharing.sharing_with_id == user_id)
                .all())
    # with me, like this lab member
    l_shs = (web.ctx.orm.query(KVMStorage, Sharing)
                .filter(Sharing.storage_id == KVMStorage.id)
                .filter(Sharing.sharing_type == Sharing.TYPES["lab"])
                .filter(Sharing.sharing_with_id == lab_id)
                .all())
    users_id = []
    stors_id = []
    for _, sh in u_shs:
        users_id.append(sh.user_id)
        stors_id.append(sh.storage_id)
    for _, sh in l_shs:
        users_id.append(sh.user_id)
        stors_id.append(sh.storage_id)
    users = get_users_d(users_id)
    rels = get_rels_d(stors_id)
    result = {}
    result["users"] = []
    result["labs"]  = []
    for stor, sh in u_shs:
        if sh.user_id in users:
            r = rels.get(stor.id, [])
            result["users"].append([users[sh.user_id], stor, sh, r])
    for stor, sh in l_shs:
        if sh.user_id in users:
            r = rels.get(stor.id, [])
            result["labs"].append([users[sh.user_id], stor, sh, r])
    return result


class MembersForSharing(object):

    def get_users(self, lab_id, storage_id, shs, counter):
        "Return this lab members."
        users = (web.ctx.orm.query(User)
                    .filter(UserToLab.user_id == User.id)
                    .filter(UserToLab.lab_id == lab_id)
                    .filter(UserToLab.perms.in_([
                        UserToLab.PERMS["owner"],
                        UserToLab.PERMS["member"]]))
                    .filter(User.id != web.ctx.user.id)
                    .all())
        shared_with = []
        user_sh = {}
        for sh in shs:
            for user in users:
                if (sh.is_type("user") and
                    sh.sharing_with_id == user.id):
                        shared_with.append(sh.sharing_with_id)
                        user_sh[user.id] = sh
                        break
        shared = []
        not_shared = []
        for user in users:
            sh = user_sh.get(user.id, None)
            count = counter[sh.id] if sh else None
            data = {"unit_id":        user.id,
                    "owner_url":      user.url,
                    "owner_gravatar": user.gravatar,
                    "owner_name":     user.full_name,
                    "unit_url":       user.url,
                    "unit_name":      user.full_name,
                    "count":          count}
            if user.id in shared_with:
                shared.append(data)
            else:
                not_shared.append(data)
        return shared, not_shared

    def get_labs(self, lab_id, storage_id, shs, counter):
        labs = (web.ctx.orm.query(Lab, User)
                .filter(UserToLab.lab_id == Lab.id)
                .filter(UserToLab.user_id == User.id)
                .filter(UserToLab.perms == UserToLab.PERMS["owner"])
                .filter(Lab.status == Lab.STATUS["active"])
                .filter(Lab.id != lab_id)
                .all())
        shared_with = []
        lab_sh = {}
        for sh in shs:
            for lab, _ in labs:
                if (sh.is_type("lab") and
                    sh.sharing_with_id == lab.id):
                    shared_with.append(sh.sharing_with_id)
                    lab_sh[lab.id] = sh
                    break

        shared = []
        not_shared = []
        for lab, user in labs:
            sh = lab_sh.get(lab.id, None)
            count = counter[sh.id] if sh else None
            data = {"unit_id":        lab.id,
                    "owner_url":      user.url,
                    "owner_gravatar": user.gravatar,
                    "owner_name":     user.full_name,
                    "unit_url":       lab.url,
                    "unit_name":      lab.name,
                    "count":          count}
            if lab.id in shared_with:
                shared.append(data)
            else:
                not_shared.append(data)
        return shared, not_shared

    @has_relations("owner", "member")
    @as_json
    def GET(self, lab_id, storage_id, **kw):
        "Show users or labs avaible for sharing."
        params = web.input()
        section = params.get("section", "members")
        if section not in ["members", "labs"]:
            section = "members"

        storage = (web.ctx.orm.query(KVMStorage)
                    .filter_by(id = storage_id)
                    .first())
        if not storage:
            return {"result":  "error",
                    "message": "storage"}

        shs = (web.ctx.orm.query(Sharing)
                .filter_by(storage_id = storage.id)
                .all())
        counter = unit_mount_counter(storage, shs)
        u_shared, u_not_shared = self.get_users(lab_id,
                                                storage_id,
                                                shs,
                                                counter)
        l_shared, l_not_shared = self.get_labs(lab_id,
                                               storage_id,
                                               shs,
                                               counter)

        response = {"result": "ok"}
        if section == "members":
            response["shared"] = u_shared
            response["not_shared"] = u_not_shared
            response["add_limit"] = len(l_shared)
        elif section == "labs":
            response["shared"] = l_shared
            response["not_shared"] = l_not_shared
            response["add_limit"] = len(u_shared)
        return response

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, storage_id, **kw):
        "Share disk."
        params = web.input()
        if not ("units"     in params,
                "unit_type" in params):
            return {"result": "error"}
        if params["unit_type"] not in ["members", "labs"]:
            return {"result":  "error",
                    "message": "wrong type"}

        storage = (web.ctx.orm.query(KVMStorage)
                .filter(KVMStorage.id == storage_id)
                .filter(KVMStorage.lab_id == lab_id)
                .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
                .filter(KVMStorage.status.in_([
                    KVMStorage.STATUS["CREATING"],
                    KVMStorage.STATUS["CREATED"]]))
                .first())
        if not storage:
            return {"result":  "error",
                    "message": "wrong storage"}

        type = "user" if params["unit_type"] == "members" else "lab"

        units = params["units"].split(",")
        units = [int(u) for u in units if u.isdigit()]

        # remove unselected
        shs = (web.ctx.orm.query(Sharing)
                .filter_by(storage_id = storage_id)
                .filter_by(is_deleting = False)
                .filter_by(sharing_type = Sharing.TYPES[type])
                .all())
        for sh in shs:
            if sh.sharing_with_id not in units:
                rels = unmount_my_shared(type, storage_id, lab_id)
                sh.is_deleting = rels
                if not rels:
                    web.ctx.orm.delete(sh)

        shs = (web.ctx.orm.query(Sharing)
                .filter_by(storage_id = storage_id)
                .filter_by(is_deleting = False)
                .filter_by(sharing_type = Sharing.TYPES[type])
                .filter_by(sharing_type = type)
                .all())
        already_shared = []
        for sh in shs:
            already_shared.append(sh.sharing_with_id)

        for unit_id in units:
            if unit_id in already_shared:
                continue
            if len(already_shared) >= Sharing.LIMIT:
                break
            if params["unit_type"] == "members":
                unit = (web.ctx.orm.query(User)
                        .filter_by(id=unit_id)
                        .filter_by(is_active=True)
                        .first())
            elif params["unit_type"] == "labs":
                unit = (web.ctx.orm.query(Lab)
                        .filter_by(id=unit_id)
                        .filter_by(status=Lab.STATUS["active"])
                        .first())
            if not unit:
                continue
            sharing = Sharing(
                        user_id         = web.ctx.user.id,
                        storage_id      = storage.id,
                        sharing_type    = Sharing.TYPES[type],
                        sharing_with_id = unit.id)
            web.ctx.orm.add(sharing)
            already_shared.append(sharing.sharing_with_id)
        return {"result": "ok"}


class ShowStorageSharings:

    @has_relations("owner", "member")
    @as_html("my/_show_storage_sharings.html")
    def GET(self, lab_id, storage_id, **kw):
        storage = (web.ctx.orm.query(KVMStorage)
                    .filter_by(id=storage_id)
                    .filter_by(lab_id=lab_id)
                    .first())
        if not storage:
            raise web.notfound()
        sharings = (web.ctx.orm.query(Sharing)
                    .filter_by(storage_id=storage.id)
                    .all())
        shs = []
        for sh in sharings:
            if sh.sharing_type == Sharing.TYPES["user"]:
                usr = (web.ctx.orm.query(User)
                        .filter_by(id=sh.sharing_with_id)
                        .first())
                if usr:
                    shs.append((usr.full_name, usr.url, sh))
            elif sh.sharing_type == Sharing.TYPES["lab"]:
                if kw["lab"].id == sh.sharing_with_id:
                    shs.append((kw["lab"].name, kw["lab"].url, sh))
                else:
                    lab = (web.ctx.orm.query(Lab)
                            .filter_by(id=sh.sharing_with_id)
                            .first())
                    if lab:
                        shs.append((lab.name, lab.url, sh))
        return {"sharings": shs,
                "storage":  storage}


class ShowLabSharing:

    @has_relations("owner", "member")
    @as_html("my/show_sharing.html")
    def GET(self, lab_id, **kw):
        return {"cpage":       "mylabs",
                "labpage":     "sharings",
                "lab":         kw["lab"],
                "u2lab":       kw["u2l"],
                "c_plan":      kw["c_plan"]}


class ShowNFSStorages:

    @has_relations("owner", "member")
    @as_html("my/_show_nfs_storages.html")
    def GET(self, lab_id, **kw):
        storages = (web.ctx.orm.query(KVMStorage)
                    .filter(KVMStorage.user_id == web.ctx.user.id)
                    .filter(KVMStorage.lab_id == lab_id)
                    .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
                    .filter(KVMStorage.status == KVMStorage.STATUS["CREATED"])
                    .order_by(KVMStorage.name)
                    .all())
        # TODO: optimize
        data = get_my_shs(web.ctx.user.id, lab_id)
        shared = []
        not_shared = []
        for storage in storages:
            is_added = False
            for stor, shs in data:
                if stor.id == storage.id:
                    shared.append((storage, shs))
                    is_added = True
                    break
            if not is_added:
                not_shared.append((storage, []))
        return {"storages": shared + not_shared}


class ShowSharingWithMe:

    @has_relations("owner", "member")
    @as_html("my/_show_sharing_with_me.html")
    def GET(self, lab_id, **kw):
        shs_with_me = get_shs_with_me(web.ctx.user.id, lab_id)
        return {"shs_with_me": shs_with_me}


class DeleteSharing:

    def umount_user(self, storage_id, user_id):
        rels = (web.ctx.orm.query(KVMServer, KVMStorage, KVMMountRelation)
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMMountRelation.storage_id == KVMStorage.id)
                .filter(KVMStorage.id == storage_id)
                .filter(KVMServer.user_id == user_id)
                .filter(not_(KVMMountRelation.status.in_([
                    KVMMountRelation.STATUS["UMOUNTING"],
                    KVMMountRelation.STATUS["UMOUNTED"]])))
                .all())
        if rels:
            self.change_status(rels)
            return True
        return False

    def umount_lab(self, storage_id, lab_id):
        rels = (web.ctx.orm.query(KVMServer, KVMStorage, KVMMountRelation)
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMMountRelation.storage_id == KVMStorage.id)
                .filter(KVMStorage.id == storage_id)
                .filter(KVMServer.lab_id == lab_id)
                .filter(not_(KVMMountRelation.status.in_([
                    KVMMountRelation.STATUS["UMOUNTING"],
                    KVMMountRelation.STATUS["UMOUNTED"]])))
                .all())
        if rels:
            self.change_status(rels)
            return True
        return False

    def change_status(self, rels):
        for _, _, rel in rels:
            rel.status = KVMMountRelation.STATUS["UMOUNTING"]

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, storage_id, **kw):
        params = web.input()
        if "sh_id" not in params:
            return {"result":  "error",
                    "message": "wrong request"}

        sh = (web.ctx.orm.query(Sharing)
                .filter_by(storage_id = storage_id)
                .filter_by(id = params["sh_id"])
                .filter_by(is_deleting = False)
                .first())
        if not sh:
            return {"result":  "error",
                    "message": "unknown sharing"}

        # remove my sharing
        if sh.user_id == web.ctx.user.id:
            if sh.sharing_type == Sharing.TYPES["user"]:
                res = self.umount_user(sh.storage_id, sh.sharing_with_id)
                sh.is_deleting = res
                if not res:
                    web.ctx.orm.delete(sh)
            elif sh.sharing_type == Sharing.TYPES["lab"]:
                res = self.umount_lab(sh.storage_id, sh.sharing_with_id)
                if not res:
                    web.ctx.orm.delete(sh)
            return {"result":   "ok",
                    "deleting": res}

        # shared with me
        if (sh.sharing_type == Sharing.TYPES["user"] and
            sh.sharing_with_id == web.ctx.user.id):
            res = self.umount_user(sh.storage_id, sh.sharing_with_id)
            sh.is_deleting = res
            if not res:
                web.ctx.orm.delete(sh)
            return {"result":   "ok",
                    "deleting": res}

        # shared with my lab
        if (sh.sharing_type == Sharing.TYPES["lab"] and
            sh.sharing_with_id == kw["lab"].id):
            res = self.umount_lab(sh.storage_id, sh.sharing_with_id)
            sh.is_deleting = res
            if not res:
                web.ctx.orm.delete(sh)
            return {"result":   "ok",
                    "deleting": res}

        return {"result": "error"}


class ServersForSharing:

    @has_relations("owner", "member")
    @as_html("my/_servers_for_sharing.html")
    def GET(self, lab_id, **kw):
        "Mount dialog. Show server for sharing."
        params = web.input()
        if "sh_id" not in params:
            return {}
        sh = (web.ctx.orm.query(Sharing)
                .filter_by(id = params["sh_id"])
                .filter_by(is_deleting = False)
                .first())
        if not sh:
            return {}

        rels = []
        query = (web.ctx.orm.query(KVMServer, KVMStorage, KVMMountRelation)
                    .filter(KVMMountRelation.server_id == KVMServer.id)
                    .filter(KVMMountRelation.storage_id == KVMStorage.id)
                    .filter(KVMMountRelation.status !=
                            KVMMountRelation.STATUS["UMOUNTING"])
                    .filter(KVMMountRelation.status !=
                            KVMMountRelation.STATUS["UMOUNTED"])
                    .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
                    .filter(KVMStorage.id == sh.storage_id))
        if sh.sharing_type == Sharing.TYPES["user"]:
            rels = query.filter(KVMServer.user_id == sh.sharing_with_id).all()
        elif sh.sharing_type == Sharing.TYPES["lab"]:
            rels = query.filter(KVMServer.lab_id == sh.sharing_with_id).all()
        if len(rels) >= Sharing.MOUNT_LIMIT:
            return {"limit": True}

        servs_id = [rel.server_id for _, _, rel in rels]
        if servs_id:
            servers = (web.ctx.orm.query(KVMServer)
                      .filter(KVMServer.user_id == web.ctx.user.id)
                      .filter(KVMServer.lab_id == lab_id)
                      .filter(not_(KVMServer.id.in_(servs_id)))
                      .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                      .all())
        else:
            servers = (web.ctx.orm.query(KVMServer)
                      .filter(KVMServer.user_id == web.ctx.user.id)
                      .filter(KVMServer.lab_id == lab_id)
                      .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                      .all())
        return {"servers": servers}


class MountSharedStorage:

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, **kw):
        # TODO: mount limit (see: ServersForSharing)
        params = web.input()
        if not ("server_id"  in params and
                "storage_id" in params and
                "sh_id"      in params):
            return {"result": "error"}

        server = (web.ctx.orm.query(KVMServer)
                    .filter(KVMServer.id == params["server_id"])
                    .filter(KVMServer.user_id == web.ctx.user.id)
                    .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                    .first())
        if not server:
            return {"result":  "error",
                    "message": "unknown server"}

        q = (web.ctx.orm.query(KVMStorage, Sharing)
                .filter(KVMStorage.id == Sharing.storage_id)
                .filter(KVMStorage.id == params["storage_id"])
                .filter(Sharing.id == params["sh_id"])
                .first())
        if not q:
            return {"result":  "error",
                    "message": "unknown storage and sharing"}
        storage, sh = q

        check_rel = (web.ctx.orm.query(KVMMountRelation)
                        .filter_by(server_id = server.id)
                        .filter_by(storage_id = storage.id)
                        .first())
        if check_rel:
            check_rel.status = KVMMountRelation.STATUS["MOUNTING"]
        else:
            rel = KVMMountRelation(
                storage_id = storage.id,
                server_id  = server.id,
                rights     = KVMMountRelation.RIGHTS["RO"])
            web.ctx.orm.add(rel)

        return {"result": "ok"}


class UmountSharedStorage:

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, **kw):
        params = web.input()
        if not ("rel_id" in params):
            return {"result": "error"}

        q = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMMountRelation.id == params["rel_id"])
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMServer.user_id == web.ctx.user.id)
                .first())
        if not q:
            return {"result":  "error",
                    "message": "wrong data"}
        server, rel = q
        rel.status = KVMMountRelation.STATUS["UMOUNTING"]
        return {"result": "ok"}
