import web
from sqlalchemy import not_


from app.core.decorators import as_html, as_json
from app.my.models       import KVMServer, KVMStorage, KVMMountRelation, \
        LabProject, MyEvent, Sharing
from app.my.decorators   import has_relations


def is_shared(storage_id, lab_id):
    # TODO: storage status(not removed)
    stor_sh = (web.ctx.orm.query(KVMStorage, Sharing)
                .filter(KVMStorage.id == Sharing.storage_id)
                .filter(KVMStorage.id == storage_id)
                .filter(Sharing.sharing_type == Sharing.TYPES["lab"])
                .filter(Sharing.sharing_with_id == lab_id)
                .first())
    if stor_sh:
        # this storage shared with this lab
        stor, sh = stor_sh
        # lets check for limit
        # TODO: server status
        rels = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMMountRelation.status !=
                        KVMMountRelation.STATUS["UMOUNTING"])
                .filter(KVMMountRelation.status !=
                        KVMMountRelation.STATUS["UMOUNTED"])
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMMountRelation.storage_id == storage_id)
                .filter(KVMServer.lab_id == lab_id)  # <-
                .all())
        if len(rels) < Sharing.LIMIT:
            return True
        else:
            return False

    stor_sh = (web.ctx.orm.query(KVMStorage, Sharing)
            .filter(KVMStorage.id == Sharing.storage_id)
            .filter(KVMStorage.id == storage_id)
            .filter(Sharing.sharing_type == Sharing.TYPES["user"])
            .filter(Sharing.sharing_with_id == web.ctx.user.id)
            .first())
    if stor_sh:
        stor, sh = stor_sh
        rels = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMMountRelation.status !=
                        KVMMountRelation.STATUS["UMOUNTING"])
                .filter(KVMMountRelation.status !=
                        KVMMountRelation.STATUS["UMOUNTED"])
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMMountRelation.storage_id == storage_id)
                .filter(KVMServer.user_id == web.ctx.user.id)  # <-
                .all())
        if len(rels) < Sharing.LIMIT:
            return True
        else:
            return False


class StoragesForMount:

    def not_mounted(self, storages, rels, cserver_id=None):
        # filter
        m_ids = [r.storage_id for r in rels]
        not_mounted = []
        not_mounted_oth = []
        if cserver_id:
            for storage in storages:
                if storage.id not in m_ids:
                    if storage.server_id == cserver_id:
                        not_mounted.append(storage)
                    else:
                        not_mounted_oth.append(storage)
            return not_mounted, not_mounted_oth
        else:
            for storage in storages:
                if storage.id not in m_ids:
                    not_mounted.append(storage)
            return not_mounted

    def my_local_storages(self, cserver_id, project_id):
        # Conditions
        # + my local storages
        # + on the same cserver
        # + not mounted
        storages = (web.ctx.orm.query(KVMStorage)
                .filter(KVMStorage.user_id == web.ctx.user.id)
                .filter(KVMStorage.project_id == project_id)
                .filter(KVMStorage.type == KVMStorage.TYPES["LOCAL"])
                .filter(KVMStorage.status == KVMStorage.STATUS["CREATED"])
                .all())
        s_ids = [s.id for s in storages]
        if s_ids:
            m_rels = (web.ctx.orm.query(KVMMountRelation)
                    .filter(KVMMountRelation.storage_id.in_(s_ids))
                    .filter(KVMMountRelation.status.in_([
                        KVMMountRelation.STATUS["MOUNTING"],
                        KVMMountRelation.STATUS["MOUNTED"],
                        ]))
                    .all())
            return self.not_mounted(storages, m_rels, cserver_id)
        else:
            return [], []

    def my_nfs_storages(self, server_id):
        # Conditions
        # + my nfs storages
        # + not mounted
        storages = (web.ctx.orm.query(KVMStorage)
                .filter(KVMStorage.user_id == web.ctx.user.id)
                .filter(KVMStorage.type == KVMStorage.TYPES['NFS'])
                .filter(KVMStorage.status == KVMStorage.STATUS['CREATED'])
                .all())
        s_ids = [s.id for s in storages]
        if s_ids:
            m_rels = (web.ctx.orm.query(KVMMountRelation)
                    .filter(KVMMountRelation.storage_id.in_(s_ids))
                    .filter(KVMMountRelation.server_id == server_id)
                    .filter(KVMMountRelation.status.in_([
                        KVMMountRelation.STATUS['MOUNTING'],
                        KVMMountRelation.STATUS['MOUNTED'],
                        ]))
                    .all())
            return self.not_mounted(storages, m_rels)
        else:
            return storages

    def shared_storages(self, lab_id, server_id):
        # + not mounted
        # + limit ?
        # TODO: mount limit
        user_shs = (web.ctx.orm.query(KVMStorage, Sharing)
                    .filter(Sharing.storage_id == KVMStorage.id)
                    .filter(Sharing.sharing_type == Sharing.TYPES["user"])
                    .filter(Sharing.sharing_with_id == web.ctx.user.id)
                    .filter(Sharing.is_deleting == False)
                    .all())
        lab_shs = (web.ctx.orm.query(KVMStorage, Sharing)
                    .filter(Sharing.storage_id == KVMStorage.id)
                    .filter(Sharing.sharing_type == Sharing.TYPES["lab"])
                    .filter(Sharing.sharing_with_id == lab_id)
                    .filter(Sharing.is_deleting == False)
                    .all())
        sh_storages = []
        for stor, sh in user_shs + lab_shs:
            rel = (web.ctx.orm.query(KVMMountRelation)
                    .filter_by(storage_id = stor.id)
                    .filter_by(server_id = server_id)
                    .filter(KVMMountRelation.status.in_([
                        KVMMountRelation.STATUS["MOUNTING"],
                        KVMMountRelation.STATUS["MOUNTED"]]))
                    .first())
            if not rel:
                sh_storages.append(stor)
        return sh_storages

    @has_relations("owner", "member")
    @as_html("my/_storages_for_mount.html")
    def GET(self, lab_id, server_id, **kw):
        "Mount dialog. Return list of storages avaible to mount."
        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab_id)
                  .first())
        if not server:
            return {}

        if server.is_status("STOPPED"):
            my_storages, my_oth = self.my_local_storages(server.server_id,
                                                         server.project_id)
        else:
            my_storages = []
            my_oth = []
        nfs_storages = self.my_nfs_storages(server.id)
        shs_storages = self.shared_storages(lab_id, server.id)

        return {"lab":          kw["lab"],
                "server":       server,
                "my_storages":  my_storages,
                "oth_storages": my_oth,
                "nfs_storages": nfs_storages,
                "shs_storages": shs_storages}


class ShowServerMountList:

    @has_relations("owner", "member")
    @as_html("my/_show_server_mount_list.html")
    def POST(self, lab_id, server_id, **kw):
        "Return storages mounted to server."
        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab_id)
                  .first())
        if not server:
            return {}

        rels = (web.ctx.orm.query(KVMStorage, KVMMountRelation)
                .filter(KVMStorage.id == KVMMountRelation.storage_id)
                .filter(KVMMountRelation.server_id == server.id)
                .filter(not_(KVMMountRelation.status.in_([
                    KVMMountRelation.STATUS["UMOUNTING"],
                    KVMMountRelation.STATUS["UMOUNTED"]])))
                .order_by(KVMStorage.type, KVMStorage.name)
                .all())
        my_rels = []
        sh_rels = []
        for stor, rel in rels:
            if stor.user_id == server.user_id:
                enable_umount = False
                if stor.type == KVMStorage.TYPES["NFS"]:
                    enable_umount = True
                elif server.status == KVMServer.STATUS["STOPPED"]:
                    enable_umount = True
                my_rels.append((stor, rel, enable_umount))
            else:
                sh_rels.append((stor, rel))
        server_owner = True if server.user_id == web.ctx.user.id else False
        return {"server":   server,
                "my_rels":  my_rels,
                "sh_rels":  sh_rels,
                "is_owner": server_owner}


class MountToServer:

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, server_id, **kw):
        params = web.input()
        if not ("my_local" in params and
                "my_nfs"   in params and
                "shared"   in params):
            return {"result": "error",
                    "mesage": "you shall not pass"}

        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab_id)
                  .filter_by(user_id = web.ctx.user.id)
                  .first())
        if not server:
            return {"result":  "error",
                    "message": "Wrong server"}

        project = (web.ctx.orm.query(LabProject)
                   .filter_by(id = server.project_id)
                   .first())

        rels = (web.ctx.orm.query(KVMMountRelation, KVMStorage)
                .filter(KVMMountRelation.server_id == server.id)
                .filter(KVMMountRelation.storage_id == KVMStorage.id)
                .all())

        conn_ids, not_conn_ids = self.sort_storages_ids(rels)
        local_ids, nfs_ids, shared_ids = self.get_storages_ids(params)
        # filter already mounted
        local_ids  = [i for i in local_ids if i not in conn_ids]
        nfs_ids    = [i for i in nfs_ids if i not in conn_ids]
        shared_ids = [i for i in shared_ids if i not in conn_ids]

        if server.status != KVMServer.STATUS["STOPPED"]:
            local_ids = []

        for storage_id in local_ids + nfs_ids:
            storage = (web.ctx.orm.query(KVMStorage)
                        .filter_by(id = storage_id)
                        .filter_by(user_id = web.ctx.user.id)
                        .filter_by(project_id = project.id)
                        .first())
            if not storage:
                continue

            grel = None
            # update record if exists or create new
            if storage_id in not_conn_ids:
                for rel, _ in rels:
                    if rel.storage_id == storage_id:
                        rel.status = KVMMountRelation.STATUS["MOUNTING"]
                        not_conn_ids.remove(storage_id)
                        conn_ids.append(storage_id)
                        grel = rel
                        break
            else:
                grel = KVMMountRelation(
                    storage_id = storage.id,
                    server_id  = server.id,
                    status     = KVMMountRelation.STATUS["MOUNTING"],
                    rights     = KVMMountRelation.RIGHTS["RW"])
                web.ctx.orm.add(grel)
                conn_ids.append(storage_id)

            my_event = MyEvent(
                user_id      = web.ctx.user.id,
                full_name    = web.ctx.user.full_name,
                lab_id       = kw["lab"].id,
                lab_name     = kw["lab"].name,
                project_id   = project.id,
                project_name = project.name,
                event_type          = MyEvent.TYPES["mount storage"],
                event_object_type   = MyEvent.OBJ_TYPES["storage"],
                event_object_id     = storage.id,
                event_object_name   = storage.name,
                event_object_2_type = MyEvent.OBJ_TYPES["server"],
                event_object_2_id   = server.id,
                event_object_2_name = server.name)
            web.ctx.orm.add(my_event)

        for storage_id in shared_ids:
            # + storage is shared with user or lab
            # + limit is not reached
            sh = is_shared(storage_id, kw["lab"].id)
            if sh:
                # TODO: check server, storage, rel status
                if storage_id in not_conn_ids:
                    for rel, sto in rels:
                        if rel.storage_id == storage_id:
                            rel.status = KVMMountRelation.STATUS["MOUNTING"]
                            not_conn_ids.remove(storage_id)
                            conn_ids.append(storage_id)
                            # TODO: add event
                            break
                else:
                    rel = KVMMountRelation(
                        storage_id = storage_id,
                        server_id  = server.id,
                        status     = KVMMountRelation.STATUS["MOUNTING"],
                        rights     = KVMMountRelation.RIGHTS["RO"])
                    web.ctx.orm.add(rel)
                    conn_ids.append(storage_id)
                    # TODO: add event
                    continue

        return {'result': 'ok'}

    def get_storages_ids(self, params):
        'Get data from request.'
        # Sorry.
        try:
            my_local_ids = set([int(p) for p in params['my_local'].split(',')])
        except:
            my_local_ids = []
        try:
            my_nfs_ids = set([int(p) for p in params['my_nfs'].split(',')])
        except:
            my_nfs_ids = []
        try:
            shared_ids = set([int(p) for p in params['shared'].split(',')])
        except:
            shared_ids = []
        return my_local_ids, my_nfs_ids, shared_ids

    def sort_storages_ids(self, rels):
        conn_ids = []
        not_conn_ids = []
        for rel, _ in rels:
            if (rel.status == KVMMountRelation.STATUS["MOUNTING"] or
                rel.status == KVMMountRelation.STATUS["MOUNTED"]):
                conn_ids.append(rel.storage_id)
            else:
                not_conn_ids.append(rel.storage_id)
        return conn_ids, not_conn_ids


class UmountStorage:

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, server_id, **kw):

        params = web.input()
        if "storage_id" not in params:
            return {}

        server = (web.ctx.orm.query(KVMServer)
                    .filter_by(id = server_id)
                    .first())
        if not server:
            return {"result":  "error",
                    "message": "unknown server"}

        if web.ctx.user.id != server.user_id:
            return {"result":  "error",
                    "message": "who you are?"}

        q = (web.ctx.orm.query(KVMStorage, KVMMountRelation)
                .filter(KVMMountRelation.storage_id == KVMStorage.id)
                .filter(KVMMountRelation.storage_id == params["storage_id"])
                .filter(KVMMountRelation.server_id == server.id)
                .first())
        if not q:
            return {"result":  "ok",
                    "message": "unknown relation"}

        stor, rel = q
        if stor.type == KVMStorage.TYPES["LOCAL"]:
            if server.status == KVMServer.STATUS["STOPPED"]:
                rel.status = KVMMountRelation.STATUS["UMOUNTED"]
                rel.real_status = KVMMountRelation.REAL_STATUS["UMOUNTED"]
                rel.point = ""
        elif stor.type == KVMStorage.TYPES["NFS"]:
            rel.status = KVMMountRelation.STATUS["UMOUNTING"]
        return {"result": "ok"}


class ShowStorageNFSMounts:

    @has_relations("owner", "member")
    @as_html("my/_show_storage_nfs_mounts.html")
    def GET(self, lab_id, storage_id, **kw):
        storage = (web.ctx.orm.query(KVMStorage)
                    .filter_by(id = storage_id)
                    .filter_by(status = KVMStorage.STATUS["CREATED"])
                    .first())
        if not storage:
            return {}
        rels = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMMountRelation.storage_id == storage.id)
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMServer.user_id == storage.user_id)
                .filter(KVMMountRelation.status.in_([
                    KVMMountRelation.STATUS["MOUNTING"],
                    KVMMountRelation.STATUS["MOUNTED"]]))
                .all())
        return {"storage": storage,
                "rels":    rels}
