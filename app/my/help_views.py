import web

from app.core.decorators import as_html
from app.sso.models import UserCert


class HelpMe:

    @as_html("my/help_me.html")
    def GET(self):
        return {"cpage": "help"}


class ShowMainHelp:

    @as_html("my/_show_main_help.html")
    def GET(self):
        return {}


class HelpMeWithVPN:

    @as_html("my/help_me_with_vpn.html")
    def GET(self):
        "TODO: remove or not"
        return {}


class HelpMeWithOpenVPN:

    @as_html("my/_help_me_with_openvpn.html")
    def GET(self):
        ucert = (web.ctx.orm.query(UserCert)
                 .filter_by(user_id = web.ctx.user.id)
                 .first())
        return {"ucert": ucert}


class ShowDisksHelp:

    @as_html("my/_show_disks_help.html")
    def GET(self):
        return {}


class ShowSharingHelp:

    @as_html("my/_show_sharing_help.html")
    def GET(self):
        return {}
