import random
from datetime import datetime
from collections import OrderedDict

import web
from sqlalchemy import Column, Integer, SmallInteger, String, DateTime, \
    Text, Boolean, DECIMAL, Float

from settings.db import Base, ModelExt


class Lab(Base, ModelExt):
    __tablename__ = 'labs'

    CPU_LIMIT      = 35
    RAM_LIMIT      = 1024 * 16 * 3
    STORAGES_LIMIT = 1024 * 1024 * 3

    STATUS = {
        'not verified': 0,
        'verification': 1,
        'active':       2,
        'trial block':  3,
    }

    id = Column(Integer, primary_key=True)

    name            = Column(String(200), nullable=None)
    type            = Column(String(100), nullable=None)
    email           = Column(String(75),  nullable=None, default='')
    wait_to_approve = Column(Integer,     nullable=None, default=0)

    databases_info = Column(Text, nullable=None, default='')

    cpu_used      = Column(Integer, nullable=None, default=0)
    ram_used      = Column(Integer, nullable=None, default=0)
    storages_used = Column(Integer, nullable=None, default=0)

    cpu_limit      = Column(Integer, nullable=None)
    ram_limit      = Column(Integer, nullable=None)  # mb
    storages_limit = Column(Integer, nullable=None)  # mb

    created = Column(DateTime, nullable=None, default=datetime.utcnow)
    updated = Column(DateTime, nullable=None, default=datetime.utcnow,
                                              onupdate=datetime.utcnow)

    status = Column(SmallInteger, nullable=None,
                                  default=STATUS['not verified'])
    # TODO: remove this field
    is_active     = Column(Boolean, nullable=None, default=False)
    is_ds_visible = Column(Boolean, nullable=None, default=False)

    def __repr__(self):
        return '<Lab({}, {!r})>'.format(self.id, self.name)

    @property
    def status_str(self):
        for key, val in self.STATUS.items():
            if val == self.status:
                return key

    @property
    def url(self):
        return '/my/{}'.format(self.id)

    @property
    def abs_url(self):
        return web.ctx.site_url + self.url

    @property
    def info_url(self):
        # + create page with lab info
        return "#"

    @property
    def root_url(self):
        return '/root/labs/{}'.format(self.id)

    @property
    def root_abs_url(self):
        return web.ctx.site_url + self.root_url

    @property
    def cpu_used_proc(self):
        return round(self.cpu_used * 100.0 / self.cpu_limit, 2)

    @property
    def ram_used_proc(self):
        return round(self.ram_used * 100.0 / self.ram_limit, 2)

    @property
    def stor_used_proc(self):
        return round(self.storages_used * 100.0 / self.storages_limit, 2)

    @property
    def cpu_free(self):
        if hasattr(self, '_cpu_free'):
            return self._cpu_free
        self._cpu_free = self.cpu_limit - self.cpu_used
        return self._cpu_free

    @property
    def ram_free(self):
        return self.ram_limit - self.ram_used

    @property
    def ram_used_gb(self):
        return self.ram_used / 1024.0

    @property
    def ram_used_gb_str(self):
        s = self.ram_used_gb
        if s == 0:
            return '0'
        if s == int(s):
            return str(int(s))
        return str(round(s, 2))

    @property
    def ram_limit_gb(self):
        return self.ram_limit / 1024

    @property
    def storages_free(self):
        return self.storages_limit - self.storages_used

    @property
    def storages_used_tb(self):
        return self.storages_used / 1024.0 / 1024

    @property
    def storages_used_tb_str(self):
        s = self.storages_used_tb
        if s == 0:
            return '0'
        if s == int(s):
            return str(int(s))
        return str(round(s, 2))

    @property
    def storages_free_gb(self):
        return self.storages_free / 1024

    @property
    def storages_limit_gb(self):
        return self.storages_limit / 1024

    @property
    def storages_limit_tb(self):
        return self.storages_limit / 1024 / 1024

    def check_server(self, server):
        if self.cpu_free < server.cpu or self.ram_free < server.memory:
            return False
        else:
            return True

    def is_status(self, status):
        return self.status == self.STATUS[status]


class Plan(Base, ModelExt):
    __tablename__ = 'plans'

    TRIAL_DAYS_LIMIT = 14

    PLANS = {
        'Trial':                0,
        'Beginner':             1,
        'Green':                2,
        'Blue':                 3,
        'Black diamond':        4,
        'Double-black diamond': 5,

        'Personal': 99,
    }

    id = Column(Integer, primary_key=True)

    name    = Column(SmallInteger, nullable=None)
    price   = Column(Integer, nullable=None)  # $
    cores   = Column(Integer, nullable=None)
    ram     = Column(Integer, nullable=None)  # GB
    storage = Column(Integer, nullable=None)  # TB

    @property
    def name_str(self):
        for key, val in self.PLANS.items():
            if val == self.name:
                return key

    @property
    def cores_str(self):
        return '{} {}'.format(self.cores, 'core' if self.cores == 1
                                                 else 'cores')

    @property
    def price_str(self):
        if self.price == 0:
            return 'Free'
        return '${}/month'.format(self.price)

    @property
    def ram_str(self):
        return '{} GB RAM'.format(self.ram)

    @property
    def storage_str(self):
        return '{} TB of storage'.format(self.storage)

    @property
    def ram_mb(self):
        return self.ram * 1024

    @property
    def storage_mb(self):
        return self.storage * 1024 * 1024


class LabPlan(Base, ModelExt):
    __tablename__ = 'lab_plans'

    TRIAL_DAYS_LIMIT = 14

    PLANS = {
        'Trial':                0,
        'Beginner':             1,
        'Green':                2,
        'Blue':                 3,
        'Black diamond':        4,
        'Double-black diamond': 5,
        'Personal': 99,
    }

    id = Column(Integer, primary_key=True)

    lab_id  = Column(Integer, nullable=None)
    plan_id = Column(Integer, nullable=None)

    name    = Column(SmallInteger, nullable=None)
    price   = Column(Integer, nullable=None)
    cores   = Column(Integer, nullable=None)
    ram     = Column(Integer, nullable=None)
    storage = Column(Integer, nullable=None)

    created = Column(DateTime, nullable=None, default=datetime.utcnow)
    deleted = Column(DateTime, default=None)

    def __repr__(self):
        return '<LabPlan({}, {!r})>'.format(self.id, self.name_str)

    @property
    def name_str(self):
        for key, val in self.PLANS.items():
            if val == self.name:
                return key

    @property
    def price_str(self):
        if self.price == 0:
            return 'Free'
        return '${}/month'.format(self.price)

    @property
    def cores_str(self):
        return '{} {}'.format(self.cores, 'core' if self.cores == 1
                                                 else 'cores')

    @property
    def ram_str(self):
        return '{} GB RAM'.format(self.ram)

    @property
    def storage_str(self):
        return '{} TB of storage'.format(self.storage)

    @property
    def url(self):
        return '/my/{}/payments/plans/{}'.format(self.lab_id, self.id)

    @property
    def days_remain(self):
        if hasattr(self, '_days_remain'):
            return self._days_remain
        if self.name != self.PLANS['Trial']:
            self._days_remain = 0
            return 0
        now = datetime.utcnow()
        dt = now - self.created
        dtt = self.TRIAL_DAYS_LIMIT - dt.days
        if dt.days > self.TRIAL_DAYS_LIMIT:
            self._days_remain = 0
            return 0
        else:
            self._days_remain = dtt
            return dtt

    def is_plan(self, plan):
        return self.name == self.PLANS[plan]


class KVMServer(Base, ModelExt):
    __tablename__ = 'kvm_servers'

    DEFAULT_SIZE = 25  # G
    CPULIMITS = OrderedDict([
        ('normal',     50),
        ('high',       70),
        ('super high', 90),
    ])

    NO_IP      = '0.0.0.0'
    NO_DISK    = -1
    NO_SERVER  = -1
    NO_PROJECT = -1

    PORT = 13013

    STATUS = {
        'CREATING':    0,
        'CONFIGURING': 1,

        'STARTING':   10,
        'RUNNING':    11,
        'STOPPING':   12,
        'STOPPED':    13,
        'RESTARTING': 14,

        'REMOVING': 31,
        'REMOVED':  32,

        'ERROR':         90,
        'NO FREE SPACE': 91,
        'NO FREE IP':    92,
    }

    IMAGES = OrderedDict([
        (1000, ("genomics",               "anna.ubuntu.maverick.amd64.qcow2")),
        (0,    ("Default - ubuntu 10.10", "ubuntu.maverick.amd64.qcow2")),
    ])

    id          = Column(Integer, primary_key=True)
    server_id   = Column(Integer, nullable=None, default=NO_SERVER)
    disk_id     = Column(Integer, nullable=None, default=NO_DISK)
    user_id     = Column(Integer, nullable=None)
    lab_id      = Column(Integer, nullable=None)
    project_id  = Column(Integer, nullable=None)

    name        = Column(String(100),  nullable=None)
    description = Column(Text,         nullable=None)
    status      = Column(SmallInteger, nullable=None)

    ip          = Column(String(15),   nullable=None, default=NO_IP)
    mac         = Column(String(17),   nullable=None, default='')
    cpu         = Column(SmallInteger, nullable=None)
    memory      = Column(Integer, nullable=None)  # M
    size        = Column(Integer, nullable=None, default=DEFAULT_SIZE)
    image       = Column(Integer, nullable=None, default=0)
    username    = Column(String(255), nullable=None, default='user')
    password    = Column(String(100), nullable=None)

    cpulimit = Column(Integer, nullable=None, default=CPULIMITS['normal'])

    is_prepared = Column(Boolean, nullable=None, default=False)

    created = Column(DateTime, nullable=None, default=datetime.utcnow)
    updated = Column(DateTime, nullable=None, default=datetime.utcnow,
                                              onupdate=datetime.utcnow)

    def __repr__(self):
        status = ""
        if self.status in [self.STATUS["REMOVING"],
                           self.STATUS["REMOVED"]]:
            status = ", {}".format(self.status_str)
        return "<KVMServer({}, {}{})>".format(self.id, self.name, status)

    @property
    def memory_gb(self):
        return str(self.memory * 1.0 / 1024) + ' GB'

    @property
    def size_gb(self):
        return str(self.size) + ' GB'

    @property
    def size_mb(self):
        return self.size * 1024

    @property
    def image_name(self):
        for key, val in self.IMAGES.items():
            if key == self.image:
                return val[0]
        return 'Unknown image'

    @property
    def image_file_name(self):
        for key, val in self.IMAGES.items():
            if key == self.image:
                return val[1]
        return 'Unknown image'

    @property
    def domain(self):
        return 'server{}'.format(self.id)

    @staticmethod
    def get_id(value):
        return value[6:]

    @property
    def url(self):
        return '/my/{}/servers/{}'.format(self.lab_id, self.id)

    @property
    def root_url(self):
        return '/root/servers/{}'.format(self.id)

    @property
    def a_tag(self):
        return '<a href="{}">{}</a>'.format(self.url, self.name)

    @property
    def project_onclick(self):
        u = ('<a href="#" onclick="ppane.showServer({});return false;">{}</a>'
                .format(self.id, self.name))
        return u

    @property
    def description_ptag(self):
        if not self.description:
            return '&nbsp;'
        else:
            return ''.join(['<p>' + line + '</p>'
                for line in self.description.split('\n')])

    @property
    def ip_str(self):
        if self.ip == self.NO_IP:
            return '&nbsp;'
        return self.ip

    @property
    def memory_str(self):
        mem = self.memory * 1.0 / 1024
        mem_int = int(mem)
        if mem != mem_int:
            return '{} GB'.format(mem)
        else:
            return '{} GB'.format(mem_int)

    @property
    def size_str(self):
        return '{} GB'.format(self.DEFAULT_SIZE)

    @property
    def status_str(self):
        for key, val in self.STATUS.items():
            if val == self.status:
                return key.capitalize()
        return ''

    @property
    def cpulimit_str(self):
        for key, value in self.CPULIMITS.items():
            if self.cpulimit == value:
                return key

    def get_access(self, **kw):
        if self.user_id == web.ctx.session['user_id']:
            server_owner = True
        else:
            server_owner = False

        if self.status >= self.STATUS['REMOVING']:
            block_pane = True
        else:
            block_pane = False

        if (self.status == self.STATUS['REMOVING'] or
            self.status == self.STATUS['REMOVED']):
            block_destroy = True
        else:
            block_destroy = False

        if kw['lab'].status == Lab.STATUS['trial block']:
            block_pane = True
            block_destroy = True

        return server_owner, block_pane, block_destroy

    def is_status(self, status):
        return self.status == self.STATUS[status]

    @classmethod
    def gen_mac(cls):
        mac_range = '0123456789ABCDEF'
        mac = ['52', '54',
                random.choice(mac_range) + random.choice(mac_range),
                random.choice(mac_range) + random.choice(mac_range),
                random.choice(mac_range) + random.choice(mac_range),
                random.choice(mac_range) + random.choice(mac_range)]
        return ':'.join(mac)


class KVMReservedImage(Base, ModelExt):
    __tablename__ = 'kvm_reserved_images'

    id       = Column(Integer, primary_key=True)
    image_id = Column(Integer, nullable=None)
    disk_id  = Column(Integer, nullable=None)


class Ip(Base, ModelExt):
    __tablename__ = 'ips'

    id        = Column(Integer, primary_key=True)
    is_used   = Column(Boolean, default=False, nullable=None)
    value     = Column(String(15), nullable=None)
    last_used = Column(DateTime,   nullable=None, default  = datetime.utcnow,
                                                  onupdate = datetime.utcnow)


class KVMServerUsageLog(Base, ModelExt):
    __tablename__ = 'kvm_server_usage_log'

    id = Column(Integer, primary_key=True)

    server_id  = Column(Integer, nullable=None)

    cpu       = Column(Integer, nullable=None)
    memory    = Column(Integer, nullable=None)
    disk_size = Column(Integer, nullable=None)
    cpulimit  = Column(Integer, nullable=None)

    created = Column(DateTime, nullable=None, default=datetime.utcnow)
    changed = Column(DateTime, default=None)

    @classmethod
    def init(cls, server):
        return cls(server_id  = server.id,
                   cpu        = server.cpu,
                   memory     = server.memory,
                   disk_size  = server.size,
                   cpulimit   = server.cpulimit)

    @property
    def memory_str(self):
        return '{} GB'.format(self.memory)

    @property
    def disk_size_str(self):
        return '{} GB'.format(self.disk_size)


class KVMStorage(Base, ModelExt):
    __tablename__ = 'kvm_storages'

    NO_SERVER  = -1
    NO_DISK    = -1
    NO_PROJECT = -1

    STATUS = {
        'CREATING':       0,
        'CREATED':        1,

        'REMOVING':       80,
        'REMOVED':        81,
        'ERROR':          90,
        'NO FREE SPACE':  91,
        'LOST':           92,
    }
    TYPES = OrderedDict([
        ('LOCAL',  0),
        ('NFS',    1),
    ])

    id         = Column(Integer, primary_key=True)
    server_id  = Column(Integer, nullable=None, default=NO_SERVER)
    disk_id    = Column(Integer, nullable=None, default=NO_DISK)
    user_id    = Column(Integer, nullable=None)
    lab_id     = Column(Integer, nullable=None)
    project_id = Column(Integer, nullable=None)

    name        = Column(String(100),  nullable=None)
    alias       = Column(String(50),   nullable=None)
    description = Column(Text,         nullable=None)
    size        = Column(Integer,      nullable=None)  # M
    status      = Column(SmallInteger, nullable=None)
    type        = Column(SmallInteger, nullable=None, default=TYPES['LOCAL'])

    uuid = Column(String(50), nullable=None, default='')

    is_prepared = Column(Boolean, nullable=None, default=False)

    created = Column(DateTime, nullable=None, default  = datetime.utcnow)
    updated = Column(DateTime, nullable=None, default  = datetime.utcnow,
                                              onupdate = datetime.utcnow)

    @property
    def size_gb(self):
        return int(self.size * 1.0 / 1024)

    @property
    def size_str(self):
        return '{} GB'.format(self.size_gb)

    @property
    def dname(self):
        return 'storage{}'.format(self.id)

    @staticmethod
    def get_id(value):
        if value.startswith("storage"):
            l = len("storage")
            return int(value[l:])

    @property
    def url(self):
        return '/my/{}/storage/{}'.format(self.lab_id, self.id)

    @property
    def root_url(self):
        return '/root/storages/{}'.format(self.id)

    @property
    def a_tag(self):
        return '<a href="{}">{}</a>'.format(self.url, self.name)

    @property
    def project_onclick(self):
        u = ('<a href="#" onclick="ppane.showStorage({});return false;">{}</a>'
                .format(self.id, self.name))
        return u

    @property
    def status_str(self):
        for text, code in self.STATUS.items():
            if self.status == code:
                if text == 'CREATED':
                    return 'Ok'
                return text.capitalize()
        return ''

    @property
    def type_str(self):
        for text, code in self.TYPES.items():
            if self.type == code:
                return text
        return ''

    @classmethod
    def get_alias(cls, raw, name):
        import re
        alias = re.findall('^[a-zA-Z0-9]{1,30}$', raw)
        if alias:
            return alias[0]
        return (''.join(re.findall('[a-zA-Z0-9]', name)))[:13]

    def is_type(self, type):
        return self.type == self.TYPES[type]

    def show_alias(self, type):
        if type == "local":
            return "/mnt/{}".format(self.alias)
        elif type == "nfs":
            return "/mnt/nfs/{}".format(self.alias)
        elif type == "shared":
            return "/mnt/shared/{}".format(self.alias)

    def __repr__(self):
        return '<KVMStorage({!r}, {!r})>'.format(self.id, self.name)


class KVMStorageUsageLog(Base, ModelExt):
    __tablename__ = 'kvm_storage_usage_log'

    id = Column(Integer, primary_key=True)

    storage_id = Column(Integer, nullable=None)
    size       = Column(Integer, nullable=None)

    created = Column(DateTime, nullable=None, default=datetime.utcnow)
    changed = Column(DateTime, default=None)  # deleted, resized, etc

    @classmethod
    def init(cls, storage):
        return cls(storage_id = storage.id,
                   size       = storage.size)

    @property
    def size_str(self):
        return '{} GB'.format(int(self.size / 1024.0))

    def __repr__(self):
        return '<KVMStorageUsageLog>'


class KVMMountRelation(Base, ModelExt):
    __tablename__ = "kvm_mount_relations"

    STATUS = {
        "MOUNTING":  0,
        "MOUNTED":   1,

        "UMOUNTING":       10,
        "UMOUNTED":        11,
        "FORCE UMOUNTING": 12,
        "BUSY":            13,
    }
    REAL_STATUS = {
        "MOUNTING DISK": 0,  # MOUNTING TO CSERVER
        "MOUNTING TO S": 1,
        "MOUNTED":       2,

        "UMOUNTING F S":      10,
        "UMOUNTING F S BUSY": 11,
        "UMOUNTING DISK":     12,  # UMOUNTING FROM CSERVER
        "UMOUNTED":           13,
    }

    RIGHTS = {
        "RO": 0,
        "RW": 1,
    }

    id         = Column(Integer, primary_key=True)
    storage_id = Column(Integer, nullable=None)
    server_id  = Column(Integer, nullable=None)
    status     = Column(SmallInteger, nullable=None,
                                      default=STATUS["MOUNTING"])
    real_status = Column(SmallInteger, nullable=None,
                                       default=REAL_STATUS["MOUNTING DISK"])
    rights      = Column(SmallInteger, nullable=None)
    point       = Column(String(10), nullable=None, default="")

    def __repr__(self):
        return ("<KVMMountRelation({}, {})>"
                .format(self.server_id, self.storage_id))

    @property
    def status_str(self):
        for key, val in self.STATUS.items():
            if val == self.status:
                return key.capitalize()
        return ''

    @property
    def rights_val(self):
        for key, val in self.RIGHTS.items():
            if self.rights == val:
                return key.lower()

    @classmethod
    def gen_point(cls, points):
        ascii_lowercase = 'bcdefghijklmnopqrstuvwxyz'
        new_point = ''
        for let in ascii_lowercase:
            new_point = 'sd' + let
            if new_point not in points:
                return new_point
        raise


class NFSMount(Base, ModelExt):
    # TODO: remove or change
    __tablename__ = "nfs_mounts"

    PERMS = {
        "RW": 0,
        "RO": 1,
    }

    STATUS = {
        "MOUNTING":  0,
        "MOUNTED":   1,
        "UMOUNTING": 10,
    }

    id         = Column(Integer, primary_key=True)
    storage_id = Column(Integer, nullable=None)
    server_id  = Column(Integer, nullable=None)
    permission = Column(SmallInteger, nullable=None)
    status     = Column(SmallInteger, nullable=None)

    created = Column(DateTime, nullable=None, default  = datetime.utcnow)
    updated = Column(DateTime, nullable=None, default  = datetime.utcnow,
                                              onupdate = datetime.utcnow)

    @property
    def perms_str(self):
        for key, val in self.PERMS.items():
            if val == self.permission:
                return key.lower()

    @property
    def status_str(self):
        for key, val in self.STATUS.items():
            if val == self.status:
                return key.capitalize()


class Sharing(Base, ModelExt):
    __tablename__ = "sharings"

    LIMIT = 3        # per storage
    MOUNT_LIMIT = 3  # per record

    TYPES = {
        "user":  0,
        "lab":   1,
        # TODO: all labs
    }
    RIGHTS = {
        "RO": 0,
        "RW": 1,
    }

    id              = Column(Integer, primary_key=True)
    user_id         = Column(Integer, nullable=None)
    storage_id      = Column(Integer, nullable=None)
    sharing_type    = Column(SmallInteger, nullable=None)
    sharing_with_id = Column(Integer, nullable=None)
    rights          = Column(SmallInteger, nullable=None, default=RIGHTS["RO"])
    is_deleting     = Column(Boolean, nullable=None, default=False)

    @property
    def perms_str(self):
        for key, val in self.PERMS.items():
            if val == self.perms:
                if key == "RO":
                    return "Read-Only"
                elif key == "RW":
                    return "Read-Write"
                return key
        return ""

    def is_type(self, type):
        return self.sharing_type == self.TYPES[type]

    def mounted_to(self, server):
        if self.is_type("user"):
            return self.sharing_with_id == server.user_id
        elif self.is_type("lab"):
            return self.sharing_with_id == server.lab_id


class LabProject(Base, ModelExt):
    __tablename__ = "lab_projects"

    id         = Column(Integer, primary_key=True)
    created_by = Column(Integer, nullable=None)
    lab_id     = Column(Integer, nullable=None)

    name        = Column(String(100), nullable=None)
    description = Column(Text,        nullable=None, default="")

    databases_info = Column(Text, nullable=None, default='')

    created = Column(DateTime, nullable=None, default  = datetime.utcnow)
    updated = Column(DateTime, nullable=None, default  = datetime.utcnow,
                                              onupdate = datetime.utcnow)

    @property
    def a_tag(self):
        return '<a href="{}">{}</a>'.format(self.url, self.name)

    @property
    def url(self):
        return '/my/{}#project={}'.format(self.lab_id, self.id)

    @property
    def abs_url(self):
        return web.ctx.site_url + self.url

    @property
    def members_abs_url(self):
        path = '/my/{}#project={};members=1'.format(self.lab_id, self.id)
        return web.ctx.site_url + path

    @property
    def description_ptag(self):
        return ''.join(['<p>' + row + '</p>'
                        for row in self.description.split('\n')])

    def __repr__(self):
        return '<LabProject({!r}, {!r})>'.format(self.id, self.name)


class UserToProject(Base, ModelExt):
    __tablename__ = 'user_to_project'

    # used to filter access
    REL_TYPES = {
        'member':  0,
        'waiting': 5,
        'blocked': 10,
    }

    # used for different rights
    POSITIONS = {
        'none':   -1,
        'owner':  0,
        'member': 5,
    }

    id         = Column(Integer, primary_key=True)
    user_id    = Column(Integer,      nullable=None)
    project_id = Column(Integer,      nullable=None)
    relation   = Column(SmallInteger, nullable=None)
    position   = Column(SmallInteger, nullable=None)

    @property
    def position_str(self):
        if self.position == self.POSITIONS['none']:
            return 'waiting to be approved'
        for pos, pos_id in self.POSITIONS.items():
            if pos_id == self.position:
                return pos

    def __repr__(self):
        return '<UserToProject({}, {})>'.format(self.user_id, self.project_id)


class MyEvent(Base, ModelExt):
    __tablename__ = 'my_events'

    OFFSET = 25
    NO_PROJECT = -1

    id = Column(Integer, primary_key=True)

    user_id   = Column(Integer,     nullable=None)
    full_name = Column(String(100), nullable=None)

    lab_id       = Column(Integer,     nullable=None)
    lab_name     = Column(String(200), nullable=None)
    project_id   = Column(Integer,     nullable=None,
                                       default=NO_PROJECT)
    project_name = Column(String(100), nullable=None, default='')

    event_type = Column(SmallInteger, nullable=None)

    event_object_type = Column(SmallInteger, nullable=None, default=-1)
    event_object_id   = Column(Integer,      nullable=None, default=-1)
    event_object_name = Column(String(200),  nullable=None, default='')

    event_object_2_type = Column(SmallInteger, nullable=None, default=-1)
    event_object_2_id   = Column(Integer,      nullable=None, default=-1)
    event_object_2_name = Column(String(200),  nullable=None, default='')

    created = Column(DateTime, nullable=None, default = datetime.utcnow)

    @property
    def text(self):
        prefix = ''
        if self.project_id != self.NO_PROJECT:
            prefix = ('[ <a href="/my/{lab_id}#project={project_id}">'
                      '{project_name}</a> ] ')
        return ((prefix + self.MESSAGES[self.event_type])
                .format(**self.__dict__))

    @property
    def text_in_project(self):
        return (self.MESSAGES[self.event_type]).format(**self.__dict__)

    TYPES = {
        'create lab':     0,

        'create project': 10,
        'delete project': 11,

        'create server':  20,
        'destroy server': 21,
        'start server':   22,
        'stop server':    23,
        'restart server': 24,

        'create storage':  40,
        'destroy storage': 41,
        'mount storage':   42,
        'umount storage':  43,

        'add server to proj':      60,
        'remove server from proj': 61,

        'add storage to proj':      70,
        'remove storage from proj': 71,

        'waiting to be approved': 80,
        'user added to lab':      81,
        'reject user from lab':   82,
        'delete user from lab':   83,
        'user leaved lab':        84,

        'waiting to project':       90,
        'user added to project':    91,
        'reject user from project': 92,
        'delete user from project': 93,
        'user leaved project':      94,
    }

    MESSAGES = {
        0:   ('<a href="/my/user/{user_id}">{full_name}</a> created '
              'lab <a href="/my/{lab_id}">{lab_name}</a>'),
        10:  ('<a href="/my/user/{user_id}">{full_name}</a> created '
              'project <a href="/my/{lab_id}#project={project_id}">'
              '{project_name}</a>'),
        11:  ('<a href="/my/user/{user_id}">{full_name}</a> deleted '
              'project <a href="/my/{lab_id}#project={project_id}">'
              '{project_name}</a>'),
        20:  ('<a href="/my/user/{user_id}">{full_name}</a> created '
              'server <a href="/my/{lab_id}/servers/{event_object_id}">'
              '{event_object_name}</a>'),
        21:  ('<a href="/my/user/{user_id}">{full_name}</a> destroyed '
              'server <a href="/my/{lab_id}/servers/{event_object_id}">'
              '{event_object_name}</a>'),
        22:  ('<a href="/my/user/{user_id}">{full_name}</a> started '
              'server <a href="/my/{lab_id}/servers/{event_object_id}">'
              '{event_object_name}</a>'),
        23:  ('<a href="/my/user/{user_id}">{full_name}</a> stopped '
              'server <a href="/my/{lab_id}/servers/{event_object_id}">'
              '{event_object_name}</a>'),
        24:  ('<a href="/my/user/{user_id}">{full_name}</a> restarted '
              'server <a href="/my/{lab_id}/servers/{event_object_id}">'
              '{event_object_name}</a>'),
        40:  ('<a href="/my/user/{user_id}">{full_name}</a> created '
              'disk <a href="/my/{lab_id}/storage/{event_object_id}">'
              '{event_object_name}</a>'),
        41:  ('<a href="/my/user/{user_id}">{full_name}</a> destroyed '
              'disk <a href="/my/{lab_id}/storage/{event_object_id}">'
              '{event_object_name}</a>'),
        42:  ('<a href="/my/user/{user_id}">{full_name}</a> mount '
              'disk <a href="/my/{lab_id}/storage/{event_object_id}">'
              '{event_object_name}</a> to '
              'server <a href="/my/{lab_id}/servers/{event_object_2_id}">'
              '{event_object_2_name}</a>'),
        43:  ('<a href="/my/user/{user_id}">{full_name}</a> umount '
              'disk <a href="/my/{lab_id}/storage/{event_object_id}">'
              '{event_object_name}</a> from '
              'server <a href="/my/{lab_id}/servers/{event_object_2_id}">'
              '{event_object_2_name}</a>'),
        60:  ('<a href="/my/user/{user_id}">{full_name}</a> added '
              'server <a href="/my/{lab_id}/servers/{event_object_id}">'
              '{event_object_name}</a> to project'),
        61:  ('<a href="/my/user/{user_id}">{full_name}</a> removed '
              'server <a href="/my/{lab_id}/servers/{event_object_id}">'
              '{event_object_name}</a> from project'),
        70:  ('<a href="/my/user/{user_id}">{full_name}</a> added '
              'disk <a href="/my{lab_id}/storage/{event_object_id}">'
              '{event_object_name}</a> to project'),
        71:  ('<a href="/my/user/{user_id}">{full_name}</a> removed '
              'disk <a href="/my{lab_id}/storage/{event_object_id}">'
              '{event_object_name}</a> from project'),

        80:  ('<a href="/my/user/{user_id}">{full_name}</a> '
              'waiting to be approved'),
        81:  ('<a href="/my/user/{user_id}">{full_name}</a> added '
              '<a href="/my/user/{event_object_id}">'
              '{event_object_name}</a> to lab'),
        82:  ('<a href="/my/user/{user_id}">{full_name}</a> rejected '
              '<a href="/my/user/{event_object_id}">'
              '{event_object_name}</a> from lab'),
        83:  ('<a href="/my/user/{user_id}">{full_name}</a> deleted '
              '<a href="/my/user/{event_object_id}">'
              '{event_object_name}</a> from lab'),
        84:  ('<a href="/my/user/{user_id}">{full_name}</a> left the lab'),

        90:  ('<a href="/my/user/{user_id}">{full_name}</a> waiting to be '
              'approved in project'),
        91:  ('<a href="/my/user/{user_id}">{full_name}</a> added user '
              '<a href="/my/user/{event_object_id}">{event_object_name}</a>'
              ' to project'),
        92:  ('<a href="/my/user/{user_id}">{full_name}</a> rejected user '
              '<a href="/my/user/{event_object_id}">{event_object_name}</a>'
              ' from project'),
        93:  ('<a href="/my/user/{user_id}">{full_name}</a> deleted user '
              '<a href="/my/user/{event_object_id}">{event_object_name}</a>'
              ' from project'),
        94:  '<a href="/my/user/{user_id}">{full_name}</a> left the project',
    }

    OBJ_TYPES = {
        'server':  0,
        'storage': 1,
        'user':    2,
    }


class LabPrice(Base, ModelExt):
    __tablename__ = 'lab_prices'

    id     = Column(Integer, primary_key=True)
    lab_id = Column(Integer, nullable=None)

    # server
    server_cpu  = Column(DECIMAL(5, 2), nullable=None)
    server_ram  = Column(DECIMAL(5, 2), nullable=None)
    server_size = Column(DECIMAL(5, 2), nullable=None)

    # disk
    disk_size = Column(Float, nullable=None)

    def to_d(self):
        return {'server-cpu-price':  self.server_cpu,
                'server-ram-price':  self.server_ram,
                'server-size-price': self.server_size,
                'disk-size-price':   self.disk_size}


class LabInvoice(Base, ModelExt):
    __tablename__ = 'lab_invoices'

    PAYMENTS = {
        'NONE':   0,
        'CHECK':  1,
        'PAYPAL': 10,
    }

    id     = Column(Integer, primary_key=True)
    lab_id = Column(Integer, nullable=None)

    date  = Column(DateTime,       nullable=None)
    price = Column(DECIMAL(10, 2), nullable=None)
    paid  = Column(SmallInteger,   nullable=None)  # was paid or not

    server_cpu_price  = Column(DECIMAL(5, 2), nullable=None)
    server_ram_price  = Column(DECIMAL(5, 2), nullable=None)
    server_size_price = Column(DECIMAL(5, 2), nullable=None)
    disk_size_price   = Column(DECIMAL(5, 2), nullable=None)

    def __repr__(self):
        return ('<Invoice({id}, {lab_id})>'
                .format(id=self.id, lab_id=self.lab_id))

    @property
    def url(self):
        return '/my/{}/payments/invoices#{}'.format(self.lab_id, self.id)

    @property
    def paid_text(self):
        for key, val in self.PAYMENTS.items():
            if val == self.paid:
                if key == 'NONE':
                    return 'not paid'
                return key

    def get_prices(self):
        return {
            'server-cpu-price':  float(self.server_cpu_price),
            'server-ram-price':  float(self.server_ram_price),
            'server-size-price': float(self.server_size_price),
            'disk-size-price':   float(self.disk_size_price)
        }
