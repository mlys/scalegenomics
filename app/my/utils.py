import web
from sqlalchemy import not_

from app.root.models import ClusterServer, ClusterDisk, LabClusterServer
from app.my.models import Lab, KVMServer, KVMStorage, \
    KVMMountRelation, LabProject, UserToProject
from app.sso.models import User, UserToLab


def set_flash_message(msg):
    web.ctx.session['flash-message'] = msg


def get_flash_message():
    if 'flash-message' in web.ctx.session:
        message = web.ctx.session.pop('flash-message')
        return message
    return None


def rm_microsec(date_str):
    last_point = date_str.rfind('.')
    return date_str[:last_point]


def get_server_limits(lab):
    "Strip limits."
    cpu_lim = web.config["SG_SETTINGS"]["KVM_CPU_LIMITS"]
    if cpu_lim[1] > lab.cpu_free:
        cpu_lim = (1, lab.cpu_free)

    mem_lim = web.config["SG_SETTINGS"]["KVM_MEMORY_LIMITS"].copy()
    if mem_lim["max"] > lab.ram_free:
        mem_lim["max"] = lab.ram_free

    if lab.cpu_limit - lab.cpu_used < 1:
        cpu_block = True
    else:
        cpu_block = False
    mem_block = True if lab.ram_free < mem_lim['min'] else False

    return cpu_lim, mem_lim, cpu_block, mem_block


def get_cservs_info(cservs, cpu_lim, mem_lim):
    free_space = False
    cservs_info = []
    for cserv, _, _, cdisk in cservs:
        cservs_info.append((cserv.id, cpu_lim, mem_lim, cdisk.free_size))
        if cdisk.free_size > 0:
            free_space = True
    return cservs_info, free_space


def get_my_labs_with_rel(with_owner=False):
    query = (web.ctx.orm.query(Lab, UserToLab)
             .filter(UserToLab.lab_id == Lab.id)
             .filter(UserToLab.user_id == web.ctx.user.id)
             #.filter(UserToLab.perms.in_([UserToLab.PERMS['owner'],
             #                             UserToLab.PERMS['member']]))
             .order_by(UserToLab.perms)
             .all())
    if with_owner:
        result = []
        for lab, u2lab in query:
            owner = (web.ctx.orm.query(User, UserToLab)
                    .filter(UserToLab.user_id == User.id)
                    .filter(UserToLab.lab_id == lab.id)
                    .filter(UserToLab.perms == UserToLab.PERMS["owner"])
                    .first())
            result.append((lab, u2lab, owner[0]))
        return result
    return query


class LabLimit(object):

    def __init__(self, lab):
        self.lab = lab

        sett = web.config['SG_SETTINGS']
        self.storage_limits = sett['STORAGE_SIZE_LIMITS'].copy()
        self.cpu_limits     = sett['KVM_CPU_LIMITS']
        self.memory_limits  = sett['KVM_MEMORY_LIMITS'].copy()

    def get_storage_limits(self, size):
        limits = web.config['SG_SETTINGS']['STORAGE_SIZE_LIMITS'].copy()
        # if lab limits reached
        if limits['max'] > self.lab.storages_free_gb:
            limits['max'] = self.lab.storages_free_gb

        # if we don't have disk with max configured size
        if size < limits['max']:
            limits['max'] = size

        # default value correction
        if limits['default'] > self.storage_limits['max']:
            limits['default'] = 1

        return limits

    def get_server_limits(self):
        if self.cpu_limits[1] > self.lab.cpu_free:
            self.cpu_limits = (1, self.lab.cpu_free)

        if self.memory_limits['max'] > self.lab.ram_free:
            self.memory_limits['max'] = self.lab.ram_free

        return self.cpu_limits, self.memory_limits


def get_lab_cservs(lab_id, size, session=None):
    if not session:
        session = web.ctx.orm

    lcss = (session.query(ClusterServer, LabClusterServer)
            .filter(LabClusterServer.server_id == ClusterServer.id)
            .filter(LabClusterServer.lab_id == lab_id)
            .order_by(ClusterServer.id)
            .all())
    cdisk = None  # TODO: message
    cservs = []
    if lcss:
        if len(lcss) == 1:
            cdisk = (session.query(ClusterDisk)
                .filter(ClusterDisk.server_id == lcss[0][0].id)
                .filter(ClusterDisk.free_size >= size)
                .first())
            cservs = [(lcss[0][0], 0, 0)]  # count or not?
        else:
            ids = [cserv.id for cserv, _ in lcss]
            # TODO: cdisk
            cdisk = (session.query(ClusterDisk)
                .filter(ClusterDisk.server_id.in_(ids))
                .filter(ClusterDisk.free_size >= size)
                .first())
            cservs = []
            for cserv, _ in lcss:
                # my servers
                servers_count = (session.query(KVMServer)
                            .filter(KVMServer.server_id == cserv.id)
                            .filter(KVMServer.lab_id == lab_id)
                            .filter(KVMServer.user_id == web.ctx.user.id)
                            .filter(KVMServer.status <
                                    KVMServer.STATUS['REMOVING'])
                            .count())
                # my disks
                storages_count = (session.query(KVMStorage)
                            .filter(KVMStorage.server_id == cserv.id)
                            .filter(KVMStorage.lab_id == lab_id)
                            .filter(KVMStorage.user_id == web.ctx.user.id)
                            .filter(KVMStorage.status <
                                    KVMStorage.STATUS['REMOVING'])
                            .count())
                cservs.append((cserv, servers_count, storages_count))
    return cdisk, cservs


def get_cservs_data(lab_id, session=None):
    """
        result = [(
            cserver,       <- cserver used by this lab
            servers_count, <- on the same cserver
            stoages_count, <- on the same cserver
            cdisk          <- cdisk with max free size
        )]
    """
    if not session:
        session = web.ctx.orm

    lcss = (session.query(ClusterServer, LabClusterServer)
            .filter(LabClusterServer.server_id == ClusterServer.id)
            .filter(LabClusterServer.lab_id == lab_id)
            .order_by(ClusterServer.id)
            .all())
    cservs = []
    if lcss:
        if len(lcss) == 1:
            cdisk = (session.query(ClusterDisk)
                .filter(ClusterDisk.server_id == lcss[0][0].id)
                .order_by(ClusterDisk.free_size.desc())
                .first())
            cservs = [(lcss[0][0], 0, 0, cdisk)]
        else:
            cservs = []
            for cserv, _ in lcss:
                cdisk = (session.query(ClusterDisk)
                        .filter(ClusterDisk.server_id == cserv.id)
                        .order_by(ClusterDisk.free_size.desc())
                        .first())
                # my servers
                servers_count = (session.query(KVMServer)
                            .filter(KVMServer.server_id == cserv.id)
                            .filter(KVMServer.lab_id == lab_id)
                            .filter(KVMServer.user_id == web.ctx.user.id)
                            .filter(KVMServer.status <
                                    KVMServer.STATUS['REMOVING'])
                            .count())
                # my disks
                storages_count = (session.query(KVMStorage)
                            .filter(KVMStorage.server_id == cserv.id)
                            .filter(KVMStorage.lab_id == lab_id)
                            .filter(KVMStorage.user_id == web.ctx.user.id)
                            .filter(KVMStorage.status <
                                    KVMStorage.STATUS['REMOVING'])
                            .count())
                cservs.append((cserv, servers_count, storages_count, cdisk))
    return cservs


def get_lab_resources(lab_id, cserver_id, size):
    # Cases
    # * 0 cservers in the lab
    # * 1 cserver in the lab
    # * 2+ cservers in the lab
    cdisk = None
    cserver = None

    lcss = (web.ctx.orm.query(ClusterServer, LabClusterServer)
            .filter(LabClusterServer.server_id == ClusterServer.id)
            .filter(LabClusterServer.lab_id == lab_id)
            .all())
    if not lcss:
        return cserver, cdisk

    if len(lcss) == 1:
        cserver = lcss[0][0]
        cdisk = (web.ctx.orm.query(ClusterDisk)
            .filter(ClusterDisk.free_size >= size)
            .filter(ClusterDisk.server_id == cserver.id)
            .first())
        return cserver, cdisk
    else:
        if not cserver_id:
            return cserver, cdisk

        for cserv, _ in lcss:
            if cserv.id == cserver_id:
                cserver = cserv
                cdisk = (web.ctx.orm.query(ClusterDisk)
                .filter(ClusterDisk.free_size >= size)
                .filter(ClusterDisk.server_id == cserver_id)
                .first())
                break
        return cserver, cdisk


def storage_editable(storage):
    can_edit = False
    if storage.type == storage.TYPES["LOCAL"]:
        rel = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMMountRelation.storage_id == storage.id)
                .filter(KVMMountRelation.status.in_([
                    KVMMountRelation.STATUS["MOUNTING"],
                    KVMMountRelation.STATUS["MOUNTED"]]))
                .first())
        if not rel:
            can_edit = True
    elif storage.type == storage.TYPES["NFS"]:
        rel = (web.ctx.orm.query(KVMMountRelation)
                .filter(KVMMountRelation.storage_id == storage.id)
                .filter(KVMMountRelation.status.in_([
                    KVMMountRelation.STATUS["MOUNTING"],
                    KVMMountRelation.STATUS["MOUNTED"]]))
                .first())
        if not rel:
            can_edit = True
    return can_edit


def get_dheight(description):
    "Return description heigth. Required for auto-resize."
    return '{}px'.format(len(description.split('\n')) * 15 + 50)


def unmount_my_shared(type, storage_id, unit_id):
    query = (web.ctx.orm.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMStorage.id == storage_id)
            .filter(not_(KVMMountRelation.status.in_([
                KVMMountRelation.STATUS["UMOUNTING"],
                KVMMountRelation.STATUS["UMOUNTED"]]))))
    if type == "user":
        rels = query.filter(KVMServer.user_id == unit_id).all()

    elif type == "labs":
        rels = query.filter(KVMServer.lab_id == unit_id).all()

    else:
        rels = []

    if rels:
        for _, _, rel in rels:
            rel.status = KVMMountRelation.STATUS["UMOUNTING"]

    return False


def get_lab_projects(lab_id):
    "Return project avaible for current user."
    return (web.ctx.orm.query(LabProject)
            .filter(LabProject.id == UserToProject.project_id)
            .filter(UserToProject.user_id == web.ctx.user.id)
            .filter(LabProject.lab_id == lab_id)
            .filter(UserToProject.relation ==
                UserToProject.REL_TYPES['member'])
            .all())
