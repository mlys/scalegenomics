import cgi
import re
import textwrap
import json
from datetime import datetime

import web
from sqlalchemy import or_
from app.core.processors import auth_check
from app.core.decorators import as_json, as_html, as_text
from app.core.templates  import filters
from app.core.mail       import send_mail
from app.core.types      import FDict
from app.core.patterns   import re_server_name, re_storage_name
from app.core.server     import openvpn
from app.my.decorators   import has_relations
from app.my.processors   import user_lab_list_processor
from app.my.utils        import set_flash_message, get_flash_message, \
    get_server_limits, get_my_labs_with_rel, LabLimit, \
    get_cservs_data, get_lab_resources, get_cservs_info, storage_editable, \
    get_dheight, get_lab_projects
from app.sso.patterns    import pat_em
from app.sso.models      import User, UserToLab, UserCert
from app.root.models     import DataSet, DataSetRequest, DataSetVolume
from app.my.models       import Lab, LabProject, UserToProject, MyEvent, \
    KVMServer, KVMServerUsageLog, KVMStorage, KVMStorageUsageLog, \
    KVMMountRelation, Plan, LabPlan, Sharing
from app.my.forms    import LabForm, ServerForm, StorageForm
from app.root.forms  import DataSetRequestForm

# project views
from app.my.projects_views import NewProject, \
    ShowAllProjects, ShowProject, JoinToProject, EditProject, \
    LoadProjectParam, DeleteProject, ShowProjectDashboard, \
    ShowProjectServers, ShowProjectServer, ShowProjectStorages, \
    ShowProjectStorage, ShowProjectMembers, ShowProjectLabMembers, \
    ShowProjectEvents, NewProjectServer, NewProjectStorage, \
    EditProjectStorage, ShowProjectServerLog, ShowProjectDatabases, \
    ShowProjectDatabasesParam, ShowMyProjects, EditProjectServer, \
    UnsubscribeProject

# payments views
from app.my.payments_views import ShowLabPayments, ShowLabPlans, \
     UpgradeLabPlan, ChangeLabPlan, LabPaymentsLog, \
     LabPaymentsInMonthSum, LabPaymentsInMonthByServer, \
     LabPaymentsInMonthByDisk, LabInvoices, ShowLabInvoices, ShowLabInvoice, \
     ShowLabPlansByDate

# mount views
from app.my.mount_views import StoragesForMount, ShowServerMountList, \
        MountToServer, UmountStorage, ShowStorageNFSMounts

# sharing views
from app.my.sharing_views import MembersForSharing, ShowStorageSharings, \
        ShowLabSharing, ShowSharingWithMe, DeleteSharing, \
        ServersForSharing, MountSharedStorage, UmountSharedStorage, \
        ShowNFSStorages

# help views
from app.my.help_views import HelpMe, HelpMeWithOpenVPN, HelpMeWithVPN, \
        ShowMainHelp, ShowSharingHelp, ShowDisksHelp


urls = (
  '',                     'Index',
  '/join',                'Join',
  '/new',                 'NewLabPlans',
  '/new/(\d+)',           'NewLab',
  '/labs/all/type',       'ShowAllLabsByType',
  '/users',               'ShowUsers',
  '/user/(\d+)',          'ShowUser',
  '/user/(\d+)/goto',     'RedirectToPersonalSite',

  '/help',                'HelpMe',
  '/help/main',           'ShowMainHelp',
  '/help/vpn',            'HelpMeWithVPN',  # hidden now
  '/help/openvpn',        'HelpMeWithOpenVPN',
  '/help/disks',          'ShowDisksHelp',
  '/help/sharing',        'ShowSharingHelp',

  '/ta\.key',            'GetUserTaKey',
  '/ca\.crt',            'GetUserCaCrt',
  '/client\.crt',        'GetUserCrt',
  '/client\.key',        'GetUserKey',
  '/scalegenomics\.zip', 'GetOpenVPNZip',

  '/(\d+)',                'ShowLabDashboard',
  '/(\d+)/members',        'ShowLabMembers',
  '/(\d+)/settings',       'LabSettings',
  '/(\d+)/updates',        'ShowLabUpdates',
  '/(\d+)/updates/get',    'GetLabUpdates',

  '/(\d+)/activation',    'RequestActivation',
  '/(\d+)/limits',        'GetLabLimits',

  '/(\d+)/payments',                       'ShowLabPayments',
  '/(\d+)/payments/plans',                 'ShowLabPlans',
  '/(\d+)/payments/plans/(\d+)/(\d+)',     'ShowLabPlansByDate',
  '/(\d+)/payments/plans/upgrade',         'UpgradeLabPlan',
  '/(\d+)/payments/plans/upgrade/(\d+)',   'ChangeLabPlan',
  #'/(\d+)/payments/log',                  'LabPaymentsLog',
  #'/(\d+)/payments/invoices',             'LabInvoices',
  #'/(\d+)/payments/invoices/all',         'ShowLabInvoices',
  #'/(\d+)/payments/invoices/(\d+)',       'ShowLabInvoice',
  #'/(\d+)/payments/all/month',            'LabPaymentsInMonthSum',
  #'/(\d+)/payments/server/month',         'LabPaymentsInMonthByServer',
  #'/(\d+)/payments/disk/month',           'LabPaymentsInMonthByDisk',

  '/(\d+)/projects/new',                      'NewProject',
  '/(\d+)/projects/all',                      'ShowAllProjects',
  '/(\d+)/projects/my',                       'ShowMyProjects',
  '/(\d+)/projects/(\d+)',                    'ShowProject',
  '/(\d+)/projects/(\d+)/join',               'JoinToProject',
  '/(\d+)/projects/(\d+)/us',                 'UnsubscribeProject',
  '/(\d+)/projects/(\d+)/edit',               'EditProject',
  '/(\d+)/projects/(\d+)/param',              'LoadProjectParam',
  '/(\d+)/projects/(\d+)/delete',             'DeleteProject',
  '/(\d+)/projects/(\d+)/dboard',             'ShowProjectDashboard',
  '/(\d+)/projects/(\d+)/servers',            'ShowProjectServers',
  '/(\d+)/projects/(\d+)/servers/new',        'NewProjectServer',
  '/(\d+)/projects/(\d+)/servers/(\d+)',      'ShowProjectServer',
  '/(\d+)/projects/(\d+)/servers/(\d+)/edit', 'EditProjectServer',
  '/(\d+)/projects/(\d+)/servers/(\d+)/log',  'ShowProjectServerLog',
  '/(\d+)/projects/(\d+)/disks',              'ShowProjectStorages',
  '/(\d+)/projects/(\d+)/disks/new',          'NewProjectStorage',
  '/(\d+)/projects/(\d+)/disks/(\d+)',        'ShowProjectStorage',
  '/(\d+)/projects/(\d+)/disks/(\d+)/ed',     'EditProjectStorage',
  '/(\d+)/projects/(\d+)/databases',          'ShowProjectDatabases',
  '/(\d+)/projects/(\d+)/databases/param',    'ShowProjectDatabasesParam',
  '/(\d+)/projects/(\d+)/members',            'ShowProjectMembers',
  '/(\d+)/projects/(\d+)/labmembers',         'ShowProjectLabMembers',
  '/(\d+)/projects/(\d+)/allevn',             'ShowProjectEvents',

  '/(\d+)/servers',               'ShowLabServers',
  '/(\d+)/servers/list',          'ShowLabServersList',
  '/(\d+)/servers/status',        'ShowLabServersStatus',
  '/(\d+)/servers/new',           'NewServer',
  '/(\d+)/servers/(\d+)',         'ShowServer',
  '/(\d+)/servers/(\d+)/start',   'StartServer',
  '/(\d+)/servers/(\d+)/stop',    'StopServer',
  '/(\d+)/servers/(\d+)/restart', 'RestartKVMServer',
  '/(\d+)/servers/(\d+)/edit',    'EditServer',
  '/(\d+)/servers/(\d+)/destroy', 'DestroyServer',
  '/(\d+)/servers/(\d+)/status',  'ShowServerStatus',
  '/(\d+)/servers/(\d+)/log',     'ShowServerLog',

  '/(\d+)/servers/(\d+)/mlist',   'ShowServerMountList',
  '/(\d+)/servers/(\d+)/slist',   'StoragesForMount',
  '/(\d+)/servers/(\d+)/mount',   'MountToServer',
  '/(\d+)/servers/(\d+)/umount',  'UmountStorage',

  '/(\d+)/storage',               'ShowLabStorages',
  '/(\d+)/storage/list',          'ShowLabStoragesList',
  '/(\d+)/storage/new',           'NewStorage',
  '/(\d+)/storage/(\d+)',         'ShowStorage',
  '/(\d+)/storage/(\d+)/edit',    'EditStorage',
  '/(\d+)/storage/(\d+)/destroy', 'DestroyStorage',
  '/(\d+)/storage/(\d+)/status',  'ShowStorageStatus',
  '/(\d+)/storage/(\d+)/nfs',     'ShowStorageNFSMounts',

  '/(\d+)/storage/(\d+)/sharing', 'MembersForSharing',
  '/(\d+)/storage/(\d+)/slist',   'ShowStorageSharings',
  '/(\d+)/sharing',               'ShowLabSharing',
  '/(\d+)/sharing/withme',        'ShowSharingWithMe',
  '/(\d+)/sharing/servers',       'ServersForSharing',
  '/(\d+)/sharing/disks',         'ShowNFSStorages',
  '/(\d+)/sharing/mount',         'MountSharedStorage',
  '/(\d+)/sharing/umount',        'UmountSharedStorage',
  '/(\d+)/sharing/(\d+)/delete',  'DeleteSharing',

  '/(\d+)/datasets',          'ShowLabDatasets',
  '/(\d+)/requests',          'ShowLabDataSetRequests',
  '/(\d+)/request/(\d+)/new', 'NewDataSetRequest',
  '/(\d+)/dataset/(\d+)',     'ShowDataSet',
  '/(\d+)/volume/(\d+)',      'ShowDataSetVolume',

  '/(\d+)/databases',       'ShowLabDatabases',
  '/(\d+)/databases/param', 'ShowLabDatabasesParam',

  '/a/join_to_lab',        'JoinToLab',

  '/a/(\d+)/leave_lab',    'LeaveLab',
  '/a/(\d+)/add_member',   'AddLabMember',
  '/a/(\d+)/confirm_user', 'ConfirmUser',
  '/a/(\d+)/delete_user',  'DeleteUser',
)


web_app = web.application(urls, locals())
web_app.add_processor(auth_check)
web_app.add_processor(user_lab_list_processor)


re_lab_name = re.compile('^[a-zA-Z0-9\'\s]{4,50}$')
re_storage_alias = re.compile('^[a-zA-Z0-9]{1,30}$')


def desc_wrap(desc):
    return textwrap.fill(cgi.escape(desc), 50)


def user_in_project(project_id):
    return (web.ctx.orm.query(LabProject, UserToProject)
            # connected
            .filter(LabProject.id == UserToProject.project_id)
            .filter(LabProject.id == project_id)
            # user
            .filter(UserToProject.user_id == web.ctx.user.id)
            .first())


def get_system_user():
    return (web.ctx.orm.query(User)
            .filter_by(position=User.POSITIONS["System"])
            .first())


class Index:

    @as_html("my/index.html")
    def GET(self):
        query = get_my_labs_with_rel(True)
        # redirect if user has one lab
        http_ref = False
        if web.ctx.env.get("HTTP_REFERER", "") == web.ctx.site_url + '/':
            http_ref = True
        if len(query) == 1 and http_ref:
            raise web.seeother(query[0][0].abs_url)
        return {"cpage": "mylabs",
                "pairs": query}


class Join:

    @as_html('my/join.html')
    def GET(self):
        lab_user_pairs = (web.ctx.orm.query(Lab, User, UserToLab)
                      .filter(UserToLab.lab_id  == Lab.id)
                      .filter(UserToLab.user_id == User.id)
                      .filter(UserToLab.perms   == UserToLab.PERMS['owner'])
                      .filter(Lab.status == Lab.STATUS['active'])
                      .all())

        u2l = (web.ctx.orm.query(UserToLab)
               .filter_by(user_id = web.ctx.session.user_id)
               .all())

        result = []
        for lab, user, u2lab in lab_user_pairs:
            in_rel = None

            for rel in u2l:
                if lab.id == rel.lab_id:
                    in_rel = rel.perms_str
                    break

            if in_rel:
                result.append((lab, user, in_rel))
            else:
                result.append((lab, user, False))

        return {'cpage'          : 'join',
                'lab_user_pairs' : result,
                'lab_types'      : enumerate(web.config['SG_SETTINGS']
                                                       ['LAB_TYPE'])}


class NewLabPlans:

    @as_html('my/new_lab_plans.html')
    def GET(self):
        plans = web.ctx.orm.query(Plan).all()
        rels = (web.ctx.orm.query(UserToLab)
                .filter_by(user_id = web.ctx.session.user_id)
                .filter_by(perms = UserToLab.PERMS['owner'])
                .all())
        return {'cpage' : 'mylabs',
                'plans' : plans,
                'rels'  : rels}


class NewLab:

    @as_html('my/new_lab.html')
    def GET(self, plan_id):
        plan = web.ctx.orm.query(Plan).filter_by(id = plan_id).first()
        if not plan:
            raise web.notfound()

        return {'cpage' : 'mylabs',
                'form'  : LabForm(),
                'plan'  : plan}

    @as_html('my/new_lab.html')
    def POST(self, plan_id):
        plan = web.ctx.orm.query(Plan).filter_by(id = plan_id).first()
        if not plan:
            raise web.notfound()

        rels = (web.ctx.orm.query(UserToLab)
                .filter_by(user_id = web.ctx.session['user_id'])
                .filter_by(perms = UserToLab.PERMS['owner'])
                .all())
        if len(rels) >= 5:
            raise web.notfound()

        params = FDict(web.input())
        form = LabForm(params)
        if not form.validate():
            return {'cpage' : 'mylabs',
                    'form'  : form,
                    'plan'  : plan}

        tp = web.config['SG_SETTINGS']['LAB_TYPE'][int(form.type.data)]
        lab = Lab(name           = form.name.data,
                  type           = tp,
                  cpu_limit      = plan.cores,
                  ram_limit      = plan.ram_mb,
                  storages_limit = plan.storage_mb)
        web.ctx.orm.add(lab)
        web.ctx.orm.commit()

        u2l = UserToLab(lab_id  = lab.id,
                        user_id = web.ctx.session.user_id,
                        perms   = UserToLab.PERMS['owner'])
        web.ctx.orm.add(u2l)

        lab_plan = LabPlan(lab_id  = lab.id,
                           plan_id = plan.id,
                           name    = plan.name,
                           price   = plan.price,
                           cores   = plan.cores,
                           ram     = plan.ram,
                           storage = plan.storage)
        web.ctx.orm.add(lab_plan)

        my_event = MyEvent(user_id    = web.ctx.session.user_id,
                           full_name  = web.ctx.session.full_name,
                           lab_id     = lab.id,
                           lab_name   = lab.name,
                           event_type = MyEvent.TYPES['create lab'])
        web.ctx.orm.add(my_event)

        raise web.seeother(lab.abs_url)


class ShowAllLabsByType:

    @as_html('my/show_all_labs_by_type.html')
    def GET(self):
        params = web.input()

        lab_type = web.config['SG_SETTINGS']['LAB_TYPE'][0]
        if 'type' in params:
            lab_type = params['type']
        if lab_type not in web.config['SG_SETTINGS']['LAB_TYPE']:
            lab_type = 'All'
        qr = (web.ctx.orm.query(Lab, UserToLab, User)
              .filter(UserToLab.lab_id == Lab.id)
              .filter(UserToLab.user_id == User.id)
              .filter(UserToLab.perms == UserToLab.PERMS['owner'])
              .order_by(User.last_name))
        if lab_type == 'All':
            labs = qr.all()
        else:
            labs = qr.filter(Lab.type == lab_type).all()

        u2rels = (web.ctx.orm.query(UserToLab)
                  .filter_by(user_id = web.ctx.session['user_id'])
                  .all())
        result = []
        for lab, u2lab, user in labs:
            r = None
            for rel in u2rels:
                if rel.lab_id == lab.id:
                    r = rel
                    break
            result.append((lab, u2lab, user, r))
        return {'labs' : result}


class ShowUsers:

    @as_html("my/show_users.html")
    def GET(self):
        users = (web.ctx.orm.query(User)
                 .filter(User.position != User.POSITIONS["System"])
                 .order_by(User.last_name)
                 .all())
        user_list = []
        while True:
            if len(users) > 3:
                user_list.append(users[:3])
                users = users[3:]
            else:
                user_list.append(users)
                break

        return {"cpage": "mylabs",
                "users": user_list}


class ShowUser:

    @as_html("my/show_user.html")
    def GET(self, user_id):
        "Display user profile."
        user = (web.ctx.orm.query(User)
                .filter(User.position != User.POSITIONS["System"])
                .filter_by(id = user_id)
                .first())
        if not user:
            raise web.notfound()

        qr = (web.ctx.orm.query(Lab, UserToLab)
              .filter(UserToLab.user_id == user.id)
              .filter(UserToLab.lab_id == Lab.id)
              .filter(UserToLab.perms.in_([UserToLab.PERMS["owner"],
                                           UserToLab.PERMS["member"]]))
              .order_by(UserToLab.perms)
              .all())

        return {"cpage":  "mylabs",
                "xuser":   user,
                "users":  qr}


class RedirectToPersonalSite:

    @as_html('my/redirect_to_personal_site.html')
    def GET(self, user_id):
        back_link = ''
        if '/my/users' in web.ctx.env['HTTP_REFERER']:
            back_link = '/my/users'
        else:
            back_link = '/my/user/{}'

        user = (web.ctx.orm.query(User)
                .filter_by(id = user_id)
                .first())

        if not user or not user.personal_site:
            raise web.notfound()

        return {'user'      : user,
                'back_link' : back_link.format(user.id) }


class GetUserTaKey:

    def GET(self):
        ucert = (web.ctx.orm.query(UserCert)
                 .filter_by(user_id = web.ctx.session['user_id'])
                 .first())
        if not ucert:
            raise web.notfound()

        web.header('Content-Type', 'application/text')
        web.header('Content-Length', 635)
        return web.config['SG_SETTINGS']['TA_KEY']


class GetUserCaCrt:

    def GET(self):
        ucert = (web.ctx.orm.query(UserCert)
                 .filter_by(user_id = web.ctx.session['user_id'])
                 .first())
        if not ucert:
            raise web.notfound()
        web.header('Content-Type', 'application/text')
        web.header('Content-Length', 1353)
        return web.config['SG_SETTINGS']['CA_CRT']


class GetUserCrt:

    def GET(self):
        ucert = (web.ctx.orm.query(UserCert)
                 .filter_by(user_id = web.ctx.session['user_id'])
                 .first())
        if not ucert:
            raise web.notfound()
        web.header('Content-Type', 'application/text')
        web.header('Content-Length', len(ucert.crt))
        return ucert.crt


class GetUserKey:

    def GET(self):
        ucert = (web.ctx.orm.query(UserCert)
                 .filter_by(user_id = web.ctx.session['user_id'])
                 .first())
        if not ucert:
            raise web.notfound()
        web.header('Content-Type', 'application/text')
        web.header('Content-Length', len(ucert.key))
        return ucert.key


class GetOpenVPNZip:

    def GET(self):
        ucert = (web.ctx.orm.query(UserCert)
                 .filter_by(user_id = web.ctx.session['user_id'])
                 .first())
        if not ucert:
            raise web.notfound()

        zip_body = openvpn.mkzip(
            '{}@scalegenomics.tblk'.format(web.ctx.session['username']),
            web.config['SG_SETTINGS']['MAC_OPENVPN_CONFIG'],
            web.config['SG_SETTINGS']['TA_KEY'],
            web.config['SG_SETTINGS']['CA_CRT'],
            ucert.crt,
            ucert.key)

        web.header('Content-Type', 'application/zip')
        web.header('Content-Length', len(zip_body))
        return zip_body


class ShowLabDashboard:

    @has_relations("owner", "member")
    @as_html("my/show_lab_dashboard.html")
    def GET(self, lab_id, **kw):
        return {"cpage":    "mylabs",
                "labpage":  "dashboard",
                "lab":      kw["lab"],
                "u2lab":    kw["u2l"],
                "c_plan":   kw["c_plan"]}


class LabSettings:

    @has_relations('owner')
    @as_html('my/show_lab_settings.html')
    def GET(self, lab_id, **kw):
        '''Show edit lab settings form.'''
        lab_type = 0
        for key, val in enumerate(web.config['SG_SETTINGS']['LAB_TYPE']):
            if kw['lab'].type == val:
                lab_type = key
        form = LabForm(FDict({
            'name' : kw['lab'].name,
            'type' : lab_type}))
        return {'cpage'   : 'mylabs',
                'labpage' : 'settings',
                'lab'     : kw['lab'],
                'u2lab'   : kw['u2l'],
                'c_plan'  : kw['c_plan'],
                'form'    : form}

    @has_relations('owner')
    @as_html('my/show_lab_settings.html')
    def POST(self, lab_id, **kw):
        '''Edit settings form handler.'''
        response = {'cpage'   : 'mylabs',
                    'labpage' : 'settings',
                    'lab'     : kw['lab'],
                    'u2lab'   : kw['u2l'],
                    'c_plan'  : kw['c_plan'] }
        params = FDict(web.input())
        form = LabForm(params)
        if not form.validate():
            response['form'] = form
            return response

        type_id = int(form.type.data)
        kw['lab'].name = form.name.data
        kw['lab'].type = web.config['SG_SETTINGS']['LAB_TYPE'][type_id]

        response['message'] = 'Saved'
        response['form'] = form
        return response


class ShowLabMembers:

    @has_relations('owner', 'member')
    @as_html("my/show_lab_members.html")
    def GET(self, lab_id, **kw):
        users = (web.ctx.orm.query(User, UserToLab)
                 .filter(User.id == UserToLab.user_id)
                 .filter(UserToLab.lab_id == lab_id)
                 .order_by(UserToLab.perms.desc())
                 .all())
        is_owner = False
        for user, u2lab in users:
            if user.id == web.ctx.session.user_id:
                if u2lab.perms == UserToLab.PERMS["owner"]:
                    is_owner = True
        return {
            'cpage'   : 'mylabs',
            'labpage' : 'members',
            'lab'     : kw['lab'],
            'u2lab'   : kw['u2l'],
            'c_plan'  : kw['c_plan'],
            'is_owner': is_owner,
            'users'   : users
            }


class AddLabMember:

    @has_relations('owner')
    @as_json
    def POST(self, lab_id, **kw):
        params = web.input()
        if 'email' not in params:
            return {'result'  : 'error'}

        email = pat_em.findall(params['email'].strip())
        if not email:
            return {'result'  : 'error',
                    'message' : 'Wrong email'}

        user = (web.ctx.orm.query(User)
                    .filter_by(email = email[0][0].lower())
                    .first())
        if not user:
            return {'result'  : 'error',
                    'message' : 'No user with this email'}

        u2l_check = (web.ctx.orm.query(UserToLab)
                        .filter_by(user_id = user.id)
                        .filter_by(lab_id = kw['lab'].id)
                        .first())
        if u2l_check:
            return {'result'  : 'error',
                    'message' : 'Relation already exists'}

        u2l = UserToLab(lab_id  = kw['lab'].id,
                        user_id = user.id,
                        perms   = UserToLab.PERMS['member'])
        web.ctx.orm.add(u2l)

        my_event = MyEvent(
            user_id      = web.ctx.session.user_id,
            full_name    = web.ctx.session.full_name,
            lab_id       = kw['lab'].id,
            lab_name     = kw['lab'].name,
            event_type = MyEvent.TYPES['user added to lab'],
            event_object_type = MyEvent.OBJ_TYPES['user'],
            event_object_id   = user.id,
            event_object_name = user.full_name)
        web.ctx.orm.add(my_event)

        send_mail('user added to lab', mail_to=user.email,
                                       owner_url=web.ctx.session.user_abs_url,
                                       owner_name=web.ctx.session.full_name,
                                       lab_url=kw['lab'].abs_url,
                                       lab_name=kw['lab'].name)


        return {'result' : 'ok', 'user' : {'id'       : user.id,
                                           'gravatar' : user.gravatar,
                                           'full_name': user.full_name,
                                           'email'    : user.email}}


class ShowLabUpdates:

    @has_relations('owner', 'member')
    @as_html('my/show_lab_updates.html')
    def GET(self, lab_id, **kw):

        return {'cpage'   : 'mylabs',
                'labpage' : 'updates',
                'lab'     : kw['lab'],
                'u2lab'   : kw['u2l'],
                'c_plan'  : kw['c_plan'] }


class GetLabUpdates:

    @has_relations('owner', 'member')
    @as_json
    def GET(self, lab_id, **kw):

        params = web.input()
        start = 0
        if 'start' in params and params['start'].isdigit():
            start = int(params['start'])

        events = (web.ctx.orm.query(MyEvent)
                  .filter_by(lab_id = lab_id)
                  .order_by(MyEvent.created.desc())
                  .all())[start:start+25]

        return [ {'data' : filters.datetimeformat(event.created),
                  'text' : event.text } for event in events ]


class RequestActivation:

    @has_relations('owner')
    @as_json
    def POST(self, lab_id, **kw):
        if kw['lab'].status == Lab.STATUS['not verified']:
            kw['lab'].status = Lab.STATUS['verification']

            mail_to = ','.join(web.config['SG_SETTINGS']['MAIL_TO']['INFO'])
            send_mail('lab activation',
                      mail_to=mail_to,
                      lab_url=kw['lab'].root_abs_url,
                      lab_name=kw['lab'].name,
                      owner_url=web.ctx.session['user_abs_url'],
                      owner_name=web.ctx.session['full_name'],
                      owner_organization=kw['user'].organization,
                      owner_department=kw['user'].department,
                      owner_title=kw['user'].title,
                      lab_plan='{} : {}'.format(kw['c_plan'].name_str,
                                                kw['c_plan'].price_str))

            return {'result' : 'ok'}
        return {'result' : 'error'}


class GetLabLimits:

    @has_relations('owner', 'members')
    @as_json
    def GET(self, lab_id, **kw):
        cpu = kw["lab"].cpu_used_proc
        if cpu > 100:
            cpu = 100
        elif cpu < 0:
            cpu = 0
        ram = kw["lab"].ram_used_proc
        if  ram > 100:
            ram = 100
        elif ram < 0:
            ram = 0
        sto = kw["lab"].stor_used_proc
        if sto > 100:
            sto = 100
        elif sto < 0:
            sto = 0
        return {
            "cpu_proc":     "{}%".format(cpu),
            "ram_proc":     "{}%".format(ram),
            "storage_proc": "{}%".format(sto),
            "cpu_text":     "{} / {}".format(kw['lab'].cpu_used,
                                             kw['lab'].cpu_limit),
            "ram_text":     "{} / {} GB".format(kw['lab'].ram_used_gb_str,
                                                kw['lab'].ram_limit_gb),
            "storage_text": "{} / {} TB".format(kw['lab'].storages_used_tb_str,
                                                kw['lab'].storages_limit_tb)}

class ShowLabServers:

    @has_relations('owner', 'member')
    @as_html('my/show_lab_servers.html')
    def GET(self, lab_id, **kw):

        query= (web.ctx.orm.query(User, KVMServer, LabProject)
                .filter(KVMServer.lab_id == lab_id)
                .filter(KVMServer.user_id == User.id)
                .filter(KVMServer.project_id == LabProject.id)
                .filter(KVMServer.status != KVMServer.STATUS['REMOVING'])
                .filter(KVMServer.status != KVMServer.STATUS['REMOVED'])
                .first())

        return {
            'cpage'     : 'mylabs',
            'labpage'   : 'servers',
            'lab'       : kw['lab'],
            'u2lab'     : kw['u2l'],
            'c_plan'    : kw['c_plan'],
            'servers'   : True if query else False,
            'message'   : get_flash_message()
            }


class ShowLabServersList:
    'Class for jqGrid.'

    @has_relations('owner', 'member')
    @as_json
    def GET(self, lab_id, **kw):

        params = web.input()
        # sidx - sorting field name
        # sord - Sorting ORDer
        if 'sidx' not in params or 'sord' not in params:
            return {'result'  : 'error',
                    'message' : 'missing params'}
        project = None
        if 'project_id' in params:
            project = (web.ctx.orm.query(LabProject)
                       .filter_by(lab_id = lab_id)
                       .filter_by(id = params['project_id'])
                       .first())

        flds = { 'name'       : KVMServer.name,
                 'project'    : LabProject.name,
                 'status'     : KVMServer.status,
                 'ip'         : KVMServer.ip,
                 'cpu'        : KVMServer.cpu,
                 'ram'        : KVMServer.memory,
                 'created_by' : User.first_name }

        if params['sidx'] not in flds or params['sord'] not in ['asc', 'desc']:
            return {'result'  : 'error',
                    'message' : 'wrong param'}

        if 'asc' == params['sord']:
            order = flds[params['sidx']]
        else:
            order = flds[params['sidx']].desc()

        query= (web.ctx.orm.query(User, KVMServer, LabProject)
                .filter(KVMServer.lab_id == lab_id)
                .filter(KVMServer.user_id == User.id)
                .filter(KVMServer.project_id == LabProject.id)
                .filter(KVMServer.status != KVMServer.STATUS['REMOVING'])
                .filter(KVMServer.status != KVMServer.STATUS['REMOVED'])
                .order_by(order))
        if project:
            query = (query.filter(LabProject.id == project.id).all())
        else:
            query = query.all()

        result = []
        if project:
            u='<a href="#" onclick="ppane.showServer({}); return false;">{}</a>'
            for user, server, project in query:
                result.append({
                        'id'   : server.id,
                        'cell' : [u.format(server.id, server.name),
                                  server.status_str,
                                  server.ip_str,
                                  server.cpu,
                                  filters.sizeformat(server.memory),
                                  user.a_tag]})
        else:
            for user, server, project in query:
                result.append({
                        'id'   : server.id,
                        'cell' : [server.a_tag,
                                  project.a_tag,
                                  server.status_str,
                                  server.ip_str,
                                  server.cpu,
                                  filters.sizeformat(server.memory),
                                  user.a_tag]})

        return {'page'    : '1',
                'total'   : len(result),
                'records' : len(result),
                'rows'    : result }


class ShowLabServersStatus:

    @has_relations('owner', 'member')
    @as_json
    def GET(self, lab_id, **kw):
        'Return lab servers id and status.'
        servers = (web.ctx.orm.query(KVMServer)
                   .filter_by(lab_id = lab_id)
                   .all())

        return [{'id':     server.id,
                 'status': server.status_str} for server in servers]


class NewServer:

    @has_relations('owner', 'member')
    @as_html('my/new_server.html')
    def GET(self, lab_id, **kw):
        'Display new kvm server form.'

        projects = get_lab_projects(lab_id)

        cservs = get_cservs_data(lab_id)
        cpu_lim, mem_lim, cpu_block, mem_block = get_server_limits(kw["lab"])
        cservs_info, free_space = get_cservs_info(cservs, cpu_lim, mem_lim)

        return {'cpage':       'mylabs',
                'labpage':     'servers',
                'lab':         kw['lab'],
                'u2lab':       kw['u2l'],
                'c_plan':      kw['c_plan'],
                'projects':    projects,
                'free_space':  free_space,
                'cpu_block':   cpu_block,
                'mem_block':   mem_block,
                'cservs':      cservs,
                'cservs_info': json.dumps(cservs_info),
                'images':      KVMServer.IMAGES,
                'form':        ServerForm(projects=projects)}

    @has_relations('owner', 'member')
    @as_json
    def POST(self, lab_id, **kw):
        '''Create new kvm server.
            * validate form data
            * validate lab limits
            * validate resources
            * create new server
            * event
        '''

        if kw['lab'].status != Lab.STATUS['active']:
            return {'result':  'error',
                    'message': 'Lab is not active'}

        params = FDict(web.input())
        if 'project' not in params:
            return {'result': 'error'}

        project = (web.ctx.orm.query(LabProject)
                   .filter(LabProject.id == UserToProject.project_id)
                   .filter(UserToProject.user_id == web.ctx.user.id)
                   .filter(LabProject.lab_id == kw['lab'].id)
                   .filter(UserToProject.relation ==
                           UserToProject.REL_TYPES['member'])
                   .filter(LabProject.id == params['project'])
                   .first())
        if not project:
            return {'result':  'error',
                    'message': 'Unknown project'}
        form = ServerForm(formdata=params, projects=[project])
        if not form.validate():
            return {'result': 'error'}

        cservs = get_cservs_data(lab_id)
        cpu_lim, mem_lim, cpu_block, mem_block = get_server_limits(kw["lab"])
        cservs_info, free_space = get_cservs_info(cservs, cpu_lim, mem_lim)
        if cpu_block:
            return {"result":  "error",
                    "message": "You have reached your lab's CPU limit."}
        if mem_block:
            return {"result":  "error",
                    "message": "You have reached your lab's RAM limit."}

        cserver_id = None
        if 'cserver' in params and params['cserver'].isdigit():
            cserver_id = int(params['cserver'])

        cserver, cdisk = get_lab_resources(lab_id,
                                           cserver_id,
                                           KVMServer.DEFAULT_SIZE)
        if not cdisk:
            return {'result':  'error',
                    'message': 'no free space'}
        if not cserver:
            return {'result':  'error',
                    'message': 'it happens'}

        # creating
        server = KVMServer(server_id   = cserver.id,
                           user_id     = web.ctx.user.id,
                           lab_id      = kw['lab'].id,
                           project_id  = project.id,
                           name        = form.name.data,
                           description = cgi.escape(form.description.data),
                           status      = KVMServer.STATUS['CREATING'],
                           cpu         = form.cpu.data,
                           memory      = form.ram.data,
                           image       = form.image.data,
                           username    = web.ctx.user.username,
                           password    = web.ctx.user.lnx_password,
                           cpulimit    = KVMServer.CPULIMITS['normal'])
        web.ctx.orm.add(server)
        web.ctx.orm.commit()

        web.ctx.orm.add(KVMServerUsageLog.init(server))

        my_event = MyEvent(user_id           = web.ctx.user.id,
                           full_name         = web.ctx.user.full_name,
                           lab_id            = kw['lab'].id,
                           lab_name          = kw['lab'].name,
                           project_id        = project.id,
                           project_name      = project.name,
                           event_type        = MyEvent.TYPES['create server'],
                           event_object_type = MyEvent.OBJ_TYPES['server'],
                           event_object_id   = server.id,
                           event_object_name = server.name)
        web.ctx.orm.add(my_event)

        return {"result":     "ok",
                "server_id":  str(server.id)}


class EditServer:

    def get_server(self, lab, server_id, u2lab):

        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab.id)
                  .filter_by(user_id = web.ctx.user.id)
                  .filter(KVMServer.status < KVMServer.STATUS['REMOVING'])
                  .first())
        if not server:
            raise web.seeother(lab.abs_url)

        return server

    @has_relations("owner", "member")
    @as_html("my/edit_server.html")
    def GET(self, lab_id, server_id, **kw):
        "Show kvm server edit form."

        server = self.get_server(kw["lab"], server_id, kw["u2l"])

        cservs = get_cservs_data(lab_id)
        cpu_lim, mem_lim, cpu_block, mem_block = get_server_limits(kw["lab"])
        cservs_info, free_space = get_cservs_info(cservs, cpu_lim, mem_lim)

        cpu_lim = None
        mem_lim = None
        for cserv_id, cpu, mem, sz in cservs_info:
            if cserv_id == server.server_id:
                cpu_lim = cpu
                mem_lim = mem
                break

        return {"cpage":      "mylabs",
                "labpage":    "servers",
                "lab":        kw["lab"],
                "u2lab":      kw["u2l"],
                "c_plan":     kw["c_plan"],
                "server":     server,
                "cpu_lim":    cpu_lim,
                "mem_lim":    mem_lim,
                "dheight":    get_dheight(server.description),
                "cpulimits":  KVMServer.CPULIMITS}

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, server_id, **kw):
        "Edit server form handler."
        params = web.input()
        if not ("name"        in params and
                "description" in params and
                "cpu"         in params and
                "memory"      in params):
            return {"result":  "error",
                    "message": "1"}

        name = re_server_name.findall(params.name.strip())
        if not name:
            return {'result':  'error',
                    'message': '2'}
        name = name[0]

        if not params['cpu'].isdigit():
            return {'result':  'error',
                    'message': '3'}
        cpu = int(params['cpu'])
        if cpu != 0:
            if (cpu < web.config['SG_SETTINGS']['KVM_CPU_LIMITS'][0] or
                cpu > web.config['SG_SETTINGS']['KVM_CPU_LIMITS'][1]):
                return {'result':  'error',
                        'message': '4'}

        if not params['memory'].isdigit():
            return {'result':  'error',
                    'message': '5'}
        memory = int(params['memory'])
        if memory != 0:
            if memory not in web.config['SG_SETTINGS']['KVM_MEMORY_LIST']:
                return {'result':  'error',
                        'message': '6'}

        server = self.get_server(kw['lab'], server_id, kw['u2l'])
        server.name        = name
        server.description = cgi.escape(params.description)

        if (server.status == KVMServer.STATUS['STOPPED'] and
            cpu != 0 and memory != 0):
            if server.cpu != cpu or server.memory != memory:

                free_cpu = kw['lab'].cpu_limit - kw['lab'].cpu_used
                inc_cpu  = cpu - server.cpu
                if inc_cpu > free_cpu:
                    return {'result':  'error',
                            'message': '8'}

                sul = (web.ctx.orm.query(KVMServerUsageLog)
                       .filter_by(server_id = server.id)
                       .filter_by(changed = None)
                       .first())
                sul.changed = datetime.utcnow()

                server.cpu    = cpu
                server.memory = memory
                web.ctx.orm.add(KVMServerUsageLog.init(server))

        if 'without-flash' not in params:
            set_flash_message('Server updated')

        return {'result': 'ok'}


class ShowServer:

    @has_relations('owner', 'member')
    @as_html('my/show_server.html')
    def GET(self, lab_id, server_id, **kw):

        query = (web.ctx.orm.query(User, KVMServer)
                 .filter(KVMServer.id == server_id)
                 .filter(KVMServer.lab_id == lab_id)
                 .filter(KVMServer.user_id == User.id)
                 .first())
        if not query:
            raise web.seeother(kw['lab'].abs_url)
        owner, server = query

        project = (web.ctx.orm.query(LabProject)
                   .filter_by(id = server.project_id)
                   .first())

        server_owner, block_pane, block_destroy = server.get_access(**kw)

        return {
            'cpage'   : 'mylabs',
            'labpage' : 'servers',
            'lab'     : kw['lab'],
            'u2lab'   : kw['u2l'],
            'c_plan'  : kw['c_plan'],
            'server'  : server,
            'owner'   : owner,
            'project' : project,
            'message' : get_flash_message(),

            'server_owner' : server_owner,
            'block_pane'   : block_pane,
            'block_destroy': block_destroy,
            }


class StartServer:

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, server_id, **kw):
        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab_id)
                  .first())
        if not server:
            return {"result":  "error",
                    "message": "unknown server"}
        if server.status != server.STATUS["STOPPED"]:
            return {"result":   "error",
                    "message":  "server is not stopped"}
        user = None
        if server.user_id != web.ctx.user.id:
            if not web.ctx.user.is_staff:
                return {"result":  "error",
                        "message": "access denied"}
            else:
                user = get_system_user()
        else:
            user = web.ctx.user

        query = (web.ctx.orm.query(LabProject, UserToProject)
                 .filter(LabProject.id == UserToProject.project_id)
                 .filter(LabProject.id == server.project_id)
                 .filter(UserToProject.user_id == server.user_id)
                 .first())
        if not query:
            return {"result":  "error",
                    "message": "unknown project"}
        project, u2proj = query

        if not kw["lab"].check_server(server):
            return {"result":  "error",
                    "message": "no avaible resources"}

        server.status = KVMServer.STATUS["STARTING"]
        server.is_prepared = False

        my_event = MyEvent(user_id           = user.id,
                           full_name         = user.full_name,
                           lab_id            = kw["lab"].id,
                           lab_name          = kw["lab"].name,
                           project_id        = project.id,
                           project_name      = project.name,
                           event_type        = MyEvent.TYPES["start server"],
                           event_object_type = MyEvent.OBJ_TYPES["server"],
                           event_object_id   = server.id,
                           event_object_name = server.name)
        web.ctx.orm.add(my_event)

        return {"result": "ok"}


class StopServer:

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, server_id, **kw):
        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab_id)
                  .first())
        if not server:
            return {"result":  "error",
                    "message": "unknown server"}
        if server.status != server.STATUS["RUNNING"]:
            return {"result":  "error",
                    "message": "server is not running"}
        user = None
        if server.user_id != web.ctx.user.id:
            if not web.ctx.user.is_staff:
                return {"result":  "error",
                        "message": "access denied"}
            else:
                user = get_system_user()
        else:
            user = web.ctx.user

        query = (web.ctx.orm.query(LabProject, UserToProject)
                 .filter(LabProject.id == UserToProject.project_id)
                 .filter(LabProject.id == server.project_id)
                 .filter(UserToProject.user_id == server.user_id)
                 .first())
        if not query:
            return {"result":  "error",
                    "message": "unknown project"}
        project, u2porj = query

        server.status = KVMServer.STATUS["STOPPING"]

        my_event = MyEvent(user_id           = user.id,
                           full_name         = user.full_name,
                           lab_id            = kw["lab"].id,
                           lab_name          = kw["lab"].name,
                           project_id        = project.id,
                           project_name      = project.name,
                           event_type        = MyEvent.TYPES["stop server"],
                           event_object_type = MyEvent.OBJ_TYPES["server"],
                           event_object_id   = server.id,
                           event_object_name = server.name)
        web.ctx.orm.add(my_event)

        return {"result": "ok"}


class RestartKVMServer:

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, server_id, **kw):
        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab_id)
                  .first())
        if not server:
            return {"result":   "error",
                    "message":  "unknown server"}
        if server.status != server.STATUS["RUNNING"]:
            return {"result":   "error",
                    "message":  "server is not running"}
        user = None
        if server.user_id != web.ctx.user.id:
            if not web.ctx.user.is_staff:
                return {"result":  "error",
                        "message": "access denied"}
            else:
                user = get_system_user()
        else:
            user = web.ctx.user

        query = (web.ctx.orm.query(LabProject, UserToProject)
                 .filter(LabProject.id == UserToProject.project_id)
                 .filter(LabProject.id == server.project_id)
                 .filter(UserToProject.user_id == server.user_id)
                 .first())
        if not query:
            return {"result":  "error",
                    "message": "unknown project"}
        project, u2porj = query

        server.status = KVMServer.STATUS["RESTARTING"]

        my_event = MyEvent(user_id           = user.id,
                           full_name         = user.full_name,
                           lab_id            = kw["lab"].id,
                           lab_name          = kw["lab"].name,
                           project_id        = project.id,
                           project_name      = project.name,
                           event_type        = MyEvent.TYPES["restart server"],
                           event_object_type = MyEvent.OBJ_TYPES["server"],
                           event_object_id   = server.id,
                           event_object_name = server.name)
        web.ctx.orm.add(my_event)

        return {"result": "ok"}


class ShowServerStatus:

    @has_relations("owner", "member")
    @as_json
    def GET(self, lab_id, server_id, **kw):
        "Return server status."
        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(lab_id = lab_id)
                  .filter_by(id = server_id)
                  .first())
        if not server:
            return {"result": "error"}

        rels = (web.ctx.orm.query(KVMStorage, KVMMountRelation)
                .filter(KVMMountRelation.storage_id == KVMStorage.id)
                .filter(KVMMountRelation.server_id == server.id)
                .all())
        mounts = []
        for stor, rel in rels:
            mounts.append({
                "storage_id":  stor.id,
                "storage_url": stor.url,
                "status":      rel.status_str,
            })

        return {"result":  "ok",
                "status":  server.status_str,
                "ip":      server.ip,
                "mounts":  mounts}


class DestroyServer:

    @has_relations('owner', 'member')
    @as_json
    def POST(self, lab_id, server_id, **kw):

        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab_id)
                  .first())
        if not server:
            return {'result':  'error',
                    'message': 'unknown server'}

        if (server.status == KVMServer.STATUS['REMOVING'] or
            server.status == KVMServer.STATUS['REMOVED']):
            return {'result':  'error',
                    'message': 'wrong status'}

        project = (web.ctx.orm.query(LabProject)
                   .filter(LabProject.id == UserToProject.project_id)
                   .filter(LabProject.id == server.project_id)
                   .filter(UserToProject.user_id == web.ctx.session.user_id)
                   .first())
        if not project:
            return {'result':  'error',
                    'message': 'unknown project'}

        rels = (web.ctx.orm.query(KVMStorage, KVMMountRelation)
                .filter(KVMMountRelation.storage_id == KVMStorage.id)
                .filter(KVMMountRelation.server_id == server.id)
                .all())
        for storage, rel in rels:
            if storage.type == storage.TYPES["LOCAL"]:
                web.ctx.orm.delete(rel)
            else:
                # TODO: test me
                rel.status      = rel.STATUS["UMOUNTING"]
                rel.real_status = rel.REAL_STATUS["UMOUNTING DISK"]

        server.status = KVMServer.STATUS["REMOVING"]

        server_usage = (web.ctx.orm.query(KVMServerUsageLog)
                        .filter_by(server_id = server.id)
                        .filter_by(changed = None)
                        .first())
        server_usage.changed = datetime.utcnow()

        my_event = MyEvent(user_id           = web.ctx.session.user_id,
                           full_name         = web.ctx.session.full_name,
                           lab_id            = kw['lab'].id,
                           lab_name          = kw['lab'].name,
                           project_id        = project.id,
                           project_name      = project.name,
                           event_type        = MyEvent.TYPES['destroy server'],
                           event_object_type = MyEvent.OBJ_TYPES['server'],
                           event_object_id   = server.id,
                           event_object_name = server.name)
        web.ctx.orm.add(my_event)

        set_flash_message('Server destroyed')
        return {'result': 'ok'}


class ShowServerLog:

    @has_relations('owner', 'member')
    @as_html('my/_show_project_server_log.html')
    def GET(self, lab_id, server_id, **kw):

        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(lab_id = lab_id)
                  .filter_by(id = server_id)
                  .first())
        if not server: return {}

        events = (web.ctx.orm.query(MyEvent)
                  .filter_by(lab_id = lab_id)
                  .filter_by(project_id = server.project_id)
                  .filter(or_(MyEvent.event_object_type ==
                              MyEvent.OBJ_TYPES['server'],
                              MyEvent.event_object_2_type ==
                              MyEvent.OBJ_TYPES['server']))
                  .filter(or_(MyEvent.event_object_id == server_id,
                              MyEvent.event_object_2_id == server_id))
                  .order_by('-created')
                  .all())
        response = { 'lab_id'    : lab_id,
                     'server_id' : server_id,
                     'events'    : events }
        params = web.input()
        if 'project_id' in params:
            response['project_id'] = params['project_id']

        return response


class ShowLabStorages:

    @has_relations('owner', 'member')
    @as_html('my/show_lab_storages.html')
    def GET(self, lab_id, **kw):

        query = (web.ctx.orm.query(User, KVMStorage, LabProject)
                 .filter(KVMStorage.lab_id == lab_id)
                 .filter(KVMStorage.user_id == User.id)
                 .filter(KVMStorage.project_id == LabProject.id)
                 .filter(KVMStorage.status != KVMStorage.STATUS['REMOVING'])
                 .filter(KVMStorage.status != KVMStorage.STATUS['REMOVED'])
                 .order_by(LabProject.name)
                 .first())

        return { 'cpage'   : 'mylabs',
                 'labpage' : 'disks',
                 'lab'     : kw['lab'],
                 'u2lab'   : kw['u2l'],
                 'c_plan'  : kw['c_plan'],
                 'storages': True if query else False,
                 'message' : get_flash_message() }

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, **kw):
        "Return status."
        storages = (web.ctx.orm.query(KVMStorage)
                    .filter_by(lab_id = kw["lab"].id)
                    .all())
        return [{"id":      storage.id,
                 "status":  storage.status_str } for storage in storages]


class ShowLabStoragesList:
    'Class for jqGrid.'

    @has_relations('owner', 'member')
    @as_json
    def GET(self, lab_id, **kw):

        params = web.input()
        # sidx - sorting field name
        # sord - Sorting ORDer
        if 'sidx' not in params or 'sord' not in params:
            return {'result':  'error',
                    'message': 'missing params'}
        project = None
        if 'project_id' in params:
            project = (web.ctx.orm.query(LabProject)
                       .filter_by(lab_id = lab_id)
                       .filter_by(id = params['project_id'])
                       .first())

        flds = {'name':       KVMStorage.name,
                'mname':      KVMStorage.alias,
                'project':    LabProject.name,
                'status':     KVMStorage.status,
                'size':       KVMStorage.size,
                'type':       KVMStorage.type,
                'created_by': User.first_name}

        if params['sidx'] not in flds or params['sord'] not in ['asc', 'desc']:
            return {'result':  'error',
                    'message': 'wrong param'}

        if params['sidx'] != 'mounted_to':
            if 'asc' == params['sord']:
                order = flds[params['sidx']]
            else:
                order = flds[params['sidx']].desc()
        else:
            order = KVMStorage.name

        query = (web.ctx.orm.query(User, KVMStorage, LabProject)
                 .filter(KVMStorage.lab_id == lab_id)
                 .filter(KVMStorage.user_id == User.id)
                 .filter(KVMStorage.project_id == LabProject.id)
                 .filter(KVMStorage.status != KVMStorage.STATUS['REMOVING'])
                 .filter(KVMStorage.status != KVMStorage.STATUS['REMOVED'])
                 .order_by(order))
        if project:
            query = (query.filter(LabProject.id == project.id).all())
        else:
            query = query.all()

        disks = []
        for user, storage, pro in query:
            rel = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                   .filter(KVMMountRelation.server_id == KVMServer.id)
                   .filter(KVMMountRelation.storage_id == storage.id)
                   .first())
            if rel:
                disks.append((user, storage, pro, rel[0]))
            else:
                disks.append((user, storage, pro, None))

        if params['sidx'] == 'mounted_to':
            def _serv_compare(x, y):
                x, y = x[3], y[3]
                if x != None and y == None:
                    return 1
                if x == None and y == None:
                    return 0
                if x == None and y != None:
                    return -1
                if x.id > y.id:
                    return 1
                elif x.id == y.id:
                    return 0
                else:
                    return -1
            disks = sorted(disks, _serv_compare)

        result = []
        if project:
            u = ('<a href="#" onclick="ppane.showStorage({});return false;">'
                 '{}</a>')
            for user, storage, pro, rel in disks:
                cell = [u.format(storage.id, storage.name),
                        storage.alias,
                        storage.status_str,
                        filters.sizeformat(storage.size),
                        storage.type_str,
                        user.a_tag]
                result.append({'id':   storage.id,
                               'cell': cell})
        else:
            for user, storage, pro, rel in disks:
                cell = [storage.a_tag,
                        storage.alias,
                        pro.a_tag,
                        storage.status_str,
                        filters.sizeformat(storage.size),
                        storage.type_str,
                        user.a_tag]
                result.append({'id':   storage.id,
                               'cell': cell})

        return {'page':    '1',
                'total':   len(result),
                'records': len(result),
                'rows':    result}


class ShowStorage:

    @has_relations("owner", "member")
    @as_html("my/show_storage.html")
    def GET(self, lab_id, storage_id, **kw):
        query = (web.ctx.orm.query(User, KVMStorage)
                 .filter(KVMStorage.lab_id == lab_id)
                 .filter(KVMStorage.id == storage_id)
                 .filter(KVMStorage.user_id == User.id)
                 .first())
        if not query:
            raise web.notfound()
        owner, storage = query

        project = (web.ctx.orm.query(LabProject)
                   .filter_by(id = storage.project_id)
                   .first())

        rel = None
        if storage.type == storage.TYPES["LOCAL"]:
            rel = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMServer.id == KVMMountRelation.server_id)
                .filter(storage.id == KVMMountRelation.storage_id)
                .first())

        if storage.user_id == web.ctx.user.id:
            storage_owner = True
        else:
            storage_owner = False

        return {
            "cpage":         'mylabs',
            "labpage":       'disks',
            "lab":           kw['lab'],
            "u2lab":         kw['u2l'],
            "c_plan":        kw['c_plan'],
            "project":       project,
            "storage":       storage,
            "owner":         owner,
            "rel":           rel,
            "storage_owner": storage_owner,
            "message":       get_flash_message()
            }


class ShowStorageStatus:

    @has_relations('owner', 'member')
    @as_json
    def GET(self, lab_id, storage_id, **kw):
        storage = (web.ctx.orm.query(KVMStorage)
                   .filter_by(lab_id = lab_id)
                   .filter_by(id = storage_id)
                   .first())
        if not storage:
            return {'result' : 'error'}
        else:
            return { 'result' : 'ok',
                     'status' : storage.status_str }


class EditStorage:

    @has_relations("owner", "member")
    @as_html('my/edit_storage.html')
    def GET(self, lab_id, storage_id, **kw):
        "Show edit storage form."
        storage = (web.ctx.orm.query(KVMStorage)
                   .filter_by(id = storage_id)
                   .filter_by(lab_id = lab_id)
                   .filter_by(user_id = web.ctx.user.id)
                   .first())
        if not storage:
            raise web.seeother(kw["lab"].abs_url)
        if storage.status != KVMStorage.STATUS['CREATED']:
            raise web.seeother(kw["lab"].abs_url)

        can_edit = storage_editable(storage)
        projects = get_lab_projects(lab_id)

        return {"cpage":      "mylabs",
                "labpage":    "disks",
                "lab":        kw["lab"],
                "u2lab":      kw["u2l"],
                "c_plan":     kw["c_plan"],
                "storage":    storage,
                "can_edit":   can_edit,
                "dheight":    get_dheight(storage.description),
                "projects":   projects}

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, storage_id, **kw):
        "Update storage info."
        params = web.input()
        if not ("name"        in params and
                "alias"       in params and
                "description" in params and
                "type"        in params and
                "project"     in params):
            return {"result": "error"}

        name = re_storage_name.findall(params["name"])
        if not name:
            return {"result":  "error",
                    "message": "wrong name"}
        name = name[0]

        storage = (web.ctx.orm.query(KVMStorage)
                   .filter_by(id = storage_id)
                   .filter_by(lab_id = lab_id)
                   .first())
        if not storage:
            return {"result":  "error",
                    "message": "unknown storage"}
        if storage.user_id != web.ctx.user.id:
            return {"result":  "error",
                    "message": ""}
        can_edit = storage_editable(storage)
        if can_edit:
            alias = KVMStorage.get_alias(params["alias"], name)
            if alias != storage.alias:
                alias_check = (web.ctx.orm.query(KVMStorage)
                            .filter_by(lab_id = lab_id)
                            .filter_by(alias = alias)
                            .first())
                if alias_check and alias_check.id != storage.id:
                    return {"result":  "error",
                            "message": "wrong alias"}
                storage.alias = alias

        if not params["type"].isdigit():
            return {"result":  "error",
                    "message": "wrong type"}
        stype = int(params["type"])
        if stype not in KVMStorage.TYPES.values():
            return {"result":  "error",
                    "message": "unknown type"}

        project = (web.ctx.orm.query(LabProject)
                   .filter(LabProject.id == UserToProject.project_id)
                   .filter(LabProject.lab_id == lab_id)
                   .filter(UserToProject.user_id == web.ctx.user.id)
                   .filter(UserToProject.relation ==
                           UserToProject.REL_TYPES["member"])
                   .filter(LabProject.id == params["project"])
                   .first())
        if not project:
            return {"result":  "error",
                    "message": "unknown project"}

        # previous
        project_changed = (True if storage.project_id != project.id
                                else False)
        pre_project_id = storage.project_id

        # updating
        storage.name        = name
        storage.description = cgi.escape(params['description'])
        if can_edit:
            storage.project_id = project.id
            storage.type = stype

        if project_changed:
            pre_project = (web.ctx.orm.query(LabProject)
                           .filter_by(id = pre_project_id)
                           .filter_by(lab_id = lab_id)
                           .first())
            my_event = MyEvent(
                user_id    = web.ctx.user.id,
                full_name  = web.ctx.user.full_name,

                lab_id       = kw['lab'].id,
                lab_name     = kw['lab'].name,
                project_id   = pre_project.id,
                project_name = pre_project.name,

                event_type = MyEvent.TYPES['remove storage from proj'],
                event_object_type = MyEvent.OBJ_TYPES['storage'],
                event_object_id   = storage.id,
                event_object_name = storage.name)
            web.ctx.orm.add(my_event)

            my_event = MyEvent(
                user_id    = web.ctx.session.user_id,
                full_name  = web.ctx.session.full_name,

                lab_id       = kw['lab'].id,
                lab_name     = kw['lab'].name,
                project_id   = project.id,
                project_name = project.name,

                event_type        = MyEvent.TYPES['add storage to proj'],
                event_object_type = MyEvent.OBJ_TYPES['storage'],
                event_object_id   = storage.id,
                event_object_name = storage.name)
            web.ctx.orm.add(my_event)

        if 'without-flash' not in params:
            set_flash_message('Disk updated')

        return {'result': 'ok'}


class DestroyStorage:

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, storage_id, **kw):
        storage = (web.ctx.orm.query(KVMStorage)
                   .filter_by(id = storage_id)
                   .filter_by(lab_id = lab_id)
                   .filter_by(status = KVMStorage.STATUS["CREATED"])
                   .filter_by(user_id = web.ctx.user.id)
                   .first())
        if not storage:
            return {"result":  "error",
                    "message": "unknown storage"}

        if storage.type == storage.TYPES["LOCAL"]:
            rels = (web.ctx.orm.query(KVMMountRelation)
                    .filter(KVMMountRelation.storage_id == storage.id)
                    .all())
            for _, rel in rels:
                web.ctx.orm.delete(rel)
        elif storage.type == storage.TYPES["NFS"]:
            shs = (web.ctx.orm.query(Sharing)
                    .filter_by(storage_id = storage.id)
                    .all())
            if shs:
                return {"result":  "error",
                        "message": "remove sharings first"}

        storage.status = KVMStorage.STATUS["REMOVING"]

        storage_usage = (web.ctx.orm.query(KVMStorageUsageLog)
                         .filter_by(storage_id = storage.id)
                         .filter_by(changed = None)
                         .first())
        storage_usage.changed = datetime.utcnow()

        project = (web.ctx.orm.query(LabProject)
                   .filter_by(id = storage.project_id)
                   .first())

        my_event = MyEvent(user_id      = web.ctx.user.id,
                           full_name    = web.ctx.user.full_name,
                           lab_id       = kw["lab"].id,
                           lab_name     = kw["lab"].name,
                           project_id   = project.id,
                           project_name = project.name,
                           event_type   = MyEvent.TYPES["destroy storage"],
                           event_object_type = MyEvent.OBJ_TYPES["storage"],
                           event_object_id   = storage.id,
                           event_object_name = storage.name)
        web.ctx.orm.add(my_event)

        set_flash_message("Disk removed")
        return {"result": "ok"}


class NewStorage:

    @has_relations("owner", "member")
    @as_html("my/new_storage.html")
    def GET(self, lab_id, **kw):
        "Show new kvm storage form."
        projects = get_lab_projects(lab_id)

        lab_limit = LabLimit(kw['lab'])
        cservs = get_cservs_data(lab_id)
        cservs_info = []
        free_space = False
        for cserv, _, _, cdisk in cservs:
            size_limits = lab_limit.get_storage_limits(cdisk.free_size)
            cservs_info.append((cserv.id, size_limits))
            if cdisk.free_size > 0:
                free_space = True

        return {"cpage":         "mylabs",
                "labpage":       "disks",
                "lab":           kw["lab"],
                "u2lab":         kw["u2l"],
                "c_plan":        kw["c_plan"],
                "projects":      projects,
                "form":          StorageForm(projects=projects),
                "storage_types": KVMStorage.TYPES,
                "free_space":    free_space,
                "cservs":        cservs,
                "cservs_info":   json.dumps(cservs_info)}

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, **kw):
        "Create new kvm storage."
        if not kw["lab"].is_status("active"):
            return {"result":  "error",
                    "message": "lab is not active"}

        params = FDict(web.input())
        if not ("name"        in params and
                "alias"       in params and
                "description" in params and
                "size"        in params and
                "type"        in params and
                "project"     in params):
            return {"result":  "error",
                    "message": "wrong request"}

        project = (web.ctx.orm.query(LabProject)
                   .filter(LabProject.id == UserToProject.project_id)
                   .filter(UserToProject.user_id == web.ctx.user.id)
                   .filter(LabProject.lab_id == lab_id)
                   .filter(UserToProject.relation ==
                           UserToProject.REL_TYPES["member"])
                   .filter(LabProject.id == params["project"])
                   .first())
        if not project:
            return {"result": "error"}

        form = StorageForm(formdata=params, projects=[project])
        if not form.validate():
            return {"result":  "error",
                    "message": "form error"}

        alias = KVMStorage.get_alias(params["alias"], form.name.data)
        alias_check = (web.ctx.orm.query(KVMStorage)
                       .filter(KVMStorage.lab_id == lab_id)
                       .filter(KVMStorage.alias == alias)
                       .filter(KVMStorage.status.in_([
                            KVMStorage.STATUS["CREATING"],
                            KVMStorage.STATUS["CREATED"]
                           ]))
                       .first())
        if alias_check:
            return {"result":  "error",
                    "message": "wrong alias"}
        size_mb = form.size.data * 1024

        cserver_id = params.get("cserver", None)
        if cserver_id and cserver_id.isdigit():
            cserver_id = int(cserver_id)
        cserver, cdisk = get_lab_resources(lab_id, cserver_id, form.size.data)
        if not cdisk:
            set_flash_message("Sorry, no free space now")
            return {"result":  "error",
                    "message": "no free space"}
        if not cserver:
            return {"result":  "error",
                    "message": "it happens"}

        if size_mb + kw["lab"].storages_used > kw["lab"].storages_limit:
            msg = "You have reached your lab's limit"
            set_flash_message(msg)
            return {"result": "error",
                    "message": msg}

        storage = KVMStorage(server_id   = cserver.id,
                             user_id     = web.ctx.user.id,
                             lab_id      = kw["lab"].id,
                             project_id  = project.id,
                             name        = form.name.data,
                             alias       = alias,
                             description = desc_wrap(form.description.data),
                             size        = size_mb,
                             type        = form.type.data,
                             status      = KVMStorage.STATUS["CREATING"])
        web.ctx.orm.add(storage)
        web.ctx.orm.commit()

        web.ctx.orm.add(KVMStorageUsageLog.init(storage))

        my_event = MyEvent(user_id      = web.ctx.user.id,
                           full_name    = web.ctx.user.full_name,
                           lab_id       = kw["lab"].id,
                           lab_name     = kw["lab"].name,
                           project_id   = project.id,
                           project_name = project.name,
                           event_type = MyEvent.TYPES["create storage"],
                           event_object_type = MyEvent.OBJ_TYPES["storage"],
                           event_object_id   = storage.id,
                           event_object_name = storage.name)
        web.ctx.orm.add(my_event)

        return {"result":      "ok",
                "redirect_to": storage.url,
                "storage_id":  storage.id}


class JoinToLab:

    @as_json
    def POST(self):
        params = web.input()
        if 'lab_id' not in params:
            return {'result': 'error'}

        query = (web.ctx.orm.query(User, Lab, UserToLab)
                 .filter(UserToLab.lab_id == Lab.id)
                 .filter(UserToLab.user_id == User.id)
                 .filter(UserToLab.perms == UserToLab.PERMS['owner'])
                 .filter(UserToLab.lab_id == params['lab_id'])
                 .first())
        if not query:
            return {'result' : 'error'}

        owner, lab, u2lab = query

        u2l_check = (web.ctx.orm.query(UserToLab)
                     .filter_by(user_id = web.ctx.session.user_id)
                     .filter_by(lab_id  = params['lab_id'])
                     .first())
        if u2l_check:
            return {'result': 'error'}

        u2l = UserToLab(user_id = web.ctx.session.user_id,
                        lab_id  = params['lab_id'],
                        perms   = UserToLab.PERMS['waiting to be approved'])
        web.ctx.orm.add(u2l)
        lab.wait_to_approve += 1

        my_event = MyEvent(
            user_id      = web.ctx.session.user_id,
            full_name    = web.ctx.session.full_name,
            lab_id       = lab.id,
            lab_name     = lab.name,
            event_type = MyEvent.TYPES['waiting to be approved'])
        web.ctx.orm.add(my_event)

        user = (web.ctx.orm.query(User)
                .filter_by(id = web.ctx.session.user_id)
                .first())

        send_mail('join to lab', mail_to=owner.email,
                                 owner_url=owner.abs_url,
                                 owner_name=owner.full_name,
                                 user_url=user.abs_url,
                                 user_name=user.full_name,
                                 user_email=user.email,
                                 lab_url=lab.abs_url,
                                 lab_name=lab.name,
                                 members_url=lab.abs_url+'/members')

        return {'result': 'ok'}


class LeaveLab:

    @as_json
    def POST(self, lab_id):

        query = (web.ctx.orm.query(Lab, User, UserToLab)
                  .filter(UserToLab.user_id == User.id)
                  .filter(UserToLab.lab_id  == Lab.id)
                  .filter(UserToLab.perms   == UserToLab.PERMS['owner'])
                  .filter(Lab.id == lab_id)
                  .first())
        if not query:
            return {'result' : 'error'}
        lab, owner, u2lab = query

        u2l = (web.ctx.orm.query(UserToLab)
               .filter_by(lab_id = lab.id)
               .filter_by(user_id = web.ctx.session.user_id)
               .first())
        if not u2l:
            return {'result' : 'error'}
        if u2l.perms == UserToLab.PERMS['owner']:
            return {'result' : 'error'}

        if u2l.perms == UserToLab.PERMS['waiting to be approved']:
            lab.wait_to_approve -= 1
        web.ctx.orm.delete(u2l)

        my_event = MyEvent(
            user_id      = web.ctx.session.user_id,
            full_name    = web.ctx.session.full_name,
            lab_id       = lab.id,
            lab_name     = lab.name,
            event_type   = MyEvent.TYPES['user leaved lab'])
        web.ctx.orm.add(my_event)

        send_mail('user leaved lab', mail_to=owner.email,
                                     user_url=web.ctx.session.user_abs_url,
                                     user_name=web.ctx.session.full_name,
                                     lab_url=lab.abs_url,
                                     lab_name=lab.name)

        return {'result' : 'ok'}


class ConfirmUser:

    @has_relations('owner')
    @as_json
    def POST(self, lab_id, **kwargs):

        params = web.input()
        if 'user_id' not in params:
            return {'result' : 'error'}

        qr = (web.ctx.orm.query(User, UserToLab, Lab)
              .filter(UserToLab.lab_id == Lab.id)
              .filter(UserToLab.user_id == User.id)
              .filter(UserToLab.user_id == params['user_id'])
              .filter(UserToLab.lab_id == lab_id)
              .first())

        if not qr and qr[1].perms != UserToLab.PERMS['waiting to be approved']:
            return {'result' : 'error'}

        user, u2l, lab = qr
        u2l.perms = UserToLab.PERMS['member']
        lab.wait_to_approve -= 1

        my_event = MyEvent(
            user_id      = web.ctx.session.user_id,
            full_name    = web.ctx.session.full_name,
            lab_id       = lab.id,
            lab_name     = lab.name,
            event_type = MyEvent.TYPES['user added to lab'],
            event_object_type = MyEvent.OBJ_TYPES['user'],
            event_object_id   = user.id,
            event_object_name = user.full_name)
        web.ctx.orm.add(my_event)

        send_mail('user added to lab', mail_to=user.email,
                                       owner_url=web.ctx.session.user_abs_url,
                                       owner_name=web.ctx.session.full_name,
                                       lab_url=lab.abs_url,
                                       lab_name=lab.name)

        return {'result' : 'ok'}


class DeleteUser:

    @has_relations('owner')
    @as_json
    def POST(self, lab_id, **kw):
        params = web.input()
        if not ('user_id'   in params and
                'is_reject' in params):
            return {'result' : 'error'}

        qr = (web.ctx.orm.query(User, UserToLab, Lab)
              .filter(UserToLab.lab_id  == Lab.id)
              .filter(UserToLab.user_id == User.id)
              .filter(UserToLab.user_id == params['user_id'])
              .filter(UserToLab.lab_id  == lab_id)
              .filter(Lab.id == lab_id)
              .first())
        if not qr:
            return {'result' : 'error'}

        user, u2l, lab = qr
        if u2l.perms == UserToLab.PERMS['owner']:
            return {'result' : 'error'}
        if u2l.perms == UserToLab.PERMS['waiting to be approved']:
            lab.wait_to_approve -= 1
        web.ctx.orm.delete(u2l)

        u2projs = (web.ctx.orm.query(UserToProject, LabProject)
                   .filter(UserToProject.project_id == LabProject.id)
                   .filter(UserToProject.user_id == user.id)
                   .filter(LabProject.lab_id == kw['lab'].id)
                   .all())

        for u2proj, proj in u2projs:
            web.ctx.orm.delete(u2proj)

        is_reject = True if params['is_reject'] == 'true' else False

        if is_reject:
            event_type = MyEvent.TYPES['reject user from lab']
        else:
            event_type = MyEvent.TYPES['delete user from lab']

        my_event = MyEvent(
            user_id      = web.ctx.session.user_id,
            full_name    = web.ctx.session.full_name,
            lab_id       = lab.id,
            lab_name     = lab.name,
            event_type = event_type,
            event_object_type = MyEvent.OBJ_TYPES['user'],
            event_object_id   = user.id,
            event_object_name = user.full_name)
        web.ctx.orm.add(my_event)

        send_mail('reject user from lab' if is_reject else 'delete user from lab',
                              mail_to=user.email,
                              owner_url=web.ctx.session.user_abs_url,
                              owner_name=web.ctx.session.full_name,
                              lab_url=lab.abs_url,
                              lab_name=lab.name)

        return {'result': 'ok'}


class ShowLabDatabases:

    @has_relations('owner', 'member')
    @as_html('my/show_lab_databases.html')
    def GET(self, lab_id, **kw):
        return {'cpage':   'mylabs',
                'labpage': 'databases',
                'lab':     kw['lab'],
                'u2lab':   kw['u2l'],
                'c_plan':  kw['c_plan']}

    @has_relations('owner')
    @as_text
    def POST(self, lab_id, **kw):
        params = web.input()
        if 'databases' not in params:
            return ''

        kw['lab'].databases_info = cgi.escape(params['databases'])
        if kw['lab'].databases_info == '':
            response = 'Click to edit databases info ...'
        else:
            response = kw['lab'].databases_info
        return '<pre>{}</pre>'.format(response)


class ShowLabDatabasesParam:

    @has_relations('owner')
    @as_text
    def GET(self, lab_id, **kw):
        return kw['lab'].databases_info


class ShowLabDatasets:

    @has_relations("owner", "member")
    @as_html("my/show_lab_datasets.html")
    def GET(self, lab_id, **kw):
        datasets = []
        if kw["lab"].is_ds_visible:
            datasets = (web.ctx.orm.query(DataSet, User)
                        .filter(DataSet.lab_id == lab_id)
                        .filter(DataSet.user_id == User.id)
                        .all())

        return {"cpage":    "mylabs",
                "labpage":  "datasets",
                "lab":      kw["lab"],
                "c_plan":   kw["c_plan"],
                "u2lab":    kw["u2l"],
                "datasets": datasets}


class ShowLabDataSetRequests:

    @has_relations('owner', 'member')
    @as_html("my/show_dataset_requests.html")
    def GET(self, lab_id, **kw):
        q = (web.ctx.orm.query(User, DataSet, DataSetRequest)
            .filter(User.id == DataSetRequest.user_id)
            .filter(DataSet.id == DataSetRequest.dataset_id)
            .filter(DataSet.lab_id == lab_id)
            .filter(DataSet.user_id == DataSetRequest.user_id)
            .all())
        volumes = {}
        for user, ds, request in q:
            if request.dataset_volume_id != DataSetRequest.ALL_VOLUMES:
                volume = (web.ctx.orm.query(DataSetVolume)
                         .filter(DataSetVolume.id == request.dataset_volume_id)
                         .first())
                volumes[volume.id] = volume.serial

        return {'cpage':    'mylabs',
                'labpage':  'datasets',
                'lab':      kw['lab'],
                'c_plan':   kw['c_plan'],
                'u2lab':    kw['u2l'],
                'requests': q,
                'volumes':  volumes}


class NewDataSetRequest:

    @has_relations('owner', 'member')
    @as_html("my/new_dataset_request.html")
    def GET(self, lab_id, ds_id, **kw):
        "Display form for new DataSet Request."
        q = (web.ctx.orm.query(DataSet, User)
                .filter(DataSet.user_id == User.id)
                .filter(DataSet.id == ds_id)
                .first())
        if not q:
            raise web.notfound()
        dataset, owner = q

        volumes = (web.ctx.orm.query(DataSetVolume)
                    .filter(DataSetVolume.dataset_id == dataset.id)
                    .all())
        form = DataSetRequestForm(datasets=[dataset],
                                  users=[kw["user"]],
                                  volumes=volumes)
        return {'cpage':    'mylabs',
                'labpage':  'datasets',
                'lab':      kw['lab'],
                'u2lab':    kw['u2l'],
                'c_plan':   kw['c_plan'],
                'dataset':  dataset,
                'owner':    owner,
                'form':     form}

    @has_relations('owner', 'member')
    @as_html("my/new_dataset_request.html")
    def POST(self, lab_id, ds_id, **kw):
        "Get form data and create new Dataset Request."
        params = web.input()
        if "dataset_volume_id" not in params:
            raise web.notfound()

        dataset = (web.ctx.orm.query(DataSet)
                    .filter_by(id = ds_id)
                    .first())
        if not dataset:
            raise web.notfound()

        volume_id = params["dataset_volume_id"]
        if not volume_id.isdigit():
            raise web.notfound()

        if volume_id != DataSetRequest.ALL_VOLUMES:
            volume = (web.ctx.orm.query(DataSetVolume)
                        .filter_by(id = volume_id)
                        .first())
            if not volume:
                raise web.notfound()
        # check for duplicates
        request = (web.ctx.orm.query(DataSetRequest)
                .filter_by(dataset_id = dataset.id)
                .filter_by(dataset_volume_id = volume_id)
                .first())
        if request:
            owner = (web.ctx.orm.query(User)
                    .filter_by(id = dataset.user_id).first())
            volumes = (web.ctx.orm.query(DataSetVolume)
                    .filter(DataSetVolume.dataset_id == dataset.id)
                    .all())
            form = DataSetRequestForm(datasets=[dataset],
                                  users=[kw["user"]],
                                  volumes=volumes)
            message = ("Sorry, request for volume {} already exists."
                    .format(volume.serial))
            return {'cpage':    'mylabs',
                    'labpage':  'datasets',
                    'lab':      kw['lab'],
                    'u2lab':    kw['u2l'],
                    'c_plan':   kw['c_plan'],
                    'dataset':  dataset,
                    'owner':    owner,
                    'form':     form,
                    'message': message}
        else:
            new_ds_request = DataSetRequest(
                user_id           = dataset.user_id,
                dataset_id        = dataset.id,
                dataset_volume_id = volume_id,
                status            = DataSetRequest.get_status("New")
            )
            web.ctx.orm.add(new_ds_request)
            web.ctx.orm.commit()

            goto = web.ctx.site_url + "/my/{}/requests".format(lab_id)
            raise web.seeother(goto)


class ShowDataSet:

    @has_relations('owner', 'member')
    @as_html("my/show_dataset.html")
    def GET(self, lab_id, dataset_id, **kw):
        query = (web.ctx.orm.query(DataSet, User)
                    .filter(DataSet.id == dataset_id)
                    .filter(User.id == DataSet.user_id)
                    .first())
        if not query:
            raise web.notfound()
        dataset, user = query
        dataset_volumes = (web.ctx.orm.query(DataSetVolume)
            .filter(DataSetVolume.dataset_id == dataset_id)
            .order_by(DataSetVolume.id)
            .all())

        return {'cpage':            'mylabs',
                'labpage':          'datasets',
                'lab':              kw['lab'],
                'c_plan':           kw['c_plan'],
                'u2lab':            kw['u2l'],
                'dataset':          dataset,
                'xuser':            user,
                'dataset_volumes':  dataset_volumes}


class ShowDataSetVolume:

    @has_relations('owner', 'member')
    @as_html("my/show_dataset_volume.html")
    def GET(self, lab_id, volume_id, **kw):
        query = (web.ctx.orm.query(DataSet, DataSetVolume)
                .filter(DataSet.id == DataSetVolume.dataset_id)
                .filter(DataSetVolume.id == volume_id)
                .first())
        if not query:
            raise web.notfound()
        ds, volume = query
        return {'cpage':    'mylabs',
                'labpage':  'datasets',
                'lab':      kw['lab'],
                'c_plan':   kw['c_plan'],
                'u2lab':    kw['u2l'],
                'dataset':  ds,
                'volume':   volume}
