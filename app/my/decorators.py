import web

from app.sso.models import UserToLab
from app.my.models  import Lab, LabProject, UserToProject, LabPlan


def has_relations(*rights):
    """
    Checking for lab rights.
    Second argument must be lab id.

    rights = ['owner', 'member',]
    """
    def decorator(f):
        def wrapper(*args, **kwargs):
            lab = (web.ctx.orm.query(Lab, LabPlan)
                   .filter(Lab.id == LabPlan.lab_id)
                   .filter(LabPlan.deleted == None)
                   .filter(Lab.id == args[1])
                   .first())
            u2l = (web.ctx.orm.query(UserToLab)
                    .filter_by(user_id = web.ctx.user.id)
                    .filter_by(lab_id = args[1])
                    .first())

            if not (web.ctx.user and lab and u2l):
                if not (web.ctx.user.is_staff and
                        web.ctx.user.staff_role ==
                        web.ctx.user.STAFF_ROLES["ADMIN"]):
                    raise web.seeother(web.ctx.site_url + '/my')

            if u2l and u2l.perms_str not in rights:
                if not web.ctx.user.is_staff:
                    raise web.seeother(web.ctx.site_url + '/my')
            lab, lab_plan = lab

            # TODO:
            if not u2l and web.ctx.user.is_staff:
                u2l = UserToLab(lab_id  = lab.id,
                                user_id = web.ctx.user.id,
                                perms   = UserToLab.PERMS["owner"])

            kwargs['user']   = web.ctx.user
            kwargs['lab']    = lab
            kwargs['u2l']    = u2l
            kwargs['c_plan'] = lab_plan  # current plan

            return f(*args, **kwargs)
        return wrapper
    return decorator


def project_member(handler):
    def wrapper(*args, **kwargs):
        # args[0] = self
        # args[1] = lab_id
        # args[2] = project_id
        query = (web.ctx.orm.query(LabProject, UserToProject)
                 .filter(LabProject.id == args[2])
                 .filter(LabProject.lab_id == args[1])
                 .filter(LabProject.id == UserToProject.project_id)
                 .filter(UserToProject.user_id == web.ctx.session.user_id)
                 .first())
        if not query:
            raise web.notfound()

        project, u2proj = query
        if u2proj.relation != UserToProject.REL_TYPES['member']:
            raise web.notfound()

        kwargs['project'] = project
        kwargs['u2proj']  = u2proj
        return handler(*args, **kwargs)
    return wrapper
