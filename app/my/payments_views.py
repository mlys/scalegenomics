from datetime import datetime

import web

import app.my.payments       as payments
from app.core.decorators import as_html
from app.my.decorators   import has_relations
from app.my.models       import Lab, LabPrice, LabInvoice, Plan, LabPlan
from app.my.utils        import set_flash_message, get_flash_message


def validate_month():
    params = web.input()
    if 'month' not in params:
        raise web.notfound()

    if not params['month'].isdigit():
        raise web.notfound()
    month = int(params['month'])

    if month > 12 or month < 1:
        raise web.notfound()

    return month


def get_lab_prices(lab_id, now, month):
    # 2 cases
    # - this month
    # - old
    if month >= now.month:
        lab_price = (web.ctx.orm.query(LabPrice)
                     .filter_by(lab_id = lab_id)
                     .first())
        if lab_price:
            return lab_price.to_d()
        else:
            return web.config['SG_SETTINGS']['PRICES'][1]
    else:
        date = datetime(now.year, month, 1)
        invoice = (web.ctx.orm.query(LabInvoice)
                   .filter_by(lab_id = lab_id)
                   .filter_by(date = date)
                   .first())
        if not invoice:
            return web.config['SG_SETTINGS']['PRICES'][1]

        return invoice.get_prices()


class ShowLabPayments:

    @has_relations('owner')
    @as_html('my/show_lab_payments.html')
    def GET(self, lab_id, **kw):

        now = datetime.utcnow()

        invoices = (web.ctx.orm.query(LabInvoice)
                    .filter_by(lab_id = kw['lab'].id)
                    .all())

        # sort invoices
        inv_paid = []
        inv_not_paid = []
        for inv in invoices:
            if inv.paid == LabInvoice.PAYMENTS['NONE']:
                inv_not_paid.append(inv)
            else:
                inv_paid.append(inv)

        lab_plan = (web.ctx.orm.query(LabPlan)
                    .filter_by(lab_id     = kw['lab'].id)
                    .filter_by(deleted    = None)
                    .first())

        return { 'cpage'   : 'mylabs',
                 'labpage' : 'payments',
                 'lab'     : kw['lab'],
                 'u2lab'   : kw['u2l'],
                 'c_plan'  : kw['c_plan'],
                 'message'          : get_flash_message(),
                 'lab_plan'         : lab_plan,
                 'now'              : now,
                 'invoices'         : [],
                 'inv_total'        : 0,
                 'price_total'      : 0,
                 'last_month_price' : 0 }


class ShowLabPlans:

    @has_relations('owner')
    @as_html('my/show_lab_plans.html')
    def GET(self, lab_id, **kw):
        now = datetime.utcnow()
        years = payments.get_years_range(now)
        return {'cpage'   : 'mylabs',
                'labpage' : 'payments',
                'lab'     : kw['lab'],
                'u2lab'   : kw['u2l'],
                'c_plan'  : kw['c_plan'],
                'now'     : now,
                'years'   : years}


class ShowLabPlansByDate:

    @has_relations('owner')
    @as_html('my/_show_month_plans.html')
    def GET(self, lab_id, year, month, **kw):
        year = int(year)
        month = int(month)
        now = datetime.utcnow()
        lab_plans = payments.calc_plans_by_date(kw['lab'], now, year, month)
        return {'lab_plans' : lab_plans}


class UpgradeLabPlan:

    @has_relations('owner')
    @as_html('my/upgrade_lab_plan.html')
    def GET(self, lab_id, **kw):
        plan = (web.ctx.orm.query(LabPlan)
                .filter_by(lab_id = lab_id)
                .filter_by(deleted = None)
                .first())
        plans = web.ctx.orm.query(Plan).all()
        return {'cpage'   : 'mylabs',
                'labpage' : 'payments',
                'lab'     : kw['lab'],
                'u2lab'   : kw['u2l'],
                'c_plan'  : kw['c_plan'],
                'lab_plan': plan,
                'plans'   : plans}


class ChangeLabPlan:

    @has_relations('owner')
    @as_html('my/change_lab_plan.html')
    def GET(self, lab_id, plan_id, **kw):
        plan = web.ctx.orm.query(Plan).filter_by(id = plan_id).first()
        if not plan: raise web.notfound()

        now = datetime.utcnow()
        dt = now - kw['c_plan'].created
        time_block = not (dt.days >= 1)

        block_form = False
        if kw['lab'].cpu_used > plan.cores:
            block_form = True
        if kw['lab'].ram_used > plan.ram_mb:
            block_form = True
        if kw['lab'].storages_used > plan.storage_mb:
            block_form = True

        return {'cpage'      : 'mylabs',
                'labpage'    : 'payments',
                'lab'        : kw['lab'],
                'u2lab'      : kw['u2l'],
                'c_plan'     : kw['c_plan'],
                'plan'       : plan,
                'time_block' : time_block,
                'block_form' : block_form}

    @has_relations('owner')
    def POST(self, lab_id, plan_id, **kw):

        plan = web.ctx.orm.query(Plan).filter_by(id = plan_id).first()
        if not plan: raise web.notfound()

        now = datetime.utcnow()
        kw['c_plan'].deleted = now

        lab_plan = LabPlan(lab_id     = kw['lab'].id,
                           plan_id    = plan.id,
                           name       = plan.name,
                           price      = plan.price,
                           cores      = plan.cores,
                           ram        = plan.ram,
                           storage    = plan.storage,
                           created    = now)
        web.ctx.orm.add(lab_plan)

        kw['lab'].cpu_limit      = plan.cores
        kw['lab'].ram_limit      = plan.ram_mb
        kw['lab'].storages_limit = plan.storage_mb

        set_flash_message('<p>You have upgraded to {}.</p>'
                          '<p>It is effective immediately. Enjoy!</p>'
                          .format(plan.name_str))
        if kw['lab'].status == Lab.STATUS['trial block']:
            kw['lab'].status = Lab.STATUS['active']

        raise web.seeother(kw['lab'].abs_url+'/payments')



class LabInvoices:

    @has_relations('owner')
    @as_html('my/lab_invoices.html')
    def GET(self, lab_id, **kw):

        invoices = (web.ctx.orm.query(LabInvoice)
                    .filter_by(lab_id = lab_id)
                    .order_by('date')
                    .all())

        return { 'cpage'    : 'mylabs',
                 'labpage'  : 'payments',
                 'lab'      : kw['lab'],
                 'u2lab'    : kw['u2l'],
                 'c_plan'   : kw['c_plan'],
                 'invoices' : invoices }


class ShowLabInvoices:

    @has_relations('owner')
    @as_html('my/_show_lab_invoices.html')
    def GET(self, lab_id, **kw):

        invoices = (web.ctx.orm.query(LabInvoice)
                    .filter_by(lab_id = lab_id)
                    .order_by('date')
                    .all())

        not_paid = []
        is_paid = []
        for inv in invoices:
            if inv.paid == LabInvoice.PAYMENTS['NONE']:
                not_paid.append(inv)
            else:
                is_paid.append(inv)

        return { 'invoices' : invoices,
                 'not_paid' : not_paid,
                 'is_paid'  : is_paid }


class ShowLabInvoice:

    @has_relations('owner')
    @as_html('my/show_lab_invoice.html')
    def GET(self, lab_id, invoice_id, **kw):

        inv = (web.ctx.orm.query(LabInvoice)
               .filter_by(id = invoice_id)
               .first())
        if not inv:
            raise web.notfound()

        prices = inv.get_prices()

        calc = payments.CalcPayments(lab_id)
        servers, servers_total = calc.calc_month_servers(inv.date.month, prices)
        disks, disks_total = calc.calc_month_disks(inv.date.month, prices)
        total = servers_total + disks_total

        return { 'lab'           : kw['lab'],
                 'u2lab'         : kw['u2l'],
                 'invoice'       : inv,
                 'servers'       : servers,
                 'servers_total' : servers_total,
                 'disks'         : disks,
                 'disks_total'   : disks_total,
                 'total'         : total }


class LabPaymentsLog:

    @has_relations('owner')
    @as_html('my/lab_payments_log.html')
    def GET(self, lab_id, **kw):

        return { 'cpage'   : 'mylabs',
                 'labpage' : 'payments',
                 'lab'     : kw['lab'],
                 'u2lab'   : kw['u2l'],
                 'now'     : datetime.utcnow() }


class LabPaymentsInMonthSum:

    @has_relations('owner')
    @as_html('my/_show_month_lab_payments.html')
    def GET(self, lab_id, **kw):

        month = validate_month()

        now = datetime.utcnow()
        if month > now.month:
            return {'future' : True}

        prices = get_lab_prices(lab_id, now, month)

        calc = payments.CalcPayments(lab_id, now)
        calc.set_times(now.year, month)

        servers, servers_total = calc.calc_month_servers(prices)
        storages, storages_total = calc.calc_month_storages(prices)

        return { 'servers'       : servers,
                 'servers_total' : servers_total,
                 'disks'         : storages,
                 'disks_total'   : storages_total,
                 'sum_price'     : servers_total + storages_total }


class LabPaymentsInMonthByServer:

    @has_relations('owner')
    @as_html('my/_show_month_lab_payments_by_server.html')
    def GET(self, lab_id, **kw):

        month = validate_month()

        now = datetime.utcnow()
        if now.month < month:
            return {'future' : True}

        prices = get_lab_prices(lab_id, now, month)

        calc = payments.CalcPayments(lab_id, now)
        calc.set_times(now.year, month)

        servers, servers_total = calc.calc_month_servers(prices)

        return { 'lab'           : kw['lab'],
                 'servers'       : servers,
                 'servers_total' : servers_total }


class LabPaymentsInMonthByDisk:

    @has_relations('owner')
    @as_html('my/_show_month_lab_payments_by_disk.html')
    def GET(self, lab_id, **kw):

        month = validate_month()

        now = datetime.utcnow()
        if now.month < month:
            return {'future' : True}

        prices = get_lab_prices(lab_id, now, month)

        calc = payments.CalcPayments(lab_id, now)
        calc.set_times(now.year, month)

        disks, disks_total = calc.calc_month_storages(prices)

        return {
            'lab'         : kw['lab'],
            'disks'       : disks,
            'disks_total' : disks_total,
            }
