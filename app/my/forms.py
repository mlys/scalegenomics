import web
from wtforms import Form, TextField, TextAreaField, SelectField
from wtforms.validators import ValidationError, Required


from app.core.patterns import re_lab_name, re_server_name, \
        re_storage_name
from app.my.models     import KVMServer, KVMStorage

lab_types = [(str(i), t) for i, t in
             enumerate(web.config['SG_SETTINGS']['LAB_TYPE'])]


class LabForm(Form):
    name = TextField('Name', [Required()])
    type = SelectField('Type', choices=lab_types)

    def validate_name(self, field):
        is_match = re_lab_name.findall(field.data)
        if not is_match:
            raise ValidationError('Wrong lab name.')

    def validate_type(self, field):
        if not field.data.isdigit():
            raise ValidationError('Wrong type.')

        t = int(field.data)
        if t < 0 or t > len(lab_types):
            raise ValidationError('Wrong type.')


class ServerForm(Form):
    name        = TextField('Name')
    description = TextAreaField('Description')
    cpu         = TextField('Core count')
    ram         = TextField('RAM')
    image       = SelectField('Image', choices=[(str(i), n[0]) for i, n
                                                in KVMServer.IMAGES.items()])
    project     = SelectField('Project')

    def __init__(self, formdata=None, projects=None):
        if projects is None:
            projects = []
        Form.__init__(self, formdata)
        self.project.choices = [(str(p.id), p.name) for p in projects]

    def validate_name(self, field):
        is_match = re_server_name.findall(field.data.strip())
        if not is_match:
            raise ValidationError('Wrong server name.')

    def validate_cpu(self, field):
        if not field.data.isdigit():
            raise ValidationError('Wrong cpu count.')

        cpu = int(field.data)
        if (cpu < web.config['SG_SETTINGS']['KVM_CPU_LIMITS'][0] or
            cpu > web.config['SG_SETTINGS']['KVM_CPU_LIMITS'][1]):
            raise ValidationError('Wrong cpu count.')

    def validate_ram(self, field):
        if not field.data.isdigit():
            raise ValidationError('Wrong RAM.')

        memory = int(field.data)
        if memory not in web.config['SG_SETTINGS']['KVM_MEMORY_LIST']:
            raise ValidationError('Wrong RAM.')

    def validate_image(self, field):
        if not field.data.isdigit():
            raise ValidationError('Wrong image.')

    def validate_project(self, field):
        if not field.data.isdigit():
            raise ValidationError('Wrong project.')


class StorageForm(Form):
    name        = TextField("Name")
    alias       = TextField("Mount point name")
    description = TextAreaField("Description")
    size        = TextField("Size")
    type        = SelectField("Type")
    project     = SelectField("Project")

    def __init__(self, formdata=None, projects=None):
        if projects is None:
            projects = []
        Form.__init__(self, formdata)
        self.type.choices = [(str(code), type)
                                for type, code in KVMStorage.TYPES.items()]
        self.project.choices = [(str(p.id), p.name) for p in projects]

    def validate_name(self, field):
        is_match = re_storage_name.findall(field.data)
        if not is_match:
            raise ValidationError("Wrong name.")

    def validate_size(self, field):
        if not field.data.isdigit():
            raise ValidationError("Wrong size.")
        size = int(field.data)
        if (size < web.config["SG_SETTINGS"]["STORAGE_SIZE_LIMITS"]["min"] or
            size > web.config["SG_SETTINGS"]["STORAGE_SIZE_LIMITS"]["max"]):
            raise ValidationError("Wrong size.")
        self.size.data = size
