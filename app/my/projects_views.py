import cgi
import json
import textwrap
from datetime import datetime

import web
from sqlalchemy import or_

from app.core.mail       import send_mail
from app.core.decorators import as_html, as_json, as_text
from app.core.patterns   import re_project_name
from app.my.decorators   import has_relations, project_member
from app.my.models       import LabProject, UserToProject, MyEvent,\
    KVMServer, KVMStorage, KVMMountRelation
from app.my.utils        import get_flash_message, LabLimit, \
    get_cservs_data, get_server_limits, get_cservs_info, storage_editable, \
    get_dheight
from app.my.forms        import ServerForm, StorageForm
from app.sso.models      import User, UserToLab


def desc_wrap(desc):
    return textwrap.fill(cgi.escape(desc), 50)


class NewProject:

    @has_relations("owner", "member")
    @as_html("my/_new_project.html")
    def GET(self, lab_id, **kw):
        return {"lab": kw["lab"]}

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, **kw):
        params = web.input()
        if not ("name"        in params and
                "description" in params):
            return {"result": "error"}

        name = re_project_name.findall(params.name.strip())
        if not name:
            return {"result":  "error",
                    "message": "wrong name"}

        project = LabProject(created_by  = kw["user"].id,
                             lab_id      = kw["lab"].id,
                             name        = name[0],
                             description = cgi.escape(params.description),
                             created     = datetime.utcnow(),
                             updated     = datetime.utcnow())
        web.ctx.orm.add(project)
        web.ctx.orm.commit()

        u2proj = UserToProject(
            user_id    = web.ctx.user.id,
            project_id = project.id,
            relation   = UserToProject.REL_TYPES["member"],
            position   = UserToProject.POSITIONS["owner"])
        web.ctx.orm.add(u2proj)

        my_event = MyEvent(user_id    = web.ctx.session.user_id,
                           full_name  = web.ctx.session.full_name,
                           lab_id       = kw["lab"].id,
                           lab_name     = kw["lab"].name,
                           project_id   = project.id,
                           project_name = project.name,
                           event_type   = MyEvent.TYPES["create project"],
                           event_object_type = -1,
                           event_object_id   = -1,
                           event_object_name = "")
        web.ctx.orm.add(my_event)

        return {"result":     "ok",
                "project_id": project.id}


class ShowMyProjects:

    @has_relations("owner", "member")
    @as_html("my/_show_my_projects.html")
    def GET(self, lab_id, **kw):
        projects = (web.ctx.orm.query(User, LabProject, UserToProject)
                    .filter(LabProject.created_by == User.id)
                    .filter(LabProject.id == UserToProject.project_id)
                    .filter(LabProject.lab_id == lab_id)
                    .filter(UserToProject.user_id == web.ctx.user.id)
                    .order_by(LabProject.name)
                    .all())
        return {"lab":      kw["lab"],
                "projects": projects}


class ShowAllProjects:

    @has_relations('owner', 'member')
    @as_html('my/_show_all_projects.html')
    def GET(self, lab_id, **kw):

        projects = (web.ctx.orm.query(User, LabProject)
                    .filter(LabProject.created_by == User.id)
                    .filter(LabProject.lab_id == lab_id)
                    .order_by(LabProject.name)
                    .all())

        return {'lab'      : kw['lab'],
                'projects' : projects}


class ShowProject:

    @has_relations('owner', 'member')
    @as_html('my/_show_project.html')
    def GET(self, lab_id, project_id, **kw):
        user_project = (web.ctx.orm.query(User, LabProject)
                        .filter(LabProject.created_by == User.id)
                        .filter(LabProject.lab_id == lab_id)
                        .filter(LabProject.id == project_id)
                        .first())
        if not user_project:
            return {'lab' : kw['lab']}

        owner, project = user_project
        u2proj = (web.ctx.orm.query(UserToProject)
                  .filter_by(user_id    = web.ctx.session['user_id'])
                  .filter_by(project_id = project_id)
                  .first())
        if not u2proj:
            return {'lab'     : kw['lab'],
                    'owner'   : owner,
                    'project' : project}

        return {'lab'     : kw['lab'],
                'owner'   : owner,
                'project' : project,
                'u2proj'  : u2proj}


class JoinToProject:

    @has_relations('owner', 'member')
    @as_json
    def POST(self, lab_id, project_id, **kw):
        project = (web.ctx.orm.query(LabProject)
                   .filter_by(id = project_id)
                   .filter_by(lab_id = kw['lab'].id)
                   .first())
        if not project:
            return {'result' : 'error'}

        if web.ctx.session.user_id == project.created_by:
            return {'result' : 'error'}

        u2proj = (web.ctx.orm.query(UserToProject)
                  .filter_by(project_id = project_id)
                  .filter_by(user_id    = web.ctx.session.user_id)
                  .first())
        if u2proj:
            return {'result' : 'error'}

        u2proj = UserToProject(
            user_id    = web.ctx.session.user_id,
            project_id = project.id,
            relation   = UserToProject.REL_TYPES['waiting'],
            position   = UserToProject.POSITIONS['none'])
        web.ctx.orm.add(u2proj)

        my_event = MyEvent(
            user_id      = web.ctx.session.user_id,
            full_name    = web.ctx.session.full_name,
            lab_id       = kw['lab'].id,
            lab_name     = kw['lab'].name,
            project_id   = project.id,
            project_name = project.name,
            event_type   = MyEvent.TYPES['waiting to project'])
        web.ctx.orm.add(my_event)

        owner = (web.ctx.orm.query(User)
                 .filter_by(id = project.created_by)
                 .first())

        send_mail('join to project',
                    mail_to=owner.email,
                    owner_name=owner.full_name,
                    user_url=web.ctx.session.user_abs_url,
                    user_name=web.ctx.session.full_name,
                    project_url=project.abs_url,
                    project_name=project.name,
                    project_members_url=project.members_abs_url)

        return {'result' : 'ok'}


class UnsubscribeProject:

    @has_relations('owner', 'member')
    @as_json
    def POST(self, lab_id, project_id, **kw):
        u2proj = (web.ctx.orm.query(UserToProject)
                  .filter(UserToProject.user_id == web.ctx.session['user_id'])
                  .filter(UserToProject.project_id == project_id)
                  .filter(UserToProject.relation ==
                          UserToProject.REL_TYPES['waiting'])
                  .first())
        if u2proj:
            web.ctx.orm.delete(u2proj)
        return {'result': 'ok'}


class EditProject:

    @has_relations("owner", "member")
    @as_html("my/_edit_project.html")
    def GET(self, lab_id, project_id, **kw):
        project = (web.ctx.orm.query(LabProject)
                   .filter_by(created_by = web.ctx.user.id)
                   .filter_by(lab_id = lab_id)
                   .filter_by(id = project_id)
                   .first())
        if not project:
            return {}
        return {"project": project,
                "dheight": get_dheight(project.description)}

    @has_relations("owner", "member")
    @as_json
    def POST(self, lab_id, project_id, **kw):
        params = web.input()
        if not ("name"        in params and
                "description" in params and
                "project"     in params):
            return {"result": "error"}

        name = re_project_name.findall(params.name.strip())
        if not name:
            return {"result":  "error",
                    "message": "wrong name"}
        project = (web.ctx.orm.query(LabProject)
                   .filter_by(created_by = web.ctx.user.id)
                   .filter_by(lab_id = lab_id)
                   .filter_by(id = params["project"])
                   .first())
        if not project:
            return {"result":  "error",
                    "message": "unknown project"}
        project.name = name[0]
        project.description = cgi.escape(params["description"])
        return {"result": "ok"}


class LoadProjectParam:

    @has_relations('owner', 'member')
    @as_text
    def GET(self, lab_id, project_id, **kw):
        '''Return project name or description (jedidable)'''

        project = (web.ctx.orm.query(LabProject)
                   .filter_by(id = project_id)
                   .filter_by(lab_id = kw['lab'].id)
                   .filter_by(created_by = web.ctx.session.user_id)
                   .first())
        if not project:
            return ''

        if 'description' in web.input():
            return project.description
        else:
            return project.name


class DeleteProject:

    def _get_obj(self, lab_id, project_id):
        servers = (web.ctx.orm.query(KVMServer)
                   .filter(KVMServer.lab_id == lab_id)
                   .filter(KVMServer.project_id == project_id)
                   .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                   .all())
        storages = (web.ctx.orm.query(KVMStorage)
                    .filter(KVMStorage.lab_id == lab_id)
                    .filter(KVMStorage.project_id == project_id)
                    .filter(KVMStorage.status < KVMStorage.STATUS["REMOVING"])
                    .all())
        return servers, storages

    @has_relations("owner", "member")
    @project_member
    @as_html("my/_delete_project.html")
    def GET(self, lab_id, project_id, **kw):
        servers, storages = self._get_obj(lab_id, project_id)
        return {"lab":      kw["lab"],
                "project":  kw["project"],
                "servers":  servers,
                "storages": storages}

    @has_relations("owner", "member")
    @project_member
    @as_json
    def POST(self, lab_id, project_id, **kw):
        servers, storages = self._get_obj(lab_id, project_id)
        if servers or storages:
            return {"result": "error"}

        qr = (web.ctx.orm.query(User, UserToProject)
               .filter(UserToProject.user_id == User.id)
               .filter(UserToProject.project_id == kw['project'].id)
               .filter(UserToProject.relation ==
                       UserToProject.REL_TYPES['member'])
               .all())
        members = []
        rels = []
        for user, u2proj in qr:
            members.append(user)
            rels.append(u2proj)
        for rel in rels:
            web.ctx.orm.delete(rel)

        web.ctx.orm.delete(kw['project'])

        my_event = MyEvent(user_id      = web.ctx.session.user_id,
                           full_name    = web.ctx.session.full_name,
                           lab_id       = kw['lab'].id,
                           lab_name     = kw['lab'].name,
                           project_id   = kw['project'].id,
                           project_name = kw['project'].name,
                           event_type   = MyEvent.TYPES['delete project'],
                           event_object_type = -1,
                           event_object_id   = -1,
                           event_object_name = '')
        web.ctx.orm.add(my_event)

        for member in members:
            send_mail('project deleted',
                      mail_to=member.email,
                      owner_url=web.ctx.session.user_abs_url,
                      owner_name=web.ctx.session.full_name,
                      project_name=kw['project'].name)

        return {"result": "ok"}


class ShowProjectDashboard:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_show_project_dashboard.html')
    def GET(self, lab_id, project_id, **kw):

        # project servers
        servers = (web.ctx.orm.query(KVMServer)
                   .filter(KVMServer.lab_id     == lab_id)
                   .filter(KVMServer.project_id == project_id)
                   .filter(KVMServer.status != KVMServer.STATUS['REMOVING'])
                   .filter(KVMServer.status != KVMServer.STATUS['REMOVED'])
                   .all())
        # sorting, put owner on top
        user_servers = []
        others_servers = []
        for server in servers:
            if server.user_id == web.ctx.session.user_id:
                user_servers.append(server)
            else:
                others_servers.append(server)
        servers_l = user_servers + others_servers

        # project disks
        disks = (web.ctx.orm.query(KVMStorage)
                 .filter(KVMStorage.lab_id == lab_id)
                 .filter(KVMStorage.project_id == project_id)
                 .filter(KVMStorage.status != KVMStorage.STATUS['REMOVING'])
                 .filter(KVMStorage.status != KVMStorage.STATUS['REMOVED'])
                 .all())
        # sorting, put owner on top
        user_servers = []
        others_servers = []
        for disk in disks:
            if disk.user_id == web.ctx.session.user_id:
                user_servers.append(disk)
            else:
                others_servers.append(disk)
        disks_l = user_servers + others_servers

        user_u2proj = (web.ctx.orm.query(User, UserToProject)
                       .filter(User.id == UserToProject.user_id)
                       .filter(UserToProject.project_id == kw['project'].id)
                       .order_by(UserToProject.position)
                       .all()[:10])

        my_events = (web.ctx.orm.query(MyEvent)
                     .filter_by(lab_id     = kw['lab'].id)
                     .filter_by(project_id = kw['project'].id)
                     .order_by('-created')
                     .all()[:10])

        return { 'lab'         : kw['lab'],
                 'project'     : kw['project'],
                 'servers'     : servers_l[:10],
                 'disks'       : disks_l[:10],
                 'user_u2proj' : user_u2proj,
                 'events'      : my_events }


class NewProjectServer:

    @has_relations("owner", "member")
    @project_member
    @as_html("my/_new_project_server.html")
    def GET(self, lab_id, project_id, **kw):
        cservs = get_cservs_data(lab_id)
        cpu_lim, mem_lim, cpu_block, mem_block  = get_server_limits(kw["lab"])
        cservs_info, free_space = get_cservs_info(cservs, cpu_lim, mem_lim)

        return {"lab":         kw["lab"],
                "c_plan":      kw["c_plan"],
                "project":     kw["project"],
                "images":      KVMServer.IMAGES,
                "free_space":  free_space,
                "cpu_block":   cpu_block,
                "mem_block":   mem_block,
                "cservs":      cservs,
                "cservs_info": json.dumps(cservs_info),
                "cpulimits":   KVMServer.CPULIMITS,
                "form":        ServerForm()}


class ShowProjectServers:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_show_project_servers.html')
    def GET(self, lab_id, project_id, **kw):

        servers = (web.ctx.orm.query(User, KVMServer)
                   .filter(KVMServer.user_id == User.id)
                   .filter(KVMServer.lab_id == lab_id)
                   .filter(KVMServer.project_id == project_id)
                   .filter(KVMServer.status != KVMServer.STATUS['REMOVING'])
                   .filter(KVMServer.status != KVMServer.STATUS['REMOVED'])
                   .all())

        return { 'lab'     : kw['lab'],
                 'project' : kw['project'],
                 'servers' : servers }


class ShowProjectServer:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_show_project_server.html')
    def GET(self, lab_id, project_id, server_id, **kw):

        query = (web.ctx.orm.query(User, KVMServer)
                 .filter(KVMServer.user_id == User.id)
                 .filter(KVMServer.lab_id == lab_id)
                 .filter(KVMServer.project_id == project_id)
                 .filter(KVMServer.id == server_id)
                 .first())
        if not query: return {}
        owner, server = query

        server_owner, block_pane, block_destroy = server.get_access(**kw)

        return { 'lab'     : kw['lab'],
                 'project' : kw['project'],
                 'owner'   : owner,
                 'server'  : server,
                 'server_owner'  : server_owner,
                 'block_pane'    : block_pane,
                 'block_destroy' : block_destroy }


class EditProjectServer:

    @has_relations('owner', 'member')
    @as_html('my/_edit_project_server.html')
    def GET(self, lab_id, project_id, server_id, **kw):

        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .filter_by(lab_id = lab_id)
                  .filter_by(user_id = web.ctx.user.id)
                  .filter(KVMServer.status < KVMServer.STATUS['REMOVING'])
                  .first())
        if not server:
            return {}

        cservs = get_cservs_data(lab_id)
        cpu_lim, mem_lim, cpu_block, mem_block = get_server_limits(kw["lab"])
        cservs_info, free_space = get_cservs_info(cservs, cpu_lim, mem_lim)

        cpu_lim = None
        mem_lim = None
        for cserv_id, cpu, mem, sz in cservs_info:
            if cserv_id == server.server_id:
                cpu_lim = cpu
                mem_lim = mem
                break

        return {'lab':     kw['lab'],
                'server':  server,
                'cpu_lim': cpu_lim,
                'mem_lim': mem_lim}


class ShowProjectServerLog:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_show_project_server_log.html')
    def GET(self, lab_id, project_id, server_id, **kw):

        events = (web.ctx.orm.query(MyEvent)
                  .filter_by(lab_id = lab_id)
                  .filter_by(project_id = project_id)
                  .filter(or_(MyEvent.event_object_type ==
                              MyEvent.OBJ_TYPES['server'],
                              MyEvent.event_object_2_type ==
                              MyEvent.OBJ_TYPES['server']))
                  .filter(or_(MyEvent.event_object_id == server_id,
                              MyEvent.event_object_2_id == server_id))
                  .order_by('-created')
                  .all())

        return { 'lab_id'     : lab_id,
                 'project_id' : project_id,
                 'server_id'  : server_id,
                 'events'     : events }


class ShowProjectStorages:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_show_project_storages.html')
    def GET(self, lab_id, project_id, **kw):

        storages = (web.ctx.orm.query(User, KVMStorage)
                    .filter(KVMStorage.user_id == User.id)
                    .filter(KVMStorage.lab_id == lab_id)
                    .filter(KVMStorage.project_id == project_id)
                    .filter(KVMStorage.status != KVMStorage.STATUS['REMOVING'])
                    .filter(KVMStorage.status != KVMStorage.STATUS['REMOVED'])
                    .all())
        result = []
        for user, storage in storages:
            rel = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                   .filter(KVMMountRelation.server_id == KVMServer.id)
                   .filter(KVMMountRelation.storage_id == storage.id)
                   .first())
            if rel:
                result.append((user, storage, rel[0]))
            else:
                result.append((user, storage, None))

        return { 'lab'     : kw['lab'],
                 'project' : kw['project'],
                 'storages': result,
                 'message' : get_flash_message() }


class NewProjectStorage:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_new_project_storage.html')
    def GET(self, lab_id, project_id, **kw):

        lab_limit = LabLimit(kw['lab'])
        cservs = get_cservs_data(lab_id)
        cservs_info = []
        free_space = False
        for cserv, _, _, cdisk in cservs:
            size_limits = lab_limit.get_storage_limits(cdisk.free_size)
            cservs_info.append((cserv.id, size_limits))
            if cdisk.free_size > 0:
                free_space = True

        return {'lab':           kw['lab'],
                'project':       kw['project'],
                'free_space':    free_space,
                'form':          StorageForm(),
                'storage_types': KVMStorage.TYPES,
                'cservs':        cservs,
                'cservs_info':   json.dumps(cservs_info)}


class ShowProjectStorage:

    @has_relations("owner", "member")
    @project_member
    @as_html("my/_show_project_storage.html")
    def GET(self, lab_id, project_id, storage_id, **kw):
        query = (web.ctx.orm.query(User, KVMStorage)
                 .filter(KVMStorage.user_id == User.id)
                 .filter(KVMStorage.lab_id == lab_id)
                 .filter(KVMStorage.project_id == project_id)
                 .filter(KVMStorage.id == storage_id)
                 .first())
        if not query:
            return {}
        owner, storage = query

        rel = None
        if storage.type == storage.TYPES["LOCAL"]:
            rel = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMMountRelation.storage_id == storage.id)
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .first())

        if storage.user_id == web.ctx.user.id:
            storage_owner = True
        else:
            storage_owner = False

        return {"owner":         owner,
                "storage":       storage,
                "rel":           rel,
                "storage_owner": storage_owner}


class EditProjectStorage:

    def get_query(self, lab_id, project_id, storage_id):
        query = (web.ctx.orm.query(User, KVMStorage)
                 .filter(KVMStorage.user_id == User.id)
                 .filter(KVMStorage.id == storage_id)
                 .filter(KVMStorage.lab_id == lab_id)
                 .filter(KVMStorage.project_id == project_id)
                 .filter(KVMStorage.user_id == web.ctx.user.id)
                 .first())
        return query

    @has_relations("member", "owner")
    @project_member
    @as_html("my/_edit_project_storage.html")
    def GET(self, lab_id, project_id, storage_id, **kw):
        query = self.get_query(lab_id, project_id, storage_id)
        if not query:
            return {}
        owner, storage = query
        editable = storage_editable(storage)

        return {"lab":      kw["lab"],
                "project":  kw["project"],
                "owner":    owner,
                "storage":  storage,
                "editable": editable}


class ShowProjectDatabases:

    @has_relations('member', 'owner')
    @project_member
    @as_html('my/_show_project_databases.html')
    def GET(self, lab_id, project_id, **kw):
        return {'project' : kw['project'],
                'lab'     : kw['lab'],
                'u2lab'   : kw['u2l']}

    @has_relations('owner')
    @project_member
    def POST(self, lab_id, project_id, **kw):
        params = web.input()
        if 'databases' not in params:
            return ''

        kw['project'].databases_info = cgi.escape(params['databases'])

        if kw['project'].databases_info == '':
            response = 'Click to edit databases info ...'
        else:
            response = kw['project'].databases_info
        return '<pre>{}</pre>'.format(response)


class ShowProjectDatabasesParam:

    @has_relations('owner')
    @project_member
    def GET(self, lab_id, project_id, **kw):
        web.header('Content-Type', 'text/html; charset=utf-8')
        return kw['project'].databases_info


class ShowProjectMembers:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_show_project_members.html')
    def GET(self, lab_id, project_id, **kw):

        members = (web.ctx.orm.query(User, UserToProject)
                   .filter(User.id == UserToProject.user_id)
                   .filter(UserToProject.project_id == kw['project'].id)
                   .order_by(UserToProject.position)
                   .all())

        return {'lab'     : kw['lab'],
                'project' : kw['project'],
                'members' : members}

    @has_relations('owner', 'member')
    @project_member
    @as_json
    def POST(self, lab_id, project_id, **kw):
        params = web.input()
        if not ('action'  in params and
                'user_id' in params):
            return {'result' : 'error'}

        query = (web.ctx.orm.query(User, UserToProject)
                 .filter(User.id == UserToProject.user_id)
                 .filter(UserToProject.project_id == kw['project'].id)
                 .filter(UserToProject.user_id == params['user_id'])
                 .first())
        if not query:
            return {'result' : 'error'}
        user, u2proj = query
        if u2proj.position == UserToProject.POSITIONS['owner']:
            return {'result' : 'error'}

        if params['action'] == 'add':
            u2proj.relation = UserToProject.REL_TYPES['member']
            u2proj.position = UserToProject.POSITIONS['member']
            my_event = MyEvent(
                user_id           = web.ctx.session.user_id,
                full_name         = web.ctx.session.full_name,
                lab_id            = kw['lab'].id,
                lab_name          = kw['lab'].name,
                project_id        = kw['project'].id,
                project_name      = kw['project'].name,
                event_type        = MyEvent.TYPES['user added to project'],
                event_object_type = MyEvent.OBJ_TYPES['user'],
                event_object_id   = user.id,
                event_object_name = user.full_name)
            web.ctx.orm.add(my_event)

            send_mail('user added to project',
                                        mail_to=user.email,
                                        user_name=user.first_name,
                                        owner_url=web.ctx.session.user_abs_url,
                                        owner_name=web.ctx.session.full_name,
                                        project_url=kw['project'].abs_url,
                                        project_name=kw['project'].name)

        elif params['action'] == 'reject' or params['action'] == 'delete':

            if params['action'] == 'reject':
                event_type = MyEvent.TYPES['reject user from project']
                mail_type = 'reject user from project'
            elif params['action'] == 'delete':
                event_type = MyEvent.TYPES['delete user from project']
                mail_type = 'delete user from project'

            my_event = MyEvent(
                user_id           = web.ctx.session.user_id,
                full_name         = web.ctx.session.full_name,
                lab_id            = kw['lab'].id,
                lab_name          = kw['lab'].name,
                project_id        = kw['project'].id,
                project_name      = kw['project'].name,
                event_type        = event_type,
                event_object_type = MyEvent.OBJ_TYPES['user'],
                event_object_id   = user.id,
                event_object_name = user.full_name)
            web.ctx.orm.add(my_event)
            web.ctx.orm.delete(u2proj)

            send_mail(mail_type, mail_to=user.email,
                          owner_url=web.ctx.session.user_abs_url,
                          owner_name=web.ctx.session.full_name,
                          project_url=kw['project'].abs_url,
                          project_name=kw['project'].name)

        return {'result' : 'ok'}


class ShowProjectLabMembers:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_show_project_lab_members.html')
    def GET(self, lab_id, project_id, **kw):

        user_u2lab = (web.ctx.orm.query(User, UserToLab)
                      .filter(User.id == UserToLab.user_id)
                      .filter(UserToLab.lab_id == kw['lab'].id)
                      .all())

        user_u2proj = (web.ctx.orm.query(User, UserToProject)
                       .filter(User.id == UserToProject.user_id)
                       .filter(UserToProject.project_id == kw['project'].id)
                       .all())

        users = []
        for luser, u2lab in user_u2lab:
            in_proj = False
            for puser, u2proj in user_u2proj:
                if luser.id == puser.id:
                    in_proj = True
            if not in_proj:
                users.append(luser)

        return {'lab'     : kw['lab'],
                'project' : kw['project'],
                'users'   : users}

    @has_relations('owner', 'member')
    @project_member
    @as_json
    def POST(self, lab_id, project_id, **kw):
        params = web.input()
        if 'user_id' not in params:
            return {'result' : 'error'}

        query = (web.ctx.orm.query(User, UserToProject)
                 .filter(User.id == UserToProject.user_id)
                 .filter(UserToProject.project_id == kw['project'].id)
                 .filter(UserToProject.user_id == params['user_id'])
                 .first())
        if query:
            return {'result' : 'error'}

        user = (web.ctx.orm.query(User, UserToLab)
                .filter(UserToLab.lab_id == kw['lab'].id)
                .filter(User.id == UserToLab.user_id)
                .filter(User.id == params['user_id'])
                .first())
        if not user:
            return {'result' : 'error'}
        user, u2lab = user

        u2proj = UserToProject(
            user_id    = user.id,
            project_id = kw['project'].id,
            relation   = UserToProject.REL_TYPES['member'],
            position   = UserToProject.POSITIONS['member'])
        web.ctx.orm.add(u2proj)

        my_event = MyEvent(
            user_id           = web.ctx.session.user_id,
            full_name         = web.ctx.session.full_name,
            lab_id            = kw['lab'].id,
            lab_name          = kw['lab'].name,
            project_id        = kw['project'].id,
            project_name      = kw['project'].name,
            event_type        = MyEvent.TYPES['user added to project'],
            event_object_type = MyEvent.OBJ_TYPES['user'],
            event_object_id   = user.id,
            event_object_name = user.full_name)
        web.ctx.orm.add(my_event)

        send_mail('user added to project',
                                        mail_to=user.email,
                                        user_name=user.first_name,
                                        owner_url=web.ctx.session.user_abs_url,
                                        owner_name=web.ctx.session.full_name,
                                        project_url=kw['project'].abs_url,
                                        project_name=kw['project'].name)


        return {'result': 'ok'}


class ShowProjectEvents:

    @has_relations('owner', 'member')
    @project_member
    @as_html('my/_show_project_events.html')
    def GET(self, lab_id, project_id, **kw):

        events = (web.ctx.orm.query(MyEvent)
                  .filter_by(lab_id     = lab_id)
                  .filter_by(project_id = project_id)
                  .order_by('-created')
                  .all()[:30])

        return {'project' : kw['project'],
                'events'  : events}
