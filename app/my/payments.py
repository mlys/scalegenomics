import calendar, web
from datetime import datetime
from collections import OrderedDict

import base
base.init_all_settings()

from app.my.models import LabInvoice, KVMServer, KVMServerUsageLog, \
    KVMStorage, KVMStorageUsageLog, LabPlan
from app.sso.models import User


def get_years_range(now):
    start = datetime(2011, 1, 1)
    return range(start.year, start.year + now.year - start.year + 1)


def dtime_total_seconds(dtime):
    # fix python2.6
    return ((dtime.microseconds + (dtime.seconds + dtime.days * 24 * 3600)
             * 10**6) / 10**6)


def round_price(price):
    return round(price, 2)


def server_monthly_price(log, prices, days):
    return ((log.cpu * prices['server-cpu-price'] +
             log.memory / 1024.0 * prices['server-ram-price']) * 24 * days +
            log.disk_size * prices['server-size-price'])


class CalcPayments(object):

    def __init__(self, lab_id, now, session=None):
        self.lab_id  = lab_id
        self.now     = now
        self.session = web.ctx.orm if session == None else session

    def set_times(self, year, month):

        start = datetime(year, month, 1)

        if self.now.month == month:
            end = self.now
        else:
            if month == 12:
                end = datetime(year+1, 1, 1)
            else:
                end = datetime(year, month+1, 1)

        # for example monthrange = (1, 31)
        monthrange = calendar.monthrange(year, month)

        self.times = { 'start' : start,
                       'end'   : end,
                       'days'  : monthrange[1] }

    def calc_month_servers(self, prices):

        self.prices = prices

        servers = self.get_servers_with_dtime()

        servers = self.servers_with_payments(servers)

        total_price = 0
        for server, user, logs in servers:
            for log in logs:
                total_price += log['price']

        return servers, total_price

    def get_servers_with_dtime(self):

        result = []

        servers = (self.session.query(User, KVMServer)
                   .filter(KVMServer.lab_id == self.lab_id)
                   .filter(KVMServer.user_id == User.id)
                   .all())

        for user, server in servers:
            logs = (self.session.query(KVMServerUsageLog)
                    .filter_by(server_id = server.id)
                    .order_by(KVMServerUsageLog.created)
                    .all())
            dtimes = []
            for log in logs:
                # not exists in time slice if
                # 1. deleted before
                # 2. created after
                if ((log.changed and log.changed < self.times['start']) or
                    log.created > self.times['end']):
                    continue

                if log.created < self.times['start']:
                    time_from = self.times['start']
                else:
                    time_from = log.created

                if log.changed == None:
                    dtime = self.times['end'] - time_from
                else:
                    if log.changed > self.times['end']:
                        dtime = self.times['end'] - time_from
                    else:
                        dtime = log.changed - time_from

                dtimes.append({ 'log'   : log,
                                'dtime' : dtime })

            if dtimes: result.append((server, user, dtimes))

        return result

    def servers_with_payments(self, servers):
        result = []
        for server, user, logs in servers:
            result.append((server, user, self.server_payments(logs)))
        return result

    def server_payments(self, logs):
        result = []
        for log in logs:
            dtime_sec  = log['dtime'].total_seconds()
            dtime_hrs  = dtime_sec / 3600.0
            dtime_days = dtime_hrs / 24.0

            m_price = server_monthly_price(log['log'],
                                           self.prices,
                                           self.times['days'])
            price = (dtime_days / self.times['days']) * m_price

            result.append({'log'     : log['log'],
                           'dtime'   : log['dtime'],
                           'm_price' : m_price,
                           'price'   : round_price(price) })
        return result

    def calc_month_storages(self, prices):

        self.prices = prices

        storages = self.get_storages_with_dtime()

        storages = self.storages_with_payments(storages)

        total_price = 0
        for storage, user, logs in storages:
            for log in logs:
                total_price += log['price']

        return storages, total_price

    def get_storages_with_dtime(self):

        result = []

        storages = (self.session.query(User, KVMStorage)
                    .filter(KVMStorage.user_id == User.id)
                    .filter(KVMStorage.lab_id == self.lab_id)
                    .all())

        for user, storage in storages:
            logs = (self.session.query(KVMStorageUsageLog)
                    .filter_by(storage_id = storage.id)
                    .order_by('created')
                    .all())
            dtimes = []
            for log in logs:
                # not exists in time slice if
                # 1. deleted before
                # 2. created after
                if ((log.changed and log.changed < self.times['start']) or
                    log.created > self.times['end']):
                    continue

                if log.created < self.times['start']:
                    time_from = self.times['start']
                else:
                    time_from = log.created

                if log.changed == None:
                    dtime = self.times['end'] - time_from
                else:
                    if log.changed > self.times['end']:
                        dtime = self.times['end'] - time_from
                    else:
                        dtime = log.changed - time_from
                dtimes.append({'log'   : log,
                               'dtime' : dtime})
            if dtimes: result.append((storage, user, dtimes))

        return result

    def storages_with_payments(self, storages):
        result = []
        for storage, user, logs in storages:
            result.append((storage, user, self.storage_payments(logs)))
        return result

    def storage_payments(self, logs):
        result = []
        for log in logs:
            m_price = log['log'].size / 1024.0 * self.prices['disk-size-price']

            dtime_sec  = log['dtime'].total_seconds()
            dtime_days = dtime_sec / (3600.0 * 24)

            disk_price = (dtime_days / self.times['days']) * m_price

            result.append({'log'     : log['log'],
                           'dtime'   : log['dtime'],
                           'm_price' : m_price,
                           'price'   : round_price(disk_price)})

        return result

    def calc_disk(self, disk, logs):
        result = []
        for log in logs:
            # if not exists in time slice
            # 1. if deleted before
            # 2. if created after
            if ((log.deleted and log.deleted < self.times['start']) or
                log.created > self.times['end']):
                continue

            if log.created < self.times['start']:
                time_from = self.times['start']
            else:
                time_from = log.created

            if log.deleted == None:
                dtime = self.times['end'] - time_from
            else:
                if log.deleted > self.times['end']:
                    dtime = self.times['end'] - time_from
                else:
                    dtime = log.deleted - time_from

            per_month = log.size / 1024.0 * self.prices['disk-size-price']
            day_price =  per_month / self.times['days']

            dtime_sec  = dtime_total_seconds(dtime)
            dtime_days = dtime_sec / (3600.0 * 24)

            disk_price = dtime_days * day_price

            result.append({'disk'  : disk,
                           'usage' : log,
                           'dtime' : dtime,
                           'cost'  : per_month,
                           'price' : round_price(disk_price)})
        return result



def calc_invoice(lab, prices, now, month, session=None):

    calc = CalcPayments(lab.id, session)
    servers, servers_total = calc.calc_month_servers(month, prices)
    disks, disks_total = calc.calc_month_disks(month, prices)

    total = servers_total + disks_total

    if total == 0: return None

    invoice = LabInvoice(lab_id     = lab.id,
                         date       = datetime(now.year, month, 1),
                         price      = total,
                         paid       = LabInvoice.PAYMENTS['NONE'],

                         server_cpu_price  = prices['server-cpu-price'],
                         server_ram_price  = prices['server-ram-price'],
                         server_size_price = prices['server-size-price'],
                         disk_size_price   = prices['disk-size-price'],)
    return invoice


def create_invoice(lab, prices, now, month, session=None):

    calc = CalcPayments(lab.id, session)
    servers, servers_total = calc.calc_month_servers(month, prices)
    disks, disks_total = calc.calc_month_disks(month, prices)

    total = servers_total + disks_total

    if total == 0: return None

    invoice = LabInvoice(lab_id     = lab.id,
                         date       = datetime(now.year, month, 1),
                         price      = total,
                         paid       = LabInvoice.PAYMENTS['NONE'],

                         server_cpu_price  = prices['server-cpu-price'],
                         server_ram_price  = prices['server-ram-price'],
                         server_size_price = prices['server-size-price'],
                         disk_size_price   = prices['disk-size-price'],)
    session.add(invoice)
    session.commit()

    return invoice


def calc_plans(lab, session=None):
    '''
    Fn.
    SQL:
    insert into lab_plans values (2, 1, 1, 1, 1, 99, 10, 10, 10, '2011-10-20 23:15:22', '2011-10-31 23:15:22');
    insert into lab_plans values (3, 1, 1, 2, 2, 199, 20, 20, 20, '2011-9-20 23:15:22', '2011-10-20 23:15:22');
    insert into lab_plans values (4, 1, 1, 3, 3, 299, 30, 30, 30, '2011-8-20 23:15:22', '2011-9-20 23:15:22');
    '''
    if not session: session=web.ctx.orm

    now = datetime.utcnow()
    od  = OrderedDict()
    for year in get_years_range(now):
        for month in range(1, 13):
            od[(year, month)] = []

    lab_plans = (session.query(LabPlan)
                 .filter_by(lab_id = lab.id)
                 .order_by(LabPlan.deleted.desc())
                 .all())
    # group by month
    for year, month in od.keys():
        for lplan in lab_plans:
            # later
            if lplan.created.year > year:
                continue
            if lplan.created.year == year and lplan.created.month > month:
                continue
            # before
            if lplan.deleted:
                if lplan.deleted.year < year:
                    continue
                if lplan.deleted.year == year and lplan.deleted.month < month:
                    continue
            od[(year, month)].append(lplan)

    # trim from beggining
    for key, val in od.items():
        if val == []:
            del od[key]
        else:
            break

    result = OrderedDict()
    for date, lab_plans in od.items():
        year, month = date
        result[date] = []
        if now.year == year and now.month == month:
            edate = now
        else:
            if month == 12:
                edate = datetime(year+1, 1, 1)
            else:
                edate = datetime(year, month+1, 1)

        for lplan in lab_plans:
            if lplan.created.month < month:
                sdate = datetime(year, month, 1)
            else:
                sdate = lplan.created
            if lplan.deleted:
                if lplan.deleted < edate:
                    dt = lplan.deleted - sdate
                else:
                    dt = edate - sdate
            else:
                dt = now - sdate
            result[date].append((lplan, dt))

    return result


def calc_plans_by_date(lab, now, year, month, session=None):
    if not session: session = web.ctx.orm

    # future
    if now.year < year: return []
    if now.year == year and now.month < month: return []

    lab_plans = (session.query(LabPlan)
                 .filter_by(lab_id = lab.id)
                 .order_by(LabPlan.deleted.desc())
                 .all())
    lab_plans = ([ lp for lp in lab_plans if lp.deleted == None] +
                 [ lp for lp in lab_plans if lp.deleted != None])

    lplans = []
    for lab_plan in lab_plans:
        # created after
        if lab_plan.created.year > year:
            continue
        if lab_plan.created.year == year and lab_plan.created.month > month:
            continue
        # deleted before
        if lab_plan.deleted:
            if lab_plan.deleted.year < year:
                continue
            if lab_plan.deleted.year == year and lab_plan.deleted.month < month:
                continue
        lplans.append(lab_plan)

    result = []
    for lab_plan in lplans:
        if now.year == year and now.month == month:
            edate = now
        else:
            if month == 12:
                edate = datetime(year+1, 1, 1)
            else:
                edate = datetime(year, month+1, 1)

        if lab_plan.created.month < month:
            sdate = datetime(year, month, 1)
        else:
            sdate = lab_plan.created
        if lab_plan.deleted:
            if lab_plan.deleted < edate:
                dt = lab_plan.deleted - sdate
            else:
                dt = edate - sdate
        else:
            dt = now - sdate
        result.append((lab_plan, dt))

    return result


if __name__ == '__main__':
    from settings.db import get_scoped_session
    session = get_scoped_session()

    from app.my.models import Lab
    labs = session.query(Lab).all()
    for lab in labs:
        calc_plans(lab, session)
