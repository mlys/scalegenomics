import web

from app.my.utils import get_my_labs_with_rel


def user_lab_list_processor(handler):
    '''Add user lab list to session if necessary.'''
    query = get_my_labs_with_rel()
    lst = []
    for lab, u2lab in query:
        lst.append((lab.id, lab.name))
    web.ctx.session.labs = lst

    return handler()
