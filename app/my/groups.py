

def group_rels_by_storage(rels):
    result = {}
    for rel in rels:
        if rel.storage_id not in result:
            result[rel.storage_id] = []
        result[rel.storage_id].append(rel)
    return result


def group_ssr_by_server(ssr):
    result = {}
    for server, storage, rel in ssr:
        if server.id not in result:
            result[server.id] = [server, []]
        result[server.id][1].append((storage, rel))
    return result.values()


def group_ssr_by_storage(ssr):
    result = {}
    for server, storage, rel in ssr:
        if storage.id not in result:
            result[storage.id] = [storage, []]
        result[storage.id][1].append((server, rel))
    return result.values()
