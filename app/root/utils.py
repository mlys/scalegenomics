import web

from app.root.models import SiteOption
from app.my.models   import Lab
from app.sso.models  import User, UserToLab


def get_vpn_users(session=None):
    if not session:
        session = web.ctx.orm

    labs = (session.query(Lab)
            .filter_by(status = Lab.STATUS["active"])
            .all())
    users = []
    for lab in labs:
        query = (session.query(User, UserToLab)
                 .filter(User.id == UserToLab.user_id)
                 .filter(lab.id  == UserToLab.lab_id)
                 .filter(UserToLab.perms.in_([
                     UserToLab.PERMS["owner"],
                     UserToLab.PERMS["member"]]))
                 .filter(User.is_active == True)
                 .all())
        for user, u2lab in query:
            if user not in users:
                users.append(user)
    return users


def get_option(name):
    return (web.ctx.orm.query(SiteOption)
            .filter_by(name = name)
            .first())
