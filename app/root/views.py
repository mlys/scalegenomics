import os
import cgi
import re
from datetime import datetime
from collections import OrderedDict

import web

from app.core.processors import staff_check
from app.core.decorators import as_html, as_json
from app.core.templates import filters
from app.core.server.ppp import gen_chap_secrets
from app.core.server import openvpn
from app.core.mail import send_mail
from app.core.types import FDict
from app.core.patterns import re_storage_name

from app.root.models import SiteSettings, ClusterServer, ClusterDisk, \
    MailQuery, CRMLogCommunication, DataSet, DataSetVolume, \
    LabClusterServer, DataSetRequest
from app.root.utils import get_vpn_users
from app.root.forms import SendMailForm, LogCommunicationForm, \
    DataSetForm, DataSetVolumeForm, DataSetRequestForm, LabLimitsForm, \
    ClusterDiskForm
from app.sso.models import User, UserToLab, UserCert
from app.my.models import Lab, LabInvoice, KVMServer, \
    KVMStorage, MyEvent, KVMMountRelation, LabPlan, LabProject
import app.my.payments as payments
from app.my.utils import set_flash_message, get_flash_message, get_dheight, \
    storage_editable, get_lab_projects

from daemons.net import ssl_send


urls = (
    "",                    "Index",

    "/cluster/servers",         "ShowClusterServers",
    "/cluster/servers/update",  "UpdateClusterServers",
    "/cluster/disks",           "ShowClusterDisks",
    "/cluster/disks/update",    "UpdateClusterDisk",
    "/cluster/disks/(\d+)",     "ShowClusterDisk",
    "/cluster/disks/new",       "NewClusterDisk",

    "/labs",                    "ShowLabs",
    "/labs/(\d+)",              "ShowLab",
    "/labs/(\d+)/edit/servers", "EditLabServers",
    "/labs/(\d+)/edit/limits",  "EditLabLimits",
    "/labs/(\d+)/chstate",      "ChangeLabState",

    "/resources", "ShowResources",
    "/limits",    "ShowLimits",

    "/users",                 "ShowUsers",
    "/users/tree",            "ShowUsersTree",
    "/users/vpn",             "ShowVPNUsers",
    "/users/vpnconn",         "ShowVPNConnections",
    "/users/(\d+)",           "ShowUser",
    "/users/(\d+)/activate",  "ActivateUser",
    "/users/(\d+)/block",     "BlockUser",
    "/users/(\d+)/edit",      "EditUser",
    "/users/(\d+)/setinv",    "SetUserInvitedBy",

    "/servers/running",  "ShowRunningServers",
    "/servers/removed",  "ShowRemovedServers",
    "/servers/others",   "ShowOthersServers",
    "/servers/(\d+)",    "ShowServer",

    "/storages/([a-z]+)",   "ShowStorages",
    "/storages/(\d+)",      "ShowStorage",
    "/storages/(\d+)/edit", "EditStorage",

    #"/invoices",        "ShowInvoices",
    #"/invoices/month",  "ShowMonthInvoices",
    #"/invoices/show",   "ShowInvoice",
    #"/invoices/edit",   "EditInvoice",
    "/mail", "ShowSendMail",

    "/events",     "ShowLastEvents",
    "/events/get", "GetLastEvents",

    "/crm",                 "ShowLastCommunications",
    "/crm/get",             "GetLogCommunications",
    "/crm/log/(\d+)",       "ShowLogCommunication",
    "/crm/log/new",         "NewLogCommunication",
    "/crm/log/new/(\d+)",   "NewLogCommunication",
    "/crm/user/get/(\d+)",  "GetUserCommunications",
    "/crm/log/(\d+)/edit",  "EditLogCommunication",

    "/datasets",            "ShowDataSets",
    "/dataset/new",         "NewDataSet",
    "/dataset/(\d+)",       "ShowDataSet",
    "/dataset/(\d+)/edit",  "EditDataSet",

    "/dataset/volumes",             "ShowDataSetVolumes",
    "/dataset/volume/new",          "NewDataSetVolume",
    "/dataset/volume/(\d+)",        "ShowDataSetVolume",
    "/dataset/volume/(\d+)/edit",   "EditDataSetVolume",

    "/regenppp", "RegenChapSecrets",

    "/dataset/requests",             "ShowDataSetRequests",
    "/dataset/request/new",          "NewDataSetRequest",
    "/dataset/request/(\d+)",        "ShowDataSetRequest",
    "/dataset/request/(\d+)/edit",   "EditDataSetRequest",
    "/dataset/(\d+)/volumes/load",   "LoadDSVolumes",

)


web_app = web.application(urls, locals())
web_app.add_processor(staff_check)


re_domain = re.compile("([a-zA-Z0-9\.]*[a-zA-Z0-9]{3,}\.[a-zA-Z]{2,4})")


def gb_to_str(size):
    if size > 1024:
        return "{} TB".format(round(size * 1.0 / 1024, 2))
    else:
        return "{} GB".format(size)


def check_roles(*roles):
    def decorator(f):
        def wrapper(*args, **kw):
            if web.ctx.user.staff_role_lower not in roles:
                raise web.notfound()
            return f(*args, **kw)
        return wrapper
    return decorator


class Index:

    @as_html("root/index.html")
    def GET(self):
        cservs = (web.ctx.orm.query(ClusterServer).all())
        tot_ram = sum([s.ram_total for s in cservs])
        all_servs = (web.ctx.orm.query(KVMServer)
                     .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                     .all())
        run_servs = [s for s in all_servs if s.status == s.STATUS["RUNNING"]]
        cpu = 0
        ram = 0
        for serv in run_servs:
            cpu += serv.cpu
            ram += serv.memory

        storages = (web.ctx.orm.query(KVMStorage)
                    .filter_by(status = KVMStorage.STATUS["CREATED"])
                    .all())
        tot_sto_size = sum([sto.size for sto in storages])

        return {"all_servs": all_servs,
                "run_servs": run_servs,
                "cpu":       cpu,
                "ram":       ram,
                "tot_ram":   tot_ram,
                "storages":  storages,
                "tot_sto_size": tot_sto_size}


class ShowClusterServers:

    @as_html("root/show_cluster_servers.html")
    def GET(self):
        servers = web.ctx.orm.query(ClusterServer).all()
        return {"section": "cservs",
                "servers": servers}


class UpdateClusterServers:

    @check_roles("admin")
    def GET(self):
        cservers = web.ctx.orm.query(ClusterServer).all()
        for cserv in cservers:
            cert = cserv.get_cert_path()
            cpu = ssl_send(cserv.ip, cserv.port, cert, {"endpoint":  "system",
                                                        "info":      "cpu"})
            ram = ssl_send(cserv.ip, cserv.port, cert, {"endpoint":  "system",
                                                        "info":      "ram"})
            cserv.cpu = cpu["cpu"]
            cserv.ram_total = ram["ram"]

        raise web.seeother(web.ctx.site_url + "/root/cluster/servers")


class ShowClusterDisks:

    @as_html("root/show_cluster_disks.html")
    def GET(self):
        disks = (web.ctx.orm.query(ClusterServer, ClusterDisk)
                 .filter(ClusterServer.id == ClusterDisk.server_id)
                 .order_by(ClusterDisk.server_id, ClusterDisk.mount_id)
                 .all())
        total_free = 0
        total_max  = 0
        for c_server, disk in disks:
            total_free += disk.free_size
            total_max  += disk.max_size

        return {"section":    "cdisks",
                "disks":      disks,
                "total_free": total_free,
                "total_max":  total_max}


class UpdateClusterDisk:

    @check_roles("admin")
    def GET(self):
        disks_info = {}
        cservers = web.ctx.orm.query(ClusterServer).all()
        for cserv in cservers:
            cert = cserv.get_cert_path()
            info = ssl_send(cserv.ip, cserv.port, cert, {"endpoint": "system",
                                                         "info":     "disks"})
            disks_info[cserv.id] = info.get("disks", {})
        for cserver in cservers:
            cdisks = (web.ctx.orm.query(ClusterDisk)
                      .filter_by(server_id = cserver.id)
                      .all())
            for cdisk in cdisks:
                cs = disks_info[cserver.id]
                if str(cdisk.mount_id) not in cs:
                    continue
                info = cs[str(cdisk.mount_id)]
                if cdisk.uuid != info["uuid"] and cdisk.uuid == "":
                    cdisk.uuid = info["uuid"]
                if cdisk.max_size != info["max_size"] / 1024.0:
                    cdisk.max_size = info["max_size"] / 1024.0
                if cdisk.dev_path != info["dev_path"]:
                    cdisk.dev_path = info["dev_path"]
        raise web.seeother(web.ctx.site_url + "/root/cluster/disks")


class ShowClusterDisk:

    @as_html("root/show_cluster_disk.html")
    def GET(self, disk_id):
        disk = (web.ctx.orm.query(ClusterDisk)
                .filter_by(id = disk_id)
                .first())
        if not disk:
            raise web.notfound()

        servers = (web.ctx.orm.query(KVMServer)
                   .filter_by(disk_id = disk.id)
                   .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                   .all())
        serv_tot = sum([server.size for server in servers])

        storages = (web.ctx.orm.query(User, KVMStorage)
                    .filter(KVMStorage.user_id == User.id)
                    .filter(KVMStorage.disk_id == disk.id)
                    .filter(KVMStorage.status < KVMStorage.STATUS["REMOVING"])
                    .all())
        stor_tot = sum([storage.size_gb for _, storage in storages])

        total_used = serv_tot + stor_tot
        #disk.free_size = disk.max_size - total_used
        return {"section":        "cdisks",
                "disk":           disk,
                "servers":        servers,
                "storages":       storages,
                "servers_total":  serv_tot,
                "storages_total": stor_tot,
                "total_used":     total_used}


class NewClusterDisk:

    @check_roles("admin")
    @as_html("root/new_cluster_disk.html")
    def GET(self):
        cservs = web.ctx.orm.query(ClusterServer).all()
        form = ClusterDiskForm(cservs=cservs)
        return {"section": "cdisks",
                "form":    form}

    @check_roles("admin")
    @as_html("root/new_cluster_disk.html")
    def POST(self):
        params = FDict(web.input())
        cservs = web.ctx.orm.query(ClusterServer).all()
        form = ClusterDiskForm(params, cservs)
        if not form.validate():
            return {"section": "cdisks",
                    "form":    form}
        cdisk = ClusterDisk(server_id = form.cserver.data,
                            uuid      = form.uuid.data,
                            dev_path  = form.dev_path.data,
                            mount_id  = form.mount_id.data,
                            status    = form.status.data,
                            free_size = form.free_size.data,
                            max_size  = form.max_size.data)
        web.ctx.orm.add(cdisk)
        raise web.seeother(web.ctx.site_url + "/root/cluster/disks")


class ShowResources:

    @as_html("root/show_resources.html")
    def GET(self):
        result = []
        tot_ser_count = 0
        tot_ser_size = 0
        tot_sto_size = 0
        tot_sto_count = 0
        tot_tot_size = 0

        labs = (web.ctx.orm.query(User, Lab)
                .filter(UserToLab.lab_id == Lab.id)
                .filter(UserToLab.user_id == User.id)
                .filter(UserToLab.perms == UserToLab.PERMS["owner"])
                .all())
        for user, lab in labs:
            servers = (web.ctx.orm.query(KVMServer)
                       .filter(KVMServer.lab_id == lab.id)
                       .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                       .all())
            ser_sum = sum([server.size for server in servers])

            storages = (web.ctx.orm.query(KVMStorage)
                .filter(KVMStorage.lab_id == lab.id)
                .filter(KVMStorage.status < KVMStorage.STATUS["REMOVING"])
                .all())
            sto_sum = sum([storage.size_gb for storage in storages])

            tot = ser_sum + sto_sum

            tot_ser_count += len(servers)
            tot_ser_size += ser_sum
            tot_sto_count += len(storages)
            tot_sto_size += sto_sum
            tot_tot_size += tot

            result.append({"lab":            lab,
                           "user":           user,
                           "servers_count":  len(servers),
                           "servers_size":   gb_to_str(ser_sum),
                           "storages_count": len(storages),
                           "storages_size":  gb_to_str(sto_sum),
                           "total_size":     gb_to_str(tot)})

        total = {"servers_count":  tot_ser_count,
                 "servers_size":   gb_to_str(tot_ser_size),
                 "storages_count": tot_sto_count,
                 "storages_size":  gb_to_str(tot_sto_size),
                 "total":          gb_to_str(tot_tot_size)}

        return {"section": "resources",
                "labs":    result,
                "total":   total}


class ShowLimits:

    @as_html("root/show_limits.html")
    def GET(self):
        labs = (web.ctx.orm.query(User, Lab)
                .filter(UserToLab.lab_id  == Lab.id)
                .filter(UserToLab.user_id == User.id)
                .filter(UserToLab.perms == UserToLab.PERMS["owner"])
                .all())
        return {"section": "limits",
                "labs":    labs}


class ShowLabs:

    @as_html("root/show_labs.html")
    def GET(self):
        labs = (web.ctx.orm.query(User, Lab)
                .filter(UserToLab.lab_id == Lab.id)
                .filter(UserToLab.user_id == User.id)
                .filter(UserToLab.perms == UserToLab.PERMS["owner"])
                .order_by(Lab.status)
                .all())
        return {"section": "labs",
                "labs":    labs}


class ShowLab:

    @as_html("root/show_lab.html")
    def GET(self, lab_id):
        lab = (web.ctx.orm.query(Lab)
               .filter_by(id = lab_id)
               .first())
        if not lab:
            raise web.notfound()
        lab_plan = (web.ctx.orm.query(LabPlan)
                    .filter_by(lab_id = lab.id)
                    .filter_by(deleted = None)
                    .first())
        owner = (web.ctx.orm.query(User, UserToLab)
                 .filter(UserToLab.user_id == User.id)
                 .filter(UserToLab.lab_id == lab.id)
                 .filter(UserToLab.perms == UserToLab.PERMS["owner"])
                 .first())
        owner = owner[0]
        lab_servers = (web.ctx.orm.query(ClusterServer, LabClusterServer)
                    .filter(LabClusterServer.server_id == ClusterServer.id)
                    .filter(LabClusterServer.lab_id == lab.id)
                    .order_by(ClusterServer.id)
                    .all())
        members = (web.ctx.orm.query(User, UserToLab)
                   .filter(User.id == UserToLab.user_id)
                   .filter(UserToLab.lab_id == lab.id)
                   .order_by(UserToLab.perms)
                   .all())
        servers = (web.ctx.orm.query(User, KVMServer)
                   .filter(User.id == KVMServer.user_id)
                   .filter(KVMServer.lab_id == lab_id)
                   .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                   .order_by(User.id)
                   .all())
        storages = (web.ctx.orm.query(User, KVMStorage)
                    .filter(User.id == KVMStorage.user_id)
                    .filter(KVMStorage.lab_id == lab_id)
                    .filter(KVMStorage.status < KVMStorage.STATUS["REMOVING"])
                    .all())
        sto_ser = []
        for user, storage in storages:
            rel = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                   .filter(KVMMountRelation.server_id  == KVMServer.id)
                   .filter(KVMMountRelation.storage_id == storage.id)
                   .first())
            if rel:
                sto_ser.append((user, storage, rel[0]))
            else:
                sto_ser.append((user, storage, None))
        return {"section":     "labs",
                "lab":         lab,
                'lab_servers': lab_servers,
                "lab_plan":    lab_plan,
                "owner":       owner,
                "members":     members,
                "servers":     servers,
                "storages":    sto_ser,
                "message":     get_flash_message()}


class EditLabServers:

    @check_roles('admin')
    @as_html('root/edit_lab_servers.html')
    def GET(self, lab_id):
        lab = (web.ctx.orm.query(Lab)
                .filter_by(id = lab_id)
                .first())
        if not lab:
            raise web.notfound()

        cservs = web.ctx.orm.query(ClusterServer).all()
        lab_servers = (web.ctx.orm.query(LabClusterServer)
                        .filter_by(lab_id = lab.id)
                        .all())
        result = []
        for cserv in cservs:
            is_added = False
            for rel in lab_servers:
                if rel.server_id == cserv.id:
                    result.append((cserv, rel))
                    is_added = True
                    break
            if not is_added:
                result.append((cserv, None))

        return {'section':     'labs',
                'lab':         lab,
                'lab_servers': result,
                'usage_types': LabClusterServer.USAGE_TYPES}

    @check_roles('admin')
    def POST(self, lab_id):
        lab = (web.ctx.orm.query(Lab)
                .filter_by(id = lab_id)
                .first())
        if not lab:
            raise web.notfound()

        cservs = web.ctx.orm.query(ClusterServer).all()
        cservs_id = [cs.id for cs in cservs]

        lab_servers = (web.ctx.orm.query(LabClusterServer)
                        .filter_by(lab_id = lab.id)
                        .all())

        params = web.input()
        servers_d = self.filter_params(params)
        rels = []
        # remove unselected relations or modify status
        for rel in lab_servers:
            if rel.server_id not in servers_d:
                web.ctx.orm.delete(rel)
            else:
                usage_type = servers_d[rel.server_id]
                if (usage_type != rel.usage_type and
                    usage_type in LabClusterServer.USAGE_TYPES.values()):
                    rel.usage_type = usage_type
                rels.append(rel.server_id)

        # create new
        for server_id, usage_type in servers_d.items():
            if server_id not in rels and server_id in cservs_id:
                if usage_type in LabClusterServer.USAGE_TYPES.values():
                    lcs = LabClusterServer(lab_id     = lab.id,
                                           server_id  = server_id,
                                           usage_type = usage_type)
                    web.ctx.orm.add(lcs)
                    rels.append(server_id)

        r_url = '/root/labs/{}/edit/servers'.format(lab.id)
        raise web.seeother(web.ctx.site_url + r_url)

    def filter_params(self, params):
        '''
            params = {'server_1':     'on',
                      'server_2':     'on',
                      'usage_type_1': '0',
                      'usage_type_2': '1',
                      'usage_type_3': '0'}
            result = {1: 0, 2: 1}
        '''
        servers = []
        usage_types = {}
        for key, val in params.items():
            if key.startswith('server_'):
                servers.append(int(key[len('server_'):]))
            elif key.startswith('usage_type_'):
                t = int(key[len('usage_type_'):])
                usage_types[t] = int(val)

        result = {}
        for serv in servers:
            if serv in usage_types:
                result[serv] = usage_types[serv]
        return result


class EditLabLimits:

    @check_roles("admin")
    @as_html("root/edit_lab_limits.html")
    def GET(self, lab_id):
        lab = (web.ctx.orm.query(Lab)
                .filter_by(id = lab_id)
                .first())
        if not lab:
            raise web.notfound()

        form = LabLimitsForm()
        form.reinit(lab)
        plan = (web.ctx.orm.query(LabPlan)
                    .filter_by(lab_id = lab.id)
                    .filter_by(deleted = None)
                    .first())

        return {"section": "labs",
                "lab":     lab,
                "plan":    plan,
                "form":    form}

    @check_roles("admin")
    @as_html("root/edit_lab_limits.html")
    def POST(self, lab_id):
        params = FDict(web.input())
        if not ("cpu_limit" in params,
                "ram_limit" in params,
                "storages_limit" in params):
            raise web.notfound()

        lab = (web.ctx.orm.query(Lab)
                .filter_by(id = lab_id)
                .first())
        if not lab:
            raise web.notfound()

        plan = (web.ctx.orm.query(LabPlan)
                    .filter_by(lab_id = lab.id)
                    .filter_by(deleted = None)
                    .first())
        if not plan:
            raise web.notfound()
        if not plan.is_plan("Personal"):
            raise web.notfound()

        form = LabLimitsForm(params)
        if not form.validate():
            return {"section": "labs",
                    "lab": lab,
                    "form": form}

        new_cpu = int(form.cpu_limit.data)
        new_ram = int(form.ram_limit.data)
        new_storages = int(form.storages_limit.data)

        if (lab.cpu_limit != new_cpu or
            lab.ram_limit != new_ram or
            lab.storages_limit != new_storages):

            lab.cpu_limit = new_cpu
            lab.ram_limit = new_ram
            lab.storages_limit = new_storages
            plan.deleted = datetime.utcnow()

            new_ram = new_ram / 1024.0
            new_storages = new_storages / 1024.0 / 1024.0
            plan = LabPlan(lab_id  = plan.lab_id,
                           plan_id = plan.plan_id,
                           name    = plan.name,
                           price   = plan.price,
                           cores   = new_cpu,
                           ram     = new_ram,
                           storage = new_storages)
            web.ctx.orm.add(plan)

        set_flash_message("Limits updated")
        raise web.seeother(web.ctx.site_url + "/root/labs/{}".format(lab.id))


class ChangeLabState:

    @as_json
    def POST(self, lab_id):
        qr = (web.ctx.orm.query(User, Lab, UserToLab)
               .filter(UserToLab.user_id == User.id)
               .filter(UserToLab.lab_id == Lab.id)
               .filter(UserToLab.perms == UserToLab.PERMS["owner"])
               .filter(Lab.id == lab_id)
               .first())
        if not qr:
            return {"result": "error"}
        owner, lab, u2lab = qr

        gen_chap_secrets()

        if lab.status == Lab.STATUS["active"]:
            lab.status = Lab.STATUS["not verified"]
            return {"result": "ok",
                    "state":  lab.status_str}
        else:
            lab_plan = (web.ctx.orm.query(LabPlan)
                        .filter_by(lab_id = lab.id)
                        .filter_by(deleted = None)
                        .first())
            lab_plan.created = datetime.utcnow()

            lab.status = Lab.STATUS["active"]
            send_mail("lab verified", mail_to=owner.email,
                                      lab_url=lab.abs_url,
                                      lab_name=lab.name)
            return {"result": "ok",
                    "state":  lab.status_str}


class ShowUsers:

    @as_html("root/show_users.html")
    def GET(self):
        "Display users by belongs to."
        result = []
        users = web.ctx.orm.query(User).all()
        for user in users:
            if user.invited_by:
                nu = (web.ctx.orm.query(User)
                      .filter_by(id = user.invited_by)
                      .first())
                result.append((user, nu))
            else:
                result.append((user, None))

        return {"section": "users",
                "users":   result}


class ShowVPNUsers:

    @as_html("root/show_vpn_users.html")
    def GET(self):
        users = get_vpn_users()
        return {"section": "vpn",
                "message": get_flash_message(),
                "users":   users}


class ShowVPNConnections:

    @as_html("root/show_vpn_connections.html")
    def GET(self):
        log_path = web.config["SG_SETTINGS"]["OPENVPN_STATUS_LOG"]
        response = {"section": "vpn-conn",
                    "stats":   []}
        try:
            with open(log_path) as f:
                f
        except IOError as err:
            if "No such file or directory" in err.strerror:
                response["message"] = ("No such file or directory {!r}"
                                       .format(log_path))
                return response
            if "Permission denied" in err.strerror:
                response["message"] = ("Permission denied {!r}"
                                       .format(log_path))
                return response
        stats = openvpn.statistics()
        for stat in stats:
            id = UserCert.get_id(stat["cn"])
            qry = (web.ctx.orm.query(User, UserCert)
                   .filter(User.id == UserCert.user_id)
                   .filter(UserCert.id == id)
                   .first())
            if qry:
                stat["user"] = qry[0]
                stat["cert"] = qry[1]
        response["stats"] = stats
        return response


class ShowUsersTree:

    def get_chlds(self, user):
        chlds = (web.ctx.orm.query(User)
                 .filter_by(invited_by = user.id)
                 .all())
        result = []
        for child in chlds:
            result.append({child: self.get_chlds(child)})
        return result

    @as_html("root/show_users_tree.html")
    def GET(self):
        "Display users tree."
        users = (web.ctx.orm.query(User)
                 .filter_by(invited_by = None)
                 .order_by(User.id)
                 .all())
        result = OrderedDict()
        for user in users:
            result[user] = self.get_chlds(user)

        return {"section": "users",
                "users":   result}


class ShowUser:

    @as_html("root/show_user.html")
    def GET(self, user_id):
        user = (web.ctx.orm.query(User)
                .filter_by(id = user_id)
                .first())
        if not user:
            raise web.notfound()

        invited_by = None
        if user.invited_by:
            invited_by = (web.ctx.orm.query(User)
                          .filter_by(id = user.invited_by)
                          .first())

        l_u2l = (web.ctx.orm.query(Lab, UserToLab)
                .filter(UserToLab.user_id == user.id)
                .filter(UserToLab.lab_id == Lab.id)
                .filter(UserToLab.perms.in_([UserToLab.PERMS["owner"],
                                             UserToLab.PERMS["member"]]))
                .order_by(UserToLab.perms)
                .all())
        return {"section":     "users",
                "xuser":       user,
                "invited_by":  invited_by,
                "lab_rels":    l_u2l}


class EditUser:

    @check_roles("admin")
    @as_html("root/edit_user.html")
    def GET(self, user_id):
        user = web.ctx.orm.query(User).filter_by(id = user_id).first()
        if not user:
            raise web.notfound()
        return {"section": "users",
                "xuser":   user}

    @check_roles("admin")
    def POST(self, user_id):
        params = web.input()
        if "description" not in params:
            raise web.notfound()

        user = web.ctx.orm.query(User).filter_by(id = user_id).first()
        if not user:
            raise web.notfound()

        user.description = cgi.escape(params["description"])

        raise web.seeother(web.ctx.site_url + user.root_url)


class SetUserInvitedBy:

    @check_roles("admin")
    @as_html("root/set_user_invited_by.html")
    def GET(self, user_id):
        user = (web.ctx.orm.query(User)
                .filter_by(id = user_id)
                .first())
        if not user:
            raise web.notfound()

        invited_by = None
        if user.invited_by:
            whom = (web.ctx.orm.query(User)
                    .filter_by(id = user.invited_by)
                    .first())
            if whom:
                invited_by = whom

        return {"section":    "users",
                "xuser":      user,
                "invited_by": invited_by}

    @check_roles("admin")
    def POST(self, user_id):
        params = web.input()
        if "username" not in params:
            raise web.notfound()

        who = (web.ctx.orm.query(User)
               .filter_by(id = user_id)
               .first())
        if not who:
            raise web.notfound()

        if params["username"] == "":
            who.invited_by = None
        else:
            whom = (web.ctx.orm.query(User)
                    .filter_by(username = params["username"])
                    .first())
            if not whom:
                raise web.notfound()

            if who.id == whom.id:
                raise web.notfound()

            who.invited_by = whom.id

        raise web.seeother(web.ctx.site_url + who.root_url)


class ActivateUser:

    @check_roles("admin")
    @as_json
    def POST(self, user_id):
        user = web.ctx.orm.query(User).filter_by(id = user_id).first()
        if not user:
            return {"result": "error"}
        if user.is_active:
            return {"result": "error"}
        user.is_active = True
        gen_chap_secrets()
        return {"result": "ok"}


class BlockUser:

    @check_roles("admin")
    @as_json
    def POST(self, user_id):
        user = web.ctx.orm.query(User).filter_by(id = user_id).first()
        if not user:
            return {"result": "error"}
        if not user.is_active:
            return {"result": "error"}
        user.is_active = False
        return {"result": "ok"}


class ShowRunningServers:

    @as_html("root/show_servers.html")
    def GET(self):
        servers = (web.ctx.orm.query(User, KVMServer, ClusterServer)
                   .filter(ClusterServer.id == KVMServer.server_id)
                   .filter(User.id == KVMServer.user_id)
                   .filter(KVMServer.status == KVMServer.STATUS["RUNNING"])
                   .order_by(KVMServer.id)
                   .all())
        total_ram = sum([server.memory for _, server, _ in servers])
        return {"section":     "servs",
                "page":        "running",
                "servers":     servers,
                "total_ram":   total_ram,
                "len_servers": len(servers)}


class ShowRemovedServers:

    @as_html("root/show_servers.html")
    def GET(self):
        servers = (web.ctx.orm.query(User, KVMServer, ClusterServer)
                   .filter(ClusterServer.id == KVMServer.server_id)
                   .filter(User.id == KVMServer.user_id)
                   .filter(KVMServer.status.in_([KVMServer.STATUS["REMOVING"],
                                                 KVMServer.STATUS["REMOVED"]]))
                   .order_by(KVMServer.id)
                   .all())
        total_ram = sum([server.memory for _, server, _ in servers])
        return {"section":     "servs",
                "page":        "removed",
                "servers":     servers,
                "total_ram":   total_ram,
                "len_servers": len(servers)}


class ShowOthersServers:

    @as_html("root/show_servers.html")
    def GET(self):
        servers = (web.ctx.orm.query(User, KVMServer, ClusterServer)
                   .filter(ClusterServer.id == KVMServer.server_id)
                   .filter(User.id == KVMServer.user_id)
                   .filter(KVMServer.status != KVMServer.STATUS["RUNNING"])
                   .filter(KVMServer.status != KVMServer.STATUS["REMOVING"])
                   .filter(KVMServer.status != KVMServer.STATUS["REMOVED"])
                   .order_by(KVMServer.id)
                   .all())
        total_ram = sum([server.memory for _, server, _ in servers])
        return {"section":     "servs",
                "page":        "others",
                "servers":     servers,
                "total_ram":   total_ram,
                "len_servers": len(servers)}


class ShowServer:

    @as_html("root/show_server.html")
    def GET(self, server_id):
        server = (web.ctx.orm.query(KVMServer)
                  .filter_by(id = server_id)
                  .first())
        if not server:
            raise web.notfound()
        lab = (web.ctx.orm.query(Lab)
               .filter_by(id = server.lab_id)
               .first())
        return {"section": "servs",
                "server":  server,
                "lab":     lab}


class ShowStorages:

    @as_html("root/show_storages.html")
    def GET(self, status):
        statuses = [s.lower() for s in KVMStorage.STATUS.keys()]
        if status not in statuses:
            status = statuses[0]

        storages = (web.ctx.orm.query(User, KVMStorage, ClusterServer)
                    .filter(KVMStorage.user_id == User.id)
                    .filter(KVMStorage.server_id == ClusterServer.id)
                    .filter(KVMStorage.status ==
                            KVMStorage.STATUS[status.upper()])
                    .all())
        result = []
        tot_size = 0
        for user, stor, cserv in storages:
            tot_size += stor.size
            rel = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                   .filter(KVMMountRelation.server_id == KVMServer.id)
                   .filter(KVMMountRelation.storage_id == stor.id)
                   .first())
            if rel:
                result.append((user, stor, cserv, rel[0]))
            else:
                result.append((user, stor, cserv, None))

        return {"section":    "stors",
                "page":       status,
                "storages":   result,
                "total_size": tot_size}


class ShowStorage:

    @as_html("root/show_storage.html")
    def GET(self, storage_id):
        query = (web.ctx.orm.query(User, KVMStorage, LabProject)
                    .filter(KVMStorage.user_id == User.id)
                    .filter(KVMStorage.project_id == LabProject.id)
                    .filter(KVMStorage.id == storage_id)
                    .first())
        if not query:
            raise web.notfound()
        owner, storage, project = query

        rel = None
        if storage.is_type("LOCAL"):
            rel = (web.ctx.orm.query(KVMServer, KVMMountRelation)
                .filter(KVMServer.id == KVMMountRelation.server_id)
                .filter(storage.id == KVMMountRelation.storage_id)
                .first())

        return {"section": "stors",
                "owner":   owner,
                "storage": storage,
                "project": project,
                "rel":     rel}


class EditStorage:

    @as_html("root/edit_storage.html")
    def GET(self, storage_id):
        query = (web.ctx.orm.query(KVMStorage, LabProject)
                    .filter(KVMStorage.id == storage_id)
                    .filter(KVMStorage.project_id == LabProject.id)
                    .first())
        if not query:
            raise web.notfound()

        storage, project = query
        editable = storage_editable(storage)
        projects = get_lab_projects(storage.lab_id)

        return {"section":  "stors",
                "storage":  storage,
                "project":  project,
                "editable": editable,
                "projects": projects,
                "dheight":  get_dheight(storage.description)}

    @check_roles("admin")
    @as_html("root/edit_storage.html")
    def POST(self, storage_id):
        params = web.input()
        if not ("name" in params and
                "alias" in params and
                "description" in params and
                "status" in params):
            return {"result": "error"}

        query = (web.ctx.orm.query(KVMStorage, LabProject)
                    .filter(KVMStorage.id == storage_id)
                    .filter(KVMStorage.project_id == LabProject.id)
                    .first())
        if not query:
            raise web.notfound()

        storage, project = query
        editable = storage_editable(storage)
        projects = get_lab_projects(storage.lab_id)

        response = {"section":  "stors",
                    "storage":  storage,
                    "project":  project,
                    "editable": editable,
                    "projects": projects,
                    "dheight":  get_dheight(query[0].description)}

        name = re_storage_name.findall(params["name"])
        if not name:
            response["message"] = "wrong name"
            return response
        name = name[0]

        if not params["status"].isdigit():
            response["message"] = "wrong status"
            return response

        status = int(params["status"])
        if status not in KVMStorage.STATUS.values():
            response["message"] = "wrong status"
            return response

        if editable:
            alias = KVMStorage.get_alias(params["alias"], name)
            if alias != storage.alias:
                alias_check = (web.ctx.orm.query(KVMStorage)
                                .filter_by(lab_id = storage.lab_id)
                                .filter_by(alias = alias)
                                .first())
                if alias_check and alias_check.id != storage.id:
                    response["message"] = "wrong alias"
                storage.alias = alias

        storage.name = name
        storage.description = cgi.escape(params["description"])
        storage.status = status
        return response


class ShowInvoices:

    @as_html("root/show_site_invoices.html")
    def GET(self):

        sites = web.ctx.orm.query(SiteSettings).all()
        now = datetime.utcnow()
        if now.month == 1:
            selected_month = 1
        else:
            selected_month = now.month - 1

        return {
            "sites": sites,
            "now":   now,
            "selected_month": selected_month,
        }


class ShowMonthInvoices:

    @as_html("root/_show_site_month_invoices.html")
    def GET(self):
        params = web.input()
        if "month" not in params:
            raise web.notfound()

        if not params["month"].isdigit:
            raise web.notfound()
        month = int(params["month"])
        if month < 1 or month > 12:
            raise web.notfound()

        now = datetime.utcnow()
        if month > now.month:
            return {"future": True}
        elif month == now.month:
            return {"current": True}

        date = datetime(now.year, month, 1)
        invoices = (web.ctx.orm.query(Lab, LabInvoice)
                    .filter(LabInvoice.date == date)
                    .filter(LabInvoice.lab_id == Lab.id)
                    .order_by(LabInvoice.paid)
                    .all())
        inv_paid = []
        inv_not_paid = []
        for lab, inv in invoices:
            if inv.paid == LabInvoice.PAYMENTS["NONE"]:
                inv_not_paid.append((lab, inv))
            else:
                inv_paid.append((lab, inv))

        return {"inv_not_paid": inv_not_paid,
                "inv_paid":     inv_paid}


class ShowInvoice:

    @as_html("root/_show_invoice.html")
    def GET(self):
        params = web.input()
        if not ("lab_id" in params and
                "month" in params):
            raise web.notfound()

        if not params["month"].isdigit():
            raise web.notfound()
        month = int(params["month"])
        if month < 1 or month > 12:
            raise web.notfoud()

        now = datetime.utcnow()
        date = datetime(now.year, month, 1)
        query = (web.ctx.orm.query(Lab, LabInvoice)
                 .filter(LabInvoice.lab_id == params["lab_id"])
                 .filter(LabInvoice.date == date)
                 .filter(LabInvoice.lab_id == Lab.id)
                 .first())
        if not query:
            raise web.notfound()
        lab, invoice = query
        prices = invoice.get_prices()

        calc = payments.CalcPayments(params["lab_id"])
        servers, servers_total = calc.calc_month_servers(month, prices)
        disks, disks_total = calc.calc_month_disks(month, prices)

        return {"servers":       servers,
                "servers_total": servers_total,
                "disks":         disks,
                "disks_total":   disks_total,
                "total":         servers_total + disks_total,
                "lab":           lab,
                "invoice":       invoice}


class EditInvoice:

    def __validate(self):
        params = web.input()
        if not ("lab_id" in params and
                "month" in params):
            raise web.notfound()

        if not params["month"].isdigit():
            raise web.notfound()
        month = int(params["month"])
        if month < 1 or month > 12:
            raise web.notfoud()

        now = datetime.utcnow()
        date = datetime(now.year, month, 1)
        query = (web.ctx.orm.query(Lab, LabInvoice)
                 .filter(LabInvoice.lab_id == params["lab_id"])
                 .filter(LabInvoice.date == date)
                 .filter(LabInvoice.lab_id == Lab.id)
                 .first())
        if not query:
            raise web.notfound()
        lab, invoice = query
        return params, lab, invoice, month

    @as_html("root/_edit_invoice.html")
    def GET(self):

        params, lab, invoice, month = self.__validate()

        return {
            "lab":     lab,
            "invoice": invoice,
        }

    @as_json
    def POST(self):

        params, lab, invoice, month = self.__validate()

        if not ("server_cpu" in params and
                "server_ram" in params and
                "server_size" in params and
                "disk_size" in params and
                "paid" in params):
            return {"result": "error"}
        try:
            server_cpu  = float(params["server_cpu"])
            server_ram  = float(params["server_ram"])
            server_size = float(params["server_size"])
            disk_size   = float(params["disk_size"])
        except ValueError:
            return {"result": "error"}
        pay_keys = LabInvoice.PAYMENTS.keys()
        if params["paid"] not in pay_keys:
            return {"result": "error"}

        invoice.server_cpu_price  = server_cpu
        invoice.server_ram_price  = server_ram
        invoice.server_size_price = server_size
        invoice.disk_size_price   = disk_size
        invoice.paid = LabInvoice.PAYMENTS[params["paid"]]
        web.ctx.orm.commit()

        prices = invoice.get_prices()
        calc = payments.CalcPayments(params["lab_id"])
        servers, servers_total = calc.calc_month_servers(month, prices)
        disks, disks_total = calc.calc_month_disks(month, prices)

        invoice.price = servers_total + disks_total
        return {"result": "ok"}


class ShowLastEvents:

    @as_html("root/show_last_events.html")
    def GET(self):
        return {"section": "events"}


class GetLastEvents:

    @as_json
    def GET(self):
        params = web.input()
        start = 0
        if "start" in params and params["start"].isdigit():
            start = int(params["start"])

        end = start + MyEvent.OFFSET
        events = (web.ctx.orm.query(MyEvent)
                 .order_by(MyEvent.created.desc())
                 .all())[start:end]

        return [{"data": filters.datetimeformat(event.created),
                 "text": event.text} for event in events]


class ShowSendMail:

    @as_html("root/show_send_mail.html")
    def GET(self):
        users = get_vpn_users()
        return {"form":  SendMailForm(),
                "users": ", ".join(user.email for user in users)}

    @as_html("root/show_send_mail.html")
    def POST(self):
        params = FDict(web.input())
        form = SendMailForm(params)
        if not form.validate():
            return {"form": form}

        header = "<p>Dear {},</p>"
        footer = """<p>-- Your SG Team (
<a href="mailto:support@scalegenomics.com">support@scalegenomics.com</a>)
</p>
"""
        for mail_to in re.split(",\s*", form.mail_to.data):
            user = (web.ctx.orm.query(User)
                    .filter_by(email = mail_to)
                    .first())
            if not user:
                continue
            body = header.format(user.full_name) + form.body.data + footer
            m = MailQuery(
                mail_to   = mail_to,
                mail_from = web.config["SG_SETTINGS"]["DEFAULT_MAIL_FROM"],
                subject   = form.subject.data,
                body      = body)
            web.ctx.orm.add(m)

        return {"form":    SendMailForm(),
                "message": "Email addded to query"}


class ShowLastCommunications:

    @as_html('root/show_log_communications.html')
    def GET(self):
        return {'cpage':    'crm_log',
                'section':  'crm'}


class GetLogCommunications:

    CLIENT_LINK = ('<a href="{}">{}</a>')
    LOG_LINK = ('<a href="/root/crm/log/{}">{}</a>')

    @as_json
    def GET(self):
        communications = (web.ctx.orm.query(CRMLogCommunication)
                 .order_by(CRMLogCommunication.created.desc())
                 .all())

        log_communications = []
        for log in communications:
            user = (web.ctx.orm.query(User)
                    .filter_by(id = log.user_id)
                    .first())
            if user:
                log_communications.append(
                    {"id": log.id,
                     "cell": [filters.datetimeformat(log.created),
                        self.CLIENT_LINK.format(user.root_url, user.full_name),
                        self.LOG_LINK.format(log.id, log.subject),
                               log.direction_str]})

        return {
            'page':    '1',
            'total':   len(log_communications),
            'records': len(log_communications),
            'rows':    log_communications
        }


class GetUserCommunications:

    CLIENT_LINK = ('<a href="{}">{}</a>')
    LOG_LINK = ('<a href="/root/crm/log/{}">{}</a>')

    @as_json
    def GET(self, user_id):
        communications = (web.ctx.orm.query(CRMLogCommunication)
                .filter_by(user_id = user_id)
                .order_by(CRMLogCommunication.created.desc())
                .all())

        user = (web.ctx.orm.query(User)
                .filter_by(id = user_id)
                .first())

        log_communications = []
        for log in communications:
            log_communications.append(
                {"id": log.id,
                 "cell": [filters.datetimeformat(log.created),
                        self.CLIENT_LINK.format(user.root_url, user.full_name),
                        self.LOG_LINK.format(log.id, log.subject),
                        log.direction_str]}
            )

        return {'page':    '1',
                'total':   len(log_communications),
                'records': len(log_communications),
                'rows':    log_communications}


class ShowLogCommunication:

    @as_html("root/show_log_communication.html")
    def GET(self, log_id):
        """Display all Client details and Communication details.
        """

        communication = (web.ctx.orm.query(CRMLogCommunication)
                 .filter_by(id = log_id)
                 .first())
        if not communication:
            raise web.notfound()

        user = (web.ctx.orm.query(User)
                    .filter_by(id = communication.user_id)
                    .first())

        return {"communication":    communication,
                "xuser":            user,
                "section":          "crm"}


class NewLogCommunication:

    @check_roles("admin", "crm")
    @as_html("root/new_log_communication.html")
    def GET(self, user_id=None):
        """Display form for new Communication Log.
        """
        users = []
        if user_id:
            user = (web.ctx.orm.query(User)
                    .filter_by(id = user_id)
                    .first())
            if not user:
                raise web.seeother('/users')
            else:
                users.append(user)
        else:
            users = (web.ctx.orm.query(User)
                    .filter_by(position = User.POSITIONS["Client"])
                    .order_by(User.first_name)
                    .all())

        form = LogCommunicationForm()
        form.user_id.choices = [(str(user.id), user.full_name)
                                for user in users]

        return {"form":     form,
                "section":  "crm"}

    @as_html("root/new_log_communication.html")
    def POST(self, user_id=None):
        """Get form data and create new Communication Log.
        """
        users = (web.ctx.orm.query(User)
                 .filter_by(position = User.POSITIONS["Client"])
                 .all())

        form_data = FDict(web.input())
        form = LogCommunicationForm(form_data)
        # update users in SelectField again to pass core validator
        form.user_id.choices = [(str(user.id), user.full_name)
                                for user in users]
        if not form.validate():
            return {"log_message":  "Please fix red labeled fields.",
                    "form":         form,
                    "section":      "crm"}

        new_log = CRMLogCommunication(
                        user_id   = int(form_data['user_id']),
                        subject   = form_data['subject'],
                        body      = cgi.escape(form_data['body']),
                        log_type  = form_data['log_type'],
                        direction = form_data['direction'])
        web.ctx.orm.add(new_log)
        web.ctx.orm.commit()

        raise web.seeother(web.ctx.site_url + '/root/crm')


class EditLogCommunication:

    @check_roles("admin", "crm")
    @as_html("root/edit_log_communication.html")
    def GET(self, log_id):
        """Get log data for form from DB.
        """
        communication = (web.ctx.orm.query(CRMLogCommunication)
                            .filter_by(id = log_id)
                            .first())
        if not communication:
            raise web.notfound()

        user = (web.ctx.orm.query(User)
                    .filter_by(id = communication.user_id)
                    .first())

        form = LogCommunicationForm()
        form.user_id.choices = [(str(user.id), user.full_name)]
        form.subject.data = communication.subject
        form.body.data    = communication.body
        if communication.direction:
            form.direction.choices = [(communication.direction,
                                      dict(CRMLogCommunication.LOG_DIRECTIONS)
                                      .get(communication.direction))]
        if communication.log_type:
            form.log_type.choices = [(communication.log_type,
                                        dict(CRMLogCommunication.LOG_TYPES)
                                        .get(communication.log_type))]

        return {"form":     form,
                "section":  "crm"}

    @as_html("root/edit_log_communication.html")
    def POST(self, log_id):
        """Update Communication Log from edit form.
        """
        communication = (web.ctx.orm.query(CRMLogCommunication)
                        .filter_by(id = log_id)
                        .first())
        if not communication:
            raise web.notfound()

        form_data = FDict(web.input())
        form = LogCommunicationForm(form_data)
        # update users in SelectField again to pass core validator
        users = (web.ctx.orm.query(User)
                 .filter_by(position = User.POSITIONS["Client"])
                 .all())
        form.user_id.choices = [(str(user.id), user.full_name)
                                for user in users]
        if not form.validate():
            return {"log_message":  "Please fix red labeled fields.",
                    "form":         form,
                    "section":      "crm"}

        communication.subject   = form_data['subject']
        communication.body      = cgi.escape(form_data['body'])
        communication.log_type  = form_data['log_type']
        communication.direction = form_data['direction']

        raise web.seeother(web.ctx.site_url + '/root/crm/log/' + log_id)


class ShowDataSets:

    @as_html("root/show_datasets.html")
    def GET(self):
        datasets = (web.ctx.orm.query(User, Lab, DataSet)
                    .filter(DataSet.user_id == User.id)
                    .filter(DataSet.lab_id == Lab.id)
                    .all())

        return {"section":  "dataset",
                "datasets": datasets}


class ShowDataSet:

    @as_html("root/show_dataset.html")
    def GET(self, dataset_id):
        dataset = (web.ctx.orm.query(DataSet)
                    .filter_by(id = dataset_id)
                    .first())
        if not dataset:
            raise web.notfound()

        user = (web.ctx.orm.query(User)
               .filter_by(id = dataset.user_id)
               .first())
        lab = (web.ctx.orm.query(Lab)
               .filter_by(id = dataset.lab_id)
               .first())
        dataset_volumes = (web.ctx.orm.query(DataSetVolume)
            .filter(DataSetVolume.dataset_id == dataset_id)
            .order_by(DataSetVolume.id)
            .all())

        return {"section":          "dataset",
                "dataset":          dataset,
                "xuser":            user,
                "lab":              lab,
                "dataset_volumes":  dataset_volumes}


class NewDataSet:

    @check_roles("admin")
    @as_html("root/new_dataset.html")
    def GET(self):
        """Display form for new DataSet.
        """
        users = (web.ctx.orm.query(User)
                    .filter_by(position = User.POSITIONS["Client"])
                    .order_by(User.first_name)
                    .all())
        labs = (web.ctx.orm.query(Lab)
                    .order_by(Lab.name)
                    .all())
        form = DataSetForm(users=users, labs=labs)

        return {"section":  "dataset",
                "form":     form}

    @as_html("root/new_dataset.html")
    def POST(self):
        """Get form data and create new Dataset.
        """
        users = (web.ctx.orm.query(User)
                 .filter_by(position = User.POSITIONS["Client"])
                 .all())
        labs = (web.ctx.orm.query(Lab)
                    .order_by(Lab.name)
                    .all())

        form_data = FDict(web.input())
        form = DataSetForm(form_data, users, labs)
        if not form.validate():
            return {"section": "dataset",
                    "form":    form}

        new_dataset = DataSet(
                        user_id     = int(form_data['user_id']),
                        lab_id      = int(form_data['lab_id']),
                        title       = cgi.escape(form_data['title']),
                        description = cgi.escape(form_data['description']),
                        is_public   = form_data['is_public'],
                        status      = form_data['status'])
        web.ctx.orm.add(new_dataset)
        web.ctx.orm.commit()

        raise web.seeother(web.ctx.site_url + '/root/datasets')


class EditDataSet:

    @check_roles("admin")
    @as_html("root/edit_dataset.html")
    def GET(self, dataset_id):
        """Display form for DataSet editing.
        """
        dataset = (web.ctx.orm.query(DataSet)
                    .filter_by(id = dataset_id)
                    .first())
        if not dataset:
            raise web.notfound()

        user = (web.ctx.orm.query(User)
               .filter_by(id = dataset.user_id)
               .first())
        lab = (web.ctx.orm.query(Lab)
               .filter_by(id = dataset.lab_id)
               .first())

        form = DataSetForm(users=[user], labs=[lab])
        form.title.data       = dataset.title
        form.description.data = dataset.description
        form.status.data      = dataset.status

        return {"section":  "dataset",
                "form":     form,
                "dataset":  dataset}

    @as_html("root/edit_dataset.html")
    def POST(self, dataset_id):
        """Get form data and update given Dataset.
        """
        dataset = (web.ctx.orm.query(DataSet)
                    .filter_by(id = dataset_id)
                    .first())
        if not dataset:
            raise web.notfound()

        user = (web.ctx.orm.query(User)
               .filter_by(id = dataset.user_id)
               .first())
        lab = (web.ctx.orm.query(Lab)
               .filter_by(id = dataset.lab_id)
               .first())

        form_data = FDict(web.input())
        form = DataSetForm(form_data, users=[user], labs=[lab])
        if not form.validate():
            return {"section": "dataset",
                    "form":    form,
                    "dataset": dataset}

        dataset.lab_id      = form_data['lab_id']
        dataset.title       = cgi.escape(form_data['title'])
        dataset.description = cgi.escape(form_data['description'])
        dataset.is_public   = form_data['is_public']
        dataset.status      = form_data['status']

        raise web.seeother(web.ctx.site_url + '/root/dataset/' + dataset_id)


class ShowDataSetVolumes:

    @as_html("root/show_dataset_volumes.html")
    def GET(self):
        dataset_volumes = (web.ctx.orm.query(DataSet, DataSetVolume)
            .filter(DataSet.id == DataSetVolume.dataset_id)
            .order_by(DataSet.id))

        return {"section":          "dataset_volumes",
                "dataset_volumes":  dataset_volumes}


class ShowDataSetVolume:

    @as_html("root/show_dataset_volume.html")
    def GET(self, volume_id):
        query = (web.ctx.orm.query(DataSet, DataSetVolume)
                .filter(DataSet.id == DataSetVolume.dataset_id)
                .filter(DataSetVolume.id == volume_id)
                .first())
        if not query:
            raise web.notfound()
        ds, volume = query
        return {"section":  "dataset_volumes",
                "dataset":  ds,
                "volume":   volume}


class NewDataSetVolume:

    @check_roles("admin")
    @as_html("root/new_dataset_volume.html")
    def GET(self):
        """Display form for new DataSet Volume.
        """
        datasets = (web.ctx.orm.query(DataSet)
                    .order_by(DataSet.title)
                    .all())
        form = DataSetVolumeForm(datasets=datasets)
        return {"section":  "dataset_volumes",
                "datasets": datasets,
                "form":     form}

    @as_html("root/new_dataset_volume.html")
    def POST(self):
        """Get form data and create new Dataset Volume.
        """
        datasets = (web.ctx.orm.query(DataSet)
                    .order_by(DataSet.title)
                    .all())
        if not datasets:
            raise web.notfound()

        form_data = FDict(web.input())
        # check for duplicates
        volume = (web.ctx.orm.query(DataSetVolume)
                .filter_by(dataset_id = int(form_data['dataset_id']))
                .filter_by(serial = cgi.escape(form_data['serial']))
                .first())
        form = DataSetVolumeForm(form_data, datasets)
        if not form.validate() or volume:
            return {"section": "dataset_volumes",
                    "datasets": datasets,
                    "message": "Not valid or volume serial already exists.",
                    "form":    form}

        new_ds_volume = DataSetVolume(
                        dataset_id  = int(form_data['dataset_id']),
                        serial      = cgi.escape(form_data['serial']),
                        model       = cgi.escape(form_data['model']),
                        status      = form_data['status'],
                        size        = form_data['size'],
                        backup_id   = form_data['backup_id'],
                        backup_type = form_data['backup_type'])
        web.ctx.orm.add(new_ds_volume)
        web.ctx.orm.commit()

        raise web.seeother(web.ctx.site_url + '/root/dataset/volumes')


class EditDataSetVolume:

    @check_roles("admin")
    @as_html("root/edit_dataset_volume.html")
    def GET(self, volume_id):
        """Display form for editing DataSet Volume.
        """
        volume = (web.ctx.orm.query(DataSetVolume)
            .filter_by(id = volume_id)
            .first())
        if not volume:
            raise web.notfound()

        dataset = (web.ctx.orm.query(DataSet)
                    .filter_by(id = volume.dataset_id)
                    .order_by(DataSet.title)
                    .first())

        form = DataSetVolumeForm(datasets=[dataset])
        form.reinit(volume)

        return {"section":  "dataset_volumes",
                "form":     form,
                "volume":   volume}

    @as_html("root/edit_dataset_volume.html")
    def POST(self, volume_id):
        """Get form data and update given Dataset Volume.
        """
        volume = (web.ctx.orm.query(DataSetVolume)
            .filter_by(id = volume_id)
            .first())
        if not volume:
            raise web.notfound()

        dataset = (web.ctx.orm.query(DataSet)
                    .filter_by(id = volume.dataset_id)
                    .order_by(DataSet.title)
                    .first())

        form_data = FDict(web.input())
        form = DataSetVolumeForm(form_data, [dataset])
        if not form.validate():
            return {"log_message":  "Please fix red labeled fields.",
                    "section":      "dataset_volumes",
                    "form":         form}

        volume.serial      = cgi.escape(form_data['serial'])
        volume.model       = cgi.escape(form_data['model'])
        volume.status      = form_data['status']
        volume.size        = form_data['size']
        volume.backup_id   = form_data['backup_id']
        volume.backup_type = form_data['backup_type']

        url = web.ctx.site_url + '/root/dataset/volume/' + volume_id

        raise web.seeother(url)


class RegenChapSecrets:

    def GET(self):
        res, err = gen_chap_secrets()
        if res == "ok":
            set_flash_message("Updated")
        else:
            user, group = os.getuid(), os.getgid()
            err += " ({}:{})".format(user, group)
            set_flash_message(err)
        raise web.seeother(web.ctx.site_url + "/root/users/vpn")


class ShowDataSetRequests:

    @as_html("root/show_dataset_requests.html")
    def GET(self):
        q = (web.ctx.orm.query(User, DataSet, Lab, DataSetRequest)
            .filter(User.id == DataSetRequest.user_id)
            .filter(DataSet.id == DataSetRequest.dataset_id)
            .filter(Lab.id == DataSet.lab_id)
            .order_by(DataSet.id))
        volumes = {}
        for user, ds, lab, request in q:
            if request.dataset_volume_id != -1:
                volume = (web.ctx.orm.query(DataSetVolume)
                         .filter(DataSetVolume.id == request.dataset_volume_id)
                         .first())
                volumes[volume.id] = volume.serial

        return {"section":  "dataset_requests",
                "requests": q,
                "volumes": volumes}


class ShowDataSetRequest:

    @as_html("root/show_dataset_request.html")
    def GET(self, request_id):
        q = (web.ctx.orm.query(User, DataSet, Lab, DataSetRequest)
                .filter(User.id == DataSetRequest.user_id)
                .filter(DataSet.id == DataSetRequest.dataset_id)
                .filter(Lab.id == DataSet.lab_id)
                .filter(DataSetRequest.id == request_id)
                .first())
        if not q:
            raise web.notfound()
        user, ds, lab, request = q
        volume = (web.ctx.orm.query(DataSetVolume)
                         .filter(DataSetVolume.id == request.dataset_volume_id)
                         .first())
        return {"section":  "dataset_requests",
                "xuser":    user,
                "dataset":  ds,
                "lab":      lab,
                "volume":   volume,
                "request":  request}


class NewDataSetRequest:

    @check_roles("admin")
    @as_html("root/new_dataset_request.html")
    def GET(self):
        "Display form for new DataSet Request."
        users = web.ctx.orm.query(User).order_by(User.id).all()
        datasets = web.ctx.orm.query(DataSet).order_by(DataSet.title).all()
        volumes = []
        if datasets:
            volumes = (web.ctx.orm.query(DataSetVolume)
                        .filter(DataSetVolume.dataset_id == datasets[0].id)
                        .all())
        form = DataSetRequestForm(datasets=datasets,
                                  users=users,
                                  volumes=volumes)
        return {"section":  "dataset_requests",
                "datasets": datasets,
                "form":     form}

    @as_html("root/new_dataset_request.html")
    def POST(self):
        "Get form data and create new Dataset Request."
        users = web.ctx.orm.query(User).order_by(User.id).all()
        datasets = web.ctx.orm.query(DataSet).order_by(DataSet.title).all()
        volumes = (web.ctx.orm.query(DataSetVolume)
                    .order_by(DataSetVolume.serial)
                    .all())
        if not datasets:
            raise web.notfound()

        form_data = FDict(web.input())
        # check for duplicates
        request = (web.ctx.orm.query(DataSetRequest)
                .filter_by(user_id = form_data['user_id'])
                .filter_by(dataset_id = form_data['dataset_id'])
                .filter_by(dataset_volume_id = form_data['dataset_volume_id'])
                .first())
        form = DataSetRequestForm(form_data=form_data,
                                  datasets=datasets,
                                  users=users,
                                  volumes=volumes)
        if not form.validate() or request:
            return {"section": "dataset_requests",
                    "datasets": datasets,
                    "message": "Data not valid or request already exists.",
                    "form":    form}
        new_ds_request = DataSetRequest(
                user_id             = form_data['user_id'],
                dataset_id          = form_data['dataset_id'],
                dataset_volume_id   = form_data['dataset_volume_id'],
                status              = form_data['status'])
        web.ctx.orm.add(new_ds_request)
        web.ctx.orm.commit()

        raise web.seeother(web.ctx.site_url + '/root/dataset/requests')


class EditDataSetRequest:

    @check_roles("admin")
    @as_html("root/edit_dataset_request.html")
    def GET(self, request_id):
        "Display form for editing DataSet Request."
        request = (web.ctx.orm.query(DataSetRequest)
                    .filter_by(id = request_id)
                    .first())
        if not request:
            raise web.notfound()

        query = (web.ctx.orm.query(User, DataSet)
                .filter(User.id == request.user_id)
                .filter(DataSet.id == request.dataset_id)
                .first())
        user, ds = query
        volume = (web.ctx.orm.query(DataSetVolume)
                 .filter(DataSetVolume.id == request.dataset_volume_id)
                 .first())
        volumes = []
        if volume:
            volumes = [volume]

        form = DataSetRequestForm(datasets=[ds],
                                  users=[user],
                                  volumes=volumes)
        form.reinit(request)

        return {"section":  "dataset_requests",
                "form":     form,
                "request":  request}

    @as_html("root/edit_dataset_request.html")
    def POST(self, request_id):
        "Get form data and update given Dataset Request."
        request = (web.ctx.orm.query(DataSetRequest)
                    .filter_by(id = request_id)
                    .first())
        if not request:
            raise web.notfound()

        query = (web.ctx.orm.query(User, DataSet)
                .filter(User.id == request.user_id)
                .filter(DataSet.id == request.dataset_id)
                .first())
        user, ds = query
        volume = (web.ctx.orm.query(DataSetVolume)
                 .filter(DataSetVolume.id == request.dataset_volume_id)
                 .first())
        volumes = []
        if volume:
            volumes = [volume]

        form_data = FDict(web.input())
        form = DataSetRequestForm(form_data,
                                  datasets=[ds],
                                  users=[user],
                                  volumes=volumes)
        if not form.validate():
            return {"log_message":  "Please fix red labeled fields.",
                    "section":      "dataset_requests",
                    "form":         form}
        request.status = form.status.data

        url = web.ctx.site_url + "/root/dataset/request/" + request_id
        raise web.seeother(url)


class LoadDSVolumes:

    @as_json
    def POST(self, dataset_id):
        volumes = (web.ctx.orm.query(DataSetVolume)
                    .filter(DataSetVolume.dataset_id == dataset_id)
                    .order_by(DataSetVolume.serial)
                    .all())
        if volumes:
            return {
                "result": "ok",
                "volumes": [(vol.id, vol.serial) for vol in volumes]
            }
        else:
            return {"result": "empty"}
