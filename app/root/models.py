import os
from datetime import datetime
from collections import OrderedDict

from sqlalchemy import Column, Integer, String, SmallInteger, Float, Text, \
    Boolean, DateTime, Enum

from settings.db import Base, ModelExt


class SiteSettings(Base, ModelExt):
    "TODO: Remove me"
    "Model describes settings for one site."
    __tablename__ = "site_settings"

    id      = Column(Integer, primary_key=True)
    domain  = Column(String(100), nullable=None)
    name    = Column(String(100), nullable=None)

    site_id = Column(SmallInteger, nullable=None)

    server_cpu_price  = Column(Float, nullable=None, default=0.0)
    server_ram_price  = Column(Float, nullable=None, default=0.0)
    server_size_price = Column(Float, nullable=None, default=0.0)
    disk_size_price   = Column(Float, nullable=None, default=0.0)

    def __repr__(self):
        return "<SiteSettings({})>".format(self.domain)


class SiteOption(Base, ModelExt):
    "Site options."
    __tablename__ = "site_options"

    id    = Column(Integer, primary_key=True)
    name  = Column(String(255), nullable=None, default="")
    yesno = Column(Boolean, nullable=None, default=False)
    value = Column(Text, nullable=None, default="")


class ClusterServer(Base, ModelExt):
    __tablename__ = "cluster_servers"

    id = Column(Integer, primary_key=True)

    name        = Column(String(100), nullable=None)
    description = Column(Text, nullable=None, default="")

    cpu       = Column(Integer, nullable=None, default=0)
    ram_used  = Column(Integer, nullable=None, default=0)
    ram_total = Column(Integer, nullable=None, default=0)

    ip        = Column(String(15), nullable=None)
    port      = Column(Integer, nullable=None, default=13000)
    is_online = Column(Boolean, nullable=None, default=True)
    ssl_cert  = Column(Text, nullable=None, default="")
    ssl_key   = Column(Text, nullable=None, default="")

    @property
    def ram_used_per(self):
        if not getattr(self, "_ram_used_per", None):
            self._ram_used_per = self.ram_used * 1.0 / self.ram_total
        return self._ram_used_per

    @staticmethod
    def cmp_ramusage(x, y):
        if x.ram_used_per > y.ram_used_per:
            return 1
        elif x.ram_used_per == y.ram_used_per:
            return 0
        else:
            return -1

    def get_cert_path(self):
        if not getattr(self, "__cert_path", None):
            self._cert_path = os.path.abspath(
                os.path.join(os.path.dirname(__file__), "..", "..",
                             "daemons", "certs", self.ssl_cert))
        return self._cert_path

    def __repr__(self):
        return ("<ClusterServer({!r}, {!r}, {!r}, {!r}%)>"
                .format(self.id, self.name, self.ip, self.ram_used_per))


class LabClusterServer(Base, ModelExt):
    __tablename__ = 'lab_cluster_servers'

    USAGE_TYPES = {
            # server used to create units(servers, disks)
            'use':   0,
            # server contains units, but not in use to create
            'store': 1,
    }

    id         = Column(Integer, primary_key=True)
    lab_id     = Column(Integer, nullable=None)
    server_id  = Column(Integer, nullable=None)  # cluster server
    usage_type = Column(SmallInteger, nullable=None,
                                      default=USAGE_TYPES['use'])

    @property
    def usage_type_str(self):
        for key, val in self.USAGE_TYPES.items():
            if val == self.usage_type:
                return key


class ClusterDisk(Base, ModelExt):
    __tablename__ = 'cluster_disks'

    NO_MOUNT_ID = -1
    STATUSES = OrderedDict([
        ("OK",         0),
        ("NOT IN USE", 1),
    ])

    id        = Column(Integer, primary_key=True)
    server_id = Column(Integer, nullable=None)

    uuid      = Column(String(100), nullable=None, default='')
    dev_path  = Column(String(30),  nullable=None, default='')
    mount_id  = Column(String(100), nullable=None, default=-1)

    status    = Column(SmallInteger, nullable=None, default=STATUSES["OK"])

    max_size  = Column(Integer, nullable=None, default=0)  # G
    free_size = Column(Integer, nullable=None, default=0)  # G

    @property
    def url(self):
        return '/root/cluster/disks/{}'.format(self.id)

    @staticmethod
    def cmp_freesize(x, y):
        if x.free_size > y.free_size:
            return 1
        elif x.free_size == y.free_size:
            return 0
        else:
            return -1

    @property
    def status_str(self):
        for key, val in self.STATUSES.items():
            if val == self.status:
                return key
        return ""

    def __repr__(self):
        return '<ClusterDisk({!r}, {!r})>'.format(self.id, self.uuid)


class Raid(Base, ModelExt):
    __tablename__ = 'raids'

    id        = Column(Integer, primary_key=True)
    server_id = Column(Integer,     nullable=None)
    dev_path  = Column(String(30),  nullable=None, default='')
    uuid      = Column(String(100), nullable=None, default='')
    mount_id  = Column(String(30),  nullable=None, default='')
    status    = Column(Integer,     nullable=None, default=-1)

    free_size = Column(Integer, nullable=None, default=0)
    max_size  = Column(Integer, nullable=None, default=0)

    def __repr__(self):
        return '<Raid(id={}, path={!r})>'.format(self.id, self.dev_path)


class MailQuery(Base, ModelExt):
    __tablename__ = 'mail_query'

    id         = Column(Integer, primary_key=True)

    mail_to    = Column(String(255), nullable=None)
    mail_from  = Column(String(255), nullable=None)

    subject    = Column(String(255), nullable=None, default='')
    body       = Column(Text,        nullable=None, default='')
    created    = Column(DateTime,    nullable=None, default=datetime.utcnow)


class CRMLogCommunication(Base, ModelExt):
    __tablename__ = 'crm_log_communication'

    MESSAGE = ('{subject} | <a href="/root/crm/log/{id}">more...</a>')

    LOG_TYPES = (
        ('email', 'Email'),
        ('phone', 'Phone'),
        ('skype', 'Skype'),
        ('in_person', 'In Person'),
        ('mail', 'Direct Mail')
    )

    LOG_DIRECTIONS = (
        ('from_us', 'From Us'),
        ('to_us', 'To Us')
    )

    id         = Column(Integer, primary_key=True)
    user_id    = Column(Integer, nullable=None)
    created    = Column(DateTime,    nullable=None, default=datetime.utcnow)
    subject    = Column(String(255), nullable=None, default='')
    body       = Column(Text,        nullable=None, default='')
    log_type   = Column(Enum('email', 'phone', 'skype', 'in_person', 'mail'))
    direction  = Column(Enum('from_us', 'to_us'))
    # TODO: data field

    @property
    def text(self):
        if self.user_id:
            return ((self.MESSAGE).format(**self.__dict__))

    @property
    def log_type_str(self):
        return dict(self.LOG_TYPES).get(self.log_type, 'N/A')

    @property
    def direction_str(self):
        return dict(self.LOG_DIRECTIONS).get(self.direction)


class DataSet(Base, ModelExt):
    __tablename__ = 'data_set'

    DATA_SET_STATUSES = (
            ('sending',    'Sending'), ('processing', 'Processing'),
            ('ready',      'Ready'),
            ('damaged',    'Damaged')
    )

    id          = Column(Integer,     primary_key=True)
    user_id     = Column(Integer,     nullable=None)
    lab_id      = Column(Integer,     nullable=None)
    title       = Column(String(100), nullable=None, default='')
    description = Column(Text,        nullable=None, default='')
    is_public   = Column(Boolean,     nullable=None, default=True)
    status      = Column(Enum('sending', 'processing', 'ready', 'damaged'))

    @property
    def status_str(self):
        return dict(self.DATA_SET_STATUSES).get(self.status)

    @property
    def is_public_str(self):
        return "Yes" if self.is_public else "No"

    @property
    def root_url(self):
        return '/root/dataset/{}'.format(self.id)


class DataSetVolume(Base, ModelExt):
    __tablename__ = 'data_set_volume'

    BACKUP_TYPES = (
            ('copy', 'Copy'),
            ('raid', 'RAID'),
    )
    STATUSES = (
        (0, 'NORMAL'),
    )

    id          = Column(Integer,    primary_key=True)
    dataset_id  = Column(Integer,    nullable=None)
    serial      = Column(String(50), nullable=None, default='')
    model       = Column(String(50), nullable=None, default='')
    status      = Column(Integer,    nullable=None, default=-1)
    size        = Column(Integer,    nullable=None, default=0)
    backup_id   = Column(Integer,    nullable=None)
    backup_type = Column(Enum('copy', 'raid'))
    created     = Column(DateTime,   nullable=None, default=datetime.utcnow)
    updated     = Column(DateTime,   nullable=None, default=datetime.utcnow,
                                                    onupdate=datetime.utcnow)

    @property
    def backup_type_str(self):
        return dict(self.BACKUP_TYPES).get(self.backup_type)

    @property
    def status_str(self):
        return dict(self.STATUSES).get(self.status, '').capitalize()


class DataSetRequest(Base, ModelExt):
    __tablename__ = "data_set_requests"

    STATUSES = (
        (0,  "New"),
        (1,  "Processing"),
        (2,  "Done"),      # avaible to the user
        (3,  "Finished"),  # user finished work with ds
        (10, "Canceled"),
    )

    ALL_VOLUMES = -1

    id                 = Column(Integer,  primary_key=True)
    user_id            = Column(Integer,  nullable=None)
    dataset_id         = Column(Integer,  nullable=None)
    dataset_volume_id  = Column(Integer,  nullable=None, default=-1)
    status             = Column(Integer,  nullable=None, default=-1)
    created            = Column(DateTime, nullable=None,
                                          default=datetime.utcnow)
    updated            = Column(DateTime, nullable=None,
                                          default=datetime.utcnow,
                                          onupdate=datetime.utcnow)

    @property
    def status_str(self):
        return dict(self.STATUSES).get(self.status, '').capitalize()

    @classmethod
    def get_status(cls, status):
        for key, val in cls.STATUSES:
            if val == status:
                return key
