import re

from wtforms import Form, TextField, TextAreaField, SelectField
from wtforms.validators import ValidationError, Required

from app.core.patterns import re_email, re_email_subject
from app.root.models import CRMLogCommunication, DataSet, DataSetVolume,\
    DataSetRequest, ClusterDisk


def validate_size(self, field):
    if not field.data.isdigit():
        raise ValidationError("Wrong type")
    value = int(field.data)
    if value % 1024 != 0:
        raise ValidationError("Doesn't multiplied by 1024")


class SendMailForm(Form):
    mail_to = TextAreaField('Mail to', [Required()], id='mail-to')
    subject = TextField('Subject', [Required()])
    body    = TextAreaField('Mail text', [Required()], id='mail-body')

    def validate_mail_to(self, field):
        is_match = re_email.findall(field.data)
        if not is_match:
            ems = re.split(',\s*', field.data)
            if ems:
                for em in ems:
                    is_match = re_email.findall(em)
                    if not is_match:
                        raise ValidationError('Wrong email(s).')
            else:
                raise ValidationError('Wrong email(s).')

    def validate_subject(self, field):
        is_match = re_email_subject.findall(field.data)
        if not is_match:
            raise ValidationError('Wrong subject.')


class LogCommunicationForm(Form):
    user_id   = SelectField('SG Client - User')
    subject   = TextField('Subject', [Required('Subject is required.')])
    body      = TextAreaField('Log Body')
    log_type  = SelectField('Communicated by',
                             choices=CRMLogCommunication.LOG_TYPES)
    direction = SelectField('Log direction',
                             choices=CRMLogCommunication.LOG_DIRECTIONS)

    def validate_subject(self, field):
        is_match = re_email_subject.findall(field.data)
        if not is_match:
            raise ValidationError('Subject is incorrect.')


class DataSetForm(Form):
    user_id     = SelectField('Dataset Owner')
    lab_id      = SelectField('Lab Name')
    title       = TextField('Title', [Required('Title is required.')])
    description = TextAreaField('Dataset Description')
    is_public   = SelectField('Is Public?')
    status      = SelectField('Dataset status',
                             choices=DataSet.DATA_SET_STATUSES)

    def __init__(self, form_data=None, users=None, labs=None):
        Form.__init__(self, form_data)
        if not users:
            users = []
        if not labs:
            labs = []
        self.user_id.choices = [(str(user.id), user.full_name)
                                    for user in users]
        self.lab_id.choices = [(str(lab.id), lab.name) for lab in labs]
        self.is_public.choices = [('1', 'Yes'), ('0', 'No')]


class DataSetVolumeForm(Form):
    dataset_id  = SelectField('Dataset')
    serial      = TextField('Serial Number', [Required('Serial is required.')])
    model       = TextField('Model')
    status      = SelectField('Status',
            choices=[(str(sid), val) for sid, val in DataSetVolume.STATUSES])
    size        = TextField('Size (MB)')
    backup_id   = SelectField('Backup')
    backup_type = SelectField('Backup type',
                             choices=DataSetVolume.BACKUP_TYPES)

    def __init__(self, form_data=None, datasets=None):
        if not datasets:
            datasets = []
        Form.__init__(self, form_data)
        self.dataset_id.choices = [(str(dataset.id), dataset.title)
                                    for dataset in datasets]
        # TODO: replace by real backups when it will be created
        self.backup_id.choices = [('1', 'Yes'), ('0', 'No')]

    def reinit(self, volume):
        self.serial.data      = volume.serial
        self.model.data       = volume.model
        self.status.data      = volume.status
        self.size.data        = volume.size
        self.backup_id.data   = volume.backup_id
        self.backup_type.data = volume.backup_type


class DataSetRequestForm(Form):
    dataset_id          = SelectField('Dataset')
    user_id             = SelectField('Owner')
    dataset_volume_id   = SelectField('Volume',
                                         choices = [('-1', 'ALL')])
    status              = SelectField('Status',
            choices=[(str(sid), val) for sid, val in DataSetRequest.STATUSES])

    def __init__(self, form_data=None,
                       datasets=None,
                       users=None,
                       volumes=None):
        if not datasets:
            datasets = []

        Form.__init__(self, form_data)
        self.user_id.choices = [(str(user.id), user.full_name)
                                    for user in users]
        self.dataset_id.choices = [(str(dataset.id), dataset.title)
                                    for dataset in datasets]
        if volumes:
            self.dataset_volume_id.choices = [('-1', 'ALL')]
            self.dataset_volume_id.choices += [(str(volume.id), volume.serial)
                                            for volume in volumes]

    def reinit(self, request):
        self.status.data = request.status


class LabLimitsForm(Form):
    cpu_limit      = TextField("CPU limit", [Required()])
    ram_limit      = TextField("RAM limit (MB)", [Required(), validate_size])
    storages_limit = TextField("Size limit (MB)", [Required(), validate_size])

    def reinit(self, lab):
        self.cpu_limit.data      = lab.cpu_limit
        self.ram_limit.data      = lab.ram_limit
        self.storages_limit.data = lab.storages_limit

    def validate_cpu_limit(self, field):
        if not field.data.isdigit():
            raise ValidationError("Integer only")
        val = int(field.data)
        if str(val) != field.data:
            raise ValidationError("Integer only")


class ClusterDiskForm(Form):
    cserver   = SelectField("Cluster")
    uuid      = TextField("Uuid")
    dev_path  = TextField("Dev path", [Required()])
    mount_id  = TextField("Mount id", [Required()])
    status    = SelectField("Status", [Required()])
    free_size = TextField("Free size", [Required()])
    max_size  = TextField("Max size", [Required()])

    def __init__(self, form_data=None, cservs=None):
        if not cservs:
            cservs = []

        Form.__init__(self, form_data)
        self.cserver.choices = [(str(cserv.id), cserv.name)
            for cserv in cservs]
        self.status.choices = [(str(id), val)
            for val, id in ClusterDisk.STATUSES.items()]
