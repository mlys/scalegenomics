import re

# username
pat_un = re.compile('^[a-zA-Z0-9]{4,20}$')
# email
pat_em = re.compile('^([a-zA-Z0-9][a-zA-Z0-9_\-\.]*\@([a-zA-Z0-9]{2,}\.){1,}[a-zA-Z]{2,4})$')
# password
pat_pw = re.compile('^.{6,20}$')
# first name
pat_fl = re.compile('^[a-zA-Z][a-zA-Z\-]{1,20}$')
# lab name
pat_la = re.compile('^.{3,30}$')
#
pat_org = re.compile('^[a-zA-Z0-9\s\']{0,50}$')
#
pat_phone = re.compile('^[0-9\s]{0,50}$')

re_psite = re.compile(
        r'^(https?://)?'
        r'((?:(?:[A-Z0-9-]+\.)+[A-Z]{2,6}|'
        r'localhost|'
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
        r'(?::\d+)?'
        r'(?:/?|/\S+))$',
    re.IGNORECASE)
