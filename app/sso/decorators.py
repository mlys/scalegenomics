import web

def auth(f):
    def tmp(self, *args):
        if 'username' not in web.ctx.session:
            web.ctx.session.next = web.ctx.site_url + web.ctx.env['PATH_INFO']
            raise web.redirect(web.ctx.site_url)
        return f(self, *args)
    return tmp
authentication_required = auth


def staff_only(f):
    def tmp(self, *args):
        is_staff = web.ctx.session.get('is_staff', False)
        if is_staff:
            return f(self, *args)
        else:
            raise web.notfound()
    return tmp
