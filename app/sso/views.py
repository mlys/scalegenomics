import cgi
from datetime import datetime

import web

from app.core.mail       import send_mail
from app.core.decorators import as_html, as_json, as_text
from app.core.server.ppp import gen_chap_secrets
from app.core.types      import FDict
from app.root.utils      import get_option
from app.sso.decorators  import auth
from app.sso.models      import User, RestoreMail
from app.sso.forms       import LoginForm, RestorePasswordForm, \
        RenewPasswordForm
from app.sso.patterns    import pat_un, pat_em, pat_pw, pat_fl, pat_org, \
     pat_phone, re_psite
from app.my.models       import Plan
from app.my.utils        import get_flash_message, set_flash_message


urls = (
    '/',        'Login',
    '/signup',  'Signup',
    '/profile', 'Profile',
    '/logout',  'Logout',

    '/restore',       'ShowRestorePassword',
    '/renewpassword', 'ShowRenewPassword',

    '/sso/a/userexists',  'UserExists',
    '/sso/a/emailexists', 'EmailExists',
    '/sso/a/newuser',     'AjaxNewUser',

    '/about/',              'About',
    '/terms-of-service',    'Terms',
    '/contact-us',          'ContactUs',
    '/team',                'Team',
    '/subscription-plans/', 'Plans',
)

web_app = web.application(urls, locals())


def session_data(user):
    web.ctx.session['user_id']      = user.id
    web.ctx.session['username']     = user.username
    web.ctx.session['first_name']   = user.first_name
    web.ctx.session['last_name']    = user.last_name
    web.ctx.session['full_name']    = user.full_name
    web.ctx.session['email']        = user.email
    web.ctx.session['is_staff']     = user.is_staff
    web.ctx.session['gravatar']     = user.gravatar
    web.ctx.session['user_abs_url'] = user.abs_url


class Login:

    @as_html("sso/login.html")
    def GET(self):
        "Display login form."

        if "username" in web.ctx.session:
            raise web.redirect(web.ctx.site_url + "/my")

        opt = get_option("enable_login")
        return {"cpage":        "login",
                "message":      get_flash_message(),
                "enable_login": opt,
                "form":         LoginForm()}

    @as_html('sso/login.html')
    def POST(self):
        'Handle login form.'

        params = FDict(web.input())
        form = LoginForm(params)
        response = {'cpage': 'login',
                    'form':  form}
        if not form.validate():
            return response

        user = (web.ctx.orm.query(User)
                .filter_by(username = form.username.data.lower())
                .first())
        if not user:
            response['message'] = "User doesn't exist"
            return response
        if not user.check_password(form.password.data):
            response['message'] = 'Wrong password'
            return response
        if not user.is_active:
            response['message'] = 'User is not activated'
            return response
        if user.position == User.POSITIONS["System"]:
            response["message"] = "This is system user"
            return response

        session_data(user)
        user.last_login = datetime.utcnow()

        if 'next' in web.ctx.session:
            next = web.ctx.session.pop('next')
            raise web.redirect(next)

        raise web.redirect(web.ctx.site_url + '/my')


class Signup:

    @as_html('sso/signup.html')
    def GET(self):
        'Display signup form.'
        if 'username' in web.ctx.session:
            raise web.redirect(web.ctx.site_url + '/my')
        return {'cpage': 'signup'}

    def POST(self):
        return web.seeother(web.ctx.site_url + '/signup')


class AjaxNewUser:

    @as_json
    def POST(self):
        params = web.input()
        # list of required form params
        rparams = ['username', 'email',
                   'first_name', 'last_name',
                   'password', 'confirm_password',
                   'organization', 'department', 'title', 'phone',
                   'how_how', 'tell_us']
        for param in params:
            if param not in rparams:
                return {'result':  'error',
                        'message': '1'}

        pr_un = pat_un.findall(params['username'])
        pr_em = pat_em.findall(params['email'])
        pr_fn = pat_fl.findall(params['first_name'])
        pr_ln = pat_fl.findall(params['last_name'])
        pr_pw = pat_pw.findall(params['password'])
        pr_cp = pat_pw.findall(params['confirm_password'])

        pr_org = pat_org.findall(params['organization'])
        org = pr_org[0] if pr_org else ''
        pr_dep = pat_org.findall(params['department'])
        dep = pr_dep[0] if pr_dep else ''
        pr_tit = pat_org.findall(params['title'])
        tit = pr_tit[0] if pr_tit else ''
        pr_pho = pat_phone.findall(params['phone'])
        pho = pr_pho[0] if pr_pho else ''

        # form validation
        if not (pr_un and pr_em and pr_fn and pr_ln and pr_pw and pr_cp):
            return {'result':  'error',
                    'message': '2'}
        if pr_pw[0] != params['confirm_password']:
            return {'result':  'error',
                    'message': '3'}

        username = pr_un[0].lower()
        email    = pr_em[0][0].lower()
        user_check = (web.ctx.orm.query(User)
                      .filter_by(username=username)
                      .first())
        if user_check:
            return {'return':  'error',
                    'message': '4'}
        email_check = (web.ctx.orm.query(User).filter_by(email=email).first())
        if email_check:
            return {'return':  'error',
                    'message': '5'}
        user = User(username     = username,
                    email        = email,
                    password     = pr_pw[0],
                    first_name   = pr_fn[0],
                    last_name    = pr_ln[0],
                    organization = org,
                    department   = dep,
                    title        = tit,
                    phone        = pho,
                    how          = cgi.escape(params['how_how']),
                    tell_us      = cgi.escape(params['tell_us']))

        user.set_lnx_password()

        web.ctx.orm.add(user)
        web.ctx.orm.commit()
        session_data(user)

        send_mail('signup', mail_to=user.email)

        return {'result': 'ok'}


class Profile:

    @auth
    @as_html('sso/profile.html')
    def GET(self):
        '''Display profile form.'''
        user = (web.ctx.orm.query(User)
                .filter_by(id=web.ctx.session['user_id'])
                .first())
        return {'cpage': 'profile',
                'user':  user}

    @auth
    @as_html('sso/profile.html')
    def POST(self):
        'Handle profile form.'
        params = web.input()
        user = (web.ctx.orm.query(User)
                .filter_by(id=web.ctx.session.user_id)
                .first())
        if 'first-name' in params and 'last-name' in params:
            pr_fn = pat_fl.findall(params['first-name'].strip())
            pr_ln = pat_fl.findall(params['last-name'].strip())
            if not pr_fn:
                return {'user':            user,
                        'profile_message': 'Enter valid first name'}
            if not pr_ln:
                return {'user':             user,
                        'profile_message': 'Enter valid last name'}

            pr_org = pat_org.findall(params['organization'])
            org = pr_org[0] if pr_org else ''
            pr_dep = pat_org.findall(params['department'])
            dep = pr_dep[0] if pr_dep else ''
            pr_tit = pat_org.findall(params['title'])
            tit = pr_tit[0] if pr_tit else ''
            pr_pho = pat_phone.findall(params['phone'])
            pho = pr_pho[0] if pr_pho else ''
            psite = re_psite.findall(params['personal-site'])
            if psite:
                if psite[0][0]:
                    psite = psite[0][0] + psite[0][1]
                else:
                    psite = 'http://' + psite[0][1]
            else:
                psite = ''

            user.organization  = org
            user.department    = dep
            user.title         = tit
            user.phone         = pho
            user.personal_site = psite

            user.first_name = cgi.escape(pr_fn[0])
            user.last_name  = cgi.escape(pr_ln[0])
            return {'cpage':           'profile',
                    'user':            user,
                    'profile_message': 'Saved'}

        elif ('old-password'         in params and
              'new-password'         in params and
              'confirm-new-password' in params):
            pr_pw_old = pat_pw.findall(params['old-password'])
            pr_pw_new = pat_pw.findall(params['new-password'])

            response = {'cpage': 'profile',
                        'user':  user}
            if not pr_pw_old:
                response['password_message'] = 'Enter valid old password'
                return response

            if not pr_pw_new:
                response['password_message'] = 'Enter valid new password'
                return response

            if not user.check_password(pr_pw_old[0]):
                response['password_message'] = 'Wrong old password'
                return response

            if pr_pw_new[0] == params['confirm-new-password']:
                user.password = pr_pw_new[0]
                user.set_lnx_password()
                gen_chap_secrets()
                response['password_message'] = 'Password changed'
                return response

            else:
                response['password_message'] = "New passwords doesn't match"
                return response

        raise web.notfound()


class Logout:

    def GET(self):
        '''Kill session.'''
        web.ctx.session.kill()
        raise web.seeother(web.ctx.site_url)


class UserExists:

    def POST(self):
        web.header('Content-Type', 'text/html; charset=utf-8')

        i = web.input()
        if 'data' in i:
            user_check = (web.ctx.orm.query(User)
                            .filter_by(username=cgi.escape(i.data.lower()))
                            .first())
            if not user_check:
                return 'true'
        return 'false'


class EmailExists:

    @as_text
    def POST(self):
        i = web.input()
        if 'data' in i:
            email_check = (web.ctx.orm.query(User)
                            .filter_by(email=cgi.escape(i.data.lower()))
                            .first())
            if not email_check:
                return 'true'
        return 'false'


class ShowRestorePassword:

    @as_html('sso/show_restore_password.html')
    def GET(self):
        return {
            'form': RestorePasswordForm(),
        }

    @as_html('sso/show_restore_password.html')
    def POST(self):
        params = FDict(web.input())
        form = RestorePasswordForm(params)
        if not form.validate():
            return {'form': form}

        user = (web.ctx.orm.query(User)
                .filter_by(email = form.email.data)
                .first())
        if not user:
            return {'form':    RestorePasswordForm(),
                    'message': 'Unknown email'}

        ch_rms = (web.ctx.orm.query(RestoreMail)
                  .filter_by(user_id = user.id)
                  .all())
        for ch_rm in ch_rms:
            web.ctx.orm.delete(ch_rm)

        key = RestoreMail.gen_key()
        while True:
            ch_key = (web.ctx.orm.query(RestoreMail)
                      .filter_by(key = key)
                      .first())
            if ch_key:
                key = RestoreMail.gen_key()
            else:
                break

        rm = RestoreMail(user_id = user.id,
                         key     = key)
        web.ctx.orm.add(rm)

        url = web.ctx.site_url + '/renewpassword?key={}'.format(rm.key)
        send_mail('restore_password', mail_to=user.email,
                                      restore_password_url=url)

        return {'form':     RestorePasswordForm(),
                'message': 'Please check your email'}


class ShowRenewPassword:

    @as_html('sso/show_renew_password.html')
    def GET(self):
        params = web.input()
        if 'key' not in params:
            raise web.notfound()

        rm = (web.ctx.orm.query(RestoreMail)
              .filter_by(key = params['key'])
              .first())
        if not rm:
            raise web.notfound()

        return {'form': RenewPasswordForm()}

    @as_html('sso/show_renew_password.html')
    def POST(self):
        params = FDict(web.input())
        if 'key' not in params:
            raise web.notfound()

        form = RenewPasswordForm(params)
        if not form.validate():
            return {'form': form}
        if form.new_password.data != form.confirm_password.data:
            form.errors['confirm_password'] = ['Passwords don\'t match']
            return {'form': form}

        rm = (web.ctx.orm.query(RestoreMail)
              .filter_by(key = params['key'])
              .first())
        if not rm:
            raise web.notfound()

        user = (web.ctx.orm.query(User)
                .filter_by(id = rm.user_id)
                .first())
        user.password = form.confirm_password.data
        user.set_lnx_password()
        gen_chap_secrets()

        web.ctx.orm.delete(rm)

        set_flash_message('Password changed')
        raise web.seeother(web.ctx.site_url)


class About:

    @as_html("sso/about.html")
    def GET(self):
        return {}


class Team:

    @as_html("sso/team.html")
    def GET(self):
        return {}


class Terms:

    @as_html("sso/terms.html")
    def GET(self):
        return {}


class ContactUs:

    @as_html("sso/contact_us.html")
    def GET(self):
        return {}


class Plans:

    @as_html("sso/plans.html")
    def GET(self):
        plans = web.ctx.orm.query(Plan).all()
        return {"plans": plans}
