from wtforms import Form, TextField, PasswordField
from wtforms.validators import ValidationError, Required

from app.core.patterns import re_username, re_password, re_email


class LoginForm(Form):
    username = TextField('Username', [Required()])
    password = PasswordField('Password', [Required()])

    def validate_username(self, field):
        is_match = re_username.match(field.data)
        if not is_match:
            raise ValidationError('Enter valid username.')

    def validate_password(self, field):
        is_match = re_password.match(field.data)
        if not is_match:
            raise ValidationError('Enter valid password.')


class RestorePasswordForm(Form):
    email = TextField('Email', [Required()])

    def validate_email(self, field):
        is_match = re_email.findall(field.data)
        if not is_match:
            raise ValidationError('Enter valid email.')


class RenewPasswordForm(Form):
    new_password     = PasswordField('New password', [Required()])
    confirm_password = PasswordField('Confirm password', [Required()])

    def validate_new_password(self, field):
        is_match = re_password.findall(field.data)
        if not is_match:
            raise ValidationError('Enter valid password.')

    def validate_confirm_password(self, field):
        is_match = re_password.findall(field.data)
        if not is_match:
            raise ValidationError('Enter valid password.')


