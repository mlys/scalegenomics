import hashlib
import random
import string
from datetime import datetime

import web
from sqlalchemy  import Column, Integer, String, DateTime, Boolean, Text, \
     ForeignKey, SmallInteger, CHAR, TIMESTAMP

from settings.db import Base, ModelExt


class Session(Base, ModelExt):
    __tablename__ = "sessions"
    session_id = Column(CHAR(128), nullable=None,
                                   unique=True,
                                   primary_key=True)
    atime      = Column(TIMESTAMP, nullable=None)
    data       = Column(Text)


class User(Base, ModelExt):
    __tablename__ = "users"

    POSITIONS = {
        "Developer":   0,
        "Co-Founder":  1,
        "Client":      2,
        "System":      3,
        }

    STAFF_ROLES = {
        "DEFAULT":  0,
        "ADMIN":    1,
        "CRM":      2,
    }

    id           = Column(Integer, primary_key=True)
    username     = Column(String(50), nullable=None, unique=True)
    email        = Column(String(75), nullable=None, unique=True)
    password     = Column(String(50), nullable=None)
    lnx_password = Column(String(200), nullable=None)
    first_name   = Column(String(50), nullable=None)
    last_name    = Column(String(50), nullable=None)

    date_signup  = Column(DateTime, nullable=None, default = datetime.utcnow)
    last_login   = Column(DateTime, nullable=None, default = datetime.utcnow)

    is_active    = Column(Boolean, default=True, nullable=None)

    is_staff     = Column(Boolean, default=False, nullable=None)
    staff_role   = Column(SmallInteger, nullable=None,
                                        default=STAFF_ROLES["DEFAULT"])
    # used in root section
    description = Column(Text, default="", nullable=None)

    organization = Column(String(100), nullable=None, default="")
    department   = Column(String(100), nullable=None, default="")
    title        = Column(String(100), nullable=None, default="")

    phone = Column(String(50), nullable=None, default="")

    personal_site = Column(String(150), nullable=None, default="")

    position = Column(SmallInteger, nullable=None, default=POSITIONS["Client"])

    how     = Column(Text, nullable=None, default="")
    tell_us = Column(Text, nullable=None, default="")

    invited_by = Column(Integer, default=None)

    def check_password(self, password):
        password = password.encode("utf-8", "strict")
        if password == self.password:
            return True
        return False

    def set_lnx_password(self):
        "Generate and set linux password in sha-512"
        from crypt import crypt
        salt = [random.choice(string.letters + "0123456789")
                for i in range(8)]
        salt = "$6$" + "".join(salt)
        self.lnx_password = crypt(self.password, salt)

    def is_staff_role(self, staff_role):
        return self.staff_role == self.STAFF_ROLES[staff_role]

    @property
    def gravatar(self):
        if not hasattr(self, "_gravatar"):
            email_hash = hashlib.md5(self.email.lower()).hexdigest()
            self._gravatar = ("http://www.gravatar.com/avatar/{}"
                                    .format(email_hash))
        return self._gravatar

    @property
    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    @property
    def url(self):
        return "/my/user/{id}".format(id = self.id)

    @property
    def abs_url(self):
        return web.ctx.site_url + self.url

    @property
    def a_tag(self):
        return '<a href="{}">{}</a>'.format(self.url, self.full_name)

    @property
    def root_url(self):
        return "/root/users/{}".format(self.id)

    @property
    def position_str(self):
        for key, val in self.POSITIONS.items():
            if val == self.position:
                return key
        return ""

    @property
    def staff_role_lower(self):
        for key, val in self.STAFF_ROLES.items():
            if val == self.staff_role:
                return key.lower()
        return ""


class UserCert(Base, ModelExt):
    __tablename__ = "user_certs"

    id      = Column(Integer, primary_key=True)
    user_id = Column(Integer, nullable=None)

    ip  = Column(String(100), nullable=None, default="")
    key = Column(Text, nullable=None, default="")
    csr = Column(Text, nullable=None, default="")
    crt = Column(Text, nullable=None, default="")

    created = Column(DateTime, nullable=None, default  = datetime.utcnow)
    updated = Column(DateTime, nullable=None, default  = datetime.utcnow,
                                              onupdate = datetime.utcnow)

    @property
    def cert_name(self):
        return "cert{}".format(self.id)

    @staticmethod
    def get_id(cert_name):
        return cert_name[len("cert"):]


class UserToLab(Base, ModelExt):
    __tablename__ = "user_to_lab"

    PERMS = {
        "owner":                  1,
        "member":                 2,
        "waiting to be approved": 99,
        }

    id      = Column(Integer, primary_key=True)
    lab_id  = Column(Integer, ForeignKey("labs.id"), nullable=None)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=None)
    perms   = Column(SmallInteger, nullable=None)

    @property
    def perms_str(self):
        for key, val in self.PERMS.items():
            if val == self.perms:
                return key
        return ""

    @property
    def relation_str(self):
        if not hasattr(self, "_relation_str"):
            for key, val in self.PERMS.items():
                if self.perms == val:
                    self._relation_str = key
        return getattr(self, "_relation_str", "")

    def is_relation(self, rel):
        return self.perms == self.PERMS[rel]

    def __repr__(self):
        return ("<UserToLab({}, user={}, lab={})>"
                .format(self.id, self.user_id, self.lab_id))


class RestoreMail(Base, ModelExt):
    __tablename__ = "restore_mail"

    id      = Column(Integer, primary_key=True)
    user_id = Column(Integer, nullable=None)
    key     = Column(String(255), nullable=None)

    @classmethod
    def gen_key(cls, length=50):
        return "".join([random.choice(string.letters + "0123456789")
                        for i in range(length)])
