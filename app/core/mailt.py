MAIL_TEMPLATE = '''
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    {}
  </body>
</html>
'''

LETTER_START = ''

LETTER_END   = '''
<p>-- Your SG Team (<a href="mailto:support@scalegenomics.com">support@scalegenomics.com</a>)</p>
'''

SIGNUP = {
    'subject' : 'Welcome to {site}',
    'body'    : '''
<h4>Welcome to Scale Genomics!</h4>

<p>We are a cloud-based platform for next-generation sequence (NGS) analysis.</p>
<p>Contact us at <a href="mailto:support@scalegenomics.com">support@scalegenomics.com</a> and we will help you with all of your NGS analyses needs.</p>

<p>As the first step you can:</p>

<ul>
  <li>Add/Create your own lab/organization or join existing organization/lab</li>
  <li>Create projects</li>
  <li>Create virtual hard drives</li>
  <li>Create virtual machines from genomics-ready images</li>
  <li>Send us your data by mail</li>
</ul>

<p>When your lab is approved or once you join an existing lab you can:</p>

<ul>
3  <li>Start virtual machines</li>
  <li>Connect to your virtual machine via our secure VPN</li>
  <li>Upload your data</li>
  <li>Customize virtual machines and run your computations</li>
</ul>

<p>Why SG? Here you have full transparency of action and ability to:</p>

<ul>
  <li>Store and share your data</li>
  <li>Create and share virtual machines with all the software pre-installed</li>
  <li>Manage your collaborations</li>
  <li>Full root shell access to virtual machines</li>
  <li>Access to public data and large public files</li>
  <li>Get monthly subscriptions to powerful machines at very affordable prices</li>
</ul>

{end}
'''
}

LAB_VERIFIED = {
    'subject' : '{site} - Lab verified',
    'body'    : '''
<p>Congratulations! Your <a href="{lab_url}">{lab_name}</a> has passed all of our tests with flying colors and you and your labmates can now run virtual machines on SG.</p>

<p>You can now enjoy all the benefits of SG, collaborate and run your computations by</p>

<ul>
  <li>Creating, customizing and running virtual machines</li>
  <li>Connecting to your virtual machine via our secure VPN</li>
  <li>Uploading or sending your data by mail</li>
</ul>

<p>Why SG? Here you have full transparency of action and ability to:</p>

<ul>
  <li>Store and share your data</li>
  <li>Create and share virtual machines with all the software pre-installed</li>
  <li>Manage your collaborations</li>
  <li>Full root shell access to virtual machines</li>
  <li>Access to public data and large public files</li>
  <li>Get monthly subscriptions to powerful machines at very affordable prices</li>
</ul>

{end}
'''
}

TRIAL_WILL_FINISH = {
    'subject' : '{site} - Trial plan is going to stop in 5 days',
    'body'    : '''
<p>Dear Professor {owner_name},</p>

<p>Your free trial plan is coming to an end.</p>
<p>Please <a href="{upgrade_url}">upgrade</a> it if you would like to continue using Scale Genomics.</p>
<p>If you would like to download your data and close your lab please email us at.</p>

{end}
'''
}

TRIAL_FINISHED = {
    'subject' : '{site} - Trial plan has ended',
    'body'    : '''
<p>Dear Professor {owner_name},</p>

<p>Your free trial plan has come to an end.</p>
<p>All of your virtual machines have been stopped. Your disks are currently unavailable.</p>
<p>Please <a href="{upgrade_url}">upgrade</a> it if you would like to continue using Scale Genomics.</p>
<p>If you would like to download some of the data and close your lab please email us at.</p>

{end}
'''
}

JOIN_TO_LAB = {
    'subject' : '{site} - User wants to join your lab',
    'body'    : '''
<p>Professor <a href="{owner_url}">{owner_name}</a>, <a href="{user_url}">{user_name}</a> with an email ({user_email}) is asking permission to join your SG lab <a href="{lab_url}">{lab_name}</a>.</p>

<p>To allow access please go to lab's members <a href="{members_url}">tab in your SG lab dashboard</a>.</p>

{end}
'''
}

USER_ADDED_TO_LAB = {
    'subject' : '{site} - You have been added to a lab',
    'body'    : '''
<p>Congratulations, Professor <a href="{owner_url}">{owner_name}</a> has approved your application to join the <a href="{lab_url}">{lab_name}</a> lab.</p>

<p>Go wild!</p>

{end}
'''
}
USER_LEAVED_LAB = {
    'subject' : '{site} - User left your lab',
    'body'    : '''
<p><a href="{user_url}">{user_name}</a> left <a href="{lab_url}">{lab_name}</a> lab.</p>

{end}
'''
}
DELETE_USER_FROM_LAB = {
    'subject' : '{site} - You are removed from a lab',
    'body'    : '''
<p><a href="{owner_url}">{owner_name}</a> removed you as user from <a href="{lab_url}">{lab_name}</a> lab.</p>

{end}
'''
}
REJECT_USER_FROM_LAB = {
    'subject' : '{site} - You were not allowed access to a lab',
    'body'    : '''
<p><a href="{owner_url}">{owner_name}</a> did not allow you access to <a href="{lab_url}">{lab_name}</a> lab.</p>

{end}
'''
}

JOIN_TO_PROJECT = {
    'subject' : '{site} - User wants to join your project',
    'body'    : '''
<p>Dear {owner_name}, your colleague <a href="{user_url}">{user_name}</a> wants to join your project <a href="{project_url}">{project_name}</a>.<p>
<p>To add this colleague to the {project_name} project, click <a href="{project_members_url}">here</a>.</p>

{end}
'''
}
USER_ADDED_TO_PROJECT = {
    'subject' : '{site} - You were added to a project',
    'body'    : '''
<p>Dear {user_name}, your colleague <a href="{owner_url}">{owner_name}</a> gave you permission to join the project <a href="{project_url}">{project_name}</a>.</p>

<p>Enjoy!</p>

{end}
'''
}
DELETE_USER_FROM_PROJECT = {
    'subject' : '{site} - You were removed from a project',
    'body'    : '''
<p><a href="{owner_url}">{owner_name}</a> removed you from <a href="{project_url}">{project_name}</a>.</p>

{end}
'''
}

REJECT_USER_FROM_PROJECT = {
    'subject' : '{site} - You are not allowed access to a project',
    'body'    : '''
<p><a href="{owner_url}">{owner_name}</a> did not allow you access to <a href="{project_url}">{project_name}</a>.</p>

{end}
'''
}

PROJECT_DELETED = {
    'subject' : '{site} - Project deleted',
    'body'    : '''
<p><a href="{owner_url}">{owner_name}</a> deleted {project_name}.</p>

{end}
'''
}

RESTORE_PASSWORD = {
    'subject' : '{site} - Restore password',
    'body'    : '''
Click <a href="{restore_password_url}">here</a> to restore password.

{end}
'''
}

ACT_LAB = {
    'subject' : '{site} - Lab activation request',
    'body'    : '''
<p>Full Name: <a href="{owner_url}">{owner_name}</a></p>
<p>Organization: {owner_organization}</p>
<p>Department: {owner_department}</p>
<p>Title: {owner_title}</p>
<p>Lab plan: {lab_plan}</p>
<p><a href="{lab_url}">{lab_name}</a> reguests activation.</p>
'''
}

SERVER_STOPPED = {
    'subject' : '{site} - Server stopped',
    'body'    : '''
<p>Dear {owner_name},</p>
<p>Your server <a href="{server_url}">{server_name}</a> stopped.</p>
<p>Please go to <a href="{server_url}">server page</a> if you want to restart this server.</p>

{end}
'''
}
