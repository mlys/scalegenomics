import re

re_username = re.compile('^[a-zA-Z0-9]{4,20}$')
re_password = re.compile('^.{6,30}$')

re_email = re.compile(
    '^([a-zA-Z0-9][a-zA-Z0-9_\-\.]*\@([a-zA-Z0-9]{2,}\.){1,}[a-zA-Z]{2,4})$')
re_email_subject = re.compile('^[A-Za-z0-9\'\s]+$')

re_lab_name      = re.compile('^[a-zA-Z0-9\'\s]{4,50}$')
re_server_name   = re.compile('^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{0,30}$')
re_storage_name  = re.compile('^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{0,30}$')
re_storage_alias = re.compile('^[a-zA-Z0-9]{1,30}$')
re_project_name  = re.compile('^[a-zA-Z0-9][a-zA-Z0-9\s\'\-]{1,20}$')
