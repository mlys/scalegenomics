import web
from templates import jinja_env


def custom_notfound():
    return web.notfound(jinja_env.get_template('my/not_found.html').render(
        {'cpage': 'mylabs'}))
