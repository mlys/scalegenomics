class FDict(dict):
    '''
    Change web.Storage to FDict.
    Used by WTForms.
    '''
    def __init__(self, *args, **kw):
        dict.__init__(self, *args, **kw)

    def getlist(self, key):
        try:
            return [self.__getitem__(key)]
        except KeyError:
            return []
