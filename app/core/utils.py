
from app.my.models import Ip
from settings.db   import get_scoped_session



def add_ip_range(sub, ip_from, ip_to):
    '''
    sub = '192.168.1'
    ip_from = 100
    ip_to = 200
    '''
    session = get_scoped_session()

    for i in range(ip_from, ip_to):
        ip = Ip(value = '{}.{}'.format(sub, i))
        session.add(ip)

    session.commit()
