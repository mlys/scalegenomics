import web
import json
from templates import jinja_env


def as_json(f):
    def inner(*args, **kw):
        web.header('Content-Type', 'application/json; charset=utf-8')
        return json.dumps(f(*args, **kw))
    return inner


def as_html(template_name):
    def wrapper(f):
        def inner(*args, **kwargs):
            output = f(*args, **kwargs)
            web.header('Content-Type', 'text/html; charset=utf-8')
            return jinja_env.get_template(template_name).render(output)
        return inner
    return wrapper


def as_text(f):
    def inner(*args, **kw):
        web.header('Content-Type', 'text/html; charset=utf-8')
        return f(*args, **kw)
    return inner
