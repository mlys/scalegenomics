import os
import re
import tempfile
import zipfile
import shutil

import web

from app.root.utils import get_vpn_users
from app.sso.models import UserCert
from utils.sysutils import call


def gen_user_certs(session=None):
    if web.config["SG_SETTINGS"]["DEBUG"]:
        return
    if not session:
        session = web.ctx.orm

    users = get_vpn_users(session)
    ucerts = (session.query(UserCert).all())
    ips = [ucert.ip for ucert in ucerts]

    for user in users:
        cert = (session.query(UserCert)
                .filter_by(user_id = user.id)
                .first())
        if cert:
            continue

        ip = find_last_ip(ips)
        ucert = UserCert(user_id = user.id,
                         ip = ip)
        session.add(ucert)
        session.commit()

        cname = ucert.cert_name
        mk_certs(cname)
        paths = cert_paths(cname)
        with open(paths['key_path']) as f:
            ucert.key = f.read()
        with open(paths['csr_path']) as f:
            ucert.csr = f.read()
        with open(paths['crt_path']) as f:
            ucert.crt = f.read()
        session.commit()

        set_ccd(cname, ucert.ip)
        ips.append(ip)
        ucerts.append(ucert)

    # remove unused
    user_ids = [user.id for user in users]
    for ucert in ucerts:
        if ucert.user_id not in user_ids:
            session.delete(ucert)
            rm_certs(ucert.cert_name)
    session.commit()
    session.close()


def cert_paths(common_name):
    er_dir = '/etc/openvpn/easy-rsa'
    key_path = os.path.join(er_dir, 'keys', '{}.key'.format(common_name))
    csr_path = os.path.join(er_dir, 'keys', '{}.csr'.format(common_name))
    crt_path = os.path.join(er_dir, 'keys', '{}.crt'.format(common_name))
    openssl_path = os.path.join(er_dir, 'openssl.cnf')
    index_path   = os.path.join(er_dir, 'keys', 'index.txt')

    return {'key_path':     key_path,
            'csr_path':     csr_path,
            'crt_path':     crt_path,
            'openssl_path': openssl_path,
            'index_path':   index_path}


def remove_cn_from_index(common_name, index_path):
    result = []
    changed = False
    pem_id = None
    with open(index_path) as f:
        cn = '/CN={}/'.format(common_name)
        for line in f.read().split('\n'):
            if cn in line:
                changed = True
                params = re.split('\s+', line)
                pem_id = params[2]
            else:
                result.append(line)
    if changed:
        with open(index_path, 'w') as f:
            f.write('\n'.join(result))
        pem_path = '/etc/openvpn/easy-rsa/keys/{}.pem'.format(pem_id)
        if os.path.exists(pem_path):
            os.remove(pem_path)


def mk_certs(common_name):
    paths = cert_paths(common_name)
    remove_cn_from_index(common_name, paths['index_path'])

    args = ['openssl', 'req', '-batch',
            '-days', '3650',
            '-nodes', '-new', '-newkey', 'rsa:1024',
            '-keyout', paths['key_path'],
            '-out',    paths['csr_path'],
            '-config', paths['openssl_path']]
    call(args, env={'KEY_CN': common_name})

    args = ['openssl', 'ca', '-batch',
            '-days', '3650',
            '-md', 'sha1',
            '-out',    paths['crt_path'],
            '-in',     paths['csr_path'],
            '-config', paths['openssl_path']]
    call(args, env={'KEY_CN': common_name})


def rm_certs(common_name):
    paths = cert_paths(common_name)
    remove_cn_from_index(common_name, paths['index_path'])
    os.remove(paths['key_path'])
    os.remove(paths['csr_path'])
    os.remove(paths['crt_path'])
    os.remove('/etc/openvpn/ccd/{}'.format(common_name))


def set_ccd(common_name, ip):
    ccd = '''
ifconfig-push {ip} {ip}
push "route 192.168.2.0 255.255.255.0"
push "route 192.168.3.0 255.255.255.0"
push "route 192.168.4.0 255.255.255.0"
push "route 192.168.5.0 255.255.255.0"
'''
    with open('/etc/openvpn/ccd/{}'.format(common_name), 'w') as f:
        f.write(ccd.format(ip=ip))


def find_last_ip(ips):
    ends = []
    for ip in ips:
        ends.append(int(ip.split('.')[-1]))
    ip = sorted(ends)
    if ip:
        f = int(ip[-1]) + 1
    else:
        f = 10
    return '10.10.1.{}'.format(f)


def mkzip(tblk, conf, ta_key, ca_crt, client_crt, client_key):
    td_path = tempfile.mkdtemp()
    sg_path = os.path.join(td_path, tblk)
    zip_path = os.path.join(td_path, 'scalegenomics.zip')
    os.mkdir(sg_path)

    conf_path       = os.path.join(sg_path, 'client.conf')
    ta_key_path     = os.path.join(sg_path, 'ta.key')
    ca_crt_path     = os.path.join(sg_path, 'ca.crt')
    client_crt_path = os.path.join(sg_path, 'client.crt')
    client_key_path = os.path.join(sg_path, 'client.key')

    with open(conf_path, 'wb') as f:
        f.write(conf)
    with open(ta_key_path, 'wb') as f:
        f.write(ta_key)
    with open(ca_crt_path, 'wb') as f:
        f.write(ca_crt)
    with open(client_crt_path, 'wb') as f:
        f.write(client_crt)
    with open(client_key_path, 'wb') as f:
        f.write(client_key)

    os.chdir(td_path)
    with zipfile.ZipFile(zip_path, 'w') as myzip:
        myzip.write(os.path.join(tblk, 'client.conf'))
        myzip.write(os.path.join(tblk, 'ta.key'))
        myzip.write(os.path.join(tblk, 'ca.crt'))
        myzip.write(os.path.join(tblk, 'client.crt'))
        myzip.write(os.path.join(tblk, 'client.key'))

    zip_body = ''
    with open(zip_path, 'rb') as zf:
        zip_body = zf.read()

    shutil.rmtree(td_path)
    return zip_body


def statistics():
    stats = []
    with open(web.config["SG_SETTINGS"]["OPENVPN_STATUS_LOG"]) as f:
        stats = f.readlines()

    hosts = []
    sizes = [
        (1 << 50L, "PB"),
        (1 << 40L, "TB"),
        (1 << 30L, "GB"),
        (1 << 20L, "MB"),
        (1 << 10L, "KB"),
        (1,       "B")
        ]

    def byte2str(size):
        for f, suf in sizes:
            if size >= f:
                break
        return "%.2f %s" % (size / float(f), suf)

    for line in stats:
        cols = line.split(",")
        if len(cols) == 5 and not line.startswith("Common Name"):
            host  = {}
            host["cn"]    = cols[0]
            host["real"]  = cols[1].split(":")[0]
            host["recv"]  = byte2str(int(cols[2]))
            host["sent"]  = byte2str(int(cols[3]))
            host["since"] = cols[4].strip()
            hosts.append(host)

        if len(cols) == 4 and not line.startswith("Virtual Address"):
            for h in hosts:
                if h["cn"] == cols[1]:
                    h["virt"] = cols[0]

    return hosts
