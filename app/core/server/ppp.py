import web

from app.root.utils import get_vpn_users
from app.core.mail  import mail_tb


def gen_chap_secrets(session=None):
    if not web.config["SG_SETTINGS"]["USE_PPTP"]:
        return "error", "disabled"

    users = get_vpn_users(session)
    user_data = []
    for user in users:
        user_data.append((user.username, user.password))

    try:
        path = web.config["SG_SETTINGS"]["PPP_CHAP_SECRETS_PATH"]
        with open(path, "w") as f:
            for uname, upass in user_data:
                line = '{}\t\t\tpptpd\t\t\t"{}"\t\t\t*\n'.format(uname, upass)
                f.write(line)
        return "ok", ""

    except IOError as err:
        if "Permission denied" in err.strerror:
            return "error", "Permission denied"
        elif "No such file or directory" in err.streeror:
            return "error", "No such file or directory"
        else:
            mail_tb("Problems with chap-secrets")
            return "error", "Unknown error"
    except Exception:
        mail_tb()
        return "error", "Unknow error"
