import traceback, smtplib, StringIO, datetime
from email.mime.text import MIMEText

import os, sys
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))
if path not in sys.path: sys.path.append(path)

import base
base.init_all_settings()


import web

from app.root.models import MailQuery
from app.core.mailt  import LETTER_END, SIGNUP, JOIN_TO_LAB, \
     USER_ADDED_TO_LAB, JOIN_TO_PROJECT, ACT_LAB, USER_LEAVED_LAB, \
     DELETE_USER_FROM_LAB, USER_ADDED_TO_PROJECT, DELETE_USER_FROM_PROJECT, \
     REJECT_USER_FROM_PROJECT, PROJECT_DELETED, REJECT_USER_FROM_LAB, \
     MAIL_TEMPLATE, LAB_VERIFIED, TRIAL_FINISHED, TRIAL_WILL_FINISH, \
     RESTORE_PASSWORD, SERVER_STOPPED


MAIL_MAP = {
    'signup'                   : SIGNUP,

    'lab verified'             : LAB_VERIFIED,
    'trial ended'              : TRIAL_FINISHED,
    'trial ended 5'            : TRIAL_WILL_FINISH,

    'join to lab'              : JOIN_TO_LAB,
    'user added to lab'        : USER_ADDED_TO_LAB,
    'user leaved lab'          : USER_LEAVED_LAB,
    'delete user from lab'     : DELETE_USER_FROM_LAB,
    'reject user from lab'     : REJECT_USER_FROM_LAB,

    'join to project'          : JOIN_TO_PROJECT,
    'user added to project'    : USER_ADDED_TO_PROJECT,
    'delete user from project' : DELETE_USER_FROM_PROJECT,
    'reject user from project' : REJECT_USER_FROM_PROJECT,
    'project deleted'          : PROJECT_DELETED,

    'restore_password' : RESTORE_PASSWORD,

    'server_stopped' : SERVER_STOPPED,

    'lab activation' : ACT_LAB,
}


def mail_tb(message='', subject='SG Traceback'):

    if not web.config['SG_SETTINGS']['SEND_MAIL']: return
    fp = StringIO.StringIO()
    traceback.print_exc(file=fp)

    tback = fp.getvalue()
    tback = tback if tback != 'None\n' else ''

    message = (datetime.datetime.utcnow().strftime('%H:%M %B %d, %Y UTC') +
               '\n\n' + message + '\n\n')

    msg = MIMEText(message + '\n\n' + tback)
    msg['Subject'] = subject
    msg['From']    = web.config['SG_SETTINGS']['MAIL_FROM']

    server = smtplib.SMTP(web.config['SG_SETTINGS']['SMTP_HOST'])
    server.sendmail(web.config['SG_SETTINGS']['MAIL_FROM'],
                    web.config['SG_SETTINGS']['MAIL_TO']['DEBUG'],
                    msg.as_string())

    server.quit()


def mail(message='', subject='SG Report'):

    if not web.config['SG_SETTINGS']['SEND_MAIL']: return
    message = (datetime.datetime.utcnow().strftime('%H:%M %B %d, %Y UTC') +
               '\n\n' + message + '\n\n')

    msg = MIMEText(message + '\n\n')
    msg['Subject'] = subject
    msg['From']    = web.config['SG_SETTINGS']['MAIL_FROM']

    server = smtplib.SMTP(web.config['SG_SETTINGS']['SMTP_HOST'])
    server.sendmail(web.config['SG_SETTINGS']['MAIL_FROM'],
                    web.config['SG_SETTINGS']['MAIL_TO']['DEBUG'],
                    msg.as_string())

    server.quit()


def send_mail(mail_type, **kw):
    """
    Create mail and add it to query.
    Function for request handlers.
    """
    if not web.config['SG_SETTINGS']['SEND_MAIL']: return
    if mail_type not in MAIL_MAP: return

    if 'mail_from' not in kw:
        kw['mail_from'] = web.config['SG_SETTINGS']['DEFAULT_MAIL_FROM']
    if 'session' not in kw:
        kw['session'] = web.ctx.orm

    site_name = kw['site_name'] if 'site_name' in kw else web.ctx.site_name
    subject   = MAIL_MAP[mail_type]['subject'].format(site=site_name)
    body      = MAIL_MAP[mail_type]['body'].format(end = LETTER_END, **kw)

    m = MailQuery(mail_to   = kw['mail_to'],
                  mail_from = kw['mail_from'],
                  subject   = subject,
                  body      = body)

    kw['session'].add(m)
    kw['session'].commit()


def gen_test_mails():
    vsubject = {'site' : 'ScaleGenomics'}
    vbody = {'end'                 : LETTER_END,
             'owner_url'           : 'scalegenomics.com',
             'owner_name'          : 'OWNER',
             'user_url'            : 'scalegenomics.com',
             'user_name'           : 'USER',
             'user_email'          : 'support@scalegenomics.com',
             'lab_url'             : 'scalegenomics.com',
             'lab_name'            : 'LABNAME',
             'project_url'         : 'scalegenomics.com',
             'project_name'        : 'PROJECTNAME',
             'members_url'         : 'scalegenomics.com',
             'project_members_url' : 'scalegenomics.com',
             'upgrade_url'         : 'scalegenomics.com',
             'restore_password_url': 'scalegenomics.com',
             'owner_organization'  : 'ORGANIZATION X',
             'owner_department'    : 'DEPARTMENT OF MAGIC',
             'owner_title'         : 'AGENT',
             'lab_plan'            : 'Free',
             'server_url'          : 'scalegenomics.com',
             'server_name'         : 'SERVER'}
    for key, val in MAIL_MAP.items():
        subject = val['subject'].format(**vsubject)
        body    = MAIL_TEMPLATE.format(val['body'].format(**vbody))
        yield subject, body


def test_mails():
    server = smtplib.SMTP(web.config['SG_SETTINGS']['SMTP_HOST'])
    for subject, body in gen_test_mails():
        print(subject)
        msg = MIMEText(body, 'html', 'utf-8')
        msg['Subject']= subject
        msg['From']   = web.config['SG_SETTINGS']['DEFAULT_MAIL_FROM']
        msg['To']     = web.config['SG_SETTINGS']['MAIL_TO']['DEBUG'][0]

        server.sendmail(web.config['SG_SETTINGS']['DEFAULT_MAIL_FROM'],
                        web.config['SG_SETTINGS']['MAIL_TO']['DEBUG'][0],
                        msg.as_string())
    server.quit()


if __name__ == '__main__':
    test_mails()
