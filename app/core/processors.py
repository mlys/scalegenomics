import datetime

import web
from sqlalchemy.orm import scoped_session, sessionmaker

import mail
from app.core.templates import jinja_env
from app.sso.models     import User

TIMESTAMP = datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S')


def load_sqla(handler):
    'Initialize SQLAlchemy session.'
    web.ctx.orm = scoped_session(sessionmaker(bind=web.config['sqla_engine'],
                                              autoflush=True))
    try:
        return handler()
    except web.HTTPError:
        web.ctx.orm.commit()
        raise
    except:
        mail.mail_tb('load sqla')
        web.ctx.orm.rollback()
        raise
    finally:
        web.ctx.orm.commit()
        web.ctx.orm.close()


def define_globals(handler):
    web.ctx.site_url  = "http://" + web.ctx.env["HTTP_HOST"]
    web.ctx.site_name = web.config["SG_SETTINGS"]["SITE_NAME"]

    jinja_env.globals.update({
        "SITE_NAME":       web.config["SG_SETTINGS"]["SITE_NAME"],
        "TIMESTAMP":       TIMESTAMP,
        "COPYRIGHT_NAME":  web.config["SG_SETTINGS"]["COPYRIGHT_NAME"],
        "LAB_TYPE":        web.config["SG_SETTINGS"]["LAB_TYPE"],
        "session":         web.ctx.session,
        "user":            web.ctx.user})

    return handler()


def find_user(handler):
    if "user_id" in web.ctx.session:
        user = (web.ctx.orm.query(User)
                .filter_by(id = web.ctx.session["user_id"])
                .first())
        web.ctx.user = user
    else:
        web.ctx.user = None
    return handler()


def auth_check(handler):
    if "username" not in web.ctx.session:
        if "HTTP_X_REQUESTED_WITH" in web.ctx.env:
            web.ctx.session.next = web.ctx.env["HTTP_REFERER"]
        else:
            web.ctx.session.next = web.ctx.site_url + web.ctx.env["PATH_INFO"]
        raise web.redirect(web.ctx.site_url)
    return handler()


def staff_check(handler):
    if 'user' in web.ctx and web.ctx.user:
        if web.ctx.user.is_staff:
            return handler()
    web.ctx.session.next = web.ctx.site_url + web.ctx.env['PATH_INFO']
    raise web.redirect(web.ctx.site_url)
