from jinja2 import nodes, ext
from jinja2.exceptions import TemplateSyntaxError


from filters import timedeltaformat, sizeformat


class ServersPaymentsTable(ext.Extension):
    '''
    Render servers payments table.
    USAGE:
        {% serverspaymentstable servers, servers_total %}
    '''
    tags = set(['serverspaymentstable'])

    def __init__(self, environment):
        ext.Extension.__init__(self, environment)

    def parse(self, parser):
        lineno = parser.stream.next().lineno

        try:
            servers = parser.parse_expression()
        except TemplateSyntaxError:
            parser.fail(
                'Invalid {% serverpaymentstable %} syntax: no servers')

        if parser.stream.skip_if('comma'):
            servers_total = parser.parse_expression()

        #if parser.stream.skip_if('comma'):
        #    with_links = parser.parse_expression()
        #else:
        #    with_links = nodes.Const(False)

        args = [servers, servers_total]
        return nodes.CallBlock(
                self.call_method('_render', args=args, lineno=lineno),
                    [], [], '')

    def _render(self, servers, servers_total, caller):
        thead = '''
            <tr>
              <th>Name</th>
              <th>Created by</th>
              <th class="th-right">Core</th>
              <th class="th-right">RAM</th>
              <th class="th-right">Sytem disk size</th>
              <th class="th-right">Cost/month</th>
              <th class="th-time">Time</th>
              <th>Price</th>
            </tr>'''
        tbody = ''
        for serv, user, logs in servers:
            for log in logs:
                tbody += self._get_table_row(serv, user, log)
        tbody += '''
                    <tr>
                      <td>Total</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>{:0.2f}</td>
                    </tr>'''.format(servers_total)

        return '''
            <table class="payments-servers-tbl">
              <caption>Servers</caption>
              <thead>
                {thead}
              </thead>
              <tbody>
                {tbody}
              </tbody>
            </table>'''.format(thead=thead, tbody=tbody)

    def _get_table_row(self, server, user, log):

        return '''
            <tr>
              <td>{server_name}</td>
              <td>{user}</td>
              <td class="td-right">{server_cpu}</td>
              <td class="td-right">{server_memory}</td>
              <td class="td-right">{server_size}</td>
              <td class="td-right">{cost}</td>
              <td>{server_time}</td>
              <td>{price}</td>
            </tr>'''.format(
           server_name   = server.a_tag,
           user          = user.a_tag,
           server_cpu    = server.cpu,
           server_memory = sizeformat(log['log'].memory),
           server_size   = log['log'].disk_size_str,
           cost          = "{a:0.2f}".format(a=log['m_price']),
           server_time   = timedeltaformat(log['dtime']),
           price         = "{a:0.2f}".format(a=log['price']))


class DisksPaymentsTable(ext.Extension):
    '''
    Render disks payments table.
    USAGE:
        {% diskspaymentstable disks, disks_total %}
    '''
    tags = set(['diskspaymentstable'])

    def __init__(self, env):
        ext.Extension.__init__(self, env)

    def parse(self, parser):
        lineno = parser.stream.next().lineno

        try:
            disks = parser.parse_expression()
        except TemplateSyntaxError:
            parser.fail('Invalid {% diskspaymentstable %} syntax: no disks')

        if parser.stream.skip_if('comma'):
            disks_total = parser.parse_expression()

        #if parser.stream.skip_if('comma'):
        #    with_links = parser.parse_expression()
        #else:
        #    with_links = nodes.Const(False)

        args = [disks, disks_total]
        return nodes.CallBlock(
                self.call_method('_render', args=args, lineno=lineno),
                    [], [], '')

    def _render(self, storages, storages_total, caller):
        tbody = ''
        for storage, user, logs in storages:
            for log in logs:
                tbody += self._get_table_row(storage, user, log)
        if storages_total:
            tbody += '''
                <tr>
                  <td>Total</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>{:0.2f}</td>
                </tr>'''.format(storages_total)
        return '''
            <table class="payments-disks-tbl">
              <caption>Disks</caption>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Created by</th>
                  <th class="th-right">Size</th>
                  <th class="th-right">Cost/month</th>
                  <th class="th-time">Time</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {tbody}
              </tbody>
            </table>'''.format(tbody=tbody)

    def _get_table_row(self, storage, user, log):

        return '''
            <tr>
              <td>{storage_name}</td>
              <td>{user}</td>
              <td class="td-right">{size}</td>
              <td class="td-right">{m_price}</td>
              <td>{dtime}</td>
              <td>{price}</td>
            </tr>'''.format(
            storage_name = storage.a_tag,
            user         = user.a_tag,
            size         = log['log'].size_str,
            m_price      = "{a:0.2f}".format(a=log['m_price']),
            dtime        = timedeltaformat(log['dtime']),
            price        = "{a:0.2f}".format(a=log['price']))


class UsersTree(ext.Extension):
    '''
    Render users tree.
    USAGE:
        {% userstree users %}
    '''
    tags = set(['userstree'])

    def __init__(self, env):
        ext.Extension.__init__(self, env)

    def parse(self, parser):
        lineno = parser.stream.next().lineno

        try:
            users = parser.parse_expression()
        except TemplateSyntaxError:
            parser.fail('Invalid {% userstree %} syntax: no users')

        return nodes.CallBlock(
            self.call_method('_render', args=[users], lineno=lineno), [], [], '')

    def _render(self, users, caller):
        body = ''
        line = '<li><a href="{user_url}">{username}</a>\n{chlds}</li>'
        for user, chlds in users.items():
            body += line.format(user_url = user.root_url,
                                username = user.username,
                                chlds    = self.parse_users(chlds))
        return '<ul class="ul-tree">{}</ul>'.format(body)

    def parse_users(self, users):
        body = ''
        row = ('<li>'
               '   <a href="{user_url}">{username}</a>\n'
               '{chlds}'
               '</li>')
        for i, user in enumerate(users):
            u = user.keys()[0]
            chlds = user[u]
            body += row.format(user_url = u.root_url,
                               username = u.username,
                               chlds    =  self.parse_users(chlds))
        return '<ul class="ul-tree-child">{}</ul>'.format(body) if body else ''


class InvoiceServersTable(ext.Extension):
    '''
    Render invoice servers table.
    USAGE:
        {% invoiceserverstable servers %}
    '''
    tags = set(['invoiceserverstable'])

    def __init__(self, environment):
        ext.Extension.__init__(self, environment)

    def parse(self, parser):
        lineno = parser.stream.next().lineno

        try:
            servers = parser.parse_expression()
        except TemplateSyntaxError:
            parser.fail(
                'Invalid {% invoiceserverstable %} syntax: no servers')

        return nodes.CallBlock(
            self.call_method('_render', args=[servers], lineno=lineno),
            [], [], '')

    def _render(self, servers, caller):
        thead = '''
            <tr>
              <th>Name</th>
              <th class="th-left">CPU</th>
              <th class="th-right">RAM</th>
              <th class="th-right">Disk size</th>
              <th class="th-right">Cost/month</th>
              <th class="time">Time</th>
              <th class="th-price">Price</th>
            </tr>'''
        tbody = ''
        for serv in servers:
            for s in serv:
                tbody += self._get_table_row(s)

        return '''
            <table class="invoice-tbl">
              <caption>Servers</caption>
              <thead>
                {thead}
              </thead>
              <tbody>
                {tbody}
              </tbody>
            </table>'''.format(thead=thead, tbody=tbody)

    def _get_table_row(self, serv):

        return '''
          <tr>
            <td>{server_name}</td>
            <td>{server_cpu}</td>
            <td class="td-right">{server_memory}</td>
            <td class="td-right">{server_size} GB</td>
            <td class="td-right">{cost}</td>
            <td>{server_time}</td>
            <td class="td-price">{price}</td>
          </tr>'''.format(
            server_name   = serv['server'].name,
            server_cpu    = 1,
            server_memory = serv['server'].memory_text_gb,
            server_size   = serv['server'].max_size_gb,
            cost          = "{a:0.2f}".format(a=serv['payments']['cost']),
            server_time   = timedeltaformat(serv['payments']['dtime']),
            price         = "{a:0.2f}".format(a=serv['payments']['total-price']))


class InvoiceDisksTable(ext.Extension):
    '''
    Render disks payments table.
    USAGE:
        {% invoicediskstable disks %}
    '''
    tags = set(['invoicediskstable'])

    def __init__(self, env):
        ext.Extension.__init__(self, env)

    def parse(self, parser):
        lineno = parser.stream.next().lineno

        try:
            disks = parser.parse_expression()
        except TemplateSyntaxError:
            parser.fail('Invalid syntax: no disks')

        return nodes.CallBlock(
                self.call_method('_render', args=[disks], lineno=lineno),
                    [], [], '')

    def _render(self, disks, caller):
        tbody = ''
        for disk in disks:
            for d in disk:
                tbody += self._get_table_row(d)

        return '''
            <table class="invoice-tbl">
              <caption>Disks</caption>
              <thead>
                <tr>
                  <th>Name</th>
                  <th class="th-right">Size</th>
                  <th class="th-right">Cost/month</th>
                  <th class="time">Time</th>
                  <th class="th-price">Price</th>
                </tr>
              </thead>
              <tbody>
                {tbody}
              </tbody>
            </table>'''.format(tbody=tbody)

    def _get_table_row(self, disk):
        return '''
          <tr>
            <td>{disk_name}</td>
            <td class="td-right">{disk_size} GB</td>
            <td class="td-right">{cost}</td>
            <td>{time}</td>
            <td class="td-price">{price}</td>
          </tr>'''.format(disk_name = disk['disk'].name,
                          disk_size = disk['disk'].max_size_gb,
                          cost      = '{a:0.2f}'.format(a=disk['cost']),
                          time      = timedeltaformat(disk['dtime']),
                          price     = '{a:0.2f}'.format(a=disk['price']))
