from app.my.payments import dtime_total_seconds


def datetimeformat(value):
    if not value:
        return 'Now'
    return '{month} {day}, {year}'.format(month = value.strftime('%B'),
                                          day   = value.day,
                                          year  = value.year)


def timedeltaformat(value):
    if value == 'month':
        return '1 month'
    days, mod  = divmod(dtime_total_seconds(value), 86400)
    hours, mod = divmod(mod, 3600)
    minutes, seconds = divmod(mod, 60)

    if days == 0 and hours == 0:
        min_text = 'minute' if minutes == 1 else 'minutes'
        return '{min:0.0f} {min_text}'.format(min=minutes,
                                              min_text=min_text)

    elif days == 0 and hours > 0:
        hours_text = 'hour' if hours == 1 else 'hours'
        return '{hrs:0.0f} {hours_text} and {min:0.0f} minutes'.format(
            hrs=hours, hours_text=hours_text, min=minutes)

    elif days > 0 and hours == 0:
        days_text = 'day' if days == 1 else 'days'
        return '{days:0.0f} {days_text}'.format(
            days=days, days_text=days_text)

    elif days > 0 and hours > 0:
        days_text = 'day' if days == 1 else 'days'
        hrs_text = 'hour' if hours == 1 else 'hours'

        return '{days:0.0f} {days_text} and {hrs:0.0f} {hours_text}'.format(
            days=days, days_text=days_text, hrs=hours, hours_text=hrs_text)

    return '{days:0.0f} days {hrs:02.0f}:{min:02.0f}:{sec:02.0f}'.format(
        days=days, hrs=hours, min=minutes, sec=seconds)


def messageformat(text, time=2700):
    return '''
      <div id="msg" class="message">{}</div>
      <script type="text/javascript">
        setTimeout('$("#msg").fadeOut()', {});
      </script>
    '''.format(text, time) if text else ''


def sizeformat(size):

    if size < 1024:
        return '{} MB'.format(size)

    elif size < 1024 * 1024:
        gb = round(size / 1024.0, 2)
        gb_i = int(gb)
        return '{} GB'.format(gb_i if gb_i == gb else gb)

    elif size < 1024 * 1024 * 1024:
        tb = round(size / 1024.0 / 1024.0, 2)
        tb_i = int(tb)
        return '{} TB'.format(tb_i if tb_i == tb else tb)


def formerror(form, field):
    if field in form.errors:
        return ('<small class="form-error">{}</small>'
                .format(form.errors[field][0]))
    else:
        return ''
