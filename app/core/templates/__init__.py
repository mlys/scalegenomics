from jinja2 import Environment, PrefixLoader, PackageLoader

from filters import datetimeformat, timedeltaformat, messageformat, \
        sizeformat, formerror
from ext     import ServersPaymentsTable, DisksPaymentsTable, UsersTree, \
    InvoiceServersTable, InvoiceDisksTable


jinja_env = Environment(
    loader=PrefixLoader({
        'root': PackageLoader('app.root', 'html'),
        'my':   PackageLoader('app.my',   'html'),
        'sso':  PackageLoader('app.sso',  'html'),
    }),
    extensions=[ServersPaymentsTable, DisksPaymentsTable, UsersTree,
                InvoiceServersTable, InvoiceDisksTable],
    trim_blocks=True)


jinja_env.filters['timedeltaformat'] = timedeltaformat
jinja_env.filters['datetimeformat']  = datetimeformat
jinja_env.filters['messageformat']   = messageformat
jinja_env.filters['sizeformat']      = sizeformat
jinja_env.filters['formerror']       = formerror
