import os

import web
from sqlalchemy import create_engine


class DBConnection(object):

    def __init__(self, driver='mysql+oursql',
                       host='localhost',
                       encoding='utf8'):
        self.driver   = driver
        self.encoding = encoding
        self.host     = host

    def dsn(self, **kwargs):
        'Returns a canonical dsn string.'

        if 'encoding' not in kwargs:
            kwargs['encoding'] = self.encoding
        if 'host' not in kwargs:
            kwargs['host'] = self.host

        url = '{}://{}'.format(self.driver, self.raw_dsn)
        return url.format(**kwargs)

    def create_engine(self, *args, **kwargs):
        raise NotImplementedError


class MysqlConnection(DBConnection):
    raw_dsn = '{user}:{pass}@{host}/{name}?charset={encoding}'

    def create_engine(self, url_options):
        engine_url = self.dsn(**url_options)
        return create_engine(engine_url,
                             echo=False,
                             pool_size=100,
                             pool_recycle=7200)


class SqliteConnection(DBConnection):
    raw_dsn = r'/{name}'

    def create_engine(self, url_options, **engine_options):

        if not 'poolclass' in engine_options:
            from sqlalchemy.pool import SingletonThreadPool
            engine_options['poolclass'] = SingletonThreadPool

        engine_url = self.dsn(**url_options)
        return create_engine(engine_url, **engine_options)


def init_config():

    skey = 'SG_SETTINGS'
    web.config[skey] = {}  # TODO: immutable dict

    import settings.main as config  # main is reserved
    # main settings
    for key, val in config.__dict__.items():
        if key.isupper():
            if key in web.config[skey]:
                raise ValueError('Global variable already exists')
            web.config[skey][key] = val

    # local settings, overriding variables with settings.local_settings.py
    try:
        from settings import local_settings
        for key, val in local_settings.__dict__.items():
            if key.isupper():
                if key not in web.config[skey]:
                    raise ValueError('Unknown global variable')
                web.config[skey][key] = val
    except ImportError:
        pass


def init_db():

    if web.config['SG_SETTINGS']['DB_DRV'] == 'mysql':
        conn = MysqlConnection(driver=web.config['SG_SETTINGS']['DB_DRV'],
                               host=web.config['SG_SETTINGS']['DB_HOST'])
        eng = conn.create_engine({
            'user': web.config['SG_SETTINGS']['DB_USER'],
            'pass': web.config['SG_SETTINGS']['DB_PASS'],
            'name': web.config['SG_SETTINGS']['DB_NAME']})
    else:
        conn = SqliteConnection(driver=web.config['SG_SETTINGS']['DB_DRV'],
                                host=web.config['SG_SETTINGS']['DB_HOST'])
        eng = conn.create_engine({'name': web.config['SG_SETTINGS']['DB_NAME']})

    web.config['sqla_engine'] = eng


def init_all_settings():
    init_config()
    init_db()


def get_app(path=None):

    if path is None:
        path = os.path.realpath(os.path.dirname(__file__))

    init_all_settings()

    # init application
    from app.my.views   import web_app as my
    from app.sso.views  import web_app as sso
    from app.root.views import web_app as root

    urls = ('/my',   my,
            '/root', root,
            '',      sso)

    web.config.debug = web.config['SG_SETTINGS']['DEBUG']

    app = web.application(urls, globals())

    init_session(app)

    # add processors (middleware)
    from app.core.processors import define_globals, find_user, load_sqla

    app.add_processor(web.loadhook(set_session))
    app.add_processor(load_sqla)
    app.add_processor(find_user)
    app.add_processor(define_globals)

    # reloader for apache2
    if web.config['SG_SETTINGS']['MONITOR']:
        import monitor
        monitor.start(interval=1.0)
        monitor.track(path)

    # replacement standard 404 error
    from app.core.errors import custom_notfound
    app.notfound = custom_notfound

    return app


def init_session(app):

    cookie_name = web.config['SG_SETTINGS']['SESSION_COOKIE_NAME']
    secret_key  = web.config['SG_SETTINGS']['SESSION_SECRET_KEY']
    web.config.session_parameters['cookie_name'] = cookie_name
    web.config.session_parameters['secret_key']  = secret_key

    if (web.config['SG_SETTINGS']['SESSION_DB_USER'] != None and
        web.config['SG_SETTINGS']['SESSION_DB_PASS'] != None):

        db = web.database(dbn  = web.config['SG_SETTINGS']['SESSION_DB_DRV'],
                          db   = web.config['SG_SETTINGS']['SESSION_DB_NAME'],
                          user = web.config['SG_SETTINGS']['SESSION_DB_USER'],
                          pw   = web.config['SG_SETTINGS']['SESSION_DB_PASS'])
    else:
        db = web.database(dbn = web.config['SG_SETTINGS']['SESSION_DB_DRV'],
                          db  = web.config['SG_SETTINGS']['SESSION_DB_NAME'])

    store = web.session.DBStore(db, 'sessions')
    web.config['session'] = web.session.Session(app, store)


def set_session():
    '''Make session global with sub-apps and add session to template.'''
    web.ctx.session = web.config['session']
