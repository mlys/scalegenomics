# NOTE: use uppercase letters to GLOBAL VARIABLES

import os
from collections import OrderedDict


SITE_NAME = 'Scale Genomics'
SITE_URL  = 'http://manage.scalegenomics.com'  # fix it

# project database
DB_DRV  = 'mysql'
DB_USER = 'genome'
DB_PASS = 'ruR6j20EC8ypG'
DB_HOST = 'localhost'
DB_NAME = 'scalegenomics'

# session database
SESSION_DB_DRV  = DB_DRV
SESSION_DB_USER = DB_USER
SESSION_DB_PASS = DB_PASS
SESSION_DB_NAME = DB_NAME

SESSION_COOKIE_NAME = 'session_id'
SESSION_SECRET_KEY  = 'B0e7F2S@tE599XmXQsPFj'

# apache2 monitor
MONITOR = True
DEBUG   = False

# regenerate chap-secrets
USE_PPTP = True

SEND_MAIL = True
DHCP = {
    "DEBUG": False,
    "EXE":   "/etc/init.d/isc-dhcp-server",
}

# mail settings
DEFAULT_MAIL_FROM = 'support@scalegenomics.com'
SMTP_HOST = 'localhost'
MAIL_FROM = 'web@scalegenomics.com'
MAIL_TO  = {'INFO':  ['qbuben@gmail.com',
                       'ddon@fotki.com',
                       'dpetrov@stanford.edu'],
            'DEBUG': ['qbuben@gmail.com']}


PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                            os.path.pardir))

TEMPLATES_PATH = os.path.join(PROJECT_PATH, 'templates')

LAB_TYPE = ['Academic',
            'Biotech',
            'Government',
            'Pharma',
            'Other']

COPYRIGHT_NAME  = 'Scale Genomics'

PPP_CHAP_SECRETS_PATH = '/etc/ppp/chap-secrets'

VSM_HOST = '192.168.2.2'
VSM_PORT = 50007


# in Gb
STORAGE_SIZE_LIMITS = {
    'min':     1,
    'max':     1000,
    'default': 100,
}

KVM_CPU_LIMITS = (1, 20)
KVM_MEMORY_LIMITS = {
    'min':  1024,
    'max':  16384,
    'step': 512,
    }
KVM_MEMORY_LIST = [x for x in range(KVM_MEMORY_LIMITS['min'],
                                     KVM_MEMORY_LIMITS['max'] + 1,
                                     KVM_MEMORY_LIMITS['step'])]

KVM_CPULIMITS = OrderedDict([
    ('normal',     50),
    ('high',       70),
    ('super high', 90),
])

DAEMON_HOST = '192.168.2.1'
DAEMON_PORT = 13013
DAEMON_SLEEP = 25

DAEMON_PORTS = {
    'SERVER_CREATE':   13000,
    'SERVER_REMOVE':   13001,
    'SERVER_SSR':      13002,
    'SERVER_MOUNT':    13013,  # umounting also
    'SERVER_STATUS':   13004,
    'SERVER_CPULIMIT': 13005,

    'SERVER_CONFIGURE': 13013,

    'STORAGE_CREATE': 13030,
    'STORAGE_REMOVE': 13031,

    'SYSTEM_INFO': 13040,
    }

OPENVPN_STATUS_LOG = "/var/log/openvpn-status.log"

TA_KEY = '''#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
398f3c7267bc6b8350ce46d2c2f501bb
1fe2f40c27e3a10a5187d3721a502f90
30337836c617d7f51c3dde846f83d028
bd60268c9bbefe3d524441accbce95fb
8d6d46e4b56ba414c8e335c9a457598a
573a3f78d8979b2f7cafc47b5bb0afd2
3c39d9e306ef889f57b349e1f301c0b5
6026c637aec2ccdf430abd6fc0b65dff
ffc91ba4f7f1f94d3a5813b2588f79de
3ba882d7d8bf6b8093f14094fe590286
ea636ac16b8fbd3003a064e1b5da6103
b8a33bbbdb9d3fe839897bed28bd47b3
64b9d89522bb6ce6631dd2fef613a43e
ca4fa7943d97ce809abc7ba887964299
a4afb4d0286107fbbbfe93f0a2ccfbf6
9ee281d2ff7a6b1c3278947c63306875
-----END OpenVPN Static key V1-----'''

CA_CRT = '''-----BEGIN CERTIFICATE-----
MIIDvDCCAyWgAwIBAgIJAIz2M+M1aMBGMA0GCSqGSIb3DQEBBQUAMIGbMQswCQYD
VQQGEwJDQTEZMBcGA1UECBMQQnJpdGlzaCBDb2x1bWJpYTESMBAGA1UEBxMJVmFu
Y291dmVyMRcwFQYDVQQKEw5TY2FsZSBHZW5vbWljczEaMBgGA1UEAxMRU2NhbGUg
R2Vub21pY3MgQ0ExKDAmBgkqhkiG9w0BCQEWGXN1cHBvcnRAc2NhbGVnZW5vbWlj
cy5jb20wHhcNMTExMjAyMDExNTUzWhcNMjExMTI5MDExNTUzWjCBmzELMAkGA1UE
BhMCQ0ExGTAXBgNVBAgTEEJyaXRpc2ggQ29sdW1iaWExEjAQBgNVBAcTCVZhbmNv
dXZlcjEXMBUGA1UEChMOU2NhbGUgR2Vub21pY3MxGjAYBgNVBAMTEVNjYWxlIEdl
bm9taWNzIENBMSgwJgYJKoZIhvcNAQkBFhlzdXBwb3J0QHNjYWxlZ2Vub21pY3Mu
Y29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDZ21y+vnBmKoPzUUelNrbn
S/O/kKKkNw5XxR7PdAjFTYEpfIVjfwnupNqVtUuwwaRLQNyJJ1kX+4w8tCnxxHQP
5H+QG5lMWUGjxvqnhEA2947L5whjuRXDpkdEKLYCn4Y4g/I8zm5msmvHIMMQuyBO
OLl3czcDOqwGdhooxKf9ZQIDAQABo4IBBDCCAQAwHQYDVR0OBBYEFLurk3JcVL2x
qf6lTvxv6QHh5m3kMIHQBgNVHSMEgcgwgcWAFLurk3JcVL2xqf6lTvxv6QHh5m3k
oYGhpIGeMIGbMQswCQYDVQQGEwJDQTEZMBcGA1UECBMQQnJpdGlzaCBDb2x1bWJp
YTESMBAGA1UEBxMJVmFuY291dmVyMRcwFQYDVQQKEw5TY2FsZSBHZW5vbWljczEa
MBgGA1UEAxMRU2NhbGUgR2Vub21pY3MgQ0ExKDAmBgkqhkiG9w0BCQEWGXN1cHBv
cnRAc2NhbGVnZW5vbWljcy5jb22CCQCM9jPjNWjARjAMBgNVHRMEBTADAQH/MA0G
CSqGSIb3DQEBBQUAA4GBAH6/oF+k6uqqB3wsdfIZtLaITEKvzvgxozVCIXhWQWJR
tBZL5Z+tSUrlAhri7HObnEMLbx8H0N4UfRlM0L8So577rTfCbRa7NAhjaXA81gs8
RJuS94+I79uGzOk4veHAMMYZBc5JjsC00xQWMxTg8mXtgc0T1HXd1qJIYqJk6aKe
-----END CERTIFICATE-----'''


MAC_OPENVPN_CONFIG = '''
remote scalegenomics.com
tls-client
proto tcp-client
dev tun
port 1194
pull
cipher DES-EDE3-CBC
comp-lzo
remote-cert-tls server

tls-auth ta.key 1
ca ca.crt
cert client.crt
key client.key

log-append /var/log/openvp-client-sg.log
'''
