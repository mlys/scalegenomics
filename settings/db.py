import web

from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base


metadata = MetaData(bind=web.config["sqla_engine"])
Base = declarative_base(metadata=metadata)


class ModelExt(object):
    "Extension for Base-like models."

    # used by sqlalchemy
    __table_args__ = {
        "extend_existing": True
        }

    def ptag(self, field):
        if hasattr(self, field):
            attr = str(getattr(self, field))
            if attr:
                result = ""
                for line in str(getattr(self, field)).split("\n"):
                    result += "<p>{}</p>".format(line)
                return result
        return "&nbsp;"

    def __repr__(self):
        return "<{}({})>".format(type(self).__name__, self.id)


def get_scoped_session():
    from sqlalchemy.orm import sessionmaker, scoped_session
    return scoped_session(sessionmaker(bind=web.config["sqla_engine"],
                                       autocommit=False))
