from settings.db     import get_scoped_session
from app.root.models import SiteSettings


def get_db_settings():
    session = get_scoped_session()
    site_settings = session.query(SiteSettings).all()
    prices = {}
    sites = []
    for site in site_settings:
        sites.append([site.domain, site.name, site.site_id])
        prices[site.site_id] = {
            'server-cpu-price'  : float(site.server_cpu_price),
            'server-ram-price'  : float(site.server_ram_price),
            'server-size-price' : float(site.server_size_price),
            'disk-size-price'   : float(site.disk_size_price)}

    return sites, prices
