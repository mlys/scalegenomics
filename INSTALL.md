INSTALL
=======

Dependencies
------------

* [mysql](http://dev.mysql.com/)
* [python2.7](http://python.org/)

Python packages:

* [web.py](http://webpy.org/)
* [sqlalchemy](http://www.sqlalchemy.org/)
* [sqlalchemy-migrate](http://code.google.com/p/sqlalchemy-migrate/)
* [jinja2](http://jinja.pocoo.org/docs/)
* [WTForms](http://wtforms.simplecodes.com/)
* [oursql](http://packages.python.org/oursql/)
* [mysql-python](http://sourceforge.net/projects/mysql-python/)

OurSQL (MacOS)
--------------

```bash
# switch to super user
sudo -i
# set up environment
export PIP_MYSQL_CONFIG=/usr/local/mysql/bin/mysql_config
export MYSQL_CONFIG=/usr/local/mysql/bin/mysql_config
# install
pip install oursql
```

mysql-python (MacOS)
--------------------

Download source from [mysql-python](http://sourceforge.net/projects/mysql-python/).


```bash
python2.7 setup.py install
Make sure that /etc/paths has /usr/local/mysql/bin/ line
```

In order to avoid 'Library not loaded: libmysqlclient.18.dylib' error, following symilink is needed:
```bash
sudo ln -s /usr/local/mysql/lib/libmysqlclient.18.dylib /usr/lib/libmysqlclient.18.dylib
```


Python packages
---------------

```bash
pip install -U -r req.txt
```

Prepare database
----------------

```sql
CREATE DATABASE sdm CHARACTER SET UTF8;
CREATE USER 'sdm'@'localhost' IDENTIFIED BY 'sdmsdmsdm'
GRANT ALL ON sdm.* TO 'sdm'@'localhost';
```

Sqlite

Copy local\_settings.py.sample to  settings/local\_settings.py and modify it.


Database schema
---------------

```bash
cd /path/to/ScaleGenomicsWeb
python init_db.py
```


Local run
---------

```bash
./run.sh
http://0.0.0.0:8080/
```
