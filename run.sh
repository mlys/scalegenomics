#!/bin/bash

while true
do
    /usr/bin/env python2.7 -c "import main; main.app.run()"
    sleep 3
done
