import os
import sys
import string
import random

path = os.path.realpath(os.path.dirname(__file__))
if path not in sys.path:
    sys.path.append(path)

import web

import base
base.init_config()
base.init_db()

from settings.db import get_scoped_session

engine = web.config['sqla_engine']
engine.echo = True


from app.root.models import *
from app.sso.models  import *
from app.my.models   import *


def add_migration_table():

    from settings import PROJECT_PATH

    sql = ("CREATE TABLE IF NOT EXISTS migrate_version ("
           "repository_id VARCHAR(250) NOT NULL,"
           "repository_path TEXT,"
           "version INTEGER,"
           "PRIMARY KEY (repository_id));")

    con = engine.connect()

    repo_path = os.path.join(PROJECT_PATH, 'migration')
    vs = os.listdir(os.path.join(repo_path, 'versions'))
    vs.sort()
    last_version = int(vs[-2][:3])

    sql_cmd = "INSERT INTO migrate_version VALUES ('Scale Genomics', '{}', {})"
    con.execute(sql)
    con.execute(sql_cmd.format(repo_path, last_version))
    con.close()


def add_some_info():
    "Test data."
    session = get_scoped_session()

    cs1 = ClusterServer(name = "SG1",
                        ip="localhost",
                        port=13000,
                        ssl_cert="scalegenomics1.pem",
                        ssl_key="scalegenomics1.key")
    cs2 = ClusterServer(name = "SG2",
                        ip="localhost",
                        port=13001,
                        ssl_cert="scalegenomics1.pem",
                        ssl_key="scalegenomics1.key")
    session.add(cs1)
    session.add(cs2)
    session.commit()

    session.add(ClusterDisk(server_id = cs1.id,
                            max_size  = 3000,
                            free_size = 3000,
                            uuid      = '',
                            mount_id  = 1))
    session.add(ClusterDisk(server_id = cs1.id,
                            max_size  = 3000,
                            free_size = 3000,
                            uuid      = '',
                            mount_id  = 2))
    session.add(ClusterDisk(server_id = cs2.id,
                            max_size  = 3000,
                            free_size = 3000,
                            uuid      = '',
                            mount_id  = 1))
    session.add(ClusterDisk(server_id = cs2.id,
                            max_size  = 3000,
                            free_size = 3000,
                            uuid      = '',
                            mount_id  = 2))
    session.commit()

    for i in range(100):
        ip = Ip(value = '192.168.2.{}'.format(100 + i))
        session.add(ip)
    session.commit()


def add_system_user():
    session = get_scoped_session()
    password = ''.join([random.choice(string.ascii_letters + string.digits)
                        for _ in range(30)])
    user = User(username     = "system",
                email        = "system",
                password     = password,
                lnx_password = "",
                first_name   = "System",
                last_name    = "Monitor",
                position     = User.POSITIONS["System"])
    session.add(user)
    session.commit()


def add_plans():
    session = get_scoped_session()

    t = Plan(name    = Plan.PLANS['Trial'],
             price   = 0,
             cores   = 1,
             ram     = 4,
             storage = 2)
    session.add(t)
    session.commit()

    t = Plan(name    = Plan.PLANS['Beginner'],
             price   = 99,
             cores   = 8,
             ram     = 12,
             storage = 4)
    session.add(t)
    session.commit()

    t = Plan(name    = Plan.PLANS['Green'],
             price   = 999,
             cores   = 24,
             ram     = 48,
             storage = 30)
    session.add(t)
    session.commit()

    t = Plan(name    = Plan.PLANS['Blue'],
             price   = 1999,
             cores   = 36,
             ram     = 144,
             storage = 48)
    session.add(t)
    session.commit()

    t = Plan(name    = Plan.PLANS['Black diamond'],
             price   = 2999,
             cores   = 48,
             ram     = 192,
             storage = 66)
    session.add(t)
    session.commit()

    t = Plan(name     = Plan.PLANS['Double-black diamond'],
              price   = 3999,
              cores   = 56,
              ram     = 288,
              storage = 80)
    session.add(t)
    session.commit()


def add_options():
    session = get_scoped_session()
    enable_login = SiteOption(name  = "enable_login",
                              yesno = True,
                              value = """
      The site is undergoing scheduled maintenance.
      We will be back shortly.""")
    session.add(enable_login)
    session.commit()


def install():
    from settings.db import metadata
    metadata.create_all(bind=engine)

    add_migration_table()

    add_some_info()
    add_system_user()

    add_plans()
    add_options()


if __name__ == '__main__':
    install()
