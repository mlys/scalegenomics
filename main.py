import os
import sys

path = os.path.realpath(os.path.dirname(__file__))
if path not in sys.path:
    sys.path.append(path)


import base


app = base.get_app(path)
application = app.wsgifunc()
