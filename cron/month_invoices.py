import os, sys
from datetime import datetime

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if path not in sys.path: sys.path.append(path)

import web

import base
import app.my.payments         as payments
from settings.db           import get_scoped_session
from app.core              import mail
from app.my.models         import Lab, LabInvoice, LabPrice



def create_prev_month_invoices():

    base.init_all_settings()

    now = datetime.utcnow()
    session = get_scoped_session()

    invoices = []
    labs = session.query(Lab).all()
    for lab in labs:
        if now.month == 1:
            inv_date = datetime(now.year-1, 12, 1)
        else:
            inv_date = datetime(now.year, now.month-1, 1)

        invoice_check = (session.query(LabInvoice)
                         .filter_by(lab_id = lab.id)
                         .filter_by(date = inv_date)
                         .first())
        if invoice_check: continue

        lab_price = (session.query(LabPrice)
                     .filter_by(lab_id = lab.id)
                     .first())
        if lab_price:
            prices = lab_price.to_d()
        else:
            prices = web.config['SG_SETTINGS']['PRICES'][1]

        inv = payments.create_invoice(lab,
                                      prices,
                                      now,
                                      inv_date.month,
                                      session)
        if inv: invoices.append(inv)

    if invoices:
        message = 'Created: {count} invoices'.format(count=len(invoices))
        mail.mail(message)


if __name__ == '__main__':
    create_prev_month_invoices()
