# */5 * * * * python2.7 /path/to/__file__

import os
import sys
import smtplib
from email.mime.text import MIMEText

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if path not in sys.path:
    sys.path.append(path)

import web


import base
base.init_all_settings()


from settings.db     import get_scoped_session
from app.root.models import MailQuery
from app.core.mail   import mail_tb


def send_mails():

    server = smtplib.SMTP(web.config["SG_SETTINGS"]["SMTP_HOST"])

    session = get_scoped_session()
    mquery = (session.query(MailQuery).all())
    for mail in mquery:
        try:
            for mail_to in mail.mail_to.split(","):
                msg = MIMEText(mail.body, "html", "utf-8")
                msg["Subject"] = mail.subject
                msg["From"]    = mail.mail_from
                msg["To"]      = mail_to

                server.sendmail(mail.mail_from,
                                mail_to,
                                msg.as_string())
            session.delete(mail)
            session.commit()
        except:
            mail_tb()

    server.quit()


if __name__ == "__main__":
    send_mails()
