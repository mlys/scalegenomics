# 0 * * * * python2.7 /path/to/__file__

import os
import sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if path not in sys.path:
    sys.path.append(path)

import web

import base
base.init_all_settings()

from settings.db     import get_scoped_session
from app.core.mail   import send_mail, mail
from app.core.server import ppp
from app.my.models   import Lab, LabPlan, KVMServer
from app.sso.models  import User, UserToLab


def send_trial_mail():
    session = get_scoped_session()
    site_name = web.config["SG_SETTINGS"]["SITE_NAME"]
    site_url  = web.config["SG_SETTINGS"]["SITE_URL"]

    q = (session.query(Lab, LabPlan)
         .filter(Lab.id == LabPlan.lab_id)
         .filter(LabPlan.name == LabPlan.PLANS["Trial"])
         .filter(LabPlan.deleted == None)
         .all())
    for lab, lab_plan in q:
        if (lab_plan.days_remain > 5):
            continue

        q = (session.query(User, UserToLab)
                 .filter(UserToLab.user_id == User.id)
                 .filter(UserToLab.lab_id == lab.id)
                 .filter(UserToLab.perms == UserToLab.PERMS["owner"])
                 .first())
        owner, u2lab = q
        owner_url = site_url + owner.url
        rel_path = "/my/{}/payments/plans/upgrade".format(lab.id)
        upgrade_url = site_url + rel_path

        if lab_plan.days_remain == 5:
            send_mail("trial ended 5", session=session,
                                       site_name=site_name,
                                       mail_to=owner.email,
                                       owner_name=owner.full_name,
                                       upgrade_url=upgrade_url)
            mail("Lab {!r} {!r} trial period 5 days".format(lab.id, lab.name))

        elif lab_plan.days_remain <= 0:

            lab.status = Lab.STATUS["trial block"]
            ppp.gen_chap_secrets(session)

            send_mail("trial ended", session=session,
                                     site_name=site_name,
                                     mail_to=owner.email,
                                     owner_url=owner_url,
                                     owner_name=owner.full_name,
                                     upgrade_url=upgrade_url)
            mail("Lab {!r} {!r} trial period ends".format(lab.id, lab.name))

            servers = (session.query(KVMServer)
                       .filter_by(lab_id = lab.id)
                       .filter_by(status = KVMServer.STATUS["RUNNING"])
                       .all())
            for server in servers:
                server.status = KVMServer.STATUS["STOPPING"]

    session.commit()


if __name__ == "__main__":
    send_trial_mail()
