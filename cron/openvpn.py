# */5 * * * * python2.7 /path/to/__file__

import os
import sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if path not in sys.path:
    sys.path.append(path)

import base
base.init_all_settings()

from settings.db             import get_scoped_session
from app.core.server.openvpn import gen_user_certs
from app.core.mail           import mail_tb


if __name__ == "__main__":
    session = get_scoped_session()
    try:
        gen_user_certs(session)
    except:
        mail_tb("OpenVPN")
