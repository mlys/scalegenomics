from utils import cclient


DOIT_CONFIG = {'verbosity': 2,
               'default_tasks' : ['loading']}


def task_loading():
    'Default task'
    return {'actions' : ['echo "You seek our service?"']}


def task_delpyc():
    'Delete *.pyc files'
    cmd1 = 'find . -name \"*.pyc\"'
    cmd2 = 'find . -name \"*.pyc\" -delete'
    return {'actions': [
        'echo ".. {}"'.format(cmd1),
        'echo "----------------------------------"',
        cmd1,
        'echo "----------------------------------"',
        'echo ".. {}"'.format(cmd2),
        cmd2
    ]}


def task_storages():
    'Check storages'
    return {'actions' : [cclient.get_storages]}

