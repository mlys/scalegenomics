import os
import sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
if path not in sys.path: sys.path.append(path)

import web

import base
base.init_all_settings()

repository_path = os.path.join(web.config['SG_SETTINGS']['PROJECT_PATH'],
                               'migration')

from migrate.versioning.shell import main
main(debug='True', repository=repository_path, url=web.config['sqla_engine'])

