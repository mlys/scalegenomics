from datetime import datetime

from sqlalchemy import *
from migrate import *

import web

import base
from app.my.models   import Lab, LabPrice, LabInvoice
from app.my.payments import calc_invoice
from settings.db     import get_scoped_session

base.init_all_settings()

session = get_scoped_session()


def upgrade(migrate_engine):
    inv_s = session.query(LabInvoice).all()
    for inv in inv_s:
        session.delete(inv)
    session.commit()

    now = datetime.utcnow()

    month_range = range(1, datetime.utcnow().month)
    labs = session.query(Lab).all()
    for lab in labs:
        lab_price = (session.query(LabPrice)
                     .filter_by(lab_id = lab.id)
                     .first())
        if lab_price:
            prices = lab_price.to_d()
        else:
            prices = web.config['SG_SETTINGS']['PRICES'][1]

        for month in month_range:
            inv = calc_invoice(lab, prices, now, month, session)
            if inv: session.add(inv)
        session.commit()


def downgrade(migrate_engine):
    inv_s = session.query(LabInvoice).all()
    for inv in inv_s:
        session.delete(inv)
    session.commit()
