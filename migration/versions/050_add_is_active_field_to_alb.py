from sqlalchemy import *
from migrate import *

from app.my.models import Lab

col = Column('is_active', Boolean, default=True)


def upgrade(migrate_engine):
    col.create(Lab.__table__)

def downgrade(migrate_engine):
    col.drop(Lab.__table__)
