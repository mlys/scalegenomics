from sqlalchemy import *
from migrate import *

from app.root.models import DataSetVolume

def upgrade(migrate_engine):
    DataSetVolume.__table__.create()

def downgrade(migrate_engine):
    DataSetVolume.__table__.drop()
