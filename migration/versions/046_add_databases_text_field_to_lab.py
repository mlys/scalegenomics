from sqlalchemy import *
from migrate import *

from app.my.models import Lab


databases = Column('databases_info', Text, nullable=None, default='')

def upgrade(migrate_engine):
    databases.create(Lab.__table__)


def downgrade(migrate_engine):
    databases.drop(Lab.__table__)
