from sqlalchemy     import *
from sqlalchemy.orm import scoped_session, sessionmaker
from migrate        import *

from app.my.models  import Lab
from app.sso.models import UserToLab

col = Column("perms", SmallInteger, nullable=None, default=-1)


def upgrade(engine):
    col.create(UserToLab.__table__)
    session = scoped_session(sessionmaker(bind=engine))
    u2labs = session.query(UserToLab).all()
    for u2lab in u2labs:
        u2lab.perms = UserToLab.PERMS[u2lab.relation]
    session.commit()


def downgrade(migrate_engine):
    col.drop(UserToLab.__table__)
