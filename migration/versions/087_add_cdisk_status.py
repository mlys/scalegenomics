from sqlalchemy import *
from migrate import *

from app.root.models import ClusterDisk


col = Column("status", SmallInteger, nullable=None, default=0)


def upgrade(migrate_engine):
    col.create(ClusterDisk.__table__)


def downgrade(migrate_engine):
    col.drop(ClusterDisk.__table__)
