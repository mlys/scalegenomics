from sqlalchemy import *
from migrate import *

from app.my.models import Lab

col1 = Column('cpu_used', Integer, nullable=None, default=0)
col2 = Column('ram_used', Integer, nullable=None, default=0)
col3 = Column('storages_used', Integer, nullable=None, default=0)


def upgrade(migrate_engine):
    col1.create(Lab.__table__)
    col2.create(Lab.__table__)
    col3.create(Lab.__table__)


def downgrade(migrate_engine):
    col1.drop(Lab.__table__)
    col2.drop(Lab.__table__)
    col3.drop(Lab.__table__)

