from sqlalchemy import *
from migrate import *

from app.my.models   import KVMServer
from app.root.models import ClusterServer, ClusterDisk


def upgrade(migrate_engine):
    KVMServer.__table__.create()
    ClusterServer.__table__.create()
    ClusterDisk.__table__.create()


def downgrade(migrate_engine):
    KVMServer.__table__.drop()
    ClusterServer.__table__.drop()
    ClusterDisk.__table__.drop()

