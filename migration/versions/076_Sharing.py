from sqlalchemy import *
from migrate import *

from app.my.models import Sharing


def upgrade(migrate_engine):
    Sharing.__table__.create(bind=migrate_engine, checkfirst=True)


def downgrade(migrate_engine):
    Sharing.__table__.drop(checkfirst=True)
