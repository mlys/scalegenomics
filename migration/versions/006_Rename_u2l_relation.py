from sqlalchemy     import *
from sqlalchemy.orm import scoped_session, sessionmaker
from migrate        import *

from settings.db    import *
from app.my.models  import Lab
from app.sso.models import User, UserToLab


session = scoped_session(sessionmaker(bind=engine))


def upgrade(migrate_engine):
    
    u2l = (session.query(UserToLab)
            .filter_by(relation='waiting for joining')
            .all())
    for rel in u2l:
        rel.relation = 'waiting to be approved'
    session.commit()

    u2l = (session.query(UserToLab)
            .filter_by(relation='waiting for confirmation')
            .all())
    for rel in u2l:
        rel.relation = 'waiting to be approved'
    session.commit()


def downgrade(migrate_engine):
    
    u2l = (session.query(UserToLab)
            .filter_by(relation='waiting to be approved')
            .all())
    for rel in u2l:
        rel.relation = 'waiting for confirmation'
    session.commit()

