from sqlalchemy import *
from migrate import *

from app.root.models import ClusterDisk

col_model  = Column('model',  String(50), nullable=None, default='')
col_status = Column('status', Integer,    nullable=None, default=-1)


def upgrade(migrate_engine):
    col_model.create(ClusterDisk.__table__)
    col_status.create(ClusterDisk.__table__)


def downgrade(migrate_engine):
    col_model.drop(ClusterDisk.__table__)
    col_status.drop(ClusterDisk.__table__)

