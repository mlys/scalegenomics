from sqlalchemy import *
from migrate import *

from app.root.models import ClusterDisk
from app.my.models   import KVMServer, KVMStorage

col = Column('raid_id', Integer, nullable=None, default=-1)


def upgrade(migrate_engine):
    col.drop(ClusterDisk.__table__)
    col.drop(KVMServer.__table__)
    col.drop(KVMStorage.__table__)


def downgrade(migrate_engine):
    col.create(ClusterDisk.__table__)
    col.create(KVMServer.__table__)
    col.create(KVMStorage.__table__)

