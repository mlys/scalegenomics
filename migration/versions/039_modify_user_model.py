from sqlalchemy import *
from migrate import *

from app.sso.models import User


col_org  = Column('organization', String(100), nullable=None, default='')
col_dep  = Column('department',   String(100), nullable=None, default='')
col_tit  = Column('title',        String(100), nullable=None, default='')
col_pho  = Column('phone',        String(50),  nullable=None, default='')
col_how  = Column('how',     Text, default='', nullable=None)
col_tell = Column('tell_us', Text, default='', nullable=None)


def upgrade(migrate_engine):
    col_org.create(User.__table__)
    col_dep.create(User.__table__)
    col_tit.create(User.__table__)
    col_pho.create(User.__table__)
    col_how.create(User.__table__)
    col_tell.create(User.__table__)


def downgrade(migrate_engine):
    col_org.drop(User.__table__)
    col_dep.drop(User.__table__)
    col_tit.drop(User.__table__)
    col_pho.drop(User.__table__)
    col_how.drop(User.__table__)
    col_tell.drop(User.__table__)

