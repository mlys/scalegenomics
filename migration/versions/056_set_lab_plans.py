from sqlalchemy import *
from migrate    import *


from settings.db   import get_scoped_session
from app.my.models import Lab, Plan, LabPlan


def upgrade(migrate_engine):
    session = get_scoped_session()
    plan = session.query(Plan).filter_by(name = Plan.PLANS['Free']).first()
    labs = session.query(Lab).all()

    for lab in labs:

        lab.cpu_limit      = plan.cores
        lab.ram_limit      = plan.ram_mb
        lab.storages_limit = plan.storage_mb

        lab_plan = LabPlan(lab_id     = lab.id,
                           plan_id    = plan.id,
                           name       = plan.name,
                           price      = plan.price,
                           cores      = plan.cores,
                           ram        = plan.ram,
                           storage    = plan.storage)
        session.add(lab_plan)

    session.commit()


def downgrade(migrate_engine):
    pass
