from sqlalchemy import *
from migrate import *


from app.my.models import KVMReservedImage



def upgrade(migrate_engine):
    KVMReservedImage.__table__.create(bind=migrate_engine, checkfirst=True)


def downgrade(migrate_engine):
    KVMReservedImage.__table__.drop(checkfirst=True)
