from sqlalchemy import *
from migrate import *

from app.root.models import Raid


def upgrade(migrate_engine):
    Raid.__table__.create(bind=migrate_engine, checkfirst=True)

def downgrade(migrate_engine):
    Raid.__table__.drop(checkfirst=False)

