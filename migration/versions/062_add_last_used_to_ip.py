from datetime import datetime

from sqlalchemy import *
from migrate    import *

from app.my.models import Ip


col = Column('last_used', DateTime, nullable=None, default = datetime.utcnow)


def upgrade(migrate_engine):
    col.create(Ip.__table__)


def downgrade(migrate_engine):
    col.drop(Ip.__table__)
