from sqlalchemy import *
from migrate import *

from app.root.models import ClusterDisk

col_port     = Column('port',     Integer,    nullable=None, default=-1)
col_dev_path = Column('dev_path', String(30), nullable=None, default='')


def upgrade(migrate_engine):
    col_port.create(ClusterDisk.__table__)
    col_dev_path.create(ClusterDisk.__table__)


def downgrade(migrate_engine):
    col_port.drop(ClusterDisk.__table__)
    col_dev_path.drop(ClusterDisk.__table__)

