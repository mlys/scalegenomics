from sqlalchemy import *
from migrate    import *

from app.root.models import ClusterDisk

col1 = Column('uuid_sub', String(100), nullable=None, default='')
col2 = Column('port',     Integer,     nullable=None, default=-1)
col3 = Column('model',    String(50),  nullable=None, default='')
col4 = Column('serial',   String(50),  nullable=None, default='')
col5 = Column('status',   Integer,     nullable=None, default=-1)


def upgrade(migrate_engine):
    col1.drop(ClusterDisk.__table__)
    col2.drop(ClusterDisk.__table__)
    col3.drop(ClusterDisk.__table__)
    col4.drop(ClusterDisk.__table__)
    col5.drop(ClusterDisk.__table__)


def downgrade(migrate_engine):
    col1.create(ClusterDisk.__table__)
    col2.create(ClusterDisk.__table__)
    col3.create(ClusterDisk.__table__)
    col4.create(ClusterDisk.__table__)
    col5.create(ClusterDisk.__table__)
