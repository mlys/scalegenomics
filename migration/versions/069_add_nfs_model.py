from sqlalchemy import *
from migrate    import *

from app.my.models import NFSMount


def upgrade(migrate_engine):
    NFSMount.__table__.create()

def downgrade(migrate_engine):
    NFSMount.__table__.drop()
