from sqlalchemy import *
from migrate import *

from app.my.models import Lab


c1 = Column('members', Integer, nullable=None)
c2 = Column('servers', Integer, nullable=None)
c3 = Column('wait_to_approve', Integer, nullable=None)


def upgrade(migrate_engine):
    c1.drop(Lab.__table__)
    c2.drop(Lab.__table__)
    c3.create(Lab.__table__)


def downgrade(migrate_engine):
    c1.create(Lab.__table__)
    c2.create(Lab.__table__)
    c3.drop(Lab.__table__)
