from sqlalchemy import *
from migrate    import *

from app.my.models import KVMStorageUsageLog


def upgrade(migrate_engine):
    KVMStorageUsageLog.__table__.create()


def downgrade(migrate_engine):
    KVMStorageUsageLog.__table__.drop()
