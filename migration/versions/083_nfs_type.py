from sqlalchemy import *
from sqlalchemy.orm import sessionmaker, scoped_session
from migrate import *

from app.my.models import KVMStorage, KVMServer, KVMMountRelation


def upgrade(migrate_engine):
    session = scoped_session(sessionmaker(bind=migrate_engine))
    rels = (session.query(KVMStorage, KVMServer, KVMMountRelation)
                    .filter(KVMServer.id == KVMMountRelation.server_id)
                    .filter(KVMStorage.id == KVMMountRelation.storage_id)
                    .filter(KVMStorage.status == KVMStorage.STATUS["CREATED"])
                    .all())
    for storage, server, rel in rels:
        if storage.server_id != server.server_id:
            if storage.type != storage.TYPES["NFS"]:
                storage.type = storage.TYPES["NFS"]


def downgrade(migrate_engine):
    pass
