from sqlalchemy import *
from sqlalchemy.orm import scoped_session, sessionmaker
from migrate import *

from app.my.models import NFSMount, KVMStorage


def upgrade(migrate_engine):
    session = scoped_session(sessionmaker(bind=migrate_engine))
    nfs_mounts = session.query(NFSMount).all()
    for nfs_mount in nfs_mounts:
        storage = (session.query(KVMStorage)
                    .filter_by(id = nfs_mount.id)
                    .first())
        if storage:
            storage.type = storage.TYPES["NFS"]
    session.commit()


def downgrade(migrate_engine):
    pass
