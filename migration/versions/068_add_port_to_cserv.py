from sqlalchemy import *
from migrate    import *

from app.root.models import ClusterServer

col = Column("port", Integer, nullable=None, default=13000)


def upgrade(migrate_engine):
    col.create(ClusterServer.__table__)

def downgrade(migrate_engine):
    col.drop(ClusterServer.__table__)

