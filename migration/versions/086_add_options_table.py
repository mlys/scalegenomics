from sqlalchemy import *
from migrate import *

from app.root.models import SiteOption



def upgrade(migrate_engine):
    SiteOption.__table__.create()


def downgrade(migrate_engine):
    SiteOption.__table__.drop()
