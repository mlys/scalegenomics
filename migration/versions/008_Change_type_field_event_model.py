from sqlalchemy import *
from migrate import *

from app.my.models import Event


def upgrade(migrate_engine):
    
    col = Column('type', String(50), nullable=None)
    col.drop(table=Event.__table__)

    new_col = Column('type', SmallInteger, nullable=None)
    new_col.create(table=Event.__table__)


def downgrade(migrate_engine):

    col = Column('type', SmallInteger, nullable=None)
    col.drop(table=Event.__table__)

    new_col = Column('type', String(50), nullable=None)
    new_col.create(table=Event.__table__)
