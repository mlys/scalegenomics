from sqlalchemy import *
from migrate    import *

from app.root.models import Raid

col = Column('server_id', Integer, nullable=None, default=1)


def upgrade(migrate_engine):
    col.create(Raid.__table__)


def downgrade(migrate_engine):
    col.drop(Raid.__table__)
