from sqlalchemy import *
from migrate import *

from app.my.models import KVMStorage


def upgrade(migrate_engine):
    KVMStorage.__table__.create()


def downgrade(migrate_engine):
    KVMStorage.__table__.drop()
