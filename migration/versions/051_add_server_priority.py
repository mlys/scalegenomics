from sqlalchemy import *
from migrate import *

from app.my.models import KVMServer

col = Column('cpulimit', Integer, nullable=None,
             default=KVMServer.CPULIMITS['normal'])


def upgrade(migrate_engine):
    col.create(KVMServer.__table__)


def downgrade(migrate_engine):
    col.drop(KVMServer.__table__)
