from sqlalchemy import *
from migrate import *

from app.my.models import Lab


col = Column('updated', DateTime, nullable=None)


def upgrade(migrate_engine):
    col.create(table=Lab.__table__)


def downgrade(migrate_engine):
    col.drop(table=Lab.__table__)
