from sqlalchemy import *
from migrate    import *

from app.my.models import KVMMountRelation


col1 = Column("type", SmallInteger, nullable=None)
col2 = Column("is_prepared", SmallInteger, nullable=None)


col3 = Column("real_status", SmallInteger, nullable=None)


def upgrade(migrate_engine):
    col1.drop(KVMMountRelation.__table__)
    col2.drop(KVMMountRelation.__table__)
    col3.create(KVMMountRelation.__table__)


def downgrade(migrate_engine):
    col1.create(KVMMountRelation.__table__)
    col2.create(KVMMountRelation.__table__)
    col3.drop(KVMMountRelation.__table__)
