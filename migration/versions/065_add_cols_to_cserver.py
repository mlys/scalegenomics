from sqlalchemy import *
from migrate import *

from app.root.models import ClusterServer


col1 = Column('cpu',        Integer, nullable=None, default=0)
col2 = Column('ram_used',   Integer, nullable=None, default=0)
col3 = Column('ram_total',  Integer, nullable=None, default=0)


def upgrade(migrate_engine):
    col1.create(ClusterServer.__table__)
    col2.create(ClusterServer.__table__)
    col3.create(ClusterServer.__table__)


def downgrade(migrate_engine):
    col1.drop(ClusterServer.__table__)
    col2.drop(ClusterServer.__table__)
    col3.drop(ClusterServer.__table__)


