from sqlalchemy import *
from migrate import *

from app.my.models import Lab
from settings.db   import get_scoped_session

col = Column('status', SmallInteger, nullable=None, default=0)


def upgrade(migrate_engine):
    col.create(Lab.__table__)

    session = get_scoped_session()
    labs = session.query(Lab).all()
    for lab in labs:
        lab.status = 2 if lab.is_active else 0
    session.commit()


def downgrade(migrate_engine):
    col.drop(Lab.__table__)
