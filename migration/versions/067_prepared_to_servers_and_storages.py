from sqlalchemy import *
from migrate    import *

from app.my.models import KVMServer, KVMStorage


col1 = Column('is_prepared', Boolean, nullable=None, default=False)
col2 = Column('is_prepared', Boolean, nullable=None, default=False)

def upgrade(migrate_engine):
    col1.create(KVMServer.__table__)
    col2.create(KVMStorage.__table__)

def downgrade(migrate_engine):
    col1.drop(KVMServer.__table__)
    col2.drop(KVMStorage.__table__)

