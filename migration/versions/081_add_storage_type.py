from sqlalchemy import *
from migrate import *

from app.my.models import KVMStorage

col = Column('type', SmallInteger, nullable=None, default=0)


def upgrade(migrate_engine):
    col.create(KVMStorage.__table__)


def downgrade(migrate_engine):
    col.drop(KVMStorage.__table__)
