from sqlalchemy import *
from migrate import *

from app.root.models import CRMLogCommunication


def upgrade(migrate_engine):
    CRMLogCommunication.__table__.create()

def downgrade(migrate_engine):
    CRMLogCommunication.__table__.drop()
