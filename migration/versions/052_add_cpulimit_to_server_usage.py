from sqlalchemy import *
from migrate import *

import base
from app.my.models import KVMServer, KVMServerUsageLog
from settings.db   import get_scoped_session

base.init_all_settings()

col = Column('cpulimit', Integer, nullable=None, default=50)


def upgrade(migrate_engine):
    col.create(KVMServerUsageLog.__table__)
    session = get_scoped_session()
    servers = (session.query(KVMServer)
               .filter(KVMServer.cpulimit != KVMServer.CPULIMITS['normal'])
               .all())
    for server in servers:
        sul = (session.query(KVMServerUsageLog)
               .filter_by(server_id = server.id)
               .filter_by(changed = None)
               .first())
        sul.cpulimit =  server.cpulimit
    session.commit()


def downgrade(migrate_engine):
    col.drop(KVMServerUsageLog.__table__)
