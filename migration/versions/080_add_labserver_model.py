from sqlalchemy     import *
from sqlalchemy.orm import scoped_session, sessionmaker
from migrate        import *

from app.root.models import ClusterServer, LabClusterServer
from app.my.models   import Lab


def upgrade(migrate_engine):
    LabClusterServer.__table__.create()
    session = scoped_session(sessionmaker(bind=migrate_engine))
    cserv = session.query(ClusterServer).first()
    labs = (session.query(Lab)
            .filter_by(status = Lab.STATUS['active'])
            .all())
    for lab in labs:
        lcs = LabClusterServer(lab_id = lab.id,
                               server_id = cserv.id)
        session.add(lcs)
    session.commit()


def downgrade(migrate_engine):
    LabClusterServer.__table__.drop()
