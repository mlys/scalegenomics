from sqlalchemy import *
from migrate import *

from app.my.models import KVMServerUsageLog


def upgrade(migrate_engine):
    KVMServerUsageLog.__table__.create()


def downgrade(migrate_engine):
    KVMServerUsageLog.__table__.drop()
