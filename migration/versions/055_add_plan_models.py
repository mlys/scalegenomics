from sqlalchemy import *
from migrate    import *

from app.my.models import Plan, LabPlan
from settings.db   import get_scoped_session


def upgrade(migrate_engine):
    Plan.__table__.create(bind=migrate_engine, checkfirst=True)
    LabPlan.__table__.create(bind=migrate_engine, checkfirst=True)
    session = get_scoped_session()

    t = Plan(name    = Plan.PLANS['Free'],
              price   = 0,
              cores   = 1,
              ram     = 4,
              storage = 2)
    session.add(t)
    session.commit()

    t = Plan(name    = Plan.PLANS['Micro'],
              price   = 99,
              cores   = 8,
              ram     = 12,
              storage = 4)
    session.add(t)
    session.commit()

    t = Plan(name    = Plan.PLANS['Basic'],
              price   = 999,
              cores   = 24,
              ram     = 48,
              storage = 30)
    session.add(t)
    session.commit()

    t = Plan(name    = Plan.PLANS['High'],
              price   = 1999,
              cores   = 36,
              ram     = 144,
              storage = 48)
    session.add(t)
    session.commit()

    t = Plan(name     = Plan.PLANS['Professional'],
              price   = 2999,
              cores   = 48,
              ram     = 192,
              storage = 66)
    session.add(t)
    session.commit()

    t = Plan(name     = Plan.PLANS['High-professional'],
              price   = 1999,
              cores   = 36,
              ram     = 144,
              storage = 48)
    session.add(t)
    session.commit()


def downgrade(migrate_engine):
    Plan.__table__.drop(checkfirst=True)
    LabPlan.__table__.drop(checkfirst=True)

