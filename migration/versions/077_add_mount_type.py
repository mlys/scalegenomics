from sqlalchemy     import *
from migrate        import *

from app.my.models import KVMMountRelation


col = Column("type", SmallInteger, nullable=None)


def upgrade(migrate_engine):
    col.create(KVMMountRelation.__table__)


def downgrade(migrate_engine):
    col.drop(KVMMountRelation.__table__)
