from sqlalchemy import *
from migrate import *

from app.my.models import KVMServer

col = Column('username', String(255), default='user', nullable=None)


def upgrade(migrate_engine):
    col.create(KVMServer.__table__)


def downgrade(migrate_engine):
    col.drop(KVMServer.__table__)
