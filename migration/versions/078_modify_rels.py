from sqlalchemy import *
from migrate import *

from app.my.models import KVMMountRelation


col1 = Column("rights", SmallInteger, nullable=None, default=1)
col2 = Column("is_prepared", Boolean, nullable=None, default=False)


def upgrade(migrate_engine):
    col1.create(KVMMountRelation.__table__)
    col2.create(KVMMountRelation.__table__)


def downgrade(migrate_engine):
    col1.drop(KVMMountRelation.__table__)
    col2.drop(KVMMountRelation.__table__)
