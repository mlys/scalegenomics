from sqlalchemy import *
from migrate import *

from app.root.models import DataSetRequest

def upgrade(migrate_engine):
    DataSetRequest.__table__.create()

def downgrade(migrate_engine):
    DataSetRequest.__table__.drop()
