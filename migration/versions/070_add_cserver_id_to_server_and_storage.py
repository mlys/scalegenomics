from sqlalchemy import *
from migrate    import *

from app.my.models import KVMServer, KVMStorage

col1 = Column("server_id", Integer, nullable=None, default=1)
col2 = Column("server_id", Integer, nullable=None, default=1)


def upgrade(migrate_engine):
    col1.create(KVMServer.__table__)
    col2.create(KVMStorage.__table__)

def downgrade(migrate_engine):
    col1.drop(KVMServer.__table__)
    col2.drop(KVMStorage.__table__)

