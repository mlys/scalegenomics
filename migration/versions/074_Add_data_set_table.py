from sqlalchemy import *
from migrate import *

from app.root.models import DataSet


def upgrade(migrate_engine):
    DataSet.__table__.create()

def downgrade(migrate_engine):
    DataSet.__table__.drop()