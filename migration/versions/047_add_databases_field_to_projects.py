from sqlalchemy import *
from migrate import *

from app.my.models import LabProject

databases = Column('databases_info', Text, nullable=None, default='')


def upgrade(migrate_engine):
    databases.create(LabProject.__table__)


def downgrade(migrate_engine):
    databases.drop(LabProject.__table__)
