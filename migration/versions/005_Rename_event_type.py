from sqlalchemy import *
from sqlalchemy.orm import scoped_session, sessionmaker
from migrate import *

from settings.db   import *
from app.my.models import Event


session = scoped_session(sessionmaker(bind=engine))


def upgrade(migrate_engine):
    events = (session.query(Event)
                .filter_by(type='waiting_for_joining')
                .all())
    for event in events:
        event.type = 'waiting_to_be_approved'
    session.commit()


def downgrade(migrate_engine):
    events = (session.query(Event)
                .filter_by(type='waiting_to_be_approved')
                .all())
    for event in events:
        event.type = 'waiting_for_joining'
    session.commit()

