from sqlalchemy import *
from migrate import *

from app.my.models import Lab


def upgrade(migrate_engine):
    
    col = Column('date_created', DateTime, nullable=None)
    col.alter(table=Lab.__table__, name='created')


def downgrade(migrate_engine):
    
    col = Column('created', DateTime, nullable=None)
    col.alter(table=Lab.__table__, name='date_created')


