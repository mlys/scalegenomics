from sqlalchemy import *
from migrate    import *

from app.sso.models import RestoreMail


def upgrade(migrate_engine):
    RestoreMail.__table__.create(bind=migrate_engine, checkfirst=True)


def downgrade(migrate_engine):
    RestoreMail.__table__.drop(checkfirst=True)
