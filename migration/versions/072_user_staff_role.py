from sqlalchemy import *
from migrate import *

from app.sso.models import User

col = Column('staff_role', SmallInteger, default=0, nullable=None)


def upgrade(migrate_engine):
    col.create(User.__table__)


def downgrade(migrate_engine):
    col.drop(User.__table__)


