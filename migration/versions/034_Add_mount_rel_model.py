from sqlalchemy import *
from migrate import *

from app.my.models import KVMMountRelation


def upgrade(migrate_engine):
    KVMMountRelation.__table__.create()


def downgrade(migrate_engine):
    KVMMountRelation.__table__.drop()
