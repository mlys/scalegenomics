from sqlalchemy import *
from migrate import *

from app.sso.models import UserCert


def upgrade(migrate_engine):
    UserCert.__table__.create(bind=migrate_engine, checkfirst=True)


def downgrade(migrate_engine):
    UserCert.__table__.drop(checkfirst=True)
