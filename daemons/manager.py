import os
import re
import threading
import time
import types
from collections import OrderedDict

import web

import base
base.init_all_settings()

from settings.db import get_scoped_session
from daemons import net, nfs
from daemons.utils import system_call, my_error
from app.core import mail
from app.root.models import ClusterServer, ClusterDisk
from app.sso.models import User
from app.my.models import Lab, KVMServer, KVMStorage, KVMMountRelation, \
    Ip, NFSMount


BLANK_HOST = """
host {hostname} {{
    hardware ethernet  {mac};
    fixed-address      {ip};
    option subnet-mask 255.255.0.0;
    option host-name   "{hostname}";
}}"""


def info_message(message):
    "How?"


class Manager(object):

    def __init__(self):
        self.in_process = True

    def start(self):
        session = get_scoped_session()
        cservers = session.query(ClusterServer).all()
        cservs = {}
        for cserv in cservers:
            cservs[cserv.id] = OrderedDict([
                    ("create-server",    [create_server,       None]),
                    ("configure-server", [configure_server,    None]),
                    ("remove-server",    [remove_server,       None]),
                    ("ch-server-state",  [change_server_state, None]),
                    ("create-storage",   [create_storage,      None]),
                    ("remove-storage",   [remove_storage,      None]),
            ])
        while self.in_process:
            try:
                nfs.do(session)
            except:
                my_error("nfs.do")
            try:
                calc_resources()
                for cserv, ts in cservs.items():
                    for tname, action in ts.items():
                        if action[1] is None:
                            rt = RequestThread(cserv)
                            rt.action = types.MethodType(action[0], rt)
                            rt.start()
                            action[1] = rt
                        elif action[1].is_finished:
                            rt = RequestThread(cserv)
                            rt.action = types.MethodType(action[0], rt)
                            rt.start()
                            action[1] = rt
            except Exception:
                my_error("Manager")
            time.sleep(25)

    def stop(self):
        self.in_process = False
        while True:
            count = threading.activeCount()
            if count == 1:
                break
            print("Stopping :: {} active thread(s)".format(count))
            time.sleep(5)


def get_server_mounts(session, server):
    rels = (session.query(KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == server.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMStorage.status == KVMStorage.STATUS["CREATED"])
            .filter(KVMStorage.type == KVMStorage.TYPES["LOCAL"])
            .all())
    mounts = []
    points = []
    mrels = []
    urels = []
    for storage, rel in rels:
        if rel.status not in (KVMMountRelation.STATUS["MOUNTING"],
                            KVMMountRelation.STATUS["MOUNTED"]):
            rel.point = ""
            urels.append(rel)
            continue

        p = KVMMountRelation.gen_point(points)
        points.append(p)
        rel.point = p
        disk = (session.query(ClusterDisk)
                .filter_by(id = storage.disk_id)
                .filter_by(status = ClusterDisk.STATUSES["OK"])
                .first())
        if not disk:
            continue
        mounts.append({"name":   storage.dname,
                       "disk":   disk.mount_id,
                       "point":  rel.point})
        mrels.append(rel)
    return mrels, urels, mounts


def get_server_params(session, server, c_disk):
    mrels, urels, mounts = get_server_mounts(session, server)

    params = {'domain':    server.domain,
              'image':     server.image_file_name,
              'memory':    server.memory,
              'cpu':       server.cpu,
              'password':  server.password,
              'mac':       server.mac,
              'ip':        server.ip,
              'disk':      c_disk.mount_id,
              'mounts':    mounts}
    return params, mrels, urels


def get_cserver_dict(session):
    cservers = session.query(ClusterServer).all()
    result = {}
    for cserver in cservers:
        result[cserver.id] = cserver
    return result


def get_cdisks_dict(session):
    cdisks = (session.query(ClusterDisk)
                .filter_by(status = ClusterDisk.STATUSES["OK"])
                .all())
    result = {}
    for cdisk in cdisks:
        result[cdisk.id] = cdisk
    return result


def get_labs_dict(session):
    labs = session.query(Lab).all()
    result = {}
    for lab in labs:
        result[lab.id] = lab
    return result


def _cserv_count_sort(x, y):
    if x._servs > y._servs:
        return 1
    elif x._servs == y._servs:
        return 0
    else:
        return -1


def get_cserv_and_cdisk(session, server_size):
    "My little balancer. TODO: Remove?"
    cservs = session.query(ClusterServer).all()
    cservs = sorted(cservs, cmp=ClusterServer.cmp_ramusage)

    for cserv in cservs:
        cdisks = (session.query(ClusterDisk)
                  .filter(ClusterDisk.server_id == cserv.id)
                  .filter(ClusterDisk.free_size >= server_size)
                  .filter(ClusterDisk.status == ClusterDisk.STATUSES["OK"])
                  .all())
        for cdisk in cdisks:
            servs = (session.query(KVMServer)
                     .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                     .filter(KVMServer.server_id == cserv.id)
                     .filter(KVMServer.disk_id == cdisk.id)
                     .all())
            cdisk._servs = len(servs)
        cdisks = sorted(cdisks, cmp=_cserv_count_sort)
        if cdisks:
            return cserv, cdisks[0]
    return None, None


def get_cdisk(cserv, cdisks, size):
    for cdisk in cdisks.values():
        if cdisk.server_id == cserv.id and cdisk.free_size >= size:
            return cdisk


def _find_host(ip):
    host = re.findall('^\d{1,3}\.\d{1,3}\.(\d{1,3})\.\d{1,3}$', ip)
    if host:
        return host[0]
    else:
        mail.mail('Wrong data: {!r}'.format(ip))


def init_hosts(hosts):
    for info in hosts:
        host = _find_host(info["ip"])
        if not host:
            continue

        txt = BLANK_HOST.format(mac=info["mac"],
                                ip=info["ip"],
                                hostname=info["hostname"])

        hosts_path = '/etc/dhcp/dhcpd.hosts.{}.conf'.format(host)
        if not os.path.exists(hosts_path):
            mail.mail('DHCP: Path not exists ' + hosts_path)
            open(hosts_path, 'w').close()
            return

        hosts_body = ''
        with open(hosts_path) as f:
            hosts_body += f.read()

        if info["ip"] in hosts_body:
            mail.mail('Wrong ip: {!r}'.format(info["ip"]))
            continue
        if info["mac"] in hosts_body:
            mail.mail('Wrong mac: {!r}'.format(info["mac"]))
            continue
        if info["hostname"] in hosts_body:
            mail.mail('Wrong hostname: {!r}'.format(info["hostname"]))
            continue
        hosts_body += txt
        with open(hosts_path, 'w') as f:
            f.write(hosts_body)


def del_hosts(hosts):
    for info in hosts:
        host = _find_host(info["ip"])
        if not host:
            continue

        txt = BLANK_HOST.format(mac=info["mac"],
                                ip=info["ip"],
                                hostname=info["hostname"])

        hosts_path = '/etc/dhcp/dhcpd.hosts.{}.conf'.format(host)
        if not os.path.exists(hosts_path):
            mail.mail('DHCP: Path not exists ' + hosts_path)
            continue

        hosts_body = ''
        with open(hosts_path) as f:
            hosts_body += f.read()

        if txt not in hosts_body:
            continue
        hosts_body = hosts_body.replace(txt, '')
        with open(hosts_path, 'w') as f:
            f.write(hosts_body)


def calc_resources():
    session = get_scoped_session()
    cservers = get_cserver_dict(session)
    cdisks = get_cdisks_dict(session)
    cdisks_ids = cdisks.keys()
    labs = get_labs_dict(session)

    # remove storages
    storages = (session.query(KVMStorage)
                .filter_by(status = KVMStorage.STATUS["REMOVING"])
                .filter_by(is_prepared = False)
                .filter(KVMStorage.disk_id.in_(cdisks_ids))
                .all())
    for storage in storages:
        storage.is_prepared = True
        cdisks[storage.disk_id].free_size += storage.size_gb
        labs[storage.lab_id].storages_used -= storage.size
    session.commit()

    # remove servers
    remove_hosts = []
    servers = (session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS["REMOVING"])
               .filter_by(is_prepared = False)
               .filter(KVMServer.disk_id.in_(cdisks_ids))
               .all())
    for server in servers:
        ip = (session.query(Ip)
            .filter_by(value = server.ip)
            .first())
        if not ip:
            info_message("No ip for server({})".format(server))
            continue

        server.ip = KVMServer.NO_IP
        server.is_prepared = True

        ip.is_used = False
        cdisk = cdisks[server.disk_id]
        cdisk.free_size += KVMServer.DEFAULT_SIZE
        cservers[cdisk.server_id].ram_used -= server.memory
        labs[server.lab_id].cpu_used -= server.cpu
        labs[server.lab_id].ram_used -= server.memory
        labs[server.lab_id].storages_used -= server.size_mb

        remove_hosts.append({"ip":       ip.value,
                            "mac":      server.mac,
                            "hostname": server.domain})
    session.commit()
    # + if server removed then recalc

    # stop servers
    servers = (session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS["STOPPING"])
               .filter_by(is_prepared = False)
               .filter(KVMServer.disk_id.in_(cdisks_ids))
               .all())
    for server in servers:
        server.is_prepared = True
        cservers[cdisks[server.disk_id].server_id].ram_used -= server.memory
        labs[server.lab_id].cpu_used -= server.cpu
        labs[server.lab_id].ram_used -= server.memory
    session.commit()

    # start server
    servers = (session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS["STARTING"])
               .filter_by(is_prepared = False)
               .filter(KVMServer.disk_id.in_(cdisks_ids))
               .all())
    for server in servers:
        server.is_prepared = True
        cservers[cdisks[server.disk_id].server_id].ram_used += server.memory
        labs[server.lab_id].cpu_used += server.cpu
        labs[server.lab_id].ram_used += server.memory
    session.commit()

    # create servers
    # * find servers
    # * find cserver and cdisk
    # * find ip
    # * generate unique mac
    # * resources reservation
    # * add dhcp host
    create_hosts = []
    servers = (session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS['CREATING'])
               .filter_by(is_prepared = False)
               .all())
    for server in servers:
        if server.disk_id == KVMServer.NO_DISK:
            q = (session.query(ClusterServer, ClusterDisk)
                    .filter(ClusterServer.id == ClusterDisk.server_id)
                    .filter(ClusterDisk.free_size >= server.size)
                    .filter(ClusterServer.id == server.server_id)
                    .filter(ClusterDisk.status == ClusterDisk.STATUSES["OK"])
                    .first())
            if not q:
                # TODO: not free space status
                continue
            cserv, cdisk = q

            ip = (session.query(Ip)
                  .filter_by(is_used = False)
                  .order_by(Ip.last_used)
                  .first())
            if not ip:
                mail.mail('No ip')
                continue

            mac = KVMServer.gen_mac()
            while True:
                mac_chk = (session.query(KVMServer)
                            .filter_by(mac = mac)
                            .first())
                if not mac_chk:
                    break
                mac = KVMServer.gen_mac()

            server.server_id = cserv.id
            server.disk_id = cdisk.id
            server.ip = ip.value
            server.mac = mac
            server.is_prepared = True

            ip.is_used = True
            cdisk.free_size -= server.size
            cserv.ram_used += server.memory
            labs[server.lab_id].cpu_used += server.cpu
            labs[server.lab_id].ram_used += server.memory
            labs[server.lab_id].storages_used += server.size_mb

            create_hosts.append({'ip':       server.ip,
                                 'mac':      server.mac,
                                 'hostname': server.domain})
            session.commit()

    # create storage
    storages = (session.query(KVMStorage)
                .filter_by(status = KVMStorage.STATUS["CREATING"])
                .filter_by(is_prepared = False)
                .all())
    for storage in storages:
        cdisk = None
        cds = [cdisk for cdisk in cdisks.values()
                if cdisk.server_id == storage.server_id]
        for d in sorted(cds, cmp=ClusterDisk.cmp_freesize):
            if d.free_size >= storage.size_gb:
                cdisk = d
                break
        if not cdisk:
            continue  # TODO: error

        storage.disk_id = cdisk.id
        storage.is_prepared = True
        cdisk.free_size -= storage.size_gb
        labs[storage.lab_id].storages_used += storage.size
        session.commit()

    # re-init hosts
    if not web.config["SG_SETTINGS"]["DHCP"]["DEBUG"]:
        del_hosts(remove_hosts)
        init_hosts(create_hosts)
        ex = [web.config["SG_SETTINGS"]["DHCP"]["EXE"], "force-reload"]
        system_call(ex, True)

    # status
    for cserv in cservers.values():
        cert_path = cserv.get_cert_path()
        request = {"endpoint": "server-status"}
        response = {}
        try:
            response = net.ssl_send(cserv.ip,
                                    cserv.port,
                                    cert_path,
                                    request,
                                    timeout=60)
        except Exception:
            my_error("server-status :: {}".format(request))
        if not response:
            continue
        if "ok" == response["result"] and "domains" in response:
            for domain in response["domains"]:
                server_id = KVMServer.get_id(domain["domain"])
                server = (session.query(KVMServer)
                          .filter_by(id = server_id)
                          .filter_by(status = KVMServer.STATUS["RUNNING"])
                          .first())
                if server and not domain["is_active"]:
                    server.status = KVMServer.STATUS["STOPPED"]
                    cserv.ram_used -= server.memory
                    labs[server.lab_id].cpu_used -= server.cpu
                    labs[server.lab_id].ram_used -= server.memory

                    owner = (session.query(User)
                             .filter_by(id = server.user_id)
                             .first())
                    server_url = "http://manage.scalegenomics.com" + server.url
                    mail.send_mail("server_stopped",
                                   session     = session,
                                   site_name   = "Scale Genomics",
                                   mail_to     = owner.email,
                                   owner_name  = owner.full_name,
                                   server_url  = server_url,
                                   server_name = server.name)
                    msg = "server id={} status changed to {}"
                    mail.mail(msg.format(server.id, server.status_str))
                session.commit()
    session.close()


class RequestThread(threading.Thread):

    def __init__(self, cserv_id):
        threading.Thread.__init__(self)
        self.cserv_id    = cserv_id
        self.is_finished = False

    def run(self):
        self.session = get_scoped_session()
        self.cserv = (self.session.query(ClusterServer)
                      .filter_by(id = self.cserv_id)
                      .first())
        if not self.cserv:
            return
        self.cdisks = (self.session.query(ClusterDisk)
                       .filter_by(server_id = self.cserv.id)
                       .filter_by(status = ClusterDisk.STATUSES["OK"])
                       .all())
        self.disks_id = [disk.id for disk in self.cdisks]
        try:
            self.action()
        except Exception:
            my_error("request thread :: action = {}".format(self.action))
        finally:
            self.is_finished = True


def get_rels_d(rels):
    result = {}
    for _, stor, rel in rels:
        if stor.id not in result:
            result[str(stor.id)] = rel
    return result


def prepare_mounts(cservs, nfs_mounts):
    srted = {}
    for csid in cservs.keys():
        srted[csid] = []
    for serv, stor, mount in nfs_mounts:
        if stor.server_id in srted:
            srted[stor.server_id].append((serv, stor, mount))
    return srted


def handle_nfs(session):
    "All real changes with NFS."
    # todo: remove me
    cservs = get_cserver_dict(session)
    # umount
    nfs_mounts = (session.query(KVMServer, KVMStorage, NFSMount)
                  .filter(KVMServer.id == NFSMount.server_id)
                  .filter(KVMStorage.id == NFSMount.storage_id)
                  .filter(NFSMount.status == NFSMount.STATUS["UMOUNTING"])
                  .all())
    # sort umounts by cserv
    srted = prepare_mounts(cservs, nfs_mounts)
    umounts = {}
    umounts_d = {}
    for csid, data in srted.items():
        for serv, stor, mount in data:
            if csid not in umounts:
                umounts[csid] = []
            umounts[csid].append(stor.dname)
            umounts_d[str(mount.storage_id)] = mount
    # umounts request
    for csid, m in umounts.items():
        if not m:
            continue
        request = {"endpoint": "nfs",
                   "action":   "umount",
                   "umounts":  m}
        try:
            response = net.ssl_send(cservs[csid].ip,
                                    cservs[csid].port,
                                    cservs[csid].get_cert_path(),
                                    request)
            if "ok" == response["result"]:
                for sto, res, in response["umounts"].items():
                    id = str(KVMStorage.get_id(sto))
                    if id in umounts_d:
                        session.delete(umounts_d[id])
                        del umounts_d[id]
            else:
                my_error("nfs-umount :: {} :: {}".format(request, response))
        except:
            my_error("nfs-umount :: {}".format(request))

    # mount
    nfs_mounts = (session.query(KVMServer, KVMStorage, NFSMount)
                  .filter(KVMServer.id == NFSMount.server_id)
                  .filter(KVMStorage.id == NFSMount.storage_id)
                  .filter(NFSMount.status == NFSMount.STATUS["MOUNTING"])
                  .all())
    # sort mounts by cserv
    srted = prepare_mounts(cservs, nfs_mounts)
    mounts = {}
    mounts_d = {}
    for csid, data in srted.items():
        for serv, stor, mount in data:
            if csid not in mounts:
                mounts[csid] = []
            mounts[csid].append({"name":  stor.dname,
                                 "perms": [(serv.ip, mount.perms_str)]})
            mounts_d[str(mount.storage_id)] = mount
    # mounts request
    for csid, m in mounts.items():
        if not m:
            continue
        request = {"endpoint": "nfs",
                   "action":   "mount",
                   "mounts":   m}
        try:
            response = net.ssl_send(cservs[csid].ip,
                                    cservs[csid].port,
                                    cservs[csid].get_cert_path(),
                                    request)
            if "ok" == response["result"]:
                for sto, res in response["mounts"].items():
                    if "ok" == res:
                        id = str(KVMStorage.get_id(sto))
                        if id in mounts_d:
                            mounts_d[id].status = NFSMount.STATUS["MOUNTED"]
                    else:
                        my_error("{} :: {} :: {}".format(request, sto, res))
            else:
                my_error("nfs-mount :: {} :: {}".format(request, response))
        except Exception:
            my_error("nfs-mount :: {}".format(request))
    session.commit()


def umount_stor_nfs(session, storage):
    # TODO: remove me
    cserv = (session.query(ClusterServer)
             .filter_by(id = storage.server_id)
             .first())
    nfs_mounts = (session.query(NFSMount)
                  .filter_by(storage_id = storage.id)
                  .all())
    umounts = []
    nfs_mounts_d = {}
    for nfs_mount in nfs_mounts:
        umounts.append(storage.dname)
        nfs_mounts_d[str(nfs_mount.storage_id)] = nfs_mount

    request = {"endpoint":  "nfs",
               "action":    "umount",
               "umounts":   umounts}
    try:
        response = net.ssl_send(cserv.ip,
                                cserv.port,
                                cserv.get_cert_path(),
                                request)
        if "ok" == response["result"]:
            for sto, res, in response["umounts"].items():
                id = str(KVMStorage.get_id(sto))
                if id in nfs_mounts_d:
                    session.delete(nfs_mounts_d[id])
                    del nfs_mounts_d[id]
        else:
            my_error("nfs-umount :: {} :: {}".format(request, response))
    except:
        my_error("nfs-umount :: {}".format(request))
    session.commit()


def create_server(env):
    cert_path = env.cserv.get_cert_path()
    servers = (env.session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS["CREATING"])
               .filter_by(server_id = env.cserv_id)
               .filter_by(is_prepared = True)
               .all())
    for server in servers:
        cdisk = (env.session.query(ClusterDisk)
                 .filter_by(id = server.disk_id)
                 .filter_by(status = ClusterDisk.STATUSES["OK"])
                 .first())
        if not cdisk:
            continue
        request, mrel, urels = get_server_params(env.session, server, cdisk)
        request["endpoint"] = "create-server"
        try:
            response = net.ssl_send(env.cserv.ip,
                                    env.cserv.port,
                                    cert_path,
                                    request)
            if response["result"] == "ok":
                server.status = KVMServer.STATUS["CONFIGURING"]
                server.is_prepared = False
            else:
                my_error("create-server :: {}".format(str(response)))
        except Exception:
            my_error("create-server")
        env.session.commit()
    env.session.close()


def configure_server(env):
    servers = (env.session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS["CONFIGURING"])
               .filter_by(server_id = env.cserv_id)
               .all())
    for server in servers:
        request = {
            'cmds': ['renew-ssh',
                     'create-user',
                     'set-password',
                     'set-hostname'],
            'params': {
                'username': server.username,
                'hostname': server.domain,
                'password': server.password,
            }
        }
        try:
            response = net.send(server.ip, server.PORT, request)
            server.status = KVMServer.STATUS["RUNNING"]
            if response.get("errors", {}):
                my_error("configure-server :: {}, {!r} :: {} :: {}"
                        .format(server.id, server.name, request, response))
        except Exception:
            my_error("configure-server :: {}, {!r} :: {}"
                    .format(server.id, server.name, request))
        env.session.commit()
    env.session.close()


def remove_server(env):
    cert_path = env.cserv.get_cert_path()
    servers = (env.session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS["REMOVING"])
               .filter_by(is_prepared = True)
               .filter_by(server_id = env.cserv_id)
               .all())
    for server in servers:
        cdisk = (env.session.query(ClusterDisk)
                 .filter_by(id = server.disk_id)
                 .filter_by(status = ClusterDisk.STATUSES["OK"])
                 .first())
        if not cdisk:
            continue

        request = {"endpoint": "remove-server",
                   "domain":   server.domain,
                   "disk":     cdisk.mount_id}
        try:
            response = net.ssl_send(env.cserv.ip,
                                    env.cserv.port,
                                    cert_path,
                                    request)
            if "ok" == response["result"]:
                server.status = KVMServer.STATUS["REMOVED"]
            else:
                my_error("remove-server :: response = {}".format(response))

            rels = (env.session.query(KVMMountRelation)
                    .filter(KVMMountRelation.server_id == server.id)
                    .filter(KVMMountRelation.status !=
                            KVMMountRelation.STATUS["UMOUNTING"])
                    .filter(KVMMountRelation.status !=
                            KVMMountRelation.STATUS["UMOUNTED"])
                    .all())
            for rel in rels:
                rel.status = KVMMountRelation.STATUS["UMOUNTING"]
            # TODO: delete local rel
        except Exception:
            my_error("remove-server :: request = {}".format(request))
        env.session.commit()
    env.session.close()


def change_server_state(env):
    cert_path = env.cserv.get_cert_path()
    # stop server
    servers = (env.session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS["STOPPING"])
               .filter_by(server_id = env.cserv_id)
               .filter_by(is_prepared = True)
               .all())
    for server in servers:
        cdisk = (env.session.query(ClusterDisk)
                 .filter_by(id = server.disk_id)
                 .filter_by(status = ClusterDisk.STATUSES["OK"])
                 .first())
        if not cdisk:
            continue

        request = {"endpoint": "ch-server-state",
                   "action":   "stop",
                   "domain":   server.domain}
        try:
            response = net.ssl_send(env.cserv.ip,
                                    env.cserv.port,
                                    cert_path,
                                    request)
            if response["result"] == "ok":
                server.status = KVMServer.STATUS["STOPPED"]
                server.is_prepared = False
                rels = (env.session.query(KVMStorage, KVMMountRelation)
                        .filter(KVMMountRelation.storage_id == KVMStorage.id)
                        .filter(KVMMountRelation.server_id == server.id)
                        .filter(KVMMountRelation.status.in_([
                                KVMMountRelation.STATUS["MOUNTING"],
                                KVMMountRelation.STATUS["MOUNTED"]]))
                        .all())
                for stor, rel in rels:
                    rel.status = KVMMountRelation.STATUS["MOUNTING"]
                    if stor.type == KVMStorage.TYPES["NFS"]:
                        rel.real_status = rel.REAL_STATUS["MOUNTING TO S"]
            else:
                my_error("stop-server :: response = {}".format(response))

        except Exception:
            my_error("stop-server :: request = {}".format(request))
        env.session.commit()

    # restart server
    servers = (env.session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS['RESTARTING'])
               .filter_by(server_id = env.cserv_id)
               .all())
    for server in servers:
        cdisk = (env.session.query(ClusterDisk)
                 .filter_by(id = server.disk_id)
                 .filter_by(status = ClusterDisk.STATUSES["OK"])
                 .first())
        if not cdisk:
            continue

        request, mrels, urels = get_server_params(env.session, server, cdisk)
        request["action"] = "restart"
        request["endpoint"] = "ch-server-state"

        try:
            response = net.ssl_send(env.cserv.ip,
                                    env.cserv.port,
                                    cert_path,
                                    request)
            if response["result"] == "ok":
                server.status = KVMServer.STATUS["RUNNING"]
            else:
                my_error("restart-server :: response = {}".format(response))
        except Exception:
            my_error("restart-server :: request = {}".format(request))
        env.session.commit()

    # start server
    servers = (env.session.query(KVMServer)
               .filter_by(status = KVMServer.STATUS["STARTING"])
               .filter_by(server_id = env.cserv_id)
               .filter_by(is_prepared = True)
               .all())
    for server in servers:
        cdisk = (env.session.query(ClusterDisk)
                 .filter_by(id = server.disk_id)
                 .filter_by(status = ClusterDisk.STATUSES["OK"])
                 .first())
        if not cdisk:
            continue

        request, mrels, urels = get_server_params(env.session, server, cdisk)
        request["action"] = "start"
        request["endpoint"] = "ch-server-state"
        try:
            response = net.ssl_send(env.cserv.ip,
                                    env.cserv.port,
                                    cert_path,
                                    request)
            if response["result"] == "ok":
                server.status = KVMServer.STATUS["RUNNING"]
                server.is_prepared = False
                for rel in mrels:
                    rel.status = KVMMountRelation.STATUS["MOUNTED"]
                    rel.real_status = KVMMountRelation.REAL_STATUS["MOUNTED"]
                for rel in urels:
                    rel.status = KVMMountRelation.STATUS["UMOUNTED"]
                    rel.real_status = KVMMountRelation.REAL_STATUS["UMOUNTED"]
            else:
                my_error("start-server :: response = {}".format(response))
        except Exception:
            my_error("start-server :: request :: {}".format(request))
        env.session.commit()
    env.session.close()


def create_storage(env):
    cert_path = env.cserv.get_cert_path()
    storages = (env.session.query(KVMStorage)
                .filter_by(status = KVMStorage.STATUS["CREATING"])
                .filter_by(server_id = env.cserv_id)
                .filter_by(is_prepared = True)
                .all())
    for storage in storages:
        cdisk = (env.session.query(ClusterDisk)
                 .filter_by(id = storage.disk_id)
                 .filter_by(status = ClusterDisk.STATUSES["OK"])
                 .first())
        if not cdisk:
            continue
        request = {"endpoint": "create-storage",
                   "disk":     cdisk.mount_id,
                   "name":     storage.dname,
                   "size":     storage.size_gb}
        try:
            response = net.ssl_send(env.cserv.ip,
                                    env.cserv.port,
                                    cert_path,
                                    request)
            if "ok" == response["result"]:
                storage.status = KVMStorage.STATUS["CREATED"]
                storage.uuid = response["uuid"]
                storage.is_prepared = False
            else:
                my_error("create-storage :: response = {}".format(response))
        except Exception:
            my_error("create-storage :: request = {}".format(request))
        env.session.commit()
    env.session.close()


def remove_storage(env):
    cert_path = env.cserv.get_cert_path()
    storages = (env.session.query(KVMStorage)
                .filter_by(status = KVMStorage.STATUS["REMOVING"])
                .filter_by(server_id = env.cserv_id)
                .filter_by(is_prepared = True)
                .all())
    for storage in storages:
        # if storage not unmounted
        rels = (env.session.query(KVMMountRelation)
                .filter_by(storage_id = storage.id)
                .filter_by(status = KVMMountRelation.STATUS['UMOUNTED'])
                .filter_by(real_status = KVMMountRelation.STATUS['UMOUNTED'])
                .all())
        if rels:
            continue

        request = {"endpoint": "remove-storage",
                   "name":     storage.dname}
        try:
            response = net.ssl_send(env.cserv.ip,
                                    env.cserv.port,
                                    cert_path,
                                    request)
            if "ok" == response["result"]:
                storage.status = KVMStorage.STATUS["REMOVED"]
                # TODO: delete rels
            else:
                my_error("remove-storage :: response = {}".format(response))
        except Exception:
            my_error("remove-storage :: request = {}".format(request))
        env.session.commit()
    env.session.close()
