from daemons       import net
from daemons.utils import my_error

from app.root.models import ClusterServer
from app.my.models   import KVMServer, KVMStorage, KVMMountRelation, \
        Sharing


def do(session):
    cservs = get_cserver_dict(session)
    update(session)

    umount_disks(cservs, session)
    umount_in_servers(cservs, session)

    mount_disks(cservs, session)
    mount_in_servers(cservs, session)


def get_cserver_dict(session):
    cservers = session.query(ClusterServer).all()
    result = {}
    for cserver in cservers:
        result[cserver.id] = cserver
    return result


def sort_storages(rels):
    """
    storages = {"storage_id": [storage, [{"serv": serv,
                                          "rel":  rel}]]}
    """
    result = {}
    for serv, stor, rel in rels:
        if stor.id not in result:
            result[stor.id] = [stor, []]
        result[stor.id][1].append({"serv": serv,
                                   "rel":  rel})
    return result


def sort_rels_by_cserv(rels):
    """
    result = {"cserv_id" : [storage, [{"serv": serv,
                                       "rel":  rel}]]}
    """
    result = {}
    storages = sort_storages(rels)
    for vals in storages.values():
        stor, rels = vals[0], vals[1]
        if stor.server_id not in result:
            result[stor.server_id] = []
        result[stor.server_id].append(vals)
    return result


def sort_rels_by_serv(rels):
    """
    result = [[serv, [(stor, rel)]]]
    """
    result = {}
    for serv, stor, rel in rels:
        if serv.id not in result:
            result[serv.id] = [serv, []]
        result[serv.id][1].append((stor, rel))
    return result.values()


def get_devices(cservs, serv, data):
    devices = []
    rels_d = {}
    for stor, rel in data:
        if stor.server_id not in cservs:
            # TODO: error or message
            continue
        if stor.id not in rels_d:
            rels_d[str(rel.id)] = rel
        path = ("{}:/sg/mounts/storage{}"
                .format(cservs[stor.server_id].ip, stor.id))
        is_shared = False if serv.user_id == stor.user_id else True
        devices.append({"rel_id": rel.id,
                        "type":   "nfs",
                        "uuid":   stor.uuid,
                        "path":   path,
                        "point":  stor.alias,
                        "shared": is_shared})
    return devices, rels_d


def update(session):
    "Comparing and changing visible and real status."
    # Don't unmount if already unmounted.
    # status      => UMOUNTING -> UMOUNTED
    # real status => MOUNTING DISK -> UMOUNTED
    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["UMOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["MOUNTING DISK"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    for _, _, rel in rels:
        rel.status      = KVMMountRelation.STATUS["UMOUNTED"]
        rel.real_status = KVMMountRelation.REAL_STATUS["UMOUNTED"]
    session.commit()

    # Disk mounted to stopped server and then unmounted.
    # status      => UMOUNTING
    # real status => MOUNTING TO S -> UMOUNTING DISK
    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["UMOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["MOUNTING TO S"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    for _, _, rel in rels:
        rel.real_status = KVMMountRelation.REAL_STATUS["UMOUNTING DISK"]
    session.commit()

    # If user umounted disk but mounted again before mounting process starts.
    # status      => MOUNTING -> MOUNTED
    # real status => UMOUNTING F S -> MOUNTED
    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["MOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["UMOUNTING F S"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    for _, _, rel in rels:
        rel.status      = KVMMountRelation.STATUS["MOUNTED"]
        rel.real_status = KVMMountRelation.REAL_STATUS["MOUNTED"]
    session.commit()

    # Start umounting disk.
    # status      => UMOUNTING
    # real status => MOUNTED -> [UMOUNTING F S | UMOUNTING DISK]
    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["UMOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["MOUNTED"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    for serv, _, rel in rels:
        if serv.status == KVMServer.STATUS["RUNNING"]:
            rel.real_status = KVMMountRelation.REAL_STATUS["UMOUNTING F S"]
        else:
            rel.real_status = KVMMountRelation.REAL_STATUS["UMOUNTING DISK"]
    session.commit()

    # Disk unmounted.
    # status      => UMOUNTING -> UMOUNTED
    # real status => UMOUNTED
    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["UMOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["UMOUNTED"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    for _, _, rel in rels:
        rel.status = KVMMountRelation.STATUS["UMOUNTED"]
    session.commit()

    # Start mounting.
    # status      => MOUNTING
    # real status => UMOUNTED -> MOUNTING DISK
    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["MOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["UMOUNTED"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    for _, _, rel in rels:
        rel.real_status = KVMMountRelation.REAL_STATUS["MOUNTING DISK"]
    session.commit()

    # Yeah, disk mounted.
    # status      => MOUNTING -> MOUNTED
    # real status => MOUNTED
    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["MOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["MOUNTED"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    for _, _, rel in rels:
        rel.status = KVMMountRelation.STATUS["MOUNTED"]
    session.commit()

    # Remove sharings.
    shs = (session.query(Sharing)
            .filter_by(is_deleting = True)
            .all())
    for sh in shs:
        q = (session.query(KVMServer, KVMStorage, KVMMountRelation)
                .filter(KVMMountRelation.server_id == KVMServer.id)
                .filter(KVMMountRelation.storage_id == KVMStorage.id)
                .filter(KVMServer.user_id != KVMStorage.user_id)
                .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
                .filter(KVMMountRelation.storage_id == sh.storage_id))
        rels = []
        if sh.sharing_type == sh.TYPES["user"]:
            rels = q.filter(KVMServer.user_id == sh.sharing_with_id).all()
        elif sh.sharing_type == sh.TYPES["lab"]:
            rels = q.filter(KVMServer.lab_id == sh.sharing_with_id).all()
        is_mounted = False
        for _, _, rel in rels:
            if rel.real_status != rel.REAL_STATUS["UMOUNTED"]:
                is_mounted = True
                break
        if not is_mounted:
            for _, _, rel in rels:
                session.delete(rel)
            session.delete(sh)
    session.commit()


def umount_disks(cservs, session):

    def get_umounts(vals):
        umounts = []
        for stor, rels in vals:
            temp = []
            for el in rels:
                temp.append(el["serv"].ip)
            umounts.append({"name":    stor.dname,
                            "servers": temp})
        return umounts

    def mark_rels(stor_id, vals):
        for stor, rels in vals:
            if stor.id == stor_id:
                for rel in rels:
                    rel["rel"].real_status = rel["rel"].REAL_STATUS["UMOUNTED"]
                break

    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["UMOUNTING DISK"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    storages = sort_rels_by_cserv(rels)
    for cserv_id, vals in storages.items():
        umounts = get_umounts(vals)
        request = {"endpoint": "nfs",
                   "action":   "umount",
                   "umounts":  umounts}
        try:
            cserv = cservs[cserv_id]
            response = net.ssl_send(cserv.ip,
                                    cserv.port,
                                    cserv.get_cert_path(),
                                    request)
            if response["result"] == "ok":
                for dname, res in response["umounts"].items():
                    stor_id = KVMStorage.get_id(dname)
                    mark_rels(stor_id, vals)
            else:
                my_error("umount-nfs :: {} :: {}".format(request, response))
        except Exception:
            my_error("umount-nfs :: {}".format(request))
    session.commit()


def umount_in_servers(cservs, session):

    def handle_response(rels_d, response):
        # In case "Not route to host"
        # we think that the storages already umounted from VS
        if "error" in response:
            if response["error"] == "No route to host":
                for rel in rels_d.values():
                    rel.real_status = rel.REAL_STATUS["UMOUNTING DISK"]

        devs = response.get("devices", {}).get("umount", [])
        for dev in devs:
            if dev["result"] == "ok":
                rels_d[str(dev["rel_id"])].real_status = \
                    KVMMountRelation.REAL_STATUS["UMOUNTING DISK"]
            elif dev["result"] == "error":
                if dev["message"] == "already umounted":
                    rels_d[str(dev["rel_id"])].real_status = \
                        KVMMountRelation.REAL_STATUS["UMOUNTING DISK"]
                elif dev["message"] == "device is busy":
                    rels_d[str(dev["rel_id"])].real_status = \
                        KVMMountRelation.REAL_STATUS["UMOUNTING DISK"]
                else:
                    my_error("umount in server :: {}".format(response))
        if response.get("errors", []):
            my_error("umount in server :: {}".format(response))

    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["UMOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["UMOUNTING F S"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    vals = sort_rels_by_serv(rels)
    for serv, rs in vals:
        devices, rels_d = get_devices(cservs, serv, rs)
        request = {
            "cmds": ["mount"],
            "params": {"username": serv.username,
                       "devices": {"umount": devices}}
        }
        response = {}
        try:
            response = net.send(serv.ip, serv.PORT, request)
        except Exception as err:
            if "No route to host" in err.message:
                response = {"error": "No route to host"}
            else:
                raise
        handle_response(rels_d, response)
        session.commit()


def mount_disks(cservs, session):

    def get_mounts(vals):
        mounts = []
        for stor, rels in vals:
            temp = []
            for el in rels:
                temp.append({"ip":     el["serv"].ip,
                             "rel_id": el["rel"].id,
                             "rights": el["rel"].rights_val})
            mounts.append({"name":    stor.dname,
                           "servers": temp})
        return mounts

    def mark_rels(stor_id, vals):
        # TODO:
        for stor, rels in vals:
            if stor.id == stor_id:
                for el in rels:
                    el["rel"].real_status = \
                        KVMMountRelation.REAL_STATUS["MOUNTING TO S"]
                    break

    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["MOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["MOUNTING DISK"])
            .filter(KVMStorage.status == KVMStorage.STATUS["CREATED"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .filter(KVMServer.ip != KVMServer.NO_IP)
            .all())
    storages = sort_rels_by_cserv(rels)

    for cserv_id, vals in storages.items():
        mounts = get_mounts(vals)
        request = {"endpoint": "nfs",
                   "action":   "mount",
                   "mounts":   mounts}
        try:
            cserv = cservs[cserv_id]
            response = net.ssl_send(cserv.ip,
                                    cserv.port,
                                    cserv.get_cert_path(),
                                    request)
            if response["result"] == "ok":
                for sto, res in response["mounts"].items():
                    if res == "ok":
                        stor_id = KVMStorage.get_id(sto)
                        mark_rels(stor_id, vals)
                    else:
                        my_error("nfs.mount_disks :: {} :: {} :: {}"
                                    .format(request, sto, res))
            else:
                my_error("nfs.mount_disks :: {} :: {}"
                            .format(request, response))
        except Exception:
            my_error("nfs.mount_disks :: {}".format(request))
    session.commit()


def mount_in_servers(cservs, session):
    "Send request to virtual server with mount data."
    def handle_response(rels_d, response):
        for dev in response.get("devices", {}).get("mount", []):
            if dev["result"] == "ok":
                rels_d[str(dev["rel_id"])].real_status = \
                    KVMMountRelation.REAL_STATUS["MOUNTED"]
            elif dev["result"] == "error":
                if (dev["message"] == "already mounted" and
                    rels_d[str(dev["rel_id"])].real_status == \
                        KVMMountRelation.REAL_STATUS["MOUNTING TO S"]):
                    rels_d[str(dev["rel_id"])].real_status = \
                        KVMMountRelation.REAL_STATUS["MOUNTED"]
                else:
                    my_error("mount_in_servers :: {}".format(response))
        if response.get("errors", []):
            my_error("mount_in_servers :: {}".format(response["errrors"]))

    rels = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status ==
                    KVMMountRelation.STATUS["MOUNTING"])
            .filter(KVMMountRelation.real_status ==
                    KVMMountRelation.REAL_STATUS["MOUNTING TO S"])
            .filter(KVMServer.status == KVMServer.STATUS["RUNNING"])
            .filter(KVMStorage.status == KVMStorage.STATUS["CREATED"])
            .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
            .all())
    vals = sort_rels_by_serv(rels)
    for serv, rs in vals:
        devices, rels_d = get_devices(cservs, serv, rs)
        request = {
            "cmds":   ["mount"],
            "params": {"username": serv.username,
                       "devices":  {"mount": devices}}
        }
        try:
            response = net.send(serv.ip, serv.PORT, request)
            handle_response(rels_d, response)
            session.commit()
        except:
            # AttributeError: 'str' object has no attribute 'get'
            # No route to host
            # Connection refused
            my_error("mount in servers :: {}".format(request))
