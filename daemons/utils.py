import subprocess
import sys
import traceback

import web

from app.core import mail


def system_call(args, disable_error=False):
    args = [str(a) for a in args]
    process = subprocess.Popen(args,
                               stdout = subprocess.PIPE,
                               stderr = subprocess.PIPE,
                               stdin  = subprocess.PIPE)

    stdoutdata, stderrdata = process.communicate()
    if process.returncode != 0:
        if disable_error:
            return None
        raise OSError(stderrdata.strip())

    return stdoutdata.strip()


def my_error(message=None):
    if web.config['SG_SETTINGS']['DEBUG']:
        print('Traceback ({!r}):'.format(message))
        print('-' * 60)
        traceback.print_exc(file=sys.stdout)
        print('-' * 60)
    if web.config['SG_SETTINGS']['SEND_MAIL']:
        if message:
            mail.mail_tb(message)
        else:
            mail.mail_tb(message)
    # + add logger ?
