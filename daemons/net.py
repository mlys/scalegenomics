import json
import os
import socket
import ssl
import time

import app.core.mail as mail


REQUEST_START = "!START!"
REQUEST_END   = "!END!"


def strip_response(response):
    return response[len(REQUEST_START):
                        -len(REQUEST_END)]


def ssl_send(host, port, cert, params, timeout=None):
    "Send request with ssl wrapper."
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if timeout:
        sock.settimeout(timeout)
    ssl_sock = ssl.wrap_socket(sock,
                               ca_certs=cert,
                               cert_reqs=ssl.CERT_REQUIRED)
    response = {}
    try:
        ssl_sock.connect((host, port))
        ssl_sock.send(REQUEST_START + json.dumps(params) + REQUEST_END)
        response = ssl_sock.read()
        if response.startswith(REQUEST_START):
            while not response.endswith(REQUEST_END):
                response += ssl_sock.read()
            response = json.loads(strip_response(response))
    except Exception:
        mail.mail_tb("ssl_send :: {}".format(params))
        return {"result": "error"}
    finally:
        ssl_sock.close()

    return response


def send(host, port, params):
    "Send request."
    response = ""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((host, port))
        sock.send(REQUEST_START + json.dumps(params) + REQUEST_END)
        response = sock.recv(1024)
        if response.startswith(REQUEST_START):
            while not response.endswith(REQUEST_END):
                response += sock.recv(1024)
            response = json.loads(strip_response(response))
    finally:
        sock.close()

    return response


def send2(host, port, params):
    "Send request."
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((host, port))
        sock.send(json.dumps(params))
    finally:
        sock.close()


def bind(host, port):
    '''Create socket, bind and return it.'''
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    while True:
        try:
            sock.bind((host, port))
            break
        except socket.error:
            time.sleep(1)
    sock.listen(11)
    return sock


def request_handler(sock, handler, exit_file_path):
    'Eternal loop for getting request and handling it.'
    while not os.path.exists(exit_file_path):
        request = ''
        try:
            conn, addr = sock.accept()
            request = conn.recv(1024)

        except Exception as err:
            mail.mail_tb('.. it is posible ..')

        try:
            request = json.loads(request)
            res = handler(addr, request)

        except ValueError as err:
            mail.mail_tb()
            res = {'result': 'value error'}

        except Exception as err:
            mail.mail_tb()
            res = {'result':  'error',
                   'message': str(err)}

        conn.send(json.dumps(res))
        conn.close()
