#!/usr/bin/env python2.7
import os
import sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
if path not in sys.path:
    sys.path.append(path)

from daemon    import Daemon
from manager   import Manager
from listeners import Listener


USAGE = """Examples:
{path} daemon   start|stop [debug]
{path} listener start|stop [debug]""".format(path=sys.argv[0])


def parse_args():
    len_args = len(sys.argv)
    debug = True if len_args == 4 and sys.argv[3] == "debug" else False
    if len_args >= 3 and sys.argv[1] == "daemon":
        manager = Manager()
        daemon = Daemon("/var/run/sgdaemon.vsm.pid", manager)
        daemon.debug = debug
        if "start" == sys.argv[2]:
            daemon.start()
        elif "stop" == sys.argv[2]:
            daemon.stop()
        elif "restart" == sys.argv[2]:
            daemon.stop()
            daemon.start()
        else:
            print(USAGE)

    elif len_args >= 3 and sys.argv[1] == "listener":
        listener = Listener()
        daemon = Daemon("/var/run/sgdaemon.listener.pid", listener)
        daemon.debug = debug
        if "start" == sys.argv[2]:
            daemon.start()
        elif "stop" == sys.argv[2]:
            daemon.stop()
        elif "restart" == sys.argv[2]:
            daemon.stop()
            daemon.start()
        else:
            print(USAGE)

    else:
        print(USAGE)


if __name__ == "__main__":
    parse_args()
