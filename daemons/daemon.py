import atexit, os, sys, signal, subprocess


def system_call(args, disable_error=False):
    args = [ str(a) for a in args ]
    process = subprocess.Popen(args,
                               stdout = subprocess.PIPE,
                               stderr = subprocess.PIPE,
                               stdin  = subprocess.PIPE)

    stdoutdata, stderrdata = process.communicate()
    if process.returncode != 0:
        if disable_error: return None
        raise OSError(stderrdata.strip())

    return stdoutdata.strip()


class Daemon(object):

    def __init__(self, pidfile, action, **kw):
        self.pidfile = pidfile
        self.action  = action
        self.debug   = False
        self.stdout  = kw.get("stdout", "/dev/null")
        self.stderr  = kw.get("stderr", "/dev/null")
        self.stdin   = kw.get("stdin",  "/dev/null")

    def daemonize(self):
        "Daemonize process with double-fork magic."
        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
        except OSError as e:
            print("Daemon fork #1 failed: {} ({!r})".format(e.errno, e.strerror))
            sys.exit(1)

        os.chdir("/")
        os.setsid()
        os.umask(0)

        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
        except OSError as e:
            print("Daemon fork #2 failed: {} ({!r})".format(e.errno, e.strerror))
            sys.exit(1)

        # register exit handler
        atexit.register(self.delpid)
        file(self.pidfile, "w").write("{}\n".format(os.getpid()))

        # rewrite i/o
        sys.stdout.flush()
        sys.stderr.flush()
        si = file(self.stdin,  "r")
        so = file(self.stdout, "a+")
        se = file(self.stderr, "a+", 0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())

    def delpid(self):
        os.remove(self.pidfile)

    def signal_handler(self, signum, frame):
        if signal.SIGTERM == signum or signal.SIGINT == signum:
            self.action.stop()
            print("Process {!r} stopped".format(self.pidfile))

    def start(self):
        if os.path.exists(self.pidfile):
            print("Pidfile {!r} already exist. Already running?"
                  .format(self.pidfile))
            sys.exit(1)

        # action validation
        if not (hasattr(self.action, "start") and hasattr(self.action, "stop")):
            print("Missing action method (start or stop)")
            sys.exit(1)

        # register signal handler
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)

        if not self.debug:
            self.daemonize()
        self.action.start()

    def stop(self):
        pid = self.get_pid()
        if pid:
            os.kill(pid, signal.SIGTERM)

    def get_pid(self):
        pid = None
        if os.path.exists(self.pidfile):
            with file(self.pidfile) as f:
                pid = int(f.read().strip())
        if not pid:
            print("Pid file {!r} doesn't exists".format(self.pidfile))
            sys.exit(1)
        return pid

