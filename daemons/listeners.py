import json
import socket

import web

import base
base.init_all_settings()

from settings.db     import get_scoped_session
from daemons.utils   import my_error
from daemons.net     import strip_response, REQUEST_START, REQUEST_END
from app.root.models import ClusterServer
from app.my.models   import KVMServer, KVMStorage, KVMMountRelation


class Listener(object):

    def __init__(self):
        self.host = web.config["SG_SETTINGS"]["DAEMON_HOST"]
        self.port = web.config["SG_SETTINGS"]["DAEMON_PORT"]
        self.in_process = True
        self.listen_num = 25

    def get_request(self, conn):
        response = conn.recv(1024)
        if response.startswith(REQUEST_START):
            while not response.endswith(REQUEST_END):
                response += conn.recv(1024)
            response = strip_response(response)
        return json.loads(response)

    def start(self):
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock.bind((self.host, self.port))
            self.sock.listen(self.listen_num)
        except socket.error as err:
            my_error("Listener (bind)")

        while self.in_process:
            try:
                conn, addr = self.sock.accept()
                request = self.get_request(conn)
                response = self.handler(conn, addr, request)
                conn.send(REQUEST_START + json.dumps(response) + REQUEST_END)
                conn.close()
            except socket.error as err:
                if err[0] != 4:
                    my_error("Listener (socket)")
            except Exception:
                my_error("Listener (exception)")
        self.sock.close()

    def stop(self):
        self.in_process = False
        self.touch_port()

    def touch_port(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.connect((self.host, self.port))
            sock.send("{}")
        except Exception:
            my_error("Listener (touch port)")
        finally:
            sock.close()

    def handler(self, conn, addr, request):
        session = get_scoped_session()
        server = (session.query(KVMServer)
                  .filter_by(ip = addr[0])
                  .first())
        if not server:
            my_error("Request from uknown server {!r}".format(addr[0]))
            return {"result":  "error",
                    "message": "unknown server"}

        if "result" in request and request["result"] == "error":
            my_error("Image error :: {} - {} :: {} :: {}"
                    .format(server.name, server.ip,
                            request["function"], request["message"]))
            return {"result": "ok"}

        if request.get("request", "") == "mounts":
            # local mounts
            rels = (session.query(KVMStorage, KVMMountRelation)
                    .filter(KVMStorage.id == KVMMountRelation.storage_id)
                    .filter(KVMStorage.type == KVMStorage.TYPES["LOCAL"])
                    .filter(KVMMountRelation.server_id == server.id)
                    .filter(KVMMountRelation.status.in_([
                        KVMMountRelation.STATUS["MOUNTING"],
                        KVMMountRelation.STATUS["MOUNTED"]]))
                    .all())
            mounts = []
            for storage, rel in rels:
                mounts.append({"uuid":  storage.uuid,
                               "point": storage.alias,
                               "type":  "local"})
            # nfs mounts
            rels = (session.query(KVMStorage, KVMMountRelation)
                    .filter(KVMStorage.type == KVMStorage.TYPES["NFS"])
                    .filter(KVMStorage.id == KVMMountRelation.storage_id)
                    .filter(KVMMountRelation.server_id == server.id)
                    .filter(KVMMountRelation.status ==
                            KVMMountRelation.STATUS["MOUNTED"])
                    .all())
            nfs_mounts = []
            for s, rel in rels:
                cserv = (session.query(ClusterServer)
                         .filter_by(id = s.server_id)
                         .first())
                if not cserv:
                    continue
                nfs_mounts.append({
                        "path":  "{}:/sg/mounts/{}".format(cserv.ip, s.dname),
                        "point": s.alias,
                        "type":  "nfs"})
            return {"result":    "ok",
                    "username":  server.username,
                    "mounts":    mounts + nfs_mounts}
        return {"result":  "ok"}
