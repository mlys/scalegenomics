# Example: ipython -i filename.py
import web

import base
base.init_all_settings()


from settings.db import get_scoped_session

from app.root.models import *
from app.my.models import *
from app.sso.models import *


if __name__ == "__main__":
    session = get_scoped_session()
