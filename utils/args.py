import argparse


def basemain(fns):
    parser = argparse.ArgumentParser()
    parser.add_argument("cmd", help="|".join(fns.keys()))
    args = parser.parse_args()

    handler = fns.get(args.cmd, None)
    if handler:
        handler()
