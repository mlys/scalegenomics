import os, sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if path not in sys.path: sys.path.append(path)

import web

import base
base.init_all_settings()

from settings.db    import get_scoped_session
from app.sso.models import UserToLab
from app.my.models  import Lab, LabPlan, LabProject, MyEvent, LabPrice, \
    LabInvoice, KVMServer, KVMStorage, UserToProject, KVMServerUsageLog, \
    KVMStorageUsageLog, KVMMountRelation


def remove_lab(lab_id, remove=False):
    session = get_scoped_session()

    lab = (session.query(Lab)
           .filter_by(id = lab_id)
           .first())

    u2lab = (session.query(UserToLab)
             .filter_by(lab_id = lab_id)
             .all())

    lab_plans = (session.query(LabPlan)
                 .filter_by(lab_id = lab_id)
                 .all())

    r_lab_projects = []
    lab_projects = (session.query(LabProject)
                    .filter_by(lab_id = lab_id)
                    .all())
    for pro in lab_projects:
        u2proj = (session.query(UserToProject)
                  .filter_by(project_id = pro.id)
                  .all())
        r_lab_projects.append((pro, u2proj))

    my_events = (session.query(MyEvent)
                 .filter_by(lab_id = lab_id)
                 .all())

    lab_prices = (session.query(LabPrice)
                  .filter_by(lab_id = lab_id)
                  .all())

    lab_invoices = (session.query(LabInvoice)
                    .filter_by(lab_id = lab_id)
                    .all())

    r_servers = []
    servers = (session.query(KVMServer)
               .filter_by(lab_id = lab_id)
               .all())
    for server in servers:
        suls = (session.query(KVMServerUsageLog)
                .filter_by(server_id = server.id)
                .all())
        mrels = (session.query(KVMMountRelation)
                 .filter_by(server_id = server.id)
                 .all())
        r_servers.append((server, suls, mrels))

    r_storages = []
    storages = (session.query(KVMStorage)
                .filter_by(lab_id = lab_id)
                .all())
    for storage in storages:
        suls = (session.query(KVMStorageUsageLog)
                .filter_by(storage_id = storage.id)
                .all())
        r_storages.append((storage, suls))

    print('This will remove only records')
    print(lab)
    print(u2lab)
    print(lab_plans)
    print(r_lab_projects)
    print(my_events)
    print(lab_prices)
    print(lab_invoices)
    print(r_servers)
    print(r_storages)

    if remove:
        for server, suls, mrels in r_servers:
            for mrel in mrels:
                session.delete(mrel)
            for sul in suls:
                session.delete(sul)
            session.delete(server)

        for storage, suls in r_storages:
            for sul in suls:
                session.delete(sul)
            session.delete(storage)

        for lab_invoice in lab_invoices:
            session.delete(lab_invoice)

        for lab_price in lab_prices:
            session.delete(lab_price)

        for event in my_events:
            session.delete(event)

        for proj, u2proj in r_lab_projects:
            for u2p in u2proj:
                session.delete(u2p)
            session.delete(proj)

        for plan in lab_plans:
            session.delete(plan)

        for u2l in u2lab:
            session.delete(u2l)

        session.delete(lab)
        session.commit()
    session.close()


if __name__ == '__main__':
    #remove_lab(sys.argv[1], True)
    remove_lab(sys.argv[1], False)
