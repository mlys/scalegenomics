import os
import sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if path not in sys.path:
    sys.path.append(path)

import base
base.init_all_settings()


from utils.args import basemain
from settings.db import get_scoped_session

from app.root.models import ClusterDisk
from app.my.models import KVMServer, KVMStorage


def get_data(session):
    disks = (session.query(ClusterDisk)
                .filter_by(status = ClusterDisk.STATUSES["OK"])
                .all())
    data = {}
    for disk in disks:
        servers = (session.query(KVMServer)
                .filter(KVMServer.disk_id == disk.id)
                .filter(KVMServer.status < KVMServer.STATUS["REMOVING"])
                .all())
        serv_tot = sum([server.size for server in servers])

        storages = (session.query(KVMStorage)
                .filter(KVMStorage.disk_id == disk.id)
                .filter(KVMStorage.status < KVMStorage.STATUS["REMOVING"])
                .all())
        stor_tot = sum([s.size_gb for s in storages])

        total = serv_tot + stor_tot
        data[disk] = {
            "servers":  serv_tot,
            "storages": stor_tot,
            "total":    total
        }
    return data


def check():
    session = get_scoped_session()
    data = get_data(session)
    for disk, val in data.items():
        used = disk.max_size - disk.free_size
        if used != val["total"]:
            print(disk.uuid)
            print("servers    = {}".format(val["servers"]))
            print("storages   = {}".format(val["storages"]))
            print("total used = {}".format(val["total"]))
            print("db used    = {}".format(used - val["total"]))


def recalc():
    session = get_scoped_session()
    data = get_data(session)
    for disk, val in data.items():
        used = disk.max_size - disk.free_size
        if used != val["total"]:
            free_size = disk.max_size - val["total"]
            print("{} {} :: {}".format(disk.server_id, disk.id, disk.uuid))
            print("{} -> {}  ({})".format(disk.free_size,
                                          free_size,
                                          used - val["total"]))
            disk.free_size = disk.max_size - val["total"]
    session.commit()


if __name__ == "__main__":
    basemain({
        "check":  check,
        "recalc": recalc,
    })
