import os, sys
from datetime import datetime

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if path not in sys.path: sys.path.append(path)

import web

import base
import app.my.payments     as payments
from settings.db       import get_scoped_session
from app.my.models     import Lab, LabInvoice, LabPrice


base.init_all_settings()


def validate():
    'Validate if all invoices created and check params.'
    session = get_scoped_session()

    now = datetime.utcnow()
    year_start = datetime(now.year, 1, 1)
    mdiff = now.month - year_start.month
    month_range = range(1, mdiff+1)

    labs = session.query(Lab).all()
    for lab in labs:
        inv_s = (session.query(LabInvoice)
                 .filter_by(lab_id = lab.id)
                 .all())
        inv_months = [ (inv.date.month, inv) for inv in inv_s ] #
        inv_months = dict(inv_months) #

        for month in month_range:
            if month in inv_months:
                prices = inv_months[month].get_prices() #
            else:
                prices = web.config['SG_SETTINGS']['PRICES'][1]

            invoice = payments.calc_invoice(lab, prices, now, month, session)

            print(invoice)
        #print(lab)
        pass


def create_all_invoices():
    # + rewrite
    now = datetime.utcnow()
    session = get_scoped_session()

    months = range(1, now.month)

    invoices = []
    labs = session.query(Lab).all()
    for lab in labs:
        invs = (session.query(LabInvoice)
                .filter_by(lab_id = lab.id)
                .all())
        if len(invs) == now.month-1: continue # wrong logic

        lab_price = (session.query(LabPrice)
                     .filter_by(lab_id = lab.id)
                     .first())
        if lab_price:
            prices = lab_price.to_d()
        else:
            prices = web.config['SG_SETTINGS']['PRICES'][1]

        invs_ids = [ inv.date.month for inv in invs ]
        for month in months:
            if month not in invs_ids:
                inv = payments.create_invoice(lab,
                                              prices,
                                              now,
                                              month,
                                              session)
                if inv: invoices.append(inv)
    print(invoices)


if __name__ == '__main__':
    validate()
    #create_all_invoices()
