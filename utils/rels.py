import os
import sys
from pprint import pprint

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if path not in sys.path:
    sys.path.append(path)

import base
base.init_all_settings()

from utils.args import basemain
from settings.db import get_scoped_session
from app.my.models import KVMServer, KVMStorage, KVMMountRelation, \
    Sharing
from app.my.groups import group_ssr_by_server, group_ssr_by_storage


def update_storage_type():
    session = get_scoped_session()
    ssr = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMMountRelation.status.in_([
                KVMMountRelation.STATUS["MOUNTING"],
                KVMMountRelation.STATUS["MOUNTED"]]))
            .all())
    rels = group_ssr_by_storage(ssr)
    for storage, sr in rels:
        is_local = True
        for server, rel in sr:
            if storage.server_id != server.server_id:
                is_local = False
                break
        if not is_local and storage.type == storage.TYPES["LOCAL"]:
            storage.type = storage.TYPES["NFS"]
            print("Storage - {} - {} LOCAL -> NFS"
                    .format(storage.id, storage.name))
            session.commit()


def update_status_if_server_stopped():
    session = get_scoped_session()
    ssr = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMServer.status == KVMServer.STATUS["STOPPED"])
            .filter(KVMMountRelation.status.in_([
                KVMMountRelation.STATUS["MOUNTED"]]))
            .all())
    rels = group_ssr_by_server(ssr)
    pprint(rels)
    for server, sr in rels:
        for storage, rel in sr:
            rel.status = rel.STATUS["MOUNTING"]
            rel.real_status = rel.REAL_STATUS["MOUNTING DISK"]
        session.commit()


def delete_if_server_removed():
    session = get_scoped_session()
    ssr = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .filter(KVMServer.status == KVMServer.STATUS["REMOVED"])
            .all())
    rels = group_ssr_by_server(ssr)
    for server, sr in rels:
        print(server)
        for rel in sr:
            print(".\t{}".format(rel))

    if rels:
        print("\nRemove relations: (Yes)")
        yes = raw_input()
        if yes == "Yes":
            for server, sr in rels:
                for storage, rel in sr:
                    session.delete(rel)
            session.commit()
        else:
            print("Canceled")
    else:
        print("No relations with removed servers")


def delete_if_storage_removed():
    session = get_scoped_session()
    storages = (session.query(KVMStorage)
                .filter_by(status = KVMStorage.STATUS["REMOVED"])
                .all())
    data = []
    for storage in storages:
        rels = (session.query(KVMMountRelation)
                .filter_by(storage_id = storage.id)
                .all())
        if rels:
            data.append((storage, rels))
            print(storage)
            for rel in rels:
                print(".\t{}".format(rel))

    if data:
        print("\nRemove relations: (Yes)")
        yes = raw_input()
        if yes == "Yes":
            for _, rels in data:
                for rel in rels:
                    session.delete(rel)
            session.commit()
        else:
            print("Canceled")
    else:
        print("Nothing wrong")


def delete_lost_rels():
    session = get_scoped_session()
    storages = (session.query(KVMStorage)
                .filter_by(status = KVMStorage.STATUS["LOST"])
                .all())
    data = []
    for storage in storages:
        rels = (session.query(KVMMountRelation)
                .filter_by(storage_id = storage.id)
                .all())
        if rels:
            data.append([storage, rels])
            print(storage)
            for rel in rels:
                print(".\t{}".format(rel))

    if data:
        print("\nRemove relations: (Yes)")
        yes = raw_input()
        if yes == "Yes":
            for _, rels in data:
                for rel in rels:
                    session.delete(rel)
            session.commit()
        else:
            print("Canceled")
    else:
        print("Nothing wrong")


def update_lab():
    session = get_scoped_session()
    ssr = (session.query(KVMServer, KVMStorage, KVMMountRelation)
            .filter(KVMMountRelation.server_id == KVMServer.id)
            .filter(KVMMountRelation.storage_id == KVMStorage.id)
            .all())

    for server, storage, rel in ssr:
        if server.user_id != storage.user_id:
            shs = (session.query(Sharing)
                    .filter_by(storage_id = storage.id)
                    .all())
            if not shs:
                print(server, storage, rel)

    for server, storage, rel in ssr:
        if server.lab_id != storage.lab_id:
            shs = (session.query(Sharing)
                    .filter_by(storage_id = storage.id)
                    .all())
            if not shs:
                print(server, storage, rel)

    for server, storage, rel in ssr:
        if server.project_id != storage.project_id:
            shs = (session.query(Sharing)
                    .filter_by(storage_id = storage.id)
                    .all())
            if not shs:
                print(server, storage, rel)


if __name__ == "__main__":
    basemain({
        "type":            update_storage_type,
        "update":          update_status_if_server_stopped,

        "server_removed":  delete_if_server_removed,
        "storage_removed": delete_if_storage_removed,
        "lost_rels":       delete_lost_rels,

        "update_lab":      update_lab,
    })
