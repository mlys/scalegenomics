import os
import sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if path not in sys.path:
    sys.path.append(path)

import base
base.init_all_settings()


from utils.args import basemain
from settings.db import get_scoped_session

from app.my.models import KVMServer, KVMServerUsageLog, \
    KVMStorage, KVMStorageUsageLog


def missing():
    session = get_scoped_session()
    servers = (session.query(KVMServer).all())
    for server in servers:
        suls = (session.query(KVMServerUsageLog)
                .filter_by(server_id = server.id)
                .all())
        if not suls:
            print(server)

    storages = (session.query(KVMStorage).all())
    for storage in storages:
        suls = (session.query(KVMStorageUsageLog)
                .filter_by(storage_id = storage.id)
                .all())
        if not suls:
            print(storage)


if __name__ == "__main__":
    basemain({
        "missing": missing
    })
