import os, sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if path not in sys.path: sys.path.append(path)

import web

import base
base.init_all_settings()

from settings.db     import get_scoped_session
from app.root.models import ClusterServer, ClusterDisk, Raid
from app.my.models   import KVMStorage
from daemons.net     import ssl_send


def get_cert_path():
    return os.path.abspath(os.path.join(path,
                                        'daemons',
                                        'certs',
                                        'scalegenomics1.pem'))


def get_storages():

    cert_path = get_cert_path()
    session = get_scoped_session()
    cservers = session.query(ClusterServer).all()

    for cserver in cservers:
        response = ssl_send(cserver.ip,
                          web.config['SG_SETTINGS']['DAEMON_PORTS']['SYSTEM_INFO'],
                          cert_path, {'storages' : ''})
        if response['result'] == 'error':
            print(response['message'])
            continue

        for mount_id, storages in response['storages'].items():
            cdisk = (session.query(ClusterDisk)
                     .filter_by(mount_id = mount_id)
                     .first())
            if not cdisk:
                print('mount id {!r} not exists'.format(mount_id))
                continue

            xstorages = (session.query(KVMStorage)
                         .filter_by(disk_id = cdisk.id)
                         .filter_by(status = KVMStorage.STATUS['CREATED'])
                         .all())
            if len(xstorages) != len(storages):
                print('error 1')
                return

            for xstorage in xstorages:
                is_exists = False

                for storage in storages:
                    if xstorage.dname+'.raw' in storage['path']:
                        # uuid
                        if xstorage.uuid != storage['uuid']:
                            print('{} :: {} != {}'.format(xstorage.id,
                                                          xstorage.uuid,
                                                          storage['uuid']))
                        # size
                        if xstorage.size_gb != storage['size']:
                            print('{} :: {} != {}'.format(xstorage.id,
                                                          xstorage.size_gb,
                                                          storage['size']))

                        is_exists = True
                        break

                if not is_exists:
                    print('error 4')


def get_devices():

    session = get_scoped_session()
    cert_path = get_cert_path()

    cservers = (session.query(ClusterServer).all())
    for cserver in cservers:
        cdisks = (session.query(ClusterDisk)
                  .filter_by(server_id = cserver.id)
                  .all())
        request = {'devices' : [ cdisk.dev_path for cdisk in cdisks ] }
        response = ssl_send(cserver.ip,
                          web.config['SG_SETTINGS']['DAEMON_PORTS']['SYSTEM_INFO'],
                          cert_path, request)

        if response['result'] == 'error':
            print(response['message'])
        elif response['result'] == 'ok':
            return response['devices']


def update_devices():

    devices = get_devices()
    if not devices: return

    session = get_scoped_session()

    for device in devices:
        if device['result'] == 'error':
            print(device['message'])
            continue

        elif device['result'] == 'ok':
            cdisk = (session.query(ClusterDisk)
                     .filter_by(dev_path = device['path'])
                     .first())
            if not cdisk:
                print("{!r} can't find".format(device['path']))

            if cdisk.uuid != device['uuid']:
                print('{} :: {} -> {}'.format(cdisk.dev_path,
                                              cdisk.uuid,
                                              device['uuid']))
                cdisk.uuid = device['uuid']

            if cdisk.max_size != device['size'] and device['size'] != 0:
                print('{} :: {} -> {}'.format(cdisk.dev_path,
                                              cdisk.max_size,
                                              device['size']))
                cdisk.max_size = device['size']

            if cdisk.mount_id != device['mount_id']:
                print('{} :: {} -> {}'.format(cdisk.dev_path,
                                              cdisk.mount_id,
                                              device['mount_id']))
                cdisk.mount_id = device['mount_id']

        session.commit()


def check_devices():

    devices = get_devices()
    if not devices: return

    session = get_scoped_session()

    for device in devices:
        if device['result'] == 'error':
            print(device['message'])
            continue

        elif device['result'] == 'ok':
            cdisk = (session.query(ClusterDisk)
                     .filter_by(dev_path = device['path'])
                     .first())
            if not cdisk:
                print("{!r} can't find".format(device['path']))

            if cdisk.uuid != device['uuid']:
                print('{} :: {} != {} wrong uuid'.format(cdisk.dev_path,
                                                         cdisk.uuid,
                                                         device['uuid']))

            if cdisk.max_size != device['size'] and device['size'] != 0:
                print('{} :: {} != {} wrong size'.format(cdisk.dev_path,
                                                         cdisk.max_size,
                                                         device['size']))

            if cdisk.mount_id != device['mount_id']:
                print('{} :: {} != {} wrong mount id'.format(cdisk.dev_path,
                                                             cdisk.mount_id,
                                                             device['mount_id']))


def iter_raids(session):

    cert_path = get_cert_path()
    cservers = (session.query(ClusterServer).all())

    for cserver in cservers:
        request = {'raids':''}
        port = web.config['SG_SETTINGS']['DAEMON_PORTS']['SYSTEM_INFO']
        response = ssl_send(cserver.ip, port, cert_path, request)

        if response['result'] == 'ok' and 'devices' in response:
            yield cserver, response['devices']
        else:
            print(response)

def _get_raid(raids, uuid):
    for raid in raids:
        if raid['md_uuid'] == uuid:
            return raid
    return None

def _get_raid_device(devices, disk):
    for dev in devices:
        if disk.uuid == dev['uuid'] and disk.dev_path == dev['path']:
            return dev
