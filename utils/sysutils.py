import subprocess


def call(args, env={}):
    args = [ str(a) for a in args ]
    process = subprocess.Popen(args, stdout = subprocess.PIPE,
                                     stderr = subprocess.PIPE,
                                     stdin  = subprocess.PIPE,
                               env=env)

    stdoutdata, stderrdata = process.communicate()
    if process.returncode != 0:
        raise OSError(stderrdata.strip())

    return stdoutdata.strip()
