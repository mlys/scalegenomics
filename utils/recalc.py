import os, sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if path not in sys.path:
    sys.path.append(path)


import base
base.init_all_settings()

from settings.db     import get_scoped_session
from app.my.models   import Lab, KVMServer, KVMStorage, Plan, LabPlan, Ip
from app.root.models import ClusterServer, ClusterDisk
from app.sso.models  import UserToLab


USAGE = '''Usage: {fname} [commands]...

{fname} check  lablimits
{fname} recalc lablimits

{fname} check  wtoapprove
{fname} recalc wtoapprove

{fname} check labplans

{fname} check plans'''.format(fname=sys.argv[0])


def _lab_limits(session):
    '''Return lab info.'''

    labs = session.query(Lab).all()
    for lab in labs:

        servers = (session.query(KVMServer)
                   .filter(KVMServer.lab_id == lab.id)
                   .filter(KVMServer.status < KVMServer.STATUS['REMOVING'])
                   .all())
        r_servers = [ server for server in servers
                      if server.status in [KVMServer.STATUS['STARTING'],
                                           KVMServer.STATUS['RUNNING'],
                                           KVMServer.STATUS['RESTARTING']]]

        storages = (session.query(KVMStorage)
                    .filter(KVMStorage.lab_id == lab.id)
                    .filter(KVMStorage.status < KVMStorage.STATUS['REMOVING'])
                    .all())
        servers_cpu_total = sum([ ser.cpu for ser in r_servers ])
        servers_ram_total = sum([ ser.memory for ser in r_servers ])
        storages_total = sum([ sto.size for sto in storages ] +
                             [ ser.size_mb for ser in servers ])
        yield lab, servers_cpu_total, servers_ram_total, storages_total


def check_lab_limits():
    session = get_scoped_session()

    for lab, serv_cpu_tot, serv_ram_tot, stor_tot in _lab_limits(session):
        print('Lab : {}'.format(lab.name))
        if lab.cpu_used != serv_cpu_tot:
            print('cpu :: {} != {}'.format(lab.cpu_used, serv_cpu_tot))

        if lab.ram_used != serv_ram_tot:
            print('ram :: {} != {}'.format(lab.ram_used, serv_ram_tot))

        if lab.storages_used != stor_tot:
            print('sto :: {} != {}'.format(lab.storages_used, stor_tot))
        print('- ' * 13)


def recalc_lab_limits():
    '''Re-calculate lab limits.'''
    session = get_scoped_session()

    for lab, serv_cpu_tot, serv_ram_tot, stor_tot in _lab_limits(session):
        print('Lab : {}'.format(lab.name))

        if lab.cpu_used != serv_cpu_tot:
            print('cpu :: {} -> {}'.format(lab.cpu_used, serv_cpu_tot))
            lab.cpu_used = serv_cpu_tot

        if lab.ram_used != serv_ram_tot:
            print('ram :: {} -> {}'.format(lab.ram_used, serv_ram_tot))
            lab.ram_used = serv_ram_tot

        if lab.storages_used != stor_tot:
            print('sto :: {} -> {}'.format(lab.storages_used, stor_tot))
            lab.storages_used = stor_tot

        session.commit()
        print('- ' * 13)



def iter_ramusage_pairs(session):
    cservs = session.query(ClusterServer).all()
    cserv_disks = {}
    for cserv in cservs:
        disks = (session.query(ClusterDisk)
                 .filter_by(server_id = cserv.id)
                 .all())
        cserv_disks[cserv] = [ disk.id for disk in disks ]
    for cserv, disks_ids in cserv_disks.items():
        servers = (session.query(KVMServer)
                   .filter(KVMServer.disk_id.in_(disks_ids))
                   .filter(KVMServer.status == KVMServer.STATUS["RUNNING"])
                   .all())
        tot_ram = sum([ s.memory for s in servers])
        yield cserv, tot_ram

def check_ramusage():
    session = get_scoped_session()
    for cserv, tot_ram in iter_ramusage_pairs(session):
        if cserv.ram_used != tot_ram:
            print("{} :: {} != {}".format(cserv, cserv.ram_used, tot_ram))

def recalc_ramusage():
    session = get_scoped_session()
    for cserv, tot_ram in iter_ramusage_pairs(session):
        if cserv.ram_used != tot_ram:
            print("{} :: {} -> {}".format(cserv, cserv.ram_used, tot_ram))
            cserv.ram_used = tot_ram
    session.commit()

def _lab_rels(session):
    labs = session.query(Lab).all()
    for lab in labs:
        rels = (session.query(UserToLab)
                .filter_by(lab_id = lab.id)
                .filter_by(perms = UserToLab.PERMS['waiting to be approved'])
                .all())
        yield lab, rels

def check_wtoapprove():
    session = get_scoped_session()
    for lab, rels in _lab_rels(session):
        if lab.wait_to_approve != len(rels):
            print(lab.name)
            print('{} != {}'.format(lab.wait_to_approve, len(rels)))
            print('-'*13)

def recalc_wtoapprove():
    session = get_scoped_session()
    for lab, rels in _lab_rels(session):
        if lab.wait_to_approve != len(rels):
            print(lab.name)
            print('{} != {}'.format(lab.wait_to_approve, len(rels)))
            print('-'*13)
            lab.wait_to_approve = len(rels)
    session.commit()

def iter_lab_plans(session):
    labs = session.query(Lab).all()
    for lab in labs:
        lab_plans = (session.query(LabPlan)
                     .filter_by(lab_id = lab.id)
                     .order_by(LabPlan.created)
                     .all())
        yield lab, lab_plans


def check_labplans():
    session = get_scoped_session()
    for lab, lab_plans in iter_lab_plans(session):
        print(lab.name)
        for i, plan in enumerate(lab_plans):
            if i == 0: continue
            if not lab_plans[i-1].deleted: continue
            if plan.created != lab_plans[i-1].deleted:
                print(plan.id, plan.created,
                      lab_plans[i-1].id, lab_plans[i-1].deleted)
        print('-'*13)


def check_plans():
    session = get_scoped_session()
    plans = session.query(Plan).all()
    plans_d = {}
    for plan in plans:
        plans_d[plan.id] = plan
    lab_plans = session.query(LabPlan).all()
    for lab_plan in lab_plans:
        if lab_plan.name == LabPlan.PLANS['Personal']: continue
        print(lab_plan.lab_id, lab_plan.plan_id)
        if lab_plan.name != plans_d[lab_plan.plan_id].name:
            print('name')
        if lab_plan.price != plans_d[lab_plan.plan_id].price:
            print('price')
        if lab_plan.cores != plans_d[lab_plan.plan_id].cores:
            print('cores')
        if lab_plan.ram != plans_d[lab_plan.plan_id].ram:
            print('ram')
        if lab_plan.storage != plans_d[lab_plan.plan_id].storage:
            print('storage')
        print('-'*21)


def check_trial():
    session = get_scoped_session()
    labs = (session.query(Lab, LabPlan)
            .filter(LabPlan.lab_id == Lab.id)
            .filter(LabPlan.name == LabPlan.PLANS['Trial'])
            .filter(LabPlan.deleted == None)
            .all())
    for lab, lab_plan in labs:
        print(lab, lab_plan.days_remain, lab_plan.created, lab_plan.deleted)


def recalc_ip():
    session = get_scoped_session()
    ips = session.query(Ip).all()
    servers = (session.query(KVMServer)
               .filter(KVMServer.ip != KVMServer.NO_IP)
               .all())
    for ip in ips:
        ip.is_used = False
    for server in servers:
        for ip in ips:
            if server.ip == ip.value:
                ip.is_used = True
                break
    session.commit()


def parse_args():
    if len(sys.argv) == 3 and sys.argv[1] == 'check':
        if sys.argv[2] == 'lablimits':
            check_lab_limits()
            return
        if sys.argv[2] == "ramusage":
            check_ramusage()
            return

        elif sys.argv[2] == 'wtoapprove':
            check_wtoapprove()
            return
        elif sys.argv[2] == 'labplans':
            check_labplans()
            return
        elif sys.argv[2] == 'plans':
            check_plans()
            return
        elif sys.argv[2] == 'trial':
            check_trial()
            return

    elif len(sys.argv) == 3 and sys.argv[1] == 'recalc':
        if sys.argv[2] == 'lablimits':
            recalc_lab_limits()
            return
        elif sys.argv[2] == 'ramusage':
            recalc_ramusage()
            return
        elif sys.argv[2] == 'wtoapprove':
            recalc_wtoapprove()
            return
        elif sys.argv[2] == 'ip':
            recalc_ip()
            return

    print(USAGE)


if __name__ == '__main__':
    parse_args()
