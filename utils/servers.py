import os
import sys

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if path not in sys.path:
    sys.path.append(path)

import base
base.init_all_settings()

from utils.args import basemain
from settings.db import get_scoped_session
from app.my.models import KVMServer, Ip


def unset_ip():
    session = get_scoped_session()
    servers = (session.query(KVMServer)
                .filter(KVMServer.status == KVMServer.STATUS["REMOVED"])
                .filter(KVMServer.ip != KVMServer.NO_IP)
                .all())
    for server in servers:
        ip = session.query(Ip).filter_by(value = server.ip).first()
        if not ip:
            print("Can't find server {} ip {}".format(server, server.ip))
            continue
        ip.is_used = False
        server.ip = KVMServer.NO_IP
    session.commit()


def ip_dublications():
    session = get_scoped_session()
    servers = (session.query(KVMServer)
                .filter(KVMServer.status.in_([
                    KVMServer.STATUS["STOPPED"],
                    KVMServer.STATUS["RUNNING"]
                ]))
                .all())
    ips = []
    for server in servers:
        ips.append(server.ip)
    for server in servers:
        ips.remove(server.ip)
        if server.ip in ips:
            print("{} {}\t{}".format(server.id, server.name, server.ip))


if __name__ == "__main__":
    basemain({
        "unset_ip": unset_ip,
        "ip_dub":   ip_dublications,
    })
